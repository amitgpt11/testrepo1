<apex:component controller="HeaderController" allowDML="true" access="global">
   <head style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
    <title style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">SUBJECT</title>
    <style style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
    /* -------------------------------------
        GLOBAL
    ------------------------------------- */
    * {
      font-family: Arial, Verdana, Helvetica, sans-serif;
      font-size: 100%;
      line-height: 1.6em;
      margin: 0;
      padding: 0;
    }
    img {
      max-width: 600px;
      width: auto;
    }
    body {
      -webkit-font-smoothing: antialiased;
      height: 100%;
      -webkit-text-size-adjust: none;
      width: 100% !important;
    }
    /* -------------------------------------
        ELEMENTS
    ------------------------------------- */
    a {
      color: #348eda;
    }
    .btn-primary {
      Margin-bottom: 10px;
      width: auto !important;
    }
    .btn-primary td {
      background-color: #348eda; 
      border-radius: 25px;
      font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; 
      font-size: 14px; 
      text-align: center;
      vertical-align: top; 
    }
    .btn-primary td a {
      background-color: #348eda;
      border: solid 1px #348eda;
      border-radius: 25px;
      border-width: 10px 20px;
      display: inline-block;
      color: #ffffff;
      cursor: pointer;
      font-weight: bold;
      line-height: 2;
      text-decoration: none;
    }
    .last {
      margin-bottom: 0;
    }
    .first {
      margin-top: 0;
    }
    .padding {
      padding: 10px 0;
    }
    /* -------------------------------------
        BODY
    ------------------------------------- */
    table.body-wrap {
      padding: 30px;
      width: 100%;
    }
    table.body-wrap .container {
      border: 1px solid #CCCCCC;
    }
    /* -------------------------------------
        FOOTER
    ------------------------------------- */
    table.footer-wrap {
      clear: both !important;
      width: 100%;  
      padding-top:40px;
    }
    .footer-wrap .container p {
      color: #666666;
      font-size: 10px;
      padding:0px 0px 15px 0px;
      margin:0;
    }

    table.footer-wrap a {
      color: #999999;
    }

    table.footer-wrap hr{
      background: #8a8a8a;
      border: 0;
      height: 1px;
      margin:10px 0 5px 0;
    }
    /* -------------------------------------
        HEADER
    ------------------------------------- */
    table.header-wrap {
      clear: both !important;
      width: 100%;
      padding:20px 0 10px 0;
    }
    .header-wrap .container p {
      color: #666666;
      font-size: 10px;
    }
    table.header-wrap a {
      color: #999999;
    }
    /* -------------------------------------
        TYPOGRAPHY
    ------------------------------------- */
    h1, 
    h2, 
    h3 {
      display: block;
      font-style: bold;
      font-weight: bolder;
      line-height: 120%;
      letter-spacing: normal;
      margin: 0 0 7px 0;
    }
    h1 {
      font-size: 36px;
      color: #19598C;
    }
    h2 {
      font-size: 28px;
      color: #008ecd;
      text-transform: uppercase;
    }
    h3 {
      font-size: 22px;
    }
    p, 
    ul, 
    ol {
      font-size: 16px;
      font-weight: normal;
      margin-bottom: 10px;
    }
    ul li, 
    ol li {
      margin-left: 5px;
      list-style-position: inside;
    }
    /* ---------------------------------------------------
        RESPONSIVENESS
    ------------------------------------------------------ */
    /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
    .container {
      clear: both !important;
      display: block !important;
      margin: 0 auto !important;
      max-width: 600px !important;
    }
    /* Set the padding on the td rather than the div for Outlook compatibility */
    .body-wrap .container {
      padding: 20px;
    }
    /* This should also be a block element, so that it will fill 100% of the .container */
    .content {
      display: block;
      margin: 0 auto;
      max-width: 600px;
    }
    /* Let's make sure tables in the content area are 100% wide */
    .content table {
      width: 100%;
    }

    .content hr{
      border-color:#19598C;
      margin:15px 0 30px 0;
    }
    </style>
    </head>
    
    <table bgcolor="#f6f6f6" class="header-wrap" style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 20px 0 10px 0;width: 100%;clear: both !important;">
      <tr style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
      <td class="container" style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0 auto !important;padding: 0;clear: both !important;display: block !important;max-width: 600px !important;">
          
          <!-- content -->
          <div class="content" style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0 auto;padding: 0;display: block;max-width: 600px;">
            <table style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;width: 100%;">
              <tr style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
                <td align="center" style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
                  <p style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 10px;line-height: 1.6em;margin: 0;padding: 0;font-weight: normal;margin-bottom: 10px;color: #666666;">Having difficulty viewing this email Click here to view in browser.<br style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;"/>This sponsored message does not reflect the opinion of PCR, nor does it engage its responsibility.
                  </p>
                </td>
              </tr>
            </table>
          </div>
          <!-- /content -->
        </td>
      </tr>
    </table>
    <table class="body-wrap" bgcolor="#FFFFFF" style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 30px;width: 100%;padding-bottom:0;">
      <tr style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
        <td class="container" bgcolor="#FFFFFF" style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0 auto !important;padding: 20px;border: 1px solid #CCCCCC;clear: both !important;display: block !important;max-width: 600px !important;border-bottom:0;padding-bottom: 0;">

          <!-- content -->
          <div class="content" style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0 auto;padding: 0;display: block;max-width: 600px;">
          <table style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;width: 100%;">
            <tr class="logos" style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
              <td style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;"><img height="58" style="padding: 10px;font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;max-width: 600px;width: auto;" src="https://bsci--dev0--c.cs18.content.force.com/servlet/servlet.ImageServer?id=01511000000hcHB&oid=00D1100000Bv1sv&lastMod=1464339617000" /></td>
              <td style="text-align: right;font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;"><img height="80" src="https://bsci--dev0--c.cs18.content.force.com/servlet/servlet.ImageServer?id=01511000000hcHJ&oid=00D1100000Bv1sv&lastMod=1464339617000" style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;max-width: 600px;width: auto;" /></td>
            </tr>

            <tr style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
              <td colspan="2" style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
                <hr style="font-family: Arial, Verdana, Helvetica, sans-serif;font-size: 100%;line-height: 1.6em;margin: 15px 0 10px 0;padding: 0;border-color: #19598C;" />
              </td>
            </tr>
            </table>
            </div>
        </td>
      </tr>
    </table>
</apex:component>