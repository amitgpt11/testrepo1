<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Holds the details for the Endo and Cardio Community User&apos;s selected products and training</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The Contact that created the List, associated via the Community User to User.Contact</description>
        <externalId>false</externalId>
        <inlineHelpText>The Contact that created the List</inlineHelpText>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Community Orders</relationshipLabel>
        <relationshipName>Community_Lists</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Shared_Account_Name__c</fullName>
        <description>Used with the InTouch Community Customer Orders and Quote Requests.</description>
        <externalId>false</externalId>
        <formula>HYPERLINK(&quot;/&quot;&amp;Contact__r.AccountId,  Contact__r.Account.Name, &quot;_blank&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The Contact&apos;s Account</inlineHelpText>
        <label>Account Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Shared_Account_Shipping_Address__c</fullName>
        <description>Used with InTouch Community orders. Displays the Account Shipping address for easy visibility</description>
        <externalId>false</externalId>
        <formula>Contact__r.Account.ShippingStreet &amp; BR() &amp;
 Contact__r.Account.ShippingCity&amp;&quot;, &quot;&amp; Contact__r.Account.ShippingState &amp; &quot;  &quot;&amp; Contact__r.Account.ShippingPostalCode &amp; BR() &amp;
 Contact__r.Account.ShippingCountry</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Account Shipping Address.</inlineHelpText>
        <label>Account Shipping Address</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Shared_Additional_Information__c</fullName>
        <description>Used on the InTouch community to hold the order notes from the Community User when submitting an order</description>
        <externalId>false</externalId>
        <label>Additional Information</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Shared_Bill_to_Number__c</fullName>
        <description>Used with the InTouch Community.  Ship to Number from SAP, copied by the VTM into Salesforce on the Contact Record</description>
        <externalId>false</externalId>
        <formula>Contact__r.Shared_Bill_to_Number__c</formula>
        <inlineHelpText>SAP Ship-to Number, found on the Contact.</inlineHelpText>
        <label>Bill to Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Shared_Billing_Address__c</fullName>
        <description>For InTouch Community.  Displays the Account Billing Address for easy visibility.</description>
        <externalId>false</externalId>
        <formula>Contact__r.Account.BillingStreet &amp; BR() &amp;
 Contact__r.Account.BillingCity&amp;&quot;, &quot;&amp; Contact__r.Account.BillingState &amp; &quot;  &quot;&amp; Contact__r.Account.BillingPostalCode &amp; BR() &amp;
 Contact__r.Account.BillingCountry</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The Account Billing Address</inlineHelpText>
        <label>Billing Address</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Shared_Date_Submitted__c</fullName>
        <externalId>false</externalId>
        <label>Date Submitted</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Shared_Product_Count__c</fullName>
        <description>InTouch Community, the number of products on an Endo/Cardio user&apos;s List</description>
        <externalId>false</externalId>
        <label>Product Count</label>
        <summaryForeignKey>Shared_Community_Order_Item__c.Shared_Order__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Shared_Ship_to_Number__c</fullName>
        <description>Used with the InTouch Community.  Value from SAP, entered on to the Contact record in SF manually by the VTM</description>
        <externalId>false</externalId>
        <formula>Contact__r.Shared_Ship_to_Number__c</formula>
        <inlineHelpText>Ship-to Number from SAP, entered on the Contact.</inlineHelpText>
        <label>Ship to Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Shared_Total_Quantity__c</fullName>
        <description>The total quantity of all order line items combined for the order</description>
        <externalId>false</externalId>
        <inlineHelpText>The total quantity of all order line items combined for the order</inlineHelpText>
        <label>Total Quantity</label>
        <summarizedField>Shared_Community_Order_Item__c.Shared_Quantity__c</summarizedField>
        <summaryForeignKey>Shared_Community_Order_Item__c.Shared_Order__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>In Progress</fullName>
                    <default>true</default>
                    <label>In Progress</label>
                </value>
                <value>
                    <fullName>Submitted</fullName>
                    <default>false</default>
                    <label>Submitted</label>
                </value>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                    <label>Completed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Community Order</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>My_Community_Orders</fullName>
        <columns>NAME</columns>
        <columns>Shared_Account_Name__c</columns>
        <columns>Contact__c</columns>
        <columns>Shared_Product_Count__c</columns>
        <columns>Shared_Total_Quantity__c</columns>
        <columns>Shared_Date_Submitted__c</columns>
        <columns>Status__c</columns>
        <filterScope>Mine</filterScope>
        <label>My Community Orders</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>CO-{0000}</displayFormat>
        <label>Order No.</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Community Orders</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Shared_Total_Quantity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Shared_Date_Submitted__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>Contact__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Shared_Total_Quantity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Shared_Date_Submitted__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Contact__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Shared_Total_Quantity__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Shared_Date_Submitted__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Contact__c</searchFilterFields>
        <searchFilterFields>Shared_Total_Quantity__c</searchFilterFields>
        <searchFilterFields>Shared_Date_Submitted__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchFilterFields>OWNER.FIRST_NAME</searchFilterFields>
        <searchFilterFields>OWNER.LAST_NAME</searchFilterFields>
        <searchResultsAdditionalFields>Contact__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Shared_Total_Quantity__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Shared_Date_Submitted__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <sharingReasons>
        <fullName>VTM__c</fullName>
        <label>VTM</label>
    </sharingReasons>
    <visibility>Public</visibility>
</CustomObject>
