<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object is the repository of Sales Data that is loaded on a daily/monthly basis. The data herein will be used to produce Salesforce reports such as QBR, and Salesforce dashboards such as Sales Flash.

All data in these records is imported from GSR.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Performance__c</fullName>
        <description>This field&apos;s only purpose is to create a relationship between Account Performance and Sales Data to allow for reporting on accounts that are marked as QBR.</description>
        <externalId>false</externalId>
        <label>Account Performance</label>
        <referenceTo>Account_Performance__c</referenceTo>
        <relationshipLabel>Sales Data</relationshipLabel>
        <relationshipName>Sales_Data</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Sales Data</relationshipLabel>
        <relationshipName>Sales_Data</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Area_Group_Country__c</fullName>
        <description>Geographic Hierarchy\Area Group Country</description>
        <externalId>false</externalId>
        <label>Area Group Country</label>
        <length>25</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Current_QTD_ASP__c</fullName>
        <description>=IF(&apos;QTD Units&apos;&gt;0,&apos;QTD Sales USD PEG&apos;/&apos;QTD Units&apos;,0)</description>
        <externalId>false</externalId>
        <formula>IF(QTD_Units__c &gt;0, QTD_Sales__c / QTD_Units__c ,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Current QTD ASP</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Current_YTD_ASP__c</fullName>
        <description>=IF(&apos;YTD Units&apos;&gt;0,&apos;YTD Sales USD PEG&apos;/&apos;YTD Units&apos;,0)</description>
        <externalId>false</externalId>
        <formula>IF( Current_YTD_Units__c &gt;0, Current_YTD_Sales__c / Current_YTD_Units__c ,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Current YTD ASP</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Current_YTD_Sales__c</fullName>
        <description>INVOICE\Invoice Details\Invoice Measures\Invoice USD PEG\Sales USD PEG</description>
        <externalId>false</externalId>
        <label>Current YTD Sales ($)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Current_YTD_Units__c</fullName>
        <description>INVOICE\Invoice Details\Invoice Measures\Invoice Units\Units</description>
        <externalId>false</externalId>
        <label>Current YTD Units</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Division__c</fullName>
        <externalId>false</externalId>
        <label>Division</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>CA</fullName>
                    <default>false</default>
                    <label>CA</label>
                </value>
                <value>
                    <fullName>CRM</fullName>
                    <default>false</default>
                    <label>CRM</label>
                </value>
                <value>
                    <fullName>Endo</fullName>
                    <default>false</default>
                    <label>Endo</label>
                </value>
                <value>
                    <fullName>EP</fullName>
                    <default>false</default>
                    <label>EP</label>
                </value>
                <value>
                    <fullName>IC</fullName>
                    <default>false</default>
                    <label>IC</label>
                </value>
                <value>
                    <fullName>LAAC</fullName>
                    <default>false</default>
                    <label>LAAC</label>
                </value>
                <value>
                    <fullName>NM</fullName>
                    <default>false</default>
                    <label>NM</label>
                </value>
                <value>
                    <fullName>PI</fullName>
                    <default>false</default>
                    <label>PI</label>
                </value>
                <value>
                    <fullName>Uro/WH</fullName>
                    <default>false</default>
                    <label>Uro/WH</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Growth_QTD_ASP__c</fullName>
        <description>=IF(&apos;Prior QTD Sales USD PEG&apos;&gt;0,&apos;Curr QTD ASP&apos;/&apos;Prior QTD ASP&apos;-1,0)</description>
        <externalId>false</externalId>
        <formula>IF( Prior_QTD_Sales__c &gt;0, Current_QTD_ASP__c /Prior_QTD_ASP__c -1,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth QTD ASP</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_QTD_ASP_v_Plan__c</fullName>
        <description>=IF(&apos;QTD Quota ASP&apos;&gt;0,&apos;Curr QTD ASP&apos;/&apos;QTD Quota ASP&apos;-1,0)</description>
        <externalId>false</externalId>
        <formula>IF(QTD_Quota_ASP__c&gt;0, Current_QTD_ASP__c /QTD_Quota_ASP__c -1,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth QTD ASP v Plan</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_QTD_Sales__c</fullName>
        <description>=IF(&apos;Prior QTD Sales USD PEG&apos;&gt;0,&apos;QTD Sales USD PEG&apos;/&apos;Prior QTD Sales USD PEG&apos;-1,0)</description>
        <externalId>false</externalId>
        <formula>IF( Prior_QTD_Sales__c &gt;0, QTD_Sales__c /Prior_QTD_Sales__c -1,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth QTD Sales</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_QTD_Sales_v_Plan_Trend__c</fullName>
        <description>=IF(&apos;QTD Quota ASP&apos;&gt;0,&apos;QTD Sales USD PEG&apos;-&apos;QTD Quota Sales USD PEG&apos;,0)</description>
        <externalId>false</externalId>
        <formula>IF(QTD_Quota_ASP__c&gt;0,  QTD_Sales__c  - QTD_Quota_Sales__c,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth QTD Sales v Plan +/-$</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Growth_QTD_Sales_v_Plan__c</fullName>
        <description>=IF(&apos;QTD Quota Sales USD PEG&apos;&gt;0,&apos;QTD Sales USD PEG&apos;/&apos;QTD Quota Sales USD PEG&apos;-1,0)</description>
        <externalId>false</externalId>
        <formula>IF( QTD_Quota_Sales__c &gt;0, QTD_Sales__c /QTD_Quota_Sales__c-1,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth QTD Sales v Plan</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_QTD_Units__c</fullName>
        <description>=IF(&apos;Prior QTD Units&apos;&gt;0,&apos;QTD Units&apos;/&apos;Prior QTD Units&apos;-1,0)</description>
        <externalId>false</externalId>
        <formula>IF( Prior_QTD_Units__c &gt;0, QTD_Units__c/Prior_QTD_Units__c -1,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth QTD Units</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_QTD_Units_v_Plan__c</fullName>
        <description>=IF(&apos;QTD Quota ASP&apos;&gt;0,&apos;QTD Units&apos;/&apos;QTD Quota Units&apos;-1,0)</description>
        <externalId>false</externalId>
        <formula>IF( QTD_Quota_ASP__c &gt;0, QTD_Units__c/QTD_Quota_Units__c -1,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth QTD Units v Plan</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_QTD__c</fullName>
        <description>=&apos;QTD Sales USD PEG&apos;-&apos;Prior QTD Sales USD PEG&apos;</description>
        <externalId>false</externalId>
        <formula>QTD_Sales__c - Prior_QTD_Sales__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth QTD +/- $</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Growth_R12M__c</fullName>
        <description>=&apos;R12M Sales USD&apos;-&apos;Prior Year Sales USD PEG&apos;</description>
        <externalId>false</externalId>
        <formula>R12M_Sales__c - Prior_Year_Sales__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth R12M +/-$</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Growth_YTD_ASP__c</fullName>
        <description>=IF(&apos;Prior YTD Sales USD PEG&apos;&gt;0,&apos;Curr YTD ASP&apos;/&apos;Prior YTD ASP&apos;-1,1)</description>
        <externalId>false</externalId>
        <formula>IF( Prior_YTD_Sales__c &gt;0, Current_YTD_ASP__c / Prior_YTD_ASP__c -1,1)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth YTD ASP</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_YTD_ASP_v_Plan__c</fullName>
        <description>=IF(&apos;YTD Quota ASP&apos;&gt;0,&apos;Curr YTD ASP&apos;/&apos;YTD Quota ASP&apos;-1,0)</description>
        <externalId>false</externalId>
        <formula>IF( YTD_Quota_ASP__c &gt;0, Current_YTD_ASP__c /YTD_Quota_ASP__c-1,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth YTD ASP v Plan</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_YTD_Sales__c</fullName>
        <description>=IF(&apos;Prior YTD Sales USD PEG&apos;&gt;0,&apos;YTD Sales USD PEG&apos;/&apos;Prior YTD Sales USD PEG&apos;-1,0)</description>
        <externalId>false</externalId>
        <formula>IF( Prior_YTD_Sales__c &gt;0, Current_YTD_Sales__c / Prior_YTD_Sales__c -1,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth YTD Sales</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_YTD_Sales_v_Plan_Trend__c</fullName>
        <description>=IF(&apos;YTD Quota ASP&apos;&gt;0,&apos;YTD Sales USD PEG&apos;-&apos;YTD Quota Sales USD PEG&apos;,0)</description>
        <externalId>false</externalId>
        <formula>IF( YTD_Quota_ASP__c &gt;0, Current_YTD_Sales__c - YTD_Quota_Sales__c ,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth YTD Sales v Plan +/-$</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Growth_YTD_Sales_v_Plan__c</fullName>
        <description>=IF(&apos;YTD Quota Sales USD PEG&apos;&gt;0,&apos;YTD Sales USD PEG&apos;/&apos;YTD Quota Sales USD PEG&apos;-1,0)</description>
        <externalId>false</externalId>
        <formula>IF( YTD_Quota_Sales__c &gt;0, Current_YTD_Sales__c / YTD_Quota_Sales__c -1,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth YTD Sales v Plan</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_YTD_Units__c</fullName>
        <description>=IF(&apos;Prior YTD Units&apos;&gt;0,&apos;YTD Units&apos;/&apos;Prior YTD Units&apos;-1,0)</description>
        <externalId>false</externalId>
        <formula>IF( Prior_YTD_Units__c &gt;0, Current_YTD_Units__c / Prior_YTD_Units__c -1,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth YTD Units</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_YTD_Units_v_Plan__c</fullName>
        <description>=IF(&apos;YTD Quota ASP&apos;&gt;0,&apos;YTD Units&apos;/&apos;YTD Quota Units&apos;-1,0)</description>
        <externalId>false</externalId>
        <formula>IF( YTD_Quota_ASP__c &gt;0, Current_YTD_Units__c / YTD_Quota_Units__c -1,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth YTD Units v Plan</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Growth_YTD__c</fullName>
        <description>=&apos;YTD Sales USD PEG&apos;-&apos;Prior YTD Sales USD PEG&apos;</description>
        <externalId>false</externalId>
        <formula>Current_YTD_Sales__c - Prior_YTD_Sales__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Growth YTD +/- $</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>MS_R12M__c</fullName>
        <description>=&apos;R12M Sales USD&apos;/Potential Sales</description>
        <externalId>false</externalId>
        <formula>IF (Potential__c=0,0,R12M_Sales__c /Potential__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>MS% (R12M)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Potential__c</fullName>
        <description>tbd</description>
        <externalId>false</externalId>
        <formula>Current_YTD_ASP__c * Current_YTD_ASP__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Potential $</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Prior_QTD_ASP__c</fullName>
        <description>=IF( Prior_QTD_Units__c &gt;0, Prior_QTD_Sales__c /Prior_QTD_Units__c,0)</description>
        <externalId>false</externalId>
        <formula>IF( Prior_QTD_Units__c &gt;0, Prior_QTD_Sales__c /Prior_QTD_Units__c,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Prior QTD ASP</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Prior_QTD_Quota_Sales_USD_PEG__c</fullName>
        <description>from qbr data map</description>
        <externalId>false</externalId>
        <label>Prior QTD Quota Sales USD PEG</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Prior_QTD_Quota_Units__c</fullName>
        <externalId>false</externalId>
        <label>Prior QTD Quota Units</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Prior_QTD_Sales__c</fullName>
        <description>INVOICE\Invoice Details\Invoice Measures\Invoice Units\Prior QTD Units</description>
        <externalId>false</externalId>
        <label>Prior QTD Sales ($)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Prior_QTD_Units__c</fullName>
        <description>INVOICE\Invoice Details\Invoice Measures\Invoice Units\Prior QTD Units</description>
        <externalId>false</externalId>
        <label>Prior QTD Units</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Prior_YTD_ASP__c</fullName>
        <description>=IF(&apos;Prior YTD Units&apos;&gt;0,&apos;Prior YTD Sales USD PEG&apos;/&apos;Prior YTD Units&apos;,0)</description>
        <externalId>false</externalId>
        <formula>IF( Prior_YTD_Units__c &gt;0, Prior_YTD_Sales__c /Prior_YTD_Units__c,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Prior YTD ASP</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Prior_YTD_Quota_Sales_USD_PEG__c</fullName>
        <description>from qbr data sample</description>
        <externalId>false</externalId>
        <label>Prior YTD Quota Sales USD PEG</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Prior_YTD_Quota_Units__c</fullName>
        <description>from qbr data sample</description>
        <externalId>false</externalId>
        <label>Prior YTD Quota Units</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Prior_YTD_Sales__c</fullName>
        <description>INVOICE\Invoice Details\Invoice Measures\Invoice USD PEG\Prior Year Sales USD PEG</description>
        <externalId>false</externalId>
        <label>Prior YTD Sales ($)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Prior_YTD_Units__c</fullName>
        <description>INVOICE\Invoice Details\Invoice Measures\Invoice Units\Prior YTD Units</description>
        <externalId>false</externalId>
        <label>Prior YTD Units</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Prior_Year_Sales__c</fullName>
        <description>INVOICE\Invoice Details\Invoice Measures\Invoice USD PEG\Prior Year Sales USD PEG</description>
        <externalId>false</externalId>
        <label>Prior Year Sales ($)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Prior_Year_Units__c</fullName>
        <description>from qbr data sample</description>
        <externalId>false</externalId>
        <label>Prior Year Units</label>
        <precision>14</precision>
        <required>false</required>
        <scale>5</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Product_Franchise__c</fullName>
        <externalId>false</externalId>
        <label>Product Franchise</label>
        <length>35</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Group__c</fullName>
        <externalId>false</externalId>
        <label>Product Group</label>
        <length>35</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>QTD_Quota_ASP__c</fullName>
        <description>=IF(AND(&apos;QTD Quota Sales USD PEG&apos;,&apos;QTD Quota Units&apos;),&apos;QTD Quota Sales USD PEG&apos;/&apos;QTD Quota Units&apos;,0)</description>
        <externalId>false</externalId>
        <formula>IF(
AND(NOT(ISBLANK(QTD_Quota_Sales__c)) , NOT(ISBLANK(QTD_Quota_Units__c))),
QTD_Quota_Sales__c/QTD_Quota_Units__c ,0
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>QTD Quota ASP</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>QTD_Quota_Sales__c</fullName>
        <description>QUOTA\Quota USD PEG\QTD Quota Sales USD PEG</description>
        <externalId>false</externalId>
        <label>QTD Quota Sales ($)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>QTD_Quota_Units__c</fullName>
        <description>QUOTA\Quota Units\QTD Quota Units</description>
        <externalId>false</externalId>
        <label>QTD Quota Units</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>QTD_Sales__c</fullName>
        <description>INVOICE\Invoice Details\Invoice Measures\Invoice USD PEG\QTD Sales USD PEG</description>
        <externalId>false</externalId>
        <label>QTD Sales ($)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>QTD_Units__c</fullName>
        <description>INVOICE\Invoice Details\Invoice Measures\Invoice Units\QTD Units</description>
        <externalId>false</externalId>
        <label>QTD Units</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>R12M_Sales__c</fullName>
        <description>INVOICE\Invoice Details\Invoice Measures\Invoice USD PEG\Sales USD PEG</description>
        <externalId>false</externalId>
        <label>R12M Sales ($)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>R12M_Units__c</fullName>
        <description>from qbr data sample</description>
        <externalId>false</externalId>
        <label>R12M Units</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SAP_Customer_Number__c</fullName>
        <description>Seller-Hierarchies\Sales By Territory Hierarchy(CUR/DYN)\Sales Rep details (Sales by Territory)\Sales Rep Name-Number (By Territory)</description>
        <externalId>false</externalId>
        <label>SAP Customer Number</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sales_Region__c</fullName>
        <externalId>false</externalId>
        <label>Sales Region</label>
        <length>25</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sales_Rep_Manager__c</fullName>
        <externalId>false</externalId>
        <formula>Sales_Rep__r.Manager.Full_Name__c</formula>
        <label>Sales Rep Manager</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sales_Rep__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Sales Rep</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Sales_Data</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Seller_Hierarchy__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Seller Hierarchy</label>
        <referenceTo>Seller_Hierarchy__c</referenceTo>
        <relationshipLabel>Sales Data</relationshipLabel>
        <relationshipName>Sales_Data</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Type_of_Data__c</fullName>
        <externalId>false</externalId>
        <label>Type of Data</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Account Totals</fullName>
                    <default>false</default>
                    <label>Account Totals</label>
                </value>
                <value>
                    <fullName>Product Totals (by rep)</fullName>
                    <default>false</default>
                    <label>Product Totals (by rep)</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>YTD_Quota_ASP__c</fullName>
        <description>=IF(AND(&apos;YTD Quota Sales USD PEG&apos;,&apos;YTD Quota Units&apos;),&apos;YTD Quota Sales USD PEG&apos;/&apos;YTD Quota Units&apos;,0)</description>
        <externalId>false</externalId>
        <formula>IF(
AND(NOT(ISBLANK(YTD_Quota_Sales__c)),NOT(ISBLANK( YTD_Quota_Units__c ))),YTD_Quota_Sales__c/YTD_Quota_Units__c,0
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>YTD Quota ASP</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>YTD_Quota_Sales__c</fullName>
        <description>QUOTA\Quota USD PEG\YTD Quota Sales USD PEG</description>
        <externalId>false</externalId>
        <label>YTD Quota Sales ($)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>YTD_Quota_Units__c</fullName>
        <description>QUOTA\Quota Units\YTD Quota Units</description>
        <externalId>false</externalId>
        <label>YTD Quota Units</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Sales Data</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Sales Data Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Sales Data</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
