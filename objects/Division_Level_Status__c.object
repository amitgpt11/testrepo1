<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Custom object used for recording the divisional relationship to the IDN/Alliance/GPO</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Plan__c</fullName>
        <externalId>false</externalId>
        <label>Account Plan</label>
        <referenceTo>Account_Plan__c</referenceTo>
        <relationshipName>Divisional_Status</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Admin__c</fullName>
        <externalId>false</externalId>
        <formula>CASE( Quality_of_Relationship_Admin__c , 
&quot;Strong/Positive&quot;, IMAGE(&quot;/resource/AlertShieldGreen&quot;, &quot;Strong&quot; , 23, 22) , 
&quot;Developing/Transactional&quot;, IMAGE(&quot;/resource/AlertShieldYellow&quot;, &quot;Moderate&quot; , 23, 22), 
&quot;Strained&quot;, IMAGE(&quot;/resource/AlertShieldRed&quot;, &quot;Weak&quot;, 23, 22 ), &apos;None&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Admin</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Clinical__c</fullName>
        <externalId>false</externalId>
        <formula>CASE( Quality_of_Relationship_Clinical__c , 
&quot;Strong/Positive&quot;, IMAGE(&quot;/resource/AlertShieldGreen&quot;, &quot;Strong&quot; , 23, 22) , 
&quot;Developing/Transactional&quot;, IMAGE(&quot;/resource/AlertShieldYellow&quot;, &quot;Moderate&quot; , 23, 22), 
&quot;Strained&quot;, IMAGE(&quot;/resource/AlertShieldRed&quot;, &quot;Weak&quot;, 23, 22 ), &apos;None&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Clinical</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <externalId>false</externalId>
        <label>Comments</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Contract_Status__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>IE Dual Vendor 90%</inlineHelpText>
        <label>Contract Status</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Div_Lvl_Status_Ext_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>Div Lvl Status Ext Id</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Division__c</fullName>
        <externalId>false</externalId>
        <label>Division</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>CA</fullName>
                    <default>false</default>
                    <label>CA</label>
                </value>
                <value>
                    <fullName>CRM</fullName>
                    <default>false</default>
                    <label>CRM</label>
                </value>
                <value>
                    <fullName>Endo</fullName>
                    <default>false</default>
                    <label>Endo</label>
                </value>
                <value>
                    <fullName>EP</fullName>
                    <default>false</default>
                    <label>EP</label>
                </value>
                <value>
                    <fullName>IC</fullName>
                    <default>false</default>
                    <label>IC</label>
                </value>
                <value>
                    <fullName>LAAC</fullName>
                    <default>false</default>
                    <label>LAAC</label>
                </value>
                <value>
                    <fullName>NM</fullName>
                    <default>false</default>
                    <label>NM</label>
                </value>
                <value>
                    <fullName>PI</fullName>
                    <default>false</default>
                    <label>PI</label>
                </value>
                <value>
                    <fullName>Uro/WH</fullName>
                    <default>false</default>
                    <label>Uro/WH</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Leading_Vendors__c</fullName>
        <externalId>false</externalId>
        <label>Leading Vendors</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Quality_of_Relationship_Admin__c</fullName>
        <externalId>false</externalId>
        <label>Quality of Relationship - Admin</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Strong/Positive</fullName>
                    <default>false</default>
                    <label>Strong/Positive</label>
                </value>
                <value>
                    <fullName>Developing/Transactional</fullName>
                    <default>false</default>
                    <label>Developing/Transactional</label>
                </value>
                <value>
                    <fullName>Strained</fullName>
                    <default>false</default>
                    <label>Strained</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Quality_of_Relationship_Clinical__c</fullName>
        <externalId>false</externalId>
        <label>Quality of Relationship - Clinical</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Strong/Positive</fullName>
                    <default>false</default>
                    <label>Strong/Positive</label>
                </value>
                <value>
                    <fullName>Developing/Transactional</fullName>
                    <default>false</default>
                    <label>Developing/Transactional</label>
                </value>
                <value>
                    <fullName>Strained</fullName>
                    <default>false</default>
                    <label>Strained</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Strategic_Overview__c</fullName>
        <externalId>false</externalId>
        <formula>CASE(     Strategy_Overview__c     , 
&quot;Grow&quot;,  IMAGE(&quot;/resource/GrowthChart&quot;, &quot;Grow&quot; , 25, 25) , 
&quot;Defend&quot;, IMAGE(&quot;/resource/ShieldDefend&quot;, &quot;Defend&quot; , 25, 25),
&quot;Monitor&quot;, IMAGE(&quot;/resource/MonitorEye&quot;, &quot;Monitor&quot;, 25, 25 ),&apos;None&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Strategic Overview</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Strategy_Overview__c</fullName>
        <externalId>false</externalId>
        <label>Strategy Overview</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Grow</fullName>
                    <default>false</default>
                    <label>Grow</label>
                </value>
                <value>
                    <fullName>Defend</fullName>
                    <default>false</default>
                    <label>Defend</label>
                </value>
                <value>
                    <fullName>Monitor</fullName>
                    <default>false</default>
                    <label>Monitor</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Division-Level Status</label>
    <nameField>
        <displayFormat>DS-{YYYY}{MM}-{0}</displayFormat>
        <label>Div Status ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Division-Level Status</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
