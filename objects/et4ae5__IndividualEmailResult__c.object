<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Opportunity_Individual_Email_Results__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Marketing Cloud Individual Email Results</description>
        <externalId>false</externalId>
        <label>Opportunity Individual Email Results</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Individual Email Results</relationshipLabel>
        <relationshipName>Individual_Email_Results</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Patient_Individual_Email_Results__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Marketing Cloud Individual Email Results related list for Patients Custom Object</description>
        <externalId>false</externalId>
        <label>Patient Individual Email Results</label>
        <referenceTo>Patient__c</referenceTo>
        <relationshipLabel>Individual Email Results</relationshipLabel>
        <relationshipName>Individual_Email_Results</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>et4ae5__Clicked__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>et4ae5__NumberOfUniqueClicks__c &gt; 0</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Clicked</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>et4ae5__Contact_ID__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>et4ae5__Contact__r.Id</formula>
        <label>Contact ID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>et4ae5__Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Individual Email Results</relationshipLabel>
        <relationshipName>IndividualEmailResults</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>et4ae5__DateBounced__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The date and time when this subscriber’s email address bounced</inlineHelpText>
        <label>Date Bounced</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>et4ae5__DateOpened__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The date and time when the subscriber opened the email</inlineHelpText>
        <label>Date Opened</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>et4ae5__DateSent__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Date Sent</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>et4ae5__DateUnsubscribed__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The date and time when the subscriber unsubscribed</inlineHelpText>
        <label>Date Unsubscribed</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>et4ae5__Email__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF( !ISBLANK(et4ae5__Contact__r.Email), et4ae5__Contact__r.Email, et4ae5__Lead__r.Email)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Email</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>et4ae5__FromAddress__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>From Address</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>et4ae5__FromName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>From Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>et4ae5__HardBounce__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The email sent to this subscriber  was rejected due to permanent conditions (this typically results when &quot;user unknown&quot; or &quot;domain not found&quot; errors occur)</inlineHelpText>
        <label>Hard Bounce</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>et4ae5__Lead_ID__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>et4ae5__Lead__r.Id</formula>
        <label>Lead ID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>et4ae5__Lead__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Lead</label>
        <referenceTo>Lead</referenceTo>
        <relationshipLabel>Individual Email Results</relationshipLabel>
        <relationshipName>IndividualEmailResults</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>et4ae5__MergeId__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>Merge Id</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>et4ae5__NumberOfTotalClicks__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The total number of times this subscriber clicked on the links on this email</inlineHelpText>
        <label>Number of Total Clicks</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>et4ae5__NumberOfUniqueClicks__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The unique number of times this subscriber clicked on the links on this email (unique means one click per link)</inlineHelpText>
        <label>Number of Unique Clicks</label>
        <precision>6</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>et4ae5__Opened__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Indicates whether the subscriber opened the email or not</inlineHelpText>
        <label>Opened</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>et4ae5__SendDefinition__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The unique identifier of the email send. You may be asked to reference this number when calling Marketing Cloud support if the need arises.</inlineHelpText>
        <label>Send Definition</label>
        <referenceTo>et4ae5__SendDefinition__c</referenceTo>
        <relationshipLabel>Individual Email Results</relationshipLabel>
        <relationshipName>IndividualEmailResults</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>et4ae5__SoftBounce__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The email sent was rejected due to a seemingly temporary condition, such as a full inbox, after the system retries unsuccessfully to send the email every 15 minutes for 72 hours. A subscriber will be set to Held status after three soft bounces</inlineHelpText>
        <label>Soft Bounce</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>et4ae5__SubjectLine__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Subject Line</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>et4ae5__Tracking_As_Of__c</fullName>
        <defaultValue>now()</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The date and time the last tracking update was received by Salesforce</inlineHelpText>
        <label>Tracking As Of</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>et4ae5__TriggeredSendDefinition__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Triggered Send</label>
        <referenceTo>et4ae5__Automated_Send__c</referenceTo>
        <relationshipLabel>Individual Email Results</relationshipLabel>
        <relationshipName>Individual_Email_Results</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Individual Email Result</label>
    <listViews>
        <fullName>et4ae5__All</fullName>
        <columns>NAME</columns>
        <columns>et4ae5__FromName__c</columns>
        <columns>et4ae5__DateSent__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Email Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Individual Email Results</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>et4ae5__Request_Tracking_Immediately</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Request Tracking Immediately</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/19.0/connection.js&quot;)}
      {!REQUIRESCRIPT(&quot;/soap/ajax/15.0/apex.js&quot;)}
      var responder = sforce.apex.execute(&quot;et4ae5/trackingWebServices&quot;,&quot;buttonResponse&quot;, {sendDefId:&quot;{!CASESAFEID( et4ae5__IndividualEmailResult__c.et4ae5__SendDefinitionId__c )}&quot;});
      sforce.apex.execute(&quot;et4ae5/trackingWebServices&quot;,&quot;immediateTracking&quot;, {sendDefId:&quot;{!CASESAFEID( et4ae5__IndividualEmailResult__c.et4ae5__SendDefinitionId__c )}&quot;,evalString:responder});
alert(responder);</url>
    </webLinks>
</CustomObject>
