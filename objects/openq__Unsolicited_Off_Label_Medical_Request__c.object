<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>openq__Interaction__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Interaction</label>
        <referenceTo>openq__Interaction__c</referenceTo>
        <relationshipLabel>Unsolicited/Off-Label Medical Requests</relationshipLabel>
        <relationshipName>Unsolicited_Off_Label_Medical_Requests</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>openq__Product__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Argonotab</fullName>
                    <default>false</default>
                    <label>Argonotab</label>
                </value>
                <value>
                    <fullName>Hermadovil</fullName>
                    <default>false</default>
                    <label>Hermadovil</label>
                </value>
                <value>
                    <fullName>Orvantix</fullName>
                    <default>false</default>
                    <label>Orvantix</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>openq__Request__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Request</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>openq__Requested_by__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Requested by</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Unsolicited/Off-Label Medical Requests</relationshipLabel>
        <relationshipName>Unsolicited_Off_Label_Medical_Requests</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>openq__Response__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Response</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>openq__Topic__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Submitted Medical Information Request</fullName>
                    <default>false</default>
                    <label>Submitted Medical Information Request</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Treatment</controllingFieldValue>
                <controllingFieldValue>Adverse Event and Safety</controllingFieldValue>
                <controllingFieldValue>Use in Special Populations</controllingFieldValue>
                <controllingFieldValue>Availability</controllingFieldValue>
                <controllingFieldValue>Comparison</controllingFieldValue>
                <controllingFieldValue>Disease Info / Non-Product</controllingFieldValue>
                <controllingFieldValue>Dosage and Administration</controllingFieldValue>
                <controllingFieldValue>Drug Interactions / Combination</controllingFieldValue>
                <controllingFieldValue>Literature Request</controllingFieldValue>
                <controllingFieldValue>Other</controllingFieldValue>
                <controllingFieldValue>Pharmocology / Pharmacokinetics</controllingFieldValue>
                <controllingFieldValue>Prescription and Reimbursement</controllingFieldValue>
                <valueName>Submitted Medical Information Request</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>openq__Topic__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Topic</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Adverse Event and Safety</fullName>
                    <default>false</default>
                    <label>Adverse Event and Safety</label>
                </value>
                <value>
                    <fullName>Availability</fullName>
                    <default>false</default>
                    <label>Availability</label>
                </value>
                <value>
                    <fullName>Comparison</fullName>
                    <default>false</default>
                    <label>Comparison</label>
                </value>
                <value>
                    <fullName>Disease Info / Non-Product</fullName>
                    <default>false</default>
                    <label>Disease Info / Non-Product</label>
                </value>
                <value>
                    <fullName>Dosage and Administration</fullName>
                    <default>false</default>
                    <label>Dosage and Administration</label>
                </value>
                <value>
                    <fullName>Drug Interactions / Combination</fullName>
                    <default>false</default>
                    <label>Drug Interactions / Combination</label>
                </value>
                <value>
                    <fullName>Literature Request</fullName>
                    <default>false</default>
                    <label>Literature Request</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Pharmocology / Pharmacokinetics</fullName>
                    <default>false</default>
                    <label>Pharmocology / Pharmacokinetics</label>
                </value>
                <value>
                    <fullName>Prescription and Reimbursement</fullName>
                    <default>false</default>
                    <label>Prescription and Reimbursement</label>
                </value>
                <value>
                    <fullName>Treatment</fullName>
                    <default>false</default>
                    <label>Treatment</label>
                </value>
                <value>
                    <fullName>Use in Special Populations</fullName>
                    <default>false</default>
                    <label>Use in Special Populations</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Unsolicited/Off-Label Medical Request</label>
    <nameField>
        <label>Unsolicited/Off Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Unsolicited/Off-Label Medical Requests</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <visibility>Public</visibility>
</CustomObject>
