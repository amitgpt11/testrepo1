<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Declined_Date</fullName>
        <field>Declined_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Declined Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Disposition_to_Awaiting_Transfer</fullName>
        <field>Disposition__c</field>
        <literalValue>Awaiting Transfer</literalValue>
        <name>Set Disposition to Awaiting Transfer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Disposition_to_In_Transit</fullName>
        <field>Disposition__c</field>
        <literalValue>In Transit</literalValue>
        <name>Set Disposition to In Transit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Disposition_to_Received</fullName>
        <field>Disposition__c</field>
        <literalValue>Received</literalValue>
        <name>Set Disposition to Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Disposition_to_Transferred</fullName>
        <field>Disposition__c</field>
        <literalValue>Transferred</literalValue>
        <name>Set Disposition to Transferred</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Disposition_to_Used</fullName>
        <field>Disposition__c</field>
        <literalValue>Used</literalValue>
        <name>Set Disposition to Used</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Field_Support_Group_ID</fullName>
        <field>NMD_Field_Support_Group_ID__c</field>
        <formula>$Setup.Chatter_Group__c.NMD_Field_Support_Group_ID__c</formula>
        <name>Set Field Support Group ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Inventory_Transaction_Quantity</fullName>
        <field>Quantity__c</field>
        <formula>Delivery_Quantity__c</formula>
        <name>Set Inventory Transaction Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quantity_from_delivered_quantity</fullName>
        <field>Quantity__c</field>
        <formula>-(Delivery_Quantity__c)</formula>
        <name>Set Quantity from delivered quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Disposition_to_Declined</fullName>
        <field>Disposition__c</field>
        <literalValue>Transfer Declined</literalValue>
        <name>Update Disposition to Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quantity</fullName>
        <field>Quantity__c</field>
        <formula>Delivery_Quantity__c</formula>
        <name>Update Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Field Support Group ID</fullName>
        <actions>
            <name>Set_Field_Support_Group_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISBLANK( NMD_Field_Support_Group_ID__c ), $RecordType.DeveloperName=&apos;NMD_Inventory_Transaction&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Inventory Transaction Quantity %28KA%29</fullName>
        <actions>
            <name>Update_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the inventory for KA transactions</description>
        <formula>AND(   UPPER(Order_Type__c) = &apos;KA&apos;,   Sales_Order_No__c != null,   Order__c = NULL,    $RecordType.DeveloperName=&apos;NMD_Inventory_Transaction&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Inventory Transaction Quantity %28KB%29</fullName>
        <actions>
            <name>Set_Disposition_to_In_Transit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Inventory_Transaction_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Inventory for KB</description>
        <formula>AND(   UPPER(Order_Type__c) = &apos;KB&apos;,   Sales_Order_No__c != null,    Order__c = null )</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Disposition_to_Received</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Inventory_Transaction__c.CreatedDate</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Inventory Transaction Quantity %28KE%2FZKI%2FYKE%2FZKF%29</fullName>
        <actions>
            <name>Set_Disposition_to_Used</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Inventory_Transaction_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(   OR(      UPPER(Order_Type__c) = &apos;KE&apos;,      UPPER(Order_Type__c) = &apos;ZKI&apos;,      UPPER(Order_Type__c) = &apos;YKE&apos;,      UPPER(Order_Type__c) = &apos;ZKF&apos;   ),   Order__c = NULL,   Sales_Order_No__c != null,   $RecordType.DeveloperName=&apos;NMD_Inventory_Transaction&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Inventory Transaction Quantity %28YKR%2FKR%29</fullName>
        <actions>
            <name>Set_Disposition_to_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Inventory_Transaction_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND($RecordType.DeveloperName=&apos;NMD_Inventory_Transaction&apos;, OR(   UPPER(Order_Type__c) = &apos;YKR&apos;,   UPPER(Order_Type__c) = &apos;KR&apos; ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Inventory Transaction Quantity %28ZTKA%29</fullName>
        <actions>
            <name>Set_Disposition_to_Transferred</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Inventory_Transaction_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( AND(   UPPER(Order_Type__c) = &apos;ZTKA&apos;,   Sales_Order_No__c != null,   Order__c = NULL,   $RecordType.DeveloperName=&apos;NMD_Inventory_Transaction&apos; ), AND(   UPPER(Order_Type__c) = &apos;ZTKA&apos;,   Sales_Order_No__c != null,   OR( Order__c = NULL,Order__c != NULL),   Ispickval(Model_Number__r.Xfer_Type__c,&apos;Auto&apos;),   $RecordType.DeveloperName=&apos;NMD_Inventory_Transaction&apos;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Quantity</fullName>
        <actions>
            <name>Set_Disposition_to_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Inventory_Transaction_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Inventory Transaction for ZTKB with Auto Transfer</description>
        <formula>AND($RecordType.DeveloperName=&apos;NMD_Inventory_Transaction&apos;,          ISPICKVAL(Model_Number__r.Xfer_Type__c,&apos;Auto&apos;),          Order_Type__c=&apos;ZTKB&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update dispostion</fullName>
        <actions>
            <name>Set_Disposition_to_Awaiting_Transfer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates ZTKB Transactions To Awaiting Transfer if Approval is Needed.</description>
        <formula>AND(      Order_Type__c =&apos;ZTKB&apos;,      ISPICKVAL(Order__r.Order_Method__c, &apos;Approval&apos;),      Not(ISPICKVAL(Disposition__c,&apos;Received&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update the Quantity field</fullName>
        <actions>
            <name>Update_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Transaction quantity for an Inventory Transaction that was previously declined.</description>
        <formula>AND(   ISPICKVAL(PRIORVALUE(Disposition__c), &apos;Transfer Declined&apos;),   ISPICKVAL(Disposition__c, &apos;Received&apos;),   $RecordType.DeveloperName = &apos;NMD_Inventory_Transaction&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
