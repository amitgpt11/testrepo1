<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Record_Type</fullName>
        <description>Changes the List Record Type to NMD Limited Release</description>
        <field>RecordTypeId</field>
        <lookupValue>NMD_Limited_Release</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Record Type</fullName>
        <actions>
            <name>Change_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Salesforce will update the List Record Type to NMD Limited Release when the Material Group 1 = &quot;002&quot;, Sales organization = US10, and Product2.Product Hierarchy Level 1 = 12</description>
        <formula>AND(         Material_Group_1__c=&quot;002&quot;,         Sales_Organization__c=&quot;US10&quot;,         Model_Number__r.Level_1__c=&quot;12&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
