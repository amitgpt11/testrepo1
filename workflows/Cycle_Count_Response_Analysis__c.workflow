<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_FSR_Due_Date</fullName>
        <field>FSR_Due_Date__c</field>
        <formula>IF(
 MONTH( Start_Date__c) = 12,
 DATE( YEAR( Start_Date__c ), 12, 31 ),
 DATE( YEAR( Start_Date__c ), MONTH ( Start_Date__c ) + 1, 1 ) - 1
 )</formula>
        <name>Set FSR Due Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set FSR Due Date</fullName>
        <actions>
            <name>Set_FSR_Due_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(         $RecordType.DeveloperName=&quot;NMD_Response_Analysis&quot;,         OR(               NOT(ISBLANK(Start_Date__c)),               ISCHANGED(Start_Date__c)                )               )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
