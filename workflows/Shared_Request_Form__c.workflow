<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>UroPH_Speaker_Request_Approval_Email_Alert1</fullName>
        <description>UroPH Speaker Request Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>mary.novak@bsci.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Requesters_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Uro_PH/UroPH_Speaker_Request_Approval</template>
    </alerts>
    <alerts>
        <fullName>UroPH_Speaker_Request_Denied_Email_Alert</fullName>
        <description>UroPH Speaker Request Denied Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Requesters_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Uro_PH/UroPH_Speaker_Request_Denied</template>
    </alerts>
    <alerts>
        <fullName>UroPH_Speaker_Request_Email_Alert</fullName>
        <description>UroPH Speaker Request Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Requesters_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Uro_PH/UroPH_Speaker_Request</template>
    </alerts>
</Workflow>
