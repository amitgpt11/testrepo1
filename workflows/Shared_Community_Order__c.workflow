<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Community_Order_Submitted_VTM_Notification</fullName>
        <description>Community Order Submitted VTM Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Shared_Community_Site_Templates/Community_Quote_Request_Submitted</template>
    </alerts>
    <fieldUpdates>
        <fullName>Community_Order_Date_Submitted</fullName>
        <description>Sets the Date Submitted on Community Order records to be Today</description>
        <field>Shared_Date_Submitted__c</field>
        <formula>NOW()</formula>
        <name>Community Order Date Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CO Test</fullName>
        <active>false</active>
        <formula>NOT(ISPICKVAL( Owner:User.UserType, &quot;Standard&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Community Order Submitted</fullName>
        <actions>
            <name>Community_Order_Submitted_VTM_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Community_Order_Date_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Shared_Community_Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
