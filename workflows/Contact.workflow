<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SET_CUSTOMER_RECORD_TYPE</fullName>
        <description>Updates the Contact Record Type to &quot;Customer Physician&quot; when the SAP ID is populated.</description>
        <field>RecordTypeId</field>
        <lookupValue>Customer_Physician</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SET_CUSTOMER_RECORD_TYPE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>UPDATE_CONTACT_TO_CUSTOMER</fullName>
        <actions>
            <name>SET_CUSTOMER_RECORD_TYPE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.SAP_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When a Contact is created or edited, this rule will check to see if the SAP ID is populated. If it is, the Contact Record Type will be updated to &quot;Customer Physician.&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
