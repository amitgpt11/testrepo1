<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ANZ_Update_Interaction_Type</fullName>
        <description>US2582- DRAFT US-3839 - Activity master story</description>
        <field>Interaction_Type__c</field>
        <literalValue>Remote</literalValue>
        <name>ANZ_Update_Interaction_Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ANZ_Update_Interaction_Type_2</fullName>
        <field>Interaction_Type__c</field>
        <literalValue>In-Person</literalValue>
        <name>ANZ_Update_Interaction_Type_2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Task_Closed_Date</fullName>
        <description>Clears the Task Closed Date value to blank if the task was reopened.</description>
        <field>Task_Close_Date__c</field>
        <name>Clear Task Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_National_Tiering_on_Activity</fullName>
        <field>National_Tiering__c</field>
        <formula>IF 
( $User.Division=&quot;CRM&quot;,Account.National_Tiering_CRM__c,
 (IF ($User.Division=&quot;EP&quot;,Account.National_Tiering_EP__c,
  (IF ($User.Division=&quot;NC&quot;,Account.National_Tiering_IC__c,
   (IF ($User.Division=&quot;PI&quot;,Account.National_Tiering_PI__c,
    (IF ($User.Division=&quot;U/WH&quot;,Account.National_Tiering_U_WH__c,&quot;&quot;)
    )
   )
  )
 )
))))</formula>
        <name>Set National Tiering on Activity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Task_Close_Date</fullName>
        <description>Sets the date on the Task Close Date field to the actual date the Task was closed.</description>
        <field>Task_Close_Date__c</field>
        <formula>today()</formula>
        <name>Set Task Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ANZ_Update_Interaction_Type</fullName>
        <actions>
            <name>ANZ_Update_Interaction_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <description>US2582- DRAFT US-3839 - Activity master story</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ANZ_Update_Interaction_Type_2</fullName>
        <actions>
            <name>ANZ_Update_Interaction_Type_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>notEqual</operation>
            <value>Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>notEqual</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>ANZ Task Record</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Interaction_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>US2582- DRAFT US-3839 - Activity master story</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Task Close Date</fullName>
        <actions>
            <name>Clear_Task_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Clears the Task Close Date field Is the task is reopened.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Task Close Date</fullName>
        <actions>
            <name>Set_Task_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates the Task Close Date field with the date on which the task was closed by a user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Tiering on Activity</fullName>
        <actions>
            <name>Set_National_Tiering_on_Activity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.National_Tiering__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
