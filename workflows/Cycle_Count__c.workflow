<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Status_to_Initiate</fullName>
        <field>Status__c</field>
        <literalValue>Initiated</literalValue>
        <name>Set Status to Initiate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Initiate Cycle Count</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Cycle_Count__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD Cycle Count</value>
        </criteriaItems>
        <criteriaItems>
            <field>Cycle_Count__c.Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Cycle_Count__c.Status__c</field>
            <operation>equals</operation>
            <value>Published</value>
        </criteriaItems>
        <description>Sets the Cycle Count to Initiated when the Start Date is Today</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Status_to_Initiate</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Cycle_Count__c.Start_Date__c</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
