<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Product_Name</fullName>
        <description>Populates Product Name upon creation with the Product Description and Product Code</description>
        <field>Name</field>
        <formula>ProductCode &amp; &quot; &quot;  &amp; Description</formula>
        <name>Populate Product Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Record_Type_Parent_Product</fullName>
        <description>Sets the Product2 Record Type to Parent Product.</description>
        <field>RecordTypeId</field>
        <lookupValue>Shared_Parent_Product</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Product Record Type: Parent Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_NMD</fullName>
        <field>RecordTypeId</field>
        <lookupValue>NMD</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to NMD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Product Set To Top Level</fullName>
        <actions>
            <name>Product_Record_Type_Parent_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Shared_Top_Level_Parent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Record Type to NMD</fullName>
        <actions>
            <name>Set_Record_Type_to_NMD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Product_Hierarchy__c</field>
            <operation>startsWith</operation>
            <value>12</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Product Name</fullName>
        <actions>
            <name>Populate_Product_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.RecordTypeId</field>
            <operation>equals</operation>
            <value>NMD</value>
        </criteriaItems>
        <description>When a Product is created, the system will create a concatenation of the Product Description and Product Code to populate the Product name.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
