<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>X30_days_before_Contract_End</fullName>
        <description>30 days before Contract End</description>
        <protected>false</protected>
        <recipients>
            <recipient>Area Business Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Area Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>CA P&amp;C Analyst</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Dir. of Corp Accounts</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Europe Marketing</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Field Clinical Specialist</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Group Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Guide</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>HCP</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Local Marketing</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>MDM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Mentor</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Mgr. of Corp. Accounts</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>NCAM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>P&amp;C Business Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Sales Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Responsible Clinical Support</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Representative</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Team Admin</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Territory Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>VP, CA Sales</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Vice President</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Alert_30_days_before_Contract_End_Date</template>
    </alerts>
    <alerts>
        <fullName>X60_days_before_Contract_End</fullName>
        <description>60 days before Contract End</description>
        <protected>false</protected>
        <recipients>
            <recipient>Area Business Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Area Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>CA P&amp;C Analyst</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Dir. of Corp Accounts</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Europe Marketing</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Field Clinical Specialist</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Group Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Guide</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>HCP</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Local Marketing</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>MDM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Mentor</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Mgr. of Corp. Accounts</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>NCAM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>P&amp;C Business Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Sales Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Responsible Clinical Support</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Representative</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Team Admin</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Territory Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>VP, CA Sales</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Vice President</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Alert_60_days_before_Contract_End_Date</template>
    </alerts>
    <alerts>
        <fullName>X90_Day_before_Contract_Expiration_Notice</fullName>
        <description>90 Day before Contract Expiration Notice</description>
        <protected>false</protected>
        <recipients>
            <recipient>Area Business Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Area Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>CA P&amp;C Analyst</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Dir. of Corp Accounts</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Europe Marketing</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Field Clinical Specialist</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Group Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Guide</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>HCP</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Local Marketing</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>MDM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Mentor</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Mgr. of Corp. Accounts</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>NCAM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>P&amp;C Business Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Sales Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Responsible Clinical Support</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Representative</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Team Admin</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Territory Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>VP, CA Sales</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Vice President</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Alert_90_days_before_Contract_End_Date</template>
    </alerts>
    <alerts>
        <fullName>X90_Day_before_Contract_Expiration_Notice_for_US_Endo</fullName>
        <ccEmails>sipra.a.das@accenture.com</ccEmails>
        <ccEmails>j.kumar.sunkari@accenture.com</ccEmails>
        <ccEmails>devika.arun.risbud@accenture.com</ccEmails>
        <description>90 Day before Contract Expiration Notice for US Endo</description>
        <protected>false</protected>
        <recipients>
            <recipient>Area Business Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Area Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>CA P&amp;C Analyst</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Capital Specialist</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Dir. of Corp Accounts</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Europe Marketing</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>External Rep</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Field Clinical Specialist</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Group Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Guide</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>HCP</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Local Marketing</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>MDM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Mentor</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Mgr. of Corp. Accounts</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>NCAM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>P&amp;C Business Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Sales Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Responsible Clinical Support</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Representative</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Team Admin</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Territory Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>VP, CA Sales</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Vice President</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Alert_90_days_before_Contract_End_Date</template>
    </alerts>
    <alerts>
        <fullName>X90_days_before_Contract_End</fullName>
        <description>90 days before Contract End</description>
        <protected>false</protected>
        <recipients>
            <recipient>Area Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Regional Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Responsible Clinical Support</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Representative</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Team Admin</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Alert_90_days_before_Contract_End_Date</template>
    </alerts>
    <fieldUpdates>
        <fullName>Contract_Status_to_Expired</fullName>
        <description>Set Contract Status field to Expired.</description>
        <field>Status</field>
        <literalValue>Expired</literalValue>
        <name>Contract Status to Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contract_State_Active</fullName>
        <field>Status</field>
        <literalValue>Active</literalValue>
        <name>Set Contract State - Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contract_State_InActive</fullName>
        <field>Status</field>
        <literalValue>Inactive</literalValue>
        <name>Set Contract State - InActive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Status to Expired</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Status</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <description>Checks to see if the Status field is Active and will set a time based workflow to change the status field to expired.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_Status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>EP%2CIC Contract Expiration Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Status</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.Division__c</field>
            <operation>equals</operation>
            <value>EP,IC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.EndDate</field>
            <operation>lessOrEqual</operation>
            <value>NEXT 90 DAYS</value>
        </criteriaItems>
        <description>30, 60, 90 day rule for email notification of contract expiration (Europe)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X90_Day_before_Contract_Expiration_Notice</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>X60_days_before_Contract_End</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>X30_days_before_Contract_End</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
