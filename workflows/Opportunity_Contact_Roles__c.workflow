<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Opportunity_Contact_Role_Name</fullName>
        <description>Created for US2175 to update the Contact Role Name to a concatenation of Contact Full Name and Role.</description>
        <field>Name</field>
        <formula>Shared_Contact__r.FirstName + &quot; &quot; + Shared_Contact__r.LastName    + &quot; - &quot; +  Text(Shared_Role__c)</formula>
        <name>Set Opportunity Contact Role Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Opportunity Contact Role Name</fullName>
        <actions>
            <name>Set_Opportunity_Contact_Role_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity_Contact_Roles__c.Shared_Role__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Created for US2175. Sets Opportunity Contact Role Name to &quot;Contact Full Name - Role&quot;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
