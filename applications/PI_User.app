<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>A custom app used for the PI users.</description>
    <formFactors>Large</formFactors>
    <label>PI User</label>
    <tab>standard-Account</tab>
    <tab>Sales_Performance</tab>
    <tab>Territory_Analysis</tab>
    <tab>standard-Contract</tab>
    <tab>standard-Chatter</tab>
    <tab>Anaplan_Mobile</tab>
    <tab>standard-Contact</tab>
    <tab>box__Box_Files2</tab>
    <tab>standard-Document</tab>
    <tab>PI_Lutonix_SRAI__c</tab>
</CustomApplication>
