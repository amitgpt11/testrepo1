<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>US2635 - DRAFT US-3922 - User Interface</description>
    <formFactors>Large</formFactors>
    <label>Interventional Cardiology</label>
    <tab>standard-Account</tab>
    <tab>Sales_Performance</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Chatter</tab>
    <tab>SLCA2__Calendar_Anything</tab>
    <tab>standard-Opportunity</tab>
    <tab>Sales_Reports</tab>
    <tab>Territory_Analysis</tab>
    <tab>box__Box_Files2</tab>
</CustomApplication>
