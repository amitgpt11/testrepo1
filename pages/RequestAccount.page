<apex:page controller="RequestAccountController" showHeader="false" sidebar="false" action="{!init}">
    <apex:composition template="BSC_SiteTemplate">
        <apex:define name="body">
            <div id="requestAccount">
                <div id="header-wrap" >
                    <div class="container">
                        <header id="header" >
                            <div class="row">
                                <ul id="nav-utility" >
                                    <li><a href="{!$Page.Login}"><i class="fa fa-key"></i> {!$Label.Site.Login}</a></li>
                                </ul>
                            </div>
                            
                            <div id="logo-title-inTouch" class="pull-left">
                                <a href="{!$Page.AuthenticatedHomePage}">
                                    <apex:image value="{!URLFOR($Resource.bsc,'build/images/intouch-logo.jpg')}"  styleClass="img-reponsive" alt="In Touch"/>
                                </a>
                            </div>
                        </header>
                    </div>
                </div>
                
                <div class="container-greybkgd">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <section class="section">
                                    <h1>{!$Label.Boston_UnAuthHome_Body_Link_RequestAnAccount}</h1>
                                </section>
                                <apex:form id="unAthenticatedContactFrm" >
                                    
                                    <apex:pageMessages />
                                    
                                    <p>{!$Label.Boston_Unauthenticated_ContactUs_Page_Sub_Title}</p><br/><br/>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label class="control-label">{!$Label.Boston_Unauthenticated_ContactUs_ByEmail_FirstName_Title}*</label>
                                                <apex:inputText value="{!firstName}" styleClass="form-control"></apex:inputText>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label class="control-label">{!$Label.Boston_Unauthenticated_ContactUs_yBEmail_LastName_Title}*</label>
                                                <apex:inputText value="{!lastName}" styleClass="form-control"></apex:inputText>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label class="control-label">{!$Label.Boston_Unauthenticated_ContactUs_yBEmail_Email_Title}*</label>
                                                <apex:inputText value="{!contactEmail}" styleClass="form-control"></apex:inputText>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label class="control-label">{!$Label.Boston_Unauthenticated_ContactUs_yBEmail_Company_Title}*</label>
                                                <apex:inputText value="{!contactCompany}" styleClass="form-control"></apex:inputText>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label class="control-label">{!$Label.Boston_Unauthenticated_ContactUs_ByEmail_City_Title}*</label>
                                                <apex:inputText value="{!contactCity}" styleClass="form-control"></apex:inputText>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label class="control-label">{!$Label.Boston_Unauthenticated_ContactUs_Page_Specialty_Title}*</label>
                                                <apex:selectList required="true" multiselect="false" size="1" label="Type"  value="{!contactDivision}" styleClass="form-control" >
                                                    <apex:selectOptions value="{!Divisions}"/>
                                                    <apex:actionSupport event="onchange" reRender="contactLanguage,contactCountries" action="{!fetchLanguageAndCountryBasedOnSpeciality}"/>
                                                </apex:selectList>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label class="control-label">{!$Label.Boston_Unauthenticated_ContactUs_ByEmail_Country_Title}*</label>
                                                <apex:outputPanel id="contactCountries">
                                                    <apex:selectList required="true" multiselect="false" size="1" label="Type"  value="{!contactCountry}" styleClass="form-control">
                                                        <apex:selectOptions value="{!Countries}" />
                                                    </apex:selectList>
                                                </apex:outputPanel>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label class="control-label">{!$Label.Boston_Unauthenticated_ContactUs_Page_Profession_Title}*</label>
                                                <apex:selectList required="true" multiselect="false" size="1" label="Type"  value="{!contactFunction}" styleClass="form-control">
                                                    <apex:selectOptions value="{!functions}"/>
                                                </apex:selectList>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <apex:outputPanel id="contactLanguage">
                                                    <label class="control-label">{!$Label.Boston_Unauthenticated_ContactUs_Page_Language_Title}*</label>
                                                    <apex:selectList required="true" multiselect="false" size="1" label="Type" value="{!contactLanguage}" styleClass="form-control">
                                                        <apex:selectOptions value="{!Languages}"/>
                                                    </apex:selectList>
                                                </apex:outputPanel>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <label class="control-label">{!$Label.Boston_ContactUs_ByEmail_Message_Title}</label>
                                                <apex:inputTextArea required="false" value="{!messageDescription}" styleClass="form-control"></apex:inputTextArea><br/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <apex:commandButton value="Send" action="{!contactVTMForAccess}" styleClass="btn btn-primary btn-long " rerender="unAthenticatedContactFrm" oncomplete="openConfirmationModal();"></apex:commandButton>
                                            </div>
                                        </div>
                                    </div>
                                </apex:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End #Request Account -->
        </apex:define>
    </apex:composition>
    <script>
    $(document).ready(function(){
        // Hide header on Non Authenticated Page- No Nav needed
        $('#header-container').hide();
        $('.nav-footer').hide();
    });
    function openConfirmationModal(){
        
        if($('.warningM3').length == 0) {
         
            $(".requestRecieved-modal").modal('show');
        }
    };
    </script>
    <!-- Model for request recieved confirmation Detail -->
    <div class="modal fade requestRecieved-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button"  id="btn-decline-close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{!$Label.Boston_UnAuthHome_Body_Link_RequestAnAccount}</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <p>{!$Label.Boston_Submit_Success}</p>
                        <div class="modal-footer">
                            <button type="button" id="btn-accept-terms" class="btn btn-default" data-dismiss="modal">{!$Label.Boston_Modal_Close}</button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</apex:page>