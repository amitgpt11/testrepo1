<!--                                                                                                              
	QuincyPhysicianReport.page
	
	@author Salesforce services
	@date	2015-02-03
	@desc 	Creates a PDF report (see US152).  Used as an email attachment
-->
<apex:page standardController="Contact"
	extensions="NMD_QuincyReportExtension"
	showHeader="false" 
	standardStylesheets="false"
	applyHtmlTag="false"
	applyBodyTag="false"
	renderAs="pdf"
	cache="true"
>
<html> 
	<head>
		<style>
			@media print {
			    table { 
			    	page-break-inside: auto; 
			    }
   				tr { 
   					page-break-inside: avoid; 
   					page-break-after: auto; 
   				}
   				tr.break {
   					page-break-before: always;
   				}
			}

			body {
				font-family: Helvetica, "Arial Unicode MS", Times;
			}

			table {
				border-collapse: collapse;
				border: 1px solid black;
				width: 100%;
			}
			table td {
				border: 1px solid black;
				padding: 3px;
				min-width: 75px;
				padding-left: 9px;
			}
			td.header {
				background-color: #e6e6e6;
				text-align: center;
				font-weight: bold;
			}
			td.highlight {
				background-color: #fffd38;
				text-align: center;
				font-weight: bold;
			}
			table.flat {
				border: none;
			}
			td.flat {
				border: none;
				padding: 0px;
				margin: 0px;
			}

			span {
				font-weight: normal;
			}

			div.logo {
				margin-left: 75px;
				margin-bottom: 75px;
			}

		</style>
	</head>
	<body>

		<div class="logo">
			<img src="{!URLFOR($Resource.QuincyReportImages, 'BSC_150blue_RGB.png')}"/>
		</div>

		<!-- Contact -->
		<table>
			<tr>
				<td class="highlight" colspan="2">SURGEON</td>
			</tr>
			<tr>
				<td class="header" colspan="2">
					Name of Surgeon
				</td>
				<td colspan="3">{!UPPER(Contact.Name)}</td>
				<td class="header" rowspan="2" colspan="4">
					Date of First Surgery<br/>
					(month/day/year)
				</td>
				<td rowspan="2" colspan="2">
					<apex:outputText value="{0,date,MM/dd/yyyy}">
						<apex:param value="{!Contact.Date_of_First_Surgery__c}"/>
					</apex:outputText>
				</td>
			</tr>
			<tr>
				<td class="header" colspan="2">
					Doctor’s Credential <span>(M.D., etc.)</span>
				</td>
				<td colspan="3">
					<apex:outputText value="{!UPPER(BLANKVALUE(Contact.Academic_Title__c,'&nbsp;'))}" escape="false"/>
				</td>
			</tr>
			<tr>
				<td class="header">
					Phone
				</td>
				<td class="header" colspan="3">
					Ext
				</td>
				<td class="header" colspan="3">
					Fax
				</td>
				<td class="header" colspan="4">
					Email
				</td>
			</tr>
			<tr>
				<td>
					<apex:outputText value="{!BLANKVALUE($CurrentPage.parameters.Phone,BLANKVALUE(Contact.Phone,'&nbsp;'))}" escape="false"/>
				</td>
				<td colspan="3">&nbsp;</td>
				<td colspan="3">
					<apex:outputText value="{!BLANKVALUE($CurrentPage.parameters.Fax,BLANKVALUE(Contact.Fax,'&nbsp;'))}" escape="false"/>
				</td>
				<td colspan="4">
					<apex:outputText value="{!UPPER(BLANKVALUE(Contact.Email,'&nbsp;'))}" escape="false"/>
				</td>
			</tr>
			<tr>
				<td class="header" colspan="6">
					Practice Name and Address <span>(For communication purposes)</span>
				</td>
				<td class="header" colspan="2">
					City
				</td>
				<td class="header" colspan="2">
					State
				</td>
				<td class="header">Zip</td>
			</tr>
			<tr>
				<td colspan="6">
					<apex:outputText value="{!UPPER(BLANKVALUE(Contact.Account.Name,'&nbsp;'))}" escape="false"/>
				</td>
				<td rowspan="2" colspan="2">
					<apex:outputText value="{!UPPER(BLANKVALUE($CurrentPage.parameters.MailingCity,BLANKVALUE(Contact.MailingCity,'&nbsp;')))}" escape="false"/>
				</td>
				<td rowspan="2" colspan="2">
					<apex:outputText value="{!UPPER(BLANKVALUE($CurrentPage.parameters.MailingState,BLANKVALUE(Contact.MailingState,'&nbsp;')))}" escape="false"/>
				</td>
				<td rowspan="2">
					<apex:outputText value="{!UPPER(BLANKVALUE($CurrentPage.parameters.MailingPostalCode,BLANKVALUE(Contact.MailingPostalCode,'&nbsp;')))}" escape="false"/>
				</td>
			</tr>
			<tr>
				<td colspan="6">
					<apex:outputText value="{!UPPER(BLANKVALUE($CurrentPage.parameters.MailingStreet,BLANKVALUE(Contact.MailingStreet,'&nbsp;')))}" escape="false"/>
				</td>
			</tr>
			<tr>
				<td class="header" colspan="3">Salesforce ID</td>
				<td colspan="8">{!LEFT(Contact.Id, 15)}</td>
			</tr>
			<tr>
				<td class="header" colspan="3">SAP ID</td>
				<td colspan="8">
					<apex:outputText value="{!BLANKVALUE(Contact.SAP_ID__c,'&nbsp;')}" escape="false" />
				</td>
			</tr>
			<tr>
				<td class="header" colspan="3">NPI</td>
				<td colspan="8">
					<apex:outputText value="{!BLANKVALUE(Contact.NPI_Number__c,'&nbsp;')}" escape="false" />
				</td>
			</tr>
			<tr>
				<td class="header" colspan="3">AVP</td>
				<td colspan="8">{!IF(OR(ISBLANK(Contact.Owner.ManagerId),ISBLANK(Contact.Owner.Manager.ManagerId)),' ',UPPER(BLANKVALUE(Contact.Owner.Manager.Manager.Name,' ')))}</td>
			</tr>
			<tr>
				<td class="header" colspan="3">RBM</td>
				<td colspan="8">{!IF(ISBLANK(Contact.Owner.ManagerId),' ',UPPER(BLANKVALUE(Contact.Owner.Manager.Name,' ')))}</td>
			</tr>
			<tr>
				<td class="header" colspan="3">Territory Owner</td>
				<td colspan="8">{!UPPER(Contact.Owner.Name)}</td>
			</tr>
			<tr>
				<td class="header" colspan="3">Territory ID</td>
				<td colspan="8">{!UPPER(Contact.Territory_ID__r.Name)}</td>
			</tr>
			<tr>
				<td class="header" colspan="3">Rep Submitting Form</td>
				<td colspan="8">{!UPPER($User.FirstName)} {!UPPER($User.LastName)}</td>
			</tr>
			<tr>
				<td class="header" colspan="3">
					Rep Owner of Acct<br/>
					<span>(Who is paid commission? One rep only)</span>
				</td>
				<td colspan="8">&nbsp;</td>
			</tr>
			<tr>
				<td class="header" colspan="3">
					Who should see account in SalesAccelerator?<br/>
					<span>(Reps must be in same region)</span>
				</td>
				<td colspan="8">&nbsp;</td>
			</tr>
		</table>

		<br/><hr/><br/>

		<!-- Accounts -->
		<table>
		<apex:repeat var="item" value="{!children}">
			<tr class="break">
				<td class="highlight" colspan="2">
					SURGICAL CENTER #{!item.index}
				</td>
			</tr>
			<tr>
				<td class="header" colspan="2">
					SOLD TO (Name, Attn: &amp; Address)<br/>
					<span>(where procedures will take place)</span>
				</td>
				<td class="header">
					Surgical Center Type
				</td>
				<td class="header" colspan="2">
					BILL TO (Name, Attn: &amp; Address)<br/>
					<span>(where invoices should be sent)</span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					{!UPPER(item.data['Name'])}<br/>
					{!UPPER(item.data['Sold_To_Attn__c'])}<br/>
					{!UPPER(item.data['Sold_to_Street__c'])}<br/>
					{!UPPER(item.data['Sold_to_City__c'])},&nbsp;{!UPPER(item.data['Sold_to_State__c'])}&nbsp;
					{!UPPER(item.data['Sold_to_Zip_Postal_Code__c'])}
				</td>
				<td>{!UPPER(item.data['Classification__c'])}</td>
				<td colspan="2">
					{!UPPER(item.data['Name'])}<br/>
					{!UPPER(item.data['Billing_Attn__c'])}<br/>
					{!UPPER(item.data['BillingStreet'])}<br/>
					{!UPPER(item.data['BillingCity'])},&nbsp;{!UPPER(item.data['BillingState'])}&nbsp;
					{!UPPER(item.data['BillingPostalCode'])}
				</td>
			</tr>
			<tr>
				<td class="header">
					Phone 1
				</td>
				<td>{!item.data['Phone']}</td>
				<td class="header">
					Group Affiliations
				</td>
				<td class="header">
					Phone 1
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="header">
					Phone 2
				</td>
				<td>&nbsp;</td>
				<td rowspan="2">&nbsp;</td>
				<td class="header">
					Phone 2
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="header">
					Fax
				</td>
				<td>{!item.data['Fax']}</td>
				<td class="header">
					Fax
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="header">
					Salesforce ID
				</td>
				<td colspan="4">
					{!LEFT(item.data.Id, 15)}
				</td>
			</tr>
			<tr>
				<td class="header">
					SAP ID
				</td>
				<td colspan="4">
					<apex:outputText value="{!UPPER(BLANKVALUE(item.data['Account_Number__c'],'&nbsp;'))}" escape="false"/>
				</td>
			</tr>
			<tr>
				<td class="header">
					Tax Exempt?
				</td>
				<td colspan="4">
					{!IF(item.data['Tax_Exempt__c'],'YES','NO')}
				</td>
			</tr>
		</apex:repeat>
		</table>


	</body>
</html>
</apex:page>