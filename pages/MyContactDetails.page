<apex:page controller="MyContactDetailsController" action="{!init}" showHeader="false">
    <apex:composition template="BSC_SiteTemplate">
        <apex:define name="body">
            <script>
            function updateContactModal(){
                $(".updateContact-modal").modal('show');
            };
            </script>
            <div id="contactDetails">
                <div class="grey-background">
                    <div class="container">
                        <div class="breadcrumb">
                            <div class="breadcrumb">
                                <a href="{!$Page.AuthenticatedHomePage}">{!$Label.Boston_Breadcrumbs_Home}</a> > <a href="{!$Page.MyAccount}"> {!$Label.Boston_Header_My_Account}</a> > {!$Label.Boston_My_Contact_Details}
                            </div>
                        </div>
                        
                        <div class="introTxt">
                            <h2>{!$Label.Boston_My_Contact_Details}</h2>
                            <p>{!$Label.Boston_My_Contact_Details_Copy}<a href="{!$Page.ContactUs}"> {!$Label.Boston_ContactUs_ContactUs_Title}</a>.</p>
                        </div>
                        <apex:form styleClass="form-horizontal">
                            <apex:outputPanel id="OPanelId">
                                <section>
                                    <h3>{!$Label.Boston_Contact_Details}</h3>
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group row">
                                                <label class="col-sm-3 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_ByEmail_FirstName_Title}</label>
                                                <div class="col-sm-9">
                                                    <apex:inputField value="{!objUser.Contact.firstname}" html-placeholder="First Name"  styleClass="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_yBEmail_LastName_Title}</label>
                                                <div class="col-sm-9">
                                                    <apex:inputField value="{!objUser.Contact.lastname}" html-placeholder="Last Name"  styleClass="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 form-control-label">{!$Label.Boston_Unauthenticated_ContactDetails_Phone_Title}</label>
                                                <div class="col-sm-9">
                                                    <apex:inputField value="{!objUser.Contact.phone}"  html-placeholder="Phone" styleClass="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_yBEmail_Email_Title}</label>
                                                <div class="col-sm-9">
                                                    <apex:inputField value="{!objUser.Contact.email}" html-placeholder="Email"  styleClass="form-control"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <apex:commandButton action="{!save}" value="{!$Label.Boston_Save}" rerender="OPanelId" styleClass="btn btn-primary btn-long" oncomplete="updateContactModal()"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <section>
                                            <h3>{!$Label.Boston_OrdersDetail_BillingAddress}</h3>
                                            <div class="form-group row">
                                                <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_ContactDetails_Street_Title}:</label>
                                                <div class="col-sm-8">
                                                    <apex:outputText value="{!objUser.Contact.Account.BillingStreet}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_ByEmail_City_Title}:</label>
                                                <div class="col-sm-8">
                                                    <apex:outputText value="{!objUser.Contact.Account.BillingCity}"/>
                                                </div>
                                            </div>
                                            <apex:outputPanel rendered="{!!isEuropeanUser}">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_ContactDetails_State_Title}:</label>
                                                    <div class="col-sm-8">
                                                        <apex:outputText value="{!objUser.Contact.Account.BillingState}"/>
                                                    </div>
                                                </div>   
                                            </apex:outputPanel>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_Details_PostalCode_Title}:</label>
                                                    <div class="col-sm-8">
                                                        <apex:outputText value="{!objUser.Contact.Account.BillingPostalCode}"/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_ByEmail_Country_Title}:</label>
                                                    <div class="col-sm-8">
                                                        <apex:outputText value="{!objUser.Contact.Account.BillingCountry}"/>
                                                    </div>
                                                </div>
                                            
                                            

                                        </section>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <section>
                                            <h3>{!$Label.Boston_OrdersDetail_ShippingAddress}</h3>
                                            <div class="form-group row">
                                                <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_ContactDetails_Street_Title}:</label>
                                                <div class="col-sm-8">
                                                    <apex:outputText value="{!objUser.Contact.Account.ShippingStreet}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_ByEmail_City_Title}:</label>
                                                <div class="col-sm-8">
                                                    <apex:outputText value="{!objUser.Contact.Account.ShippingCity}"/>
                                                </div>
                                            </div>
                                            <apex:outputPanel rendered="{!!isEuropeanUser}">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_ContactDetails_State_Title}:</label>
                                                    <div class="col-sm-8">
                                                        <apex:outputText value="{!objUser.Contact.Account.ShippingState}"/>
                                                    </div>
                                                </div>
                                            </apex:outputPanel>
                                            <div class="form-group row">
                                                <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_Details_PostalCode_Title}:</label>
                                                <div class="col-sm-8">
                                                    <apex:outputText value="{!objUser.Contact.Account.ShippingPostalCode}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_ByEmail_Country_Title}:</label>
                                                <div class="col-sm-8">
                                                    <apex:outputText value="{!objUser.Contact.Account.ShippingCountry}"/>
                                                </div>
                                            </div>
                                            
                                        </section>    
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <section>
                                            <h3>{!$Label.Boston_User_Fields}</h3>
                                            <div class="form-group row">
                                                <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_ByEmail_FirstName_Title}:</label>
                                                <div class="col-sm-8">
                                                    <apex:outputText value="{!objUser.firstname}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_yBEmail_LastName_Title}:</label>
                                                <div class="col-sm-8">
                                                    <apex:outputText value="{!objUser.lastname}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactUs_yBEmail_Email_Title}: </label>
                                                <div class="col-sm-8">
                                                    <apex:outputText value="{!objUser.email}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 form-control-label">{!$Label.Boston_Unauthenticated_ContactDetails_Phone_Title}:</label>
                                                <div class="col-sm-8">
                                                    <apex:outputText value="{!objUser.phone}"/>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </apex:outputPanel>
                        </apex:form>
                    </div>
                </div>
            </div>
            
            <!-- Model for Product Detail -->
            <div class="modal fade updateContact-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button"  id="btn-decline-close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">{!$Label.Boston_Contact_Details}</h4>
                        </div>
                        <div class="modal-body">
                            <div class="container">
                                <p>{!$Label.Boston_Contact_Details_Updated_Success}</p>
                                <div class="modal-footer">
                                    <button type="button" id="btn-accept-terms" class="btn btn-default" data-dismiss="modal"> {!$Label.Boston_Modal_Close} </button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </apex:define>
    </apex:composition>
</apex:page>