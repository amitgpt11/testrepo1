<apex:page standardController="Patient__c" 
    title="{!$Label.NMD_Add_Attachment}: {!Patient__c.Name}"
    extensions="NMD_PatientNewAttachmentExtension"
    action="{!checkRedirect}"
    standardStylesheets="false"
    showHeader="false">

    <apex:includeScript value="{!URLFOR($Resource.Javascript_Load_Image, 'load-image.all.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.jQuery_1_11_2, 'jquery-1.11.2.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.JavaScript_Canvas_to_Blob, 'js/canvas-to-blob.min.js')}"/>

    <apex:stylesheet value="{!URLFOR($Resource.SF1_Bootstrap_Theme, 'css/bootstrap.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.SF1_Bootstrap_Theme, 'css/docs.min.css')}"/>

    <!--HEADER-->
    <div class="page-header page-header-compact context-file">
        <h1>{!$Label.NMD_Add_Attachment} <span class="page-header-label">{!Patient__c.Name}</span></h1>
    </div>

    <apex:outputPanel rendered="{!ISBLANK(Patient__c.Id)}">
        
        <!--ALERT-->
        <div class="alert alert-danger" style="padding:10px">
            <button type="button" class="close">×</button>
            <strong>{!$Label.NMD_Invalid_Missing_Patient}</strong>
        </div>

    </apex:outputPanel>

    <apex:outputPanel rendered="{!NOT(ISBLANK(Patient__c.Id))}">

        <style type="text/css">
            .b {
                -webkit-transform:rotateY(180deg);
                -moz-transform:rotateY(180deg);
                -o-transform:rotateY(180deg);
                -ms-transform:rotateY(180deg);
                transform:rotateY(180deg);
            }

            .rotate {
                width: 100%;
            }

            #btn_rotleft {
                margin-left: 10px
            }

            #btn_rotright {
                float:right;
                margin-right: 10px;
            }
        </style>

        <!--ALERT-->
        <div class="alert alert-info" style="display:none;padding:10px">
            <button type="button" class="close">×</button>
            <strong></strong>
        </div>

        <!--PROGRESS-->
        <div class="progress" style="display:none">
            <div class="progress-bar" role="progressbar"></div>
        </div>

        <!--BUTTONS-->
        <div style="margin:0 auto;width:105px;">
            <button id="btn_submit" type="button" class="btn btn-lg btn-primary">{!$Label.NMD_Submit}</button>
        </div>

        <!--FORM-->
        <div style="padding:10px">
            <apex:form id="attachmentForm">
                <fieldset>
                    <legend>{!$Label.NMD_Attachment}</legend>
                    <div class="form-group">
                        <label for="classification">{!$Label.NMD_Classification}</label>
                        <apex:selectList id="classifications" styleClass="required form-control" size="1" value="{!classification}">
                            <apex:selectOptions value="{!classifications}"/>
                        </apex:selectList>
                    </div>
                    <div class="form-group">
                        <label for="imgfile">{!$Label.NMD_File}</label>
                        <input id="imgfile" type="file" class="required form-control" />
                    </div>
                </fieldset>
            </apex:form>
        </div>

        <!--PREVIEW-->
        <div class="rotate" style="display:none;padding-bottom:8px">
            <button id="btn_rotright" type="button" class="btn btn-lg btn-default"><span class="glyphicon glyphicon-share-alt"></span></button>
            <button id="btn_rotleft" type="button" class="btn btn-lg btn-default" style="height:38px;width:74px;"><div style="position:fixed;margin-top:-8px"><span class="glyphicon glyphicon-share-alt b"></span></div></button>
        </div>
        <div id="preview" style="padding:10px;margin:auto;"></div>

        <script type="text/javascript">

        //force.init({ accessToken: '{!$Api.Session_ID}' });

        (function($) {

            var is_uploaded = false,
                img_orientation = 0,
                accepts_warning = false;

            var $alert = $('.alert'),
                $classification = $('select[id$=":classifications"]'),
                $preview = $('#preview'),
                $rotate = $('.rotate'),
                $rot_left = $('#btn_rotleft'),
                $rot_right = $('#btn_rotright'),
                $submit = $('#btn_submit'),
                $progress = $('.progress'),
                $progressbar = $('.progress-bar'),
                $imgfile = $('#imgfile');

            
            $imgfile.on('change', function(e) {

                var img_file = e.target.files[0];

                if (img_file) {
                    loadImage.parseMetaData(img_file,
                        function (data) {

                            img_orientation = +((data.exif && data.exif.get('Orientation')) || 0);
                            loadPreview();
                            
                        },
                        {
                            disableImageHead: true
                        }
                    );
                }
                else {
                    $preview.html('');
                    $rotate.hide();
                }
                
            });

            $rot_left.on('click', function() {
                switch (img_orientation) {
                    case 0:
                        img_orientation = 8;
                        break;
                    case 3:
                        img_orientation = 6;
                        break;
                    case 6:
                        img_orientation = 0;
                        break;
                    case 8:
                        img_orientation = 3;
                        break;
                    default:
                        img_orientation = 0;
                }
                loadPreview();
            });

            $rot_right.on('click', function() {
                switch (img_orientation) {
                    case 0:
                        img_orientation = 6;
                        break;
                    case 3:
                        img_orientation = 8;
                        break;
                    case 6:
                        img_orientation = 3;
                        break;
                    case 8:
                        img_orientation = 0;
                        break;
                    default:
                        img_orientation = 0;
                }
                loadPreview();
            });

            $submit.on('click', function() {
                var $this = $(this);
                if ($this.text() == '{!$Label.NMD_Submit}' && validateForm()) {

                    var $preview_img = $preview.find('canvas');
                    if (!accepts_warning && $preview_img.width() < $preview_img.height()) {
                        accepts_warning = true;
                        $alert.show()
                            .removeClass().addClass('alert alert-warning')
                            .find('strong').text('{!$Label.NMD_Rotate_Image}');
                    }
                    else {

                        var file_elem = $imgfile[0].files[0],
                            file_ext = file_elem.name.indexOf('.') !== -1 ? file_elem.name.substring(file_elem.name.lastIndexOf('.')) : '',
                            file_name = $classification.val() + file_ext,
                            file_type = file_elem.type;

                        ($preview.find('canvas')[0]).toBlob(function(blob) {

                            //chatter api expects the form in a specific manner
                            var formData = new FormData();
                            formData.append('feedElementFileUpload', blob, file_name);
                            formData.append('feedElement', JSON.stringify({
                                body: {
                                    messageSegments: [{
                                        type: 'Text',
                                        text: 'New Patient Image'
                                    }]
                                },
                                capabilities: {
                                    content: {
                                        description: $classification.val(),
                                        title: file_name
                                    }
                                },
                                feedElementType: 'FeedItem',
                                subjectId: '{!Patient__c.Id}'
                            }));

                            $.ajax({
                                url: "{!LEFT($Api.Partner_Server_URL_340,FIND('/services',$Api.Partner_Server_URL_340))}services/data/v34.0/chatter/feed-elements",
                                type: 'POST',
                                xhr: xhrHandler,
                                //Ajax events
                                success: onUpload,
                                error: onUploadFail,
                                beforeSend: onBeforeSend,
                                // Form data
                                data: formData,
                                //Options to tell JQuery not to process data or worry about content-type
                                cache: false,
                                contentType: false,
                                processData: false
                            });

                        }, file_type);

                    }

                }
            });

            function loadPreview() {

                var img_file = $imgfile[0].files[0];

                loadImage(img_file,
                    function (img) {
                        var w = $preview.width();
                        $preview.html(img);
                        $preview.find('canvas').css('width', w + 'px');
                        $rotate.show();
                        accepts_warning = false;
                    },
                    {
                        canvas: true,
                        maxWidth: 1024,
                        orientation: img_orientation || 0
                    }
                );
            }

            function xhrHandler() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){ // check if upload property exists
                    myXhr.upload.addEventListener('progress', onProgress, false);
                }
                return myXhr;
            }

            function onProgress(status) {
                var val = Math.ceil(status.loaded / status.total * 100);
                $progressbar.width(val + '%');
            }

            function onUpload(result) {
                $alert.show()
                    .removeClass().addClass('alert alert-info')
                    .find('strong').text('Upload Successful');
                $submit.text('{!$Label.NMD_Submit}');
                $progress.hide();
                $imgfile.replaceWith( $imgfile = $imgfile.clone( true ) );
                $rotate.hide();
            }

            function onUploadFail(result) {
                console.log(result);
                $alert.show()
                    .removeClass().addClass('alert alert-danger')
                    .find('strong').text('Check error log');
                $submit.text('{!$Label.NMD_Submit}');
                $progress.hide();
            }

            function onBeforeSend(xhr) {
                xhr.setRequestHeader("Authorization", "OAuth {!$Api.Session_ID}");
                $submit.text('Uploading...');
                $alert.hide();
                $progressbar.width('0%');
                $progress.show();
            }

            var $errorDivA = $('<div class="msg text-danger"><strong>{!$Label.NMD_Error}</strong> {!$Label.NMD_Must_Select_Value}</div>');
            var $errorDivB = $('<div class="msg text-danger"><strong>{!$Label.NMD_Error}</strong> {!$Label.NMD_File_Too_Large_2}</div>');

            function validateForm() {

                var passed = true;
                //find all the inputs in the form that are wrapped in a requiredInput div
                $('form[id$=":attachmentForm"]').find('.required').each(function() {

                    var $input = $(this),
                        $parent = $input.parent(),
                        val = '', 
                        length = 0;

                    if ($input.is(':file')) {
                        val = $input.val();
                        if (val != '' && typeof FileReader !== "undefined") {
                            length = $input[0].files[0].size;
                        }
                    }
                    else {
                        val = $input.val();
                    }

                    //clear any active errors
                    $parent.removeClass('has-error');
                    $parent.find('.msg').remove();

                    if (!val || val == '') { 
                        passed = false;
                        $parent.addClass('has-error');
                        $parent.append($errorDivA.clone());
                    }
                    else if (length > 4194304) { //4MB
                        passed = false;
                        $parent.addClass('has-error');
                        $parent.append($errorDivB.clone());
                    }

                });

                return passed;

            }

        })(jQuery);

        </script>

    </apex:outputPanel>
    
</apex:page>