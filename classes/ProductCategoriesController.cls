public class ProductCategoriesController extends AuthorizationUtil{
    
    public List<ProductCategory> lstProductCategories {get; set;}
    
    public class ProductCategory {
        
        public String productCategoryName {get;set;}
        public Integer productCount {get;set;}
        public String strChildImageURL {get;set;}
        
        public ProductCategory(){}
        
        public ProductCategory(String productCategoryName, Integer productCount) {
            
            this.productCategoryName = productCategoryName;
            this.productCount = productCount;
        }
    }
    
    /**
        * Description: method to query product categories
    */
    public override void fetchRequestedData() {
        
        lstProductCategories = new List<ProductCategory>();
        Set<String> setProductFamilies = new Set<String>();
        Map<String,Set<String>> mapDocumentUniqueNameToCategories = new Map<String,Set<String>>();
        Map<String,String> mapProductFamilyToChildImage = new Map<String,String>();
        Map<String,ProductCategory> mapFamilyProdCategory = new Map<String,ProductCategory>();
        
        User objUser = [Select Shared_Community_Division__c, LanguageLocaleKey 
                        From User 
                        Where Id =: UserInfo.getUserId()];
        
        for(AggregateResult productCategory : [SELECT count(Id) Total, Family ProductCategory
                                               FROM Product2
                                               Where IsActive = true
                                               AND Division__c =: objUser.Shared_Community_Division__c
                                               AND Shared_Parent_Product__c = null
                                               AND Family <> null
                                               group by Family
                                               order by Family]) {
                                                   
                                                   lstProductCategories.add(new ProductCategory(String.valueOf(productCategory.get('ProductCategory')), Integer.valueOf(productCategory.get('Total'))));
                                                   setProductFamilies.add(String.valueOf(productCategory.get('ProductCategory')));
                                               }
                                               
        //get custom settings
        for(Product_Category_Image_Mapping__c objImageMapping : [SELECT Name, Document_Unique_Name__c 
                                                                FROM Product_Category_Image_Mapping__c
                                                                WHERE Name =: setProductFamilies]){
                                                                    
             if(!mapDocumentUniqueNameToCategories.containsKey(objImageMapping.Document_Unique_Name__c)){
                
                mapDocumentUniqueNameToCategories.put(objImageMapping.Document_Unique_Name__c, new Set<String>{objImageMapping.Name});    
             }else{
                 
                 mapDocumentUniqueNameToCategories.get(objImageMapping.Document_Unique_Name__c).add(objImageMapping.Name);
             }
        }
        
         
        //get the images from documents                                       
        for(Document objDocument : [SELECT id, Name, DeveloperName 
                                    FROM Document 
                                    WHERE Folder.DeveloperName = 'Product_Category_Images'
                                    AND DeveloperName =: mapDocumentUniqueNameToCategories.keySet()]){
                                        
                for(String strCategory : mapDocumentUniqueNameToCategories.get(objDocument.DeveloperName)){
                 
                    mapProductFamilyToChildImage.put(strCategory,'/servlet/servlet.FileDownload?file='+objDocument.Id);
                }
        }
                                                
        for(ProductCategory objProduct : lstProductCategories){
                
            ProductCategory objProductCategories = new ProductCategory();
            objProductCategories = objProduct;
            
            if(mapProductFamilyToChildImage.containsKey(objProduct.productCategoryName)){
                
                objProductCategories.strChildImageURL = mapProductFamilyToChildImage.get(objProduct.productCategoryName);
            }
            
            mapFamilyProdCategory.put(objProduct.productCategoryName, objProductCategories);
        }                   
        
        system.debug('## lstProductCategories : '+lstProductCategories);
    }
}