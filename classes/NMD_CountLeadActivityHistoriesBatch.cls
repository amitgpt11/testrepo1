/**
* Batch class, counts the number of Activity Histories on each open Lead. 
*
* @author 	Salesforce services
* @date 	2014-02-03
*/
global without sharing class NMD_CountLeadActivityHistoriesBatch implements Database.Batchable<sObject>, Schedulable {

	/**
	*  @desc	Immediately executes an instance of this batch
    */
	public static void runNow() {
		(new NMD_CountLeadActivityHistoriesBatch()).execute(null);
	}

	//static final List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>{};
	
	public String query = 'select Id, (select Id from ActivityHistories) from Lead';
	
	global NMD_CountLeadActivityHistoriesBatch() {
		if (Test.isRunningTest()) {
			this.query += ' order by CreatedDate desc limit 100';
		}
	}
	
	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(this.query);
	}

	/**
	*  @desc	Database.Bachable method
	*/
   	global void execute(Database.BatchableContext bc, List<Lead> scope) {
		
		List<Lead> leads = new List<Lead>{};
		for (Lead lead : scope) {
			leads.add(new Lead(
				Id = lead.Id,
				Activity_Histories_Count__c = lead.ActivityHistories.size()
			));
		}

		//List<Database.SaveResult> results = Database.update(leads, false);
		//for (Database.SaveResult result : results) {
		//	if (!result.isSuccess()) {
		//		for (Database.Error err : result.getErrors()) {
		//			logs.add(new ApplicationLogWrapper(err, result.getId(), 'NMD_CountLeadActivityHistoriesBatch.execute'));
		//		}
		//	}
		//}
		DML.save(this, leads, false);

	}

	/**
	*  @desc	Schedulable method, executes the class instance
    */
	global void execute(SchedulableContext context) {

		String className = String.valueOf(this).split(':')[0];
        Integer runAllCount = 0;
        Integer runSameCount = 0;

        // check for any other batches that may be running
        for (AsyncApexJob job : [
            select ApexClass.Name
            from AsyncApexJob
            where Status in ('Preparing', 'Processing')
            and JobType in ('BatchApex', 'BatchApexWorker')
        ]) {
            runAllCount++;
            if (job.ApexClass.Name == className) {
                runSameCount++;
            }
        }

        // only run if there is an available slot, and if no other of the same kind are
        if (runAllCount < 5 && runSameCount == 0) {
            Database.executeBatch(this);
        }
        else {
        	// TODO: log a warning that the batch was unable to be executed
        }

	}
	
	global void finish(Database.BatchableContext bc) {
		//if (!logs.isEmpty()) {
		//	Logger.logMessage(logs);
		//}
	}
	
}