/*
 @CreatedDate     05APR2016                                  
 @ModifiedDate    05APR2016                                  
 @author          Mayuri-Accenture
 @Description     This is the batch which runs everyday to recalculate the Territory sharing for My_Objectives__c records.
 @Methods         batchAssignMyObjectivesToEuropeUsers 
 @Requirement Id  
 */
 
 global class batchAssignMyObjectivesToEuropeUsers implements Database.Batchable<sObject> { 
 
    public static final set<string> recordTypeNameSet = MyObjectivesRecordType__c.getall().keyset();
    static final Map<Id,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.My_Objectives__c.getRecordTypeInfosById();     
    global Database.QueryLocator start(Database.BatchableContext BC) {
   
    String query =  'select ' +
                    'Id,OwnerId,name, ' +
                    'Account_Performance__c, ' +
                    'Contact__c, ' +
                    'Division__c, ' +
                    'End_Date__c, ' +
                    'Products__c, ' +
                    'Related_to_Account__c, '+
                    'Status__c, '+
                    'Territory__c, '+
                    'RecordTypeId '+
                    'from My_Objectives__c ' +
                    'where Territory__c != null ' +
                    'and Status__c != \'Completed\' ';                     
        
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<My_Objectives__c> scope) {
        
        system.debug('--scope--'+scope);
        list<My_Objectives__c> myObjLst = new list<My_Objectives__c>();
        list<Id> myObjIdLst = new list<Id>();
        boolean isDeleteSuccess = false;
        boolean isAssignSuccess = false;
        
         
        for(My_Objectives__c mo : scope){
            if(mo.RecordTypeId == null){
                myObjLst.add(mo);
                myObjIdLst.add(mo.Id);
            }
            else{
                if(recordTypeNameSet.contains(RECTYPES.get(mo.RecordTypeId).Name) == false){
                    myObjLst.add(mo);
                    myObjIdLst.add(mo.Id);
                }
            }
        }
        system.debug(myObjLst+'--myObjLst--');
        if(myObjIdLst!=null && myObjIdLst.size()> 0){ 
            UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
            isDeleteSuccess = util.removeMyObjectvesShares(myObjIdLst);
        }
        
        if(isDeleteSuccess == true && myObjLst != null && myObjLst.size() > 0){                 
            UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
            isAssignSuccess = util.assignMyObjectvesToAllUsers(myObjLst);
        }

    }   
    
    global void finish(Database.BatchableContext BC) {
    }

}