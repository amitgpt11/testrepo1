/**
* Test class for the Task trigger
*
* @Author salesforce Services
* @Date 02/10/2015
*/
@isTest(SeeAllData=false)
private class TaskTriggerHandlerTest {
	
	static final String NAME = 'Name';
	static final String FNAME = 'FirstName';
	static final String LNAME = 'LastName';

	@testSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		td.setupNuromodUserRoleSettings();

		Account acct = td.createConsignmentAccount();
		acct.Name = NAME;
		insert acct;

		Contact cont = td.newContact(acct.Id, NAME);
		cont.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
		insert cont;

		Patient__c patient = td.newPatient(FNAME, LNAME);
		insert patient;

		Lead lead = td.newLead(LNAME);
		lead.Patient__c = patient.Id;
		lead.Trialing_Physician__c = cont.Id;
		lead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('NMD Patient').getRecordTypeId();
		lead.Status = 'Contacted';
		insert lead;

	}

	static testMethod void test_PatientTaskSubject() {

		Patient__c patient = [select Id from Patient__c where Patient_Last_Name__c = :LNAME];
		Lead lead = [select Id from Lead where Patient__c = :patient.Id];

		User admin = new NMD_TestDataManager().newUser('System Administrator');
		System.runAs(admin) {

			Task task = new Task(
				ActivityDate = System.today().addDays(1),
				Subject = TaskManager.RAW_PATIENT_EDUCATION_SUBJECT,
				WhoId = lead.Id,
				Message__c = 'Name: test Email: test@test.com Company: XYZ City: test Country: test Division: Cardiology Function: Buyer Language: English Subject: test RequestMessage: This is test message.'
			);
			insert task;

			task = [select Subject from Task where Id = :task.Id];
			String expectedSubject = TaskManager.formatSubject(NAME, FNAME, LNAME);
			System.assertEquals(expectedSubject, task.Subject);

			delete task;

		}

	}

    static testMethod void test_BSMITaskHandler(){
       
        Test_DataCreator.cerateBostonCSConfigTestData('00311000019oiDH');
        User objUser = Test_DataCreator.createCommunityUser();
       
        System.runAs(objUser){
            
        	Task task = new Task(
    			ActivityDate = System.today().addDays(1),
    			Subject = TaskManager.RAW_PATIENT_EDUCATION_SUBJECT,
    		    WhoId = objUser.ContactId,
    			Message__c = 'Name: test Email: test@test.com Company: XYZ City: test Country: test Division: Cardiology Function: Buyer Language: English Subject: test RequestMessage: This is test message.',
    			RecordTypeId = [SELECT Id,SobjectType,Name 
                                FROM RecordType 
                                WHERE DeveloperName ='Shared_Community_Task' 
                                AND SobjectType ='Task' 
                                LIMIT 1].Id
    		);
    		
    		insert task;
    		
    		system.assertNotEquals(BSMITaskHandler.lstMails.size(),0);
        }
    }
	//static testMethod void test_makeUserTaskOwner(){
	//	NMD_TestDataManager td = new NMD_TestDataManager();

	//	User nmdFieldRep = new NMD_TestDataManager().newUser('NMD Field Rep');
	//	User nmdIBUPCUser = new NMD_TestDataManager().newUser('NMD IBU-PC');
	//	User adminUser = new NMD_TestDataManager().newUser('System Administrator');
	//	insert new List<User>{nmdFieldRep, nmdIBUPCUser};

	//	Task task;

	//	System.runAs(nmdFieldRep){

	//		Account acct = [SELECT Id FROM Account WHERE Name = :NAME];

	//		Contact cont = td.newContact(acct.Id, NAME);
	//		cont.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
	//		insert cont;

	//		Patient__c patient = td.newPatient(FNAME + 'test2', LNAME + 'test2');
	//		insert patient;

	//		Lead lead = td.newLead(LNAME);
	//		lead.Patient__c = patient.Id;
	//		lead.Trialing_Physician__c = cont.Id;
	//		lead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('NMD Patient').getRecordTypeId();
	//		lead.Status = 'Contacted';
	//		insert lead;

	//		task = new Task(
	//			ActivityDate = System.today().addDays(1),
	//			Subject = 'Owner Change',
	//			WhoId = lead.Id,
	//			OwnerId = nmdFieldRep.Id
	//		);
	//		insert task;

	//		//Boolean error = false;
	//		//try{
	//		//	task.Make_Me_Owner__c = true;
	//		//	update task;
	//		//} catch (Exception e){
	//		//	error = true;
	//		//}
	//		//System.assertEquals(true, error, 'Expecting Error when non Admin/IBU-PC user tries to change owner');

	//		//clear trigger hashes to allow another update later
	//		TriggerHandlerManager.clearTriggerHashes();
	//	}

	//	Test.startTest();

	//		System.runAs(adminUser){

	//			Boolean completedError = false;
	//			try{
	//				task.Make_Me_Owner__c = true;
	//				task.Status = 'Completed';
	//				update task;
	//			} catch (Exception e){
	//				completedError = true;
	//			}
	//			System.assertEquals(true, completedError, 'Expecting Error trying to change owner of completed task.');

	//			//clear trigger hashes to allow another update later
	//			//TriggerHandlerManager.clearTriggerHashes();
				
	//			//task.Status = 'Not Started';
	//			//task.Make_Me_Owner__c = true;
	//			//update task;

	//			//task = [SELECT Id, OwnerId, Make_Me_Owner__c FROM Task WHERE id = :task.Id];
	//			//System.assertEquals(UserInfo.getUserId(), task.OwnerId, 'Task Not owned by NMU IBU-PC/System Admin user!');
	//			//System.assertEquals(false, task.Make_Me_Owner__c, 'Task.Make_Me_Owner__c should be false after change!');
	//		}

	//	Test.stopTest();
	//}

}