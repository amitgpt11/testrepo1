/**
* Test class for NMD_ObjectSharingBatch
*
* @Author salesforce Services
* @Date 05/06/2015
*/
@isTest(SeeAllData=false)
private class NMD_ObjectSharingBatchTest {
    
    final static String TERRITORY = 'TERRITORY';
    final static NMD_TestDataManager testManager = new NMD_TestDataManager();

    static testMethod void test_AssigneeTrigger() {

        //create seller hierarchy
        Seller_Hierarchy__c seller = testManager.newSellerHierarchy(TERRITORY);
        insert seller;

        //create assignee
        Assignee__c assignee = testManager.newAssignee('Name', UserInfo.getUserId(), seller.Id);
        insert assignee;

        //create and update Patient
        Patient__c patient = testManager.newPatient();
        insert patient;
        patient.Territory_ID__c = seller.Id;
        update patient;

        //create and update lead
        Lead lead = testManager.newLead();
        lead.patient__c = patient.Id;
        insert lead;
        lead.Territory_ID__c = seller.Id;
        update lead;
        
        //create account
        Account acc = testManager.createConsignmentAccount();
        insert acc;
        
        //create and update Opptny
        Opportunity opportunity = testManager.newOpportunity(acc.Id);
        insert opportunity;
        opportunity.Territory_ID__c = seller.Id;
        update opportunity;
        

        
        //create and update Contact
        Contact contact = testManager.newContact(acc.Id);
        contact.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        insert contact;
        contact.Territory_ID__c = seller.Id;
        update contact;
        
        NMD_ObjectSharingBatch.runNowContacts();
        NMD_ObjectSharingBatch.runNowLeads();
        NMD_ObjectSharingBatch.runNowOpportunities();
        NMD_ObjectSharingBatch.runNowPatients();
        
    }

}