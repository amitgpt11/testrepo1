/**
* Manager class to find Orders relevant to the supplied user
*
* @Author salesforce Services
* @Date 2016-01-09
*/
public with sharing class NMD_OrderHistoryManager {
	
	public static List<Order> getOrdersHistory() {
		
		return getOrdersHistory(UserInfo.getUserId());

	}

	public static List<Order> getOrdersHistory(Id userId) {

		Set<Id> sharedOpptyIds = new Set<Id>();
		for (OpportunityShare share : [
			select
				OpportunityId
			from OpportunityShare
			where UserOrGroupId = :userId
			//and RowCause != 'Owner'
		]) {
			sharedOpptyIds.add(share.OpportunityId);
		}

		return new List<Order>([
			select
				Account.Name,
		        CreatedDate,
		        Delivery_Number__c,
		        Description,
		        Opportunity.Patient__r.Patient_First_Name__c,
		        Opportunity.Patient__r.Patient_Last_Name__c,
		        OrderNumber,
		        Owner.Name,
		        OwnerId,
		        Patient_Full_Name__c,
		        PONumber,
		        Receiving_Location__c,
		        Receiving_Location__r.Inventory_Data_ID__r.Account__c,
		        Receiving_Location__r.Inventory_Data_ID__r.Account__r.Name,
		        Receiving_Location__r.Inventory_Data_ID__r.Owner.Name,
		        Receiving_Location__r.Inventory_Data_ID__r.OwnerId,        
		        RecordTypeId,
		        RecordType.DeveloperName,
		        RecordType.Name,
		        Ship_Date__c,
		        Ship_Method__c,
		        Shipping_Location__c,
		        Shipping_Location__r.Inventory_Data_ID__r.Account__c,
		        Shipping_Location__r.Inventory_Data_ID__r.Account__r.Name,
		        Shipping_Location__r.Inventory_Data_ID__r.Owner.Name,
		        Shipping_Location__r.Inventory_Data_ID__r.OwnerId,
		        Stage__c,
		        Status,
		        StatusCode,
		        Surgeon__r.Name,
		        Total_Quantity__c,
		        TotalAmount
			from Order
			where OwnerId = :userId
		    or (
		    		RecordTypeId in (:OrderManager.RECTYPE_PATIENT, :OrderManager.RECTYPE_TRANSFER)
		    	and (
		    			Receiving_Location__r.Inventory_Data_ID__r.OwnerId = :userId 
		    		 or Shipping_Location__r.Inventory_Data_ID__r.OwnerId = :userId
		    	)
			)
			or (
					OpportunityId in :sharedOpptyIds
				and RecordTypeId = :OrderManager.RECTYPE_PATIENT
				and PoNumber = 'PO to Follow' 
				and Stage__c != 'New'
			)
			order by CreatedDate desc
		]);

	}

}