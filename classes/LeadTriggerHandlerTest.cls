/**
* Test Class for the TriggerHandler methods for Lead triggers.       
*
* @Author salesforce Services
* @Date 03/23/2015
*/
@isTest(SeeAllData=false)
private class LeadTriggerHandlerTest {
    
    static final String LNAME = 'LName';
    static final String LEAD_STATUS_TYPE = 'Unqualified';
    
    @testSetup      
    static void setup() {
    //static testMethod void testLeadTrigger() {    

        NMD_TestDataManager td = new NMD_TestDataManager();
        
        //create territory
        Seller_Hierarchy__c newSeller = td.newSellerHierarchy(LNAME);
        newSeller.Rep_Code__c = 'TEST';
        Seller_Hierarchy__c newSellerOther = td.newSellerHierarchy(LNAME+'Other');
        insert newSeller;
        insert newSellerOther;
        
        //create Assignee
        Assignee__c newAssignee = td.newAssignee(LNAME, UserInfo.getUserId(), newSeller.Id);
        Assignee__c newAssigneeOther = td.newAssignee(LNAME+'Other', UserInfo.getUserId(), newSellerOther.Id);
        insert newAssignee;
        insert newAssigneeOther; 
        
        Patient__c patient = td.newPatient();
        patient.Territory_ID__c = newSeller.Id;
        insert patient;       
        
        Lead newLead = td.newLead(LNAME);
        newLead.RecordTypeId = LeadManager.RECTYPE_NMD_PATIENTS;
        newLead.Status = LEAD_STATUS_TYPE;
        newLead.Territory_ID__c = newSeller.Id;
        newLead.Patient__c = patient.Id;
        insert newLead;
                
        //newLead.Territory_ID__c = newSeller.Id;
        //update newLead;
        
        newLead.Territory_ID__c = newSellerOther.Id;
        update newLead;     
        
        Id ownerId = [select ownerId from lead where Id = :newLead.Id].OwnerId;
        System.assertEquals(ownerId, UserInfo.getUserId());

    }
    
    static testMethod void testLeadTrigger() {
        Lead testLead = [select Id from lead limit 1];
        Seller_Hierarchy__c testSellerH = [select Id from Seller_Hierarchy__c limit 1];
        
        testLead.Territory_ID__c = testSellerH.Id;
        update testLead;
    }  

    static testMethod void test_RepCode() {

    	NMD_TestDataManager td = new NMD_TestDataManager();

		Lead newLead = td.newLead(LNAME);
		newLead.RecordTypeId = LeadManager.RECTYPE_NMD_PATIENTS;
		newLead.Status = LEAD_STATUS_TYPE;
		newLead.Rep_Code__c = 'TEST';
		insert newLead;

	} 

    static testMethod void test_UpdatePatientPhysician(){

        NMD_TestDataManager td = new NMD_TestDataManager();

        Account acct = td.newAccount();
        insert acct;

        Contact con = td.newContact(acct.Id);
        con.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        insert con;

        Lead testLead = [select Id, Patient__c from lead limit 1];
        testLead.Trialing_Physician__c = con.Id;
        update testLead;

        Patient__c pat = [SELECT Id, Physician_of_Record__c FROM Patient__c WHERE Id = :testLead.Patient__c limit 1];
        System.assertEquals(con.Id, pat.Physician_of_Record__c);

    }
    
}