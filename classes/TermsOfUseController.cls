public class TermsOfUseController{
    
    public Shared_community_Translation__c pageText { get; set; }
    
    public TermsOfUseController() {

        User objUser = [Select LanguageLocaleKey 
            From User 
            Where Id =: UserInfo.getUserId()];
                
        pageText = [SELECT Shared_Page__c, Shared_Language__c, Shared_Text__c
                                                    FROM Shared_Community_Translation__c
                                                    Where Shared_Page__c = 'Terms of Use'
                                                    AND Shared_Language__c =: objUser.LanguageLocaleKey
                                                    LIMIT 1];
    }
    public Shared_community_Translation__c getpageText(){
        return pageText;
    }
}