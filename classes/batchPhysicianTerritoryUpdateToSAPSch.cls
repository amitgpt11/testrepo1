/**
* Batch class, to batchPhysicianTerritoryUpdateToSAP class  
* @author   Amitabh - Accenture
* @date     26APR2016
*/
global class batchPhysicianTerritoryUpdateToSAPSch implements Schedulable{
    
    /**
    *  @desc    Schedulable method, executes the class instance
    */
    global void execute(SchedulableContext context) {
        integer batchSize = Integer.valueOf(50);
        batchPhysicianTerritoryUpdateToSAP b = new batchPhysicianTerritoryUpdateToSAP();
        database.executebatch(b,batchSize);

    }

}