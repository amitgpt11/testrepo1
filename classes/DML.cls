/**
* Wrapper class to automate the logging of DML statements (errors only)
*
* @Author Scott Will
* @Date 2015/09/15
*/
public class DML {

	//
	// Intended to replace raw insert/update/upsert/delete/undelete DML calls, so that error
	// logging can automatically occur.
	// 
	// insert records;   => DML.save(this, records); or DML.save('NameOfStaticClass', records);
	// update records;   => DML.save(this, records); or DML.save('NameOfStaticClass', records);
	// delete records;   => DML.remove(this, records); or DML.remove('NameOfStaticClass', records);
	// undelete records; => DML.restore(this, records); or DML.restore('NameOfStaticClass', records);
	//
	// Look at methods below for overloading options.
	//
	// NOTE: Upserts are not supported due to a system limitation, specifically that
	// generic SObjects cannot be used in an upsert call. as a workaround you can use the following:
	//
	// DML.evaluateResults(this, Database.upsert(records, false));
	//
	// or
	//
	// try {
	//     DML.evaluateResults(this, Database.upsert(records));
	// }
	// catch(System.DmlException de) {
	//     DML.logDmlException(this, de);
	// }
	//

	enum CallType { SAVE, REMOVE, RESTORE }

	final static Boolean DEFAULT_ALLORNONE = true;

	// whether or not the logs are immediately committed
	private static Boolean deferLogs = false;
	private static List<ApplicationLogWrapper> errorLogs = new List<ApplicationLogWrapper>{};

	// are there pending logs to be committed?
	public static Boolean hasLogs { get { return !errorLogs.isEmpty(); }}

	///////////////////////
	//// LOG DEFERMENT ////
	///////////////////////
	public static void deferLogs() {
		deferLogs(true);
	}
	public static void deferLogs(Boolean defer) {
		deferLogs = defer;
		// if the logs are being deferred from outside of a trigger context
		// then it is assumed that the calling context will also handle flushing,
		// so disable logging from within the trigger
		if (defer && !Trigger.isExecuting) {
			TriggerHandlerManager.ALLOW_LOGGING = false;
		}
	}

	public static void log(ApplicationLogWrapper log) {
		log(new List<ApplicationLogWrapper> { log });
	}

	public static void log(List<ApplicationLogWrapper> logs) {
		errorLogs.addAll(logs);
		if (!deferLogs) {
			flushLogs();
		}
	}

	public static void flushLogs() {
		flushLogs(deferLogs);
	}

	public static void flushLogs(Boolean deferNext) {
		deferLogs(deferNext);
		if (hasLogs) {
			Logger.logMessage(errorLogs);
			errorLogs.clear();
		}
		// if the logs are bing flushed from outside of a trigger context
		// then allow logging in triggers to resume afterward
		if (!deferNext && !Trigger.isExecuting) {
			TriggerHandlerManager.ALLOW_LOGGING = true;
		}
	}
	
	//////////////////////
	//// Save methods ////
	//////////////////////
	public static Database.SaveResult save(Object instance, SObject record) {
		return save(instance, record, DEFAULT_ALLORNONE);
	}

	public static Database.SaveResult save(Object instance, SObject record, Boolean allowException) {
		return save(instance, new List<SObject> { record }, allowException)[0];
	}

	public static List<Database.SaveResult> save(Object instance, List<SObject> records) {
		return save(instance, records, DEFAULT_ALLORNONE);
	}

	public static List<Database.SaveResult> save(Object instance, List<SObject> records, Boolean allOrNone) {
		return (List<Database.SaveResult>)call(CallType.SAVE, instance, records, allOrNone);
	}

	////////////////////////
	//// Remove methods ////
	////////////////////////
	public static Database.DeleteResult remove(Object instance, SObject record) {
		return remove(instance, record, DEFAULT_ALLORNONE);
	}

	public static Database.DeleteResult remove(Object instance, SObject record, Boolean allowException) {
		return remove(instance, new List<SObject> { record }, allowException)[0];
	}

	public static List<Database.DeleteResult> remove(Object instance, List<SObject> records) {
		return remove(instance, records, DEFAULT_ALLORNONE);
	}

	public static List<Database.DeleteResult> remove(Object instance, List<SObject> records, Boolean allOrNone) {
		return (List<Database.DeleteResult>)call(CallType.REMOVE, instance, records, allOrNone);
	}


	/////////////////////////
	//// Restore methods ////
	/////////////////////////
	public static Database.UndeleteResult restore(Object instance, SObject record) {
		return restore(instance, record, DEFAULT_ALLORNONE);
	}

	public static Database.UndeleteResult restore(Object instance, SObject record, Boolean allowException) {
		return restore(instance, new List<SObject> { record }, allowException)[0];
	}

	public static List<Database.UndeleteResult> restore(Object instance, List<SObject> records) {
		return restore(instance, records, DEFAULT_ALLORNONE);
	}

	public static List<Database.UndeleteResult> restore(Object instance, List<SObject> records, Boolean allOrNone) {
		return (List<Database.UndeleteResult>)call(CallType.RESTORE, instance, records, allOrNone);
	}

	///////////////////////////
	//// Result Evaluation ////
	///////////////////////////
	public static void evaluateResult(Object instance, Object result) {
		evaluateResults(instance, new List<Object> { result });
	}

	public static void evaluateResults(Object instance, List<Object> results) {

		String className = getClassName(instance);
		List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>{};

		for (Object result : results) {

			Database.Error errMsg;
			Id recId;

			// grab error details if one occurred.  unfortunately since each type of 
			// DML statement returns a different type of result they have to be created
			// separately.
			if (result instanceOf Database.DeleteResult) {
				Database.DeleteResult dr = (Database.DeleteResult)result;
				if (!dr.isSuccess()) {
					recId = dr.getId();
					errMsg = dr.getErrors()[0];
				}
			}
			else if (result instanceOf Database.SaveResult) {
				Database.SaveResult dr = (Database.SaveResult)result;
				if (!dr.isSuccess()) {
					recId = dr.getId();
					errMsg = dr.getErrors()[0];
				}
			}
			else if (result instanceOf Database.UndeleteResult) {
				Database.UndeleteResult dr = (Database.UndeleteResult)result;
				if (!dr.isSuccess()) {
					recId = dr.getId();
					errMsg = dr.getErrors()[0];
				}
			}
			else if (result instanceOf Database.UpsertResult) {
				Database.UpsertResult dr = (Database.UpsertResult)result;
				if (!dr.isSuccess()) {
					recId = dr.getId();
					errMsg = dr.getErrors()[0];
				}
			}

			if (errMsg != null) {
				// logger activities
				logs.add(new ApplicationLogWrapper(errMsg, recId, className));
			}

		}

		if (!logs.isEmpty()) {
			log(logs);
		}

	}

	public static void logDmlException(Object instance, System.DmlException de) {
		
		// retrieve the name of the caller
		String className = getClassName(instance);
		List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>();

		for (Integer i = 0; i < de.getNumDml(); i++) {
			String errorMessage = de.getDmlMessage(i);
			logs.add(new ApplicationLogWrapper('ERROR', className, '', errorMessage));
		}

		log(logs);

	}

	////////////////////////
	//// Database Call ////
	///////////////////////
	private static List<Object> call(CallType type, Object instance, List<SObject> records, Boolean allOrNone) {

		List<Object> results;

		if (records != null && records.size() > 0) {

			try {

				if (type == CallType.REMOVE) {
					results = Database.delete(records, allOrNone);
				}
				else if (type == CallType.RESTORE) {
					results = Database.undelete(records, allOrNone);
				}
				else if (type == CallType.SAVE) {
					if (records[0].Id == null) {
						results = Database.insert(records, allOrNone);
					}
					else {
						results = Database.update(records, allOrNone);
					}
				}

			}
			catch(System.DmlException de) {

				// log errors
				logDmlException(instance, de);

				// re-throw error for the caller to handle
				throw de;

			}

			// check results
			evaluateResults(instance, results);

		}
		
		return results;

	}

	private static String getClassName(Object instance) {
		// retrieve the name of the caller
		String className = String.valueOf(instance == null ? '' : instance);
		if (!(instance instanceOf String)) {
			className = className.split(':')[0];
		}
		return className;
	}

}