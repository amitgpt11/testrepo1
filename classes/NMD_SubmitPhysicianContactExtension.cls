/**
* NMD_SubmitPhysicianContactExtension.class
*
* @author   Salesforce services
* @date     2015-02-03
* @desc     Extension to SubmitPhysicianContact.page
*/
public with sharing class NMD_SubmitPhysicianContactExtension extends NMD_QuincyExtensionBase {

    final static String MSG_SUBMITTOCONTINUE = 'Click Submit to continue.';
    final static String MSG_FIELDSREQUIRED = 'The above fields are needed before the Physician can be submitted.';
    
    private Account acct = null;

    public NMD_SubmitPhysicianContactExtension(ApexPages.StandardController sc) {
        super(sc);

        Contact con = (Contact)this.sc.getRecord();
        //US2471 story change to add Classification , Type Fields to Account of the Contact in below SOQL 
        if(con.AccountId != null){
            acct = [
                select 
                    Id,
                    Name,
                    BillingStreet,
                    BillingCity,
                    BillingState,
                    BillingPostalCode,
                    BillingCountry,
                    Sold_to_Street__c,
                    Sold_to_City__c,
                    Sold_to_State__c,
                    Sold_to_Zip_Postal_Code__c,
                    Sold_to_Country__c,
                    Sold_to_Phone__c,
                    Sold_to_Fax__c,
                    NMD_Territory__c,
                    RecordTypeId,
                    Sent_to_Quincy__c,
                    Classification__c,
                    Type
                from Account
                where id = :con.AccountId
                LIMIT 1
            ];
        }

        //newInfoMessage(Label.NMD_Select_Accounts);
        if (validate()) {
            newInfoMessage(MSG_SUBMITTOCONTINUE);
        }
        else {
            newInfoMessage(MSG_FIELDSREQUIRED);
        }

    }

    /**
    * Submit Contact to Quincy.  Throw error if required fields are missing.
    *
    * @param    void
    */
    public override void submit() {



        //Verify required fields are populated and throw error if they are missing
        if (validate()) {
            Set<Id> acctIds = new Set<Id>();

            if(acct != null
                && acct.RecordTypeId == AccountManager.RECTYPE_PROSPECT
                && acct.Sent_to_Quincy__c == false
            ){
                acctIds.add(acct.Id);
            }

            //send templated email
            NMD_SendQuincyEmailManager emailManager = new NMD_SendQuincyEmailManager(this.sc.getId(), acctIds);

            isSubmitted = emailManager.sendQuincyEmail();

            if (isSubmitted) {
                newInfoMessage(Label.NMD_Report_Emailed);
            }
            else {
                String errorMessage = emailManager.lastErrorMessage + ' ' + Label.NMD_Please_Contact_Admin;
                newErrorMessage(errorMessage);
                logError('NMD_SubmitPhysicianContactExtension.submit', emailManager.lastErrorMessage);
            }

        }
        else {
            newInfoMessage(MSG_FIELDSREQUIRED);
        }

    }

    private Boolean validate() {

        Boolean success = true;
        Contact contact = (Contact)this.sc.getRecord();
        ////US2471 story change to add Classification , Type Fields to Contact required fields 
        
        if (String.isBlank(contact.MailingStreet)) {
            newErrorMessage(Schema.SObjectType.Contact.fields.MailingStreet.Label + ' is required.');
            success = false;
        }
        if (String.isBlank(contact.MailingCity)) {
            newErrorMessage(Schema.SObjectType.Contact.fields.MailingCity.Label + ' is required.');
            success = false;
        }
        if (String.isBlank(contact.MailingState)) {
            newErrorMessage(Schema.SObjectType.Contact.fields.MailingState.Label + ' is required.');
            success = false;
        }
        if (String.isBlank(contact.MailingPostalCode)) {
            newErrorMessage(Schema.SObjectType.Contact.fields.MailingPostalCode.Label + ' is required.');
            success = false;
        }
        if (String.isBlank(contact.MailingCountry)) {
            newErrorMessage(Schema.SObjectType.Contact.fields.MailingCountry.Label + ' is required.');
            success = false;
        }
        if (String.isBlank(contact.Phone)) {
            //newErrorMessage(Schema.SObjectType.Contact.fields.Phone.Label + ' is required.');
            // SObjectFieldDecribe giving back the wrong label
            newErrorMessage('Phone is required');
            success = false;
        }
        if (String.isBlank(contact.Fax)) {
            newErrorMessage('Fax is required');
            //newErrorMessage(Schema.SObjectType.Contact.fields.Fax.Label + ' is required.');
            success = false;
        }
        if (String.isBlank(contact.Academic_Title__c)) {
            newErrorMessage('Academic Title is required');
            //newErrorMessage(Schema.SObjectType.Contact.fields.Academic_Title__c.Label + ' is required.');
            success = false;
        }
        if (String.isBlank(contact.Specialty__c)) {
            newErrorMessage('Specialty is required');
            //newErrorMessage(Schema.SObjectType.Contact.fields.Specialty__c.Label + ' is required.');
            success = false;
        }               

        if(acct != null
            && acct.RecordTypeId == AccountManager.RECTYPE_PROSPECT
            && acct.Sent_to_Quincy__c == false
        ){
            if (String.isBlank(acct.Name)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.Name.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.BillingStreet)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.BillingStreet.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.BillingCity)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.BillingCity.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.BillingState)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.BillingState.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.BillingPostalCode)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.BillingPostalCode.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.BillingCountry)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.BillingCountry.Label + ' is required.');
                success = false;
            }

            if (String.isBlank(acct.Sold_to_Street__c)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.Sold_to_Street__c.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.Sold_to_City__c)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.Sold_to_City__c.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.Sold_to_State__c)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.Sold_to_State__c.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.Sold_to_Zip_Postal_Code__c)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.Sold_to_Zip_Postal_Code__c.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.Sold_to_Country__c)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.Sold_to_Country__c.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.Sold_to_Phone__c)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.Sold_to_Phone__c.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.Sold_to_Fax__c)) {
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.Sold_to_Fax__c.Label + ' is required.');
                success = false;
            }
            if (acct.NMD_Territory__c == null){
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.NMD_Territory__c.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.Type)){
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.Type.Label + ' is required.');
                success = false;
            }
            if (String.isBlank(acct.Classification__c)){
                newErrorMessage('Account: ' + Schema.SObjectType.Account.fields.Classification__c.Label + ' is required.');
                success = false;
            }

        }
       System.debug('Success:'+success);
        return success;

    }

}