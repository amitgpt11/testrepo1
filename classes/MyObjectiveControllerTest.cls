/**
 * Name : MyObjectiveControllerTest
 * Author : Deepak
 * Description : Test class used for testing the MyObjectiveController
 * Date : 4/11/16
 */
@isTest
private class MyObjectiveControllerTest { 
    /*
     *@name testSave_Scenario1()
     *@return void 
     *@description This method for save () In CLASS MyObjectiveController.cls
     */
        
        
        static testMethod void  testSave_Scenario1(){ 
        User stdUser = UtilForUnitTestDataSetup.getUser();
        
        My_Objectives__c  my_objectivescNovaTest = new My_Objectives__c();
         my_objectivescNovaTest.Name = 'test';
         insert my_objectivescNovaTest;
        
        ApexPages.currentPage().getParameters().put('id', my_objectivescNovaTest.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(my_objectivescNovaTest );
        MyObjectiveController myObjectiveControllerTest  =  new  MyObjectiveController(ctr);
        Test.startTest();
        Test.setCurrentPage(Page.MyObjectiveTerritoryPage);
        
        System.runAs(stdUser) {
            ctr.save();
            Test.stopTest();
            // System.assert(pagereferenceTest!=null);
        }
    }
    /*
     *@name testShowDetail_Scenario1()
     *@return void 
     *@description This method for showDetail () In CLASS MyObjectiveController.cls
     */
     static testMethod void  testShowDetail_Scenario1(){ 
        User stdUser = UtilForUnitTestDataSetup.getUser();
        My_Objectives__c  my_objectivescNovaTest   = new My_Objectives__c();
        ApexPages.StandardController ctr = new ApexPages.StandardController(my_objectivescNovaTest );
        MyObjectiveController myObjectiveControllerTest  =  new  MyObjectiveController(ctr);
        Test.startTest();
        Test.setCurrentPage(Page.MyObjectiveTerritoryPage);
        ApexPages.currentPage().getParameters().put('id', 'testId');
        System.runAs(stdUser) {
             myObjectiveControllerTest.showDetail();
            Test.stopTest();
           //  System.assert(pagereferenceTest!=null);
        }
    }
    /*
     *@name testSearch_Scenario1()
     *@return void 
     *@description This method for search () In CLASS MyObjectiveController.cls
     */
     static testMethod void  testSearch_Scenario1(){ 
        User stdUser = UtilForUnitTestDataSetup.getUser();
        My_Objectives__c  my_objectivescNovaTest   = new My_Objectives__c();
        ApexPages.StandardController ctr = new ApexPages.StandardController(my_objectivescNovaTest );
        MyObjectiveController myObjectiveControllerTest  =  new  MyObjectiveController(ctr);
        Test.startTest();
        Test.setCurrentPage(Page.MyObjectiveTerritoryPage);
        ApexPages.currentPage().getParameters().put('id', 'testId');
        System.runAs(stdUser) {
             myObjectiveControllerTest.search();
            Test.stopTest();
            // System.assert(pagereferenceTest!=null);
        }
    }
    /*
     *@name testReturntoParent_Scenario1()
     *@return void 
     *@description This method for ReturntoParent () In CLASS MyObjectiveController.cls
     */
     static testMethod void  testReturntoParent_Scenario1(){ 
        User stdUser = UtilForUnitTestDataSetup.getUser();
        My_Objectives__c  my_objectivescNovaTest   = new My_Objectives__c();
        ApexPages.StandardController ctr = new ApexPages.StandardController(my_objectivescNovaTest );
        MyObjectiveController myObjectiveControllerTest  =  new  MyObjectiveController(ctr);
        Test.startTest();
        Test.setCurrentPage(Page.MyObjectiveTerritoryPage);
        ApexPages.currentPage().getParameters().put('id', 'testId');
        System.runAs(stdUser) {
            PageReference pagereferenceTest = myObjectiveControllerTest.ReturntoParent();
            Test.stopTest();
            // System.assert(pagereferenceTest!=null);
        }
    }
    /*
     *@name testGetFormTag_Scenario1()
     *@return void 
     *@description This method for getFormTag () In CLASS MyObjectiveController.cls
     */
     static testMethod void  testGetFormTag_Scenario1(){ 
        User stdUser = UtilForUnitTestDataSetup.getUser();
        My_Objectives__c  my_objectivescNovaTest   = new My_Objectives__c();
        ApexPages.StandardController ctr = new ApexPages.StandardController(my_objectivescNovaTest );
        MyObjectiveController myObjectiveControllerTest  =  new  MyObjectiveController(ctr);
        Test.startTest();
        Test.setCurrentPage(Page.MyObjectiveTerritoryPage);
        ApexPages.currentPage().getParameters().put('id', 'testId');
        System.runAs(stdUser) {
            String stringTest = myObjectiveControllerTest.getFormTag();
            Test.stopTest();
            // System.assert(stringTest!=null);
        }
    }
    /*
     *@name testGetTextBox_Scenario1()
     *@return void 
     *@description This method for getTextBox () In CLASS MyObjectiveController.cls
     */
     static testMethod void  testGetTextBox_Scenario1(){ 
        User stdUser = UtilForUnitTestDataSetup.getUser();
        My_Objectives__c  my_objectivescNovaTest   = new My_Objectives__c();
        ApexPages.StandardController ctr = new ApexPages.StandardController(my_objectivescNovaTest );
        MyObjectiveController myObjectiveControllerTest  =  new  MyObjectiveController(ctr);
        Test.startTest();
        Test.setCurrentPage(Page.MyObjectiveTerritoryPage);
        ApexPages.currentPage().getParameters().put('id', 'testId');
        System.runAs(stdUser) {
            String stringTest = myObjectiveControllerTest.getTextBox();
            Test.stopTest();
           //  System.assert(stringTest!=null);
        }
    }
}