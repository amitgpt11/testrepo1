/*
@CreatedDate     16JUNE2016
@author          Mike Lankfer
@Description     The trigger handler for the Product2 standard object.  This class will call all the methods that should fire before and/or after create/update/delete/undelete DML operations.
@Requirement Id  User Story : DRAFT US2511
*/
public class ProductTriggerHandler extends TriggerHandler {
    
    private final OpportunityProductCategoryUpdate optyCategoryUpdate = new OpportunityProductCategoryUpdate();
    
    
/*
@CreatedDate     15JUNE2016
@author          Mike Lankfer
@Description     Executes bulk actions before Product2 DML
@Requirement Id  User Story : DRAFT US2511
*/       
    public override void bulkBefore(){
        
        if(!Trigger.isDelete){
        List<Product2> products = (List<Product2>) Trigger.new;
        optyCategoryUpdate.updateOptyLineItemProductCategory(products);
        } 
        
    }
}