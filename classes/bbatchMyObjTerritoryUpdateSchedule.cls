/**
* Batch class, Change Territory on My Objective records based on Territory change on Account and User
* @author   Mayuri - Accenture
* @date     26APR2016
*/
global class bbatchMyObjTerritoryUpdateSchedule implements Schedulable{
    
    /**
    *  @desc    Schedulable method, executes the class instance
    */
    global void execute(SchedulableContext context) {
        integer batchSize = Integer.valueOf(system.Label.ETM_MyObjTerritory_Update_Batch_Size);
        batchMyObjTerritoryUpdate b = new batchMyObjTerritoryUpdate();
        database.executebatch(b,batchSize);

    }

}