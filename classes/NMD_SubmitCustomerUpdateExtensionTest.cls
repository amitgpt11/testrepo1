/**
* Test class for the page/class SubmitCustomerUpdate
*
* @author   Salesforce services
* @date     2015-02-12
*/
@isTest(SeeAllData=false)
public with sharing class NMD_SubmitCustomerUpdateExtensionTest {
	
	static final String PROSPECTACCOUNT = 'ProspectAccount';

	@testSetup
	static void setup() {
		//setup test data
		NMD_TestDataManager testData = new NMD_TestDataManager();
		Account acct = testData.newAccount(PROSPECTACCOUNT);
		acct.RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
		insert acct;

		//add quincy contact
		testData.setupQuincyContact();
	}

	static testMethod void test_SubmitCustomerUpdate() {

		//grab data, need to build soql string so fieldset gets added
		String acctName = PROSPECTACCOUNT;
		String acctSoql = 'select Id';
		Schema.FieldSet fieldset = Schema.SObjectType.Account.fieldSets.getMap().get(NMD_SubmitCustomerUpdateExtension.QUINCY_FIELDSET);
		for (Schema.FieldSetMember field : fieldset.getFields()) {
			acctSoql += ',' + field.getFieldPath();
		}
		acctSoql += ' from Account where Name = :acctName';
		Account acct = (Account)Database.query(acctSoql);

		//setup page
		PageReference pr = Page.SubmitCustomerUpdate;
		pr.getParameters().put('id', acct.Id);
		Test.setCurrentPage(pr);
		NMD_SubmitCustomerUpdateExtension extension = new NMD_SubmitCustomerUpdateExtension(
			new ApexPages.StandardController(acct)
		);

		//create a dummy attachment
		extension.attchbody = Blob.valueOf('filebody');
		extension.attchname = 'filename';

		extension.submit();

		//assert the email was submitted
		System.assert(extension.isSubmitted);

	}

	static testMethod void test_SubmitCustomerUpdate_NoAttachment() {

		//grab data, need to build soql string so fieldset gets added
		String acctName = PROSPECTACCOUNT;
		String acctSoql = 'select Id';
		Schema.FieldSet fieldset = Schema.SObjectType.Account.fieldSets.getMap().get(NMD_SubmitCustomerUpdateExtension.QUINCY_FIELDSET);
		for (Schema.FieldSetMember field : fieldset.getFields()) {
			acctSoql += ',' + field.getFieldPath();
		}
		acctSoql += ' from Account where Name = :acctName';
		Account acct = (Account)Database.query(acctSoql);

		//setup page
		PageReference pr = Page.SubmitCustomerUpdate;
		pr.getParameters().put('id', acct.Id);
		Test.setCurrentPage(pr);
		NMD_SubmitCustomerUpdateExtension extension = new NMD_SubmitCustomerUpdateExtension(
			new ApexPages.StandardController(acct)
		);

		//update a field
		acct.put((fieldset.getFields()[0]).getFieldPath(), 'update');

		extension.submit();

		//assert the correct error was thrown
		List<ApexPages.Message> msgs = ApexPages.getMessages();
		Boolean pass = false;
		for (ApexPages.Message msg : msgs) {
		    pass = pass || msg.getDetail().contains(Label.NMD_Attachment_Required);
		}
		System.assert(pass);

	}

}