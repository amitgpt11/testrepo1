/**
* Manager class for the Attachments object
*
* @Author salesforce Services
* @Date 03/20/2015
*/
public with sharing class AttachmentManager {
	
	/**
    * Used with the Patient Attachments, for images >1MB in size.  They are broken into file
    * slices and uploaded separately.
    * 
    * @param void
    */
	public static Map<String,Object> uploadSlice(Id parentId, String order, String slice) {
		
		Map<String,Object> result = new Map<String,Object>{
			'is_success' => false,
			'message' => 'Unknown'
		};

		try {
			// just store the substring of the base64 image as-is
			Attachment attach = new Attachment(
				Name = EncodingUtil.convertToHex(Crypto.generateAesKey(128)), //random name
				ParentId = parentId,
				Description = order,
				Body = Blob.valueOf(slice)
			);
			insert attach;

			result.putAll(new Map<String,Object> {
				'is_success' => true,
				'message' => attach.Id
			});
		}
		catch (System.DmlException de) {

			result.putAll(new Map<String,Object> {
				'is_success' => false,
				'message' => de.getDmlMessage(0)
			});

		}

		return result;

	}

	/**
    * Used with the Patient Attachments, for images >1MB in size.  Takes a list of slices, and
    * concatentates them back together into a single file.
    * 
    * @param void
    */
	public static Map<String,Object> joinSlices(String existingId, String parentId, String filename, String classification, List<Id> sliceIds) {

		Map<String,Object> result = new Map<String,Object>{
			'is_success' => false,
			'message' => 'Unknown'
		};

		List<Attachment> slices = new List<Attachment>{};

		String massive = '';

		for (Attachment slice : [
			select Id, Body
			from Attachment
			where Id in :sliceIds
			order by Description
		]) {

			// since each slice is just a substring of the full base64 encoded image,
			// they can just be concatenated
			massive += slice.Body.toString();
			slices.add(new Attachment(Id = slice.Id));

		}
		
		try {

			// we only need the resulting record id, and don't want blob in memory
			Database.SaveResult sr = Database.insert(new Attachment(
				ParentId = String.IsBlank(parentId) ? null : parentId,
				Name = filename,
				Description = classification,
				// wait until last second to decode, pass it striaght in to keep it out of the heap (amap)
				Body = EncodingUtil.base64Decode(massive) 
			));

			// release from heap asap
			massive = null;

			if (existingId != null) {
				//remove the last image
				delete [select Id from Attachment where Id = :existingId];
			}

			result.putAll(new Map<String,Object> {
				'is_success' => true,
				'message' => sr.getId(),
				'parentId' => parentId
			});

		}
		catch (System.DmlException de) {

			result.putAll(new Map<String,Object> {
				'is_success' => false,
				'message' => de.getDmlMessage(0)
			});

		}

		// delete the slices either way, as if there was an issue the user must begin all 
		// over again from the beginning and these slices would never be touched again
		delete slices;

		return result;

	}

}