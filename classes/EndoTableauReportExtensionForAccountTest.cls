/**
 * Name : BlueprintResolvePricingControllerTest
 * Author : Accenture - Sipra
 * Description : Test class used for testing the EndoTableauReportExtension
 * Date : 25June2016
 */
@isTest
private class EndoTableauReportExtensionForAccountTest{ 
    static testMethod void testMethod1(){      
      test.startTest();
      string str = 'test';
          Account acct1 = new Account(Name = 'ACCT1NAME',Account_Number__c='897623454',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId());  
          insert acct1;
          system.assertEquals(acct1.name,'ACCT1NAME');
          
          ApexPages.currentPage().getParameters().put('id', acct1.Id);
          ApexPages.StandardController ctr = new ApexPages.StandardController(acct1);
          EndoTableauReportExtensionForAccount controller =  new EndoTableauReportExtensionForAccount(ctr);        
          Test.setCurrentPage(Page.Tableau_Report_Sales_Account_Activity);        
          controller.getTablueLinkInfoforaccount();
      test.stopTest();
    }   
    
    
    static testMethod void testMethod2(){      
      test.startTest();
      string str = 'test';
          Account acct2 = new Account(Name = 'ACCT1NAME1',Account_Number__c='',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId());  
          insert acct2; 
          system.assertEquals(acct2.name,'ACCT1NAME1');
          
          ApexPages.currentPage().getParameters().put('id', acct2.Id);
          ApexPages.StandardController ctr = new ApexPages.StandardController(acct2);
          EndoTableauReportExtensionForAccount controller =  new  EndoTableauReportExtensionForAccount(ctr);        
          Test.setCurrentPage(Page.Tableau_Report_Sales_Account_Activity);        
          controller.getTablueLinkInfoforaccount();
      test.stopTest();
    }     

}