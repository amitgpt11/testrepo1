/**
* Test class for the PhysiscianDashboard vf extension
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class NMD_PhysicianDashboardExtensionTest {

	final static String REPORTID = '0123456789abcde';

	@testSetup
	static void setup() {

		PhysicianDashboard__c dashboard = new PhysicianDashboard__c(
			Name = 'Name',
			Active__c = true,
			Order__c = 0,
			Report_Id__c = REPORTID
		);
		insert dashboard;

	}
	
	static testMethod void test_PhysicianPracticeSummaryDashboard() {

		PageReference pr = Page.PhysicianPracticeSummaryDashboard;
		Test.setCurrentPage(pr);

		NMD_PhysicianDashboardExtension ext = new NMD_PhysicianDashboardExtension(
			new ApexPages.StandardController(new Contact())
		);

		System.assertEquals(1, ext.reportIds.size());
		System.assertEquals(REPORTID, ext.reportIds[0]);

	}

}