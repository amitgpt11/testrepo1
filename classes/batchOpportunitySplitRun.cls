global class batchOpportunitySplitRun implements Database.Batchable<sObject>,Database.Stateful,Database.AllowsCallouts {
        
    List<Id> Allscope = new List<Id>();      
    global Database.QueryLocator start(Database.BatchableContext BC) {
        DateTime dT = System.now();
        Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
        String query = 'SELECT Id, (select id,name from Clinical_Data_Summaries__r),AccountId,Entry_Level__c,IPG_Side__c,Sweet_Spot__c,Pre_Cert_Authorization_Begin_Date__c,Pre_Cert_Authorization_End_Date__c,Sweet_Spot_Other__c, RecordTypeId, Name, StageName, Amount, Description, Probability, TotalOpportunityQuantity, CloseDate, Type, LeadSource, IsClosed, IsWon, OwnerId, Pricebook2Id, CampaignId, HasOpportunityLineItem, Territory2Id, CreatedById, Competitor_Name__c, Divisions__c, SAP_Account_Number__c, Actual_Procedure_Date__c, Actual_Trial_Date__c, Attending_Rep__c, Activities_limited_by_Pain_Post_Trial__c, Activities_limited_by_Pain_Pre_Implant__c, Activities_limited_by_Pain_Pre_Trial__c, CARE_Card_Physician__c, Campaign__c, Compliance_Message__c, Contact__c, Conversion_Type__c, Date_of_Last_Visit__c, Days_Since_First_Contact__c, Days_Since_Last_Update__c, Days_Since_Trial__c, Days_as_Candidate__c, Days_in_Scheduled_Procedure__c, Days_in_Procedure__c, Days_in_Scheduled_Trial__c, Days_in_Trial__c, Failed_Date__c, Final_PO_Date__c, First_Contacted_Date__c, First_Use__c, I_Acknowledge__c, Inconclusive_Date__c, OMG_Date__c, Opportunity_Type__c, Originating_Rep__c, PO__c, Pain_Area__c, Pain_Relief__c, Patient_Last_First__c, Patient__c, Physician_of_Record__c, Post_Op_Notes__c, Pre_Trial_VAS_Score__c, Primary_Insurance__c, Primary_Pain__c, Private__c, Procedure_Account__c, Procedure_Delay_Reason__c, Procedure_Consult_Date__c, Procedure_Distal_Lead_Placement__c, Procedure_Physician__c, Procedure_Placement_Other__c, Procedure_Proximal_Lead_Placement__c, Procedure_Stage_Time_Stamp__c, Procedure_VAS_Score__c, Reason_Lost__c, Referral_MD__c, Scheduled_Procedure_Date_Time__c, Scheduled_Trial_Date_Time__c, Scheduled_Procedure_Date__c, Scheduled_Trial_Date__c, Secondary_Insurance__c, Secondary_Pain__c, Territory_ID__c, Three_Day_Procedure__c, Three_Day_Trial__c, Trial_Lead_Placement_Distal__c, Trial_Lead_Placement_Other__c, Trial_Lead_Placement_Proximal__c, Trial_PO__c, Trial_Pre_Cert_Authorization_Begin__c, Trial_Pre_Cert_Authorization_End__c, Trial_PO_Time_Stamp__c, Total_Opportunity_Time__c, Trial_Entry_Level__c, Product_System__c, Trial_Status_Date__c, Trial_Status__c, Trial_Sweet_Spot_Other__c, Trial_Sweet_Spot__c, Trial_VAS_Score__c, Trialing_Account__c, Trialing_Physician__c, Primary_Waveform__c, Secondary_Waveform__c, Tertiary_Waveform__c, Trial_Revenue__c, Procedure_Revenue__c, MDT__c, STJ__c, Trial_Commitment__c, Clinical__c, Competitor_Product__c, Lead_Pull_Date__c, Patient_SAP_ID__c, Procedure_Account_SAP_ID__c, Procedure_Order_Number__c, Procedure_Physician_SAP_ID__c, Procedure_Type__c, Secondary_Source__c, Post_Op_Incision_Check_Date__c, Reprogramming_Follow_Up_Date__c, Trial_Attending_Rep__c, Trial_Order_Number__c, Trialing_Account_SAP_ID__c, Trialing_Physician_SAP_ID__c, ASP_Trial_Amount__c, Opportunity_Split__c, Lead_Educated_Date__c, Schedule_Trial_Stage_Time_Stamp__c, ETS_Side__c, PO_Time_Stamp__c, No_Longer_Opportunity__c, CreatedDate, Insurance_Approval_Submit_Date__c,Insurance_Status__c, Psych_Evaluation_Status__c,Psych_Evaluation_Date__c, Medical_Clearance_Required__c, Second_Opinion__c, Sleep_Side__c, Entry_Level_Comments__c, Lead_Created_Date__c FROM Opportunity where Opportunity_Split__c=false  and StageName not IN(\'Complete (Closed Won)\',\'Lost (Closed Lost)\',\'Cancelled (Lost)\') and recordtype.developerName  IN (\'NMD_SCS_Trial_Implant\') limit 13000';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        try{
            if(!scope.isEmpty()){
                for(Opportunity o : scope){
                    //Allscope.add(o.Id);
                    System.debug('o.Id:'+o.Id);
                }
                //if(!scope.isEmpty()){
                    new OpprtunitySplitScript(scope);
                //}
            }
        }Catch(exception e){
             System.debug('Error in Executing the Opportunity Split Script :'+e);
        }
    }   
    
    global void finish(Database.BatchableContext BC) {
      if(BC!=null){   
      AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id =:BC.getJobId()];
      System.debug('Id:'+a.id+','+a.status+','+a.NumberOfErrors+','+a.JobItemsProcessed+','+a.TotalJobItems);
      }  
        
    }
}