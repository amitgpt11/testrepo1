/**
* Manager/Utility class for the Assignee custom object
*
* @Author salesforce Services
* @Date 04/07/2015
*/
public with sharing class AssigneeManager {

	@testVisible
	final static String TM1 = 'TM1';
	@testVisible
	final static String TM2 = 'TM2';

	private Map<Id,Seller_Hierarchy__c> parentSellerHierarchies = new Map<Id,Seller_Hierarchy__c>{};

	/**
    * If the Rep Designation is TM1 then it should be the new Designated Assignee
    * 
    * @param oldAssignee	Old Assignee
    * @param newAssignee	New Assignee
    */
	public void updateParentsCurrentTMAssignees(Assignee__c oldAssignee, Assignee__c newAssignee) {

		if (oldAssignee.Assignee__c != newAssignee.Assignee__c
		 || oldAssignee.Territory__c != newAssignee.Territory__c
	     || oldAssignee.Rep_Designation__c != newAssignee.Rep_Designation__c
		) {
			updateParentsCurrentTMAssignees(newAssignee, false);
		}

	}
	
	/**
    * If the Rep Designation is TM1 then it should be the new Designated Assignee
    * 
    * @param oldAssignee	Old Assignee
    * @param newAssignee	New Assignee
    */
	public void updateParentsCurrentTMAssignees(Assignee__c assignee, Boolean isDelete) {

		if (assignee.Assignee__c != null) {

			Seller_Hierarchy__c parent;

			if (!isDelete) {
				if (this.parentSellerHierarchies.containsKey(assignee.Territory__c)) {
					parent = this.parentSellerHierarchies.get(assignee.Territory__c);
				}
				else {
					parent = new Seller_Hierarchy__c(Id = assignee.Territory__c);
				}

				if (assignee.Rep_Designation__c == TM1) {
					parent.Current_TM1_Assignee__c = assignee.Assignee__c;
				}
				else if (assignee.Rep_Designation__c == TM2) {
					parent.Current_TM2_Assignee__c = assignee.Assignee__c;
				}
			}

			this.parentSellerHierarchies.put(assignee.Territory__c, parent);

		}

	}

	/**
    * Updates in bulk any parent Seller Hierarchy's Current TM1 Assignee
    * 
    * @param void
    */
	public void commitParentSellerHierarchies() {

		if (!this.parentSellerHierarchies.isEmpty()) {

			Set<Id> parentWithDeletionIds = new Set<Id>{};
			for (Id parentId : this.parentSellerHierarchies.keySet()) {
				if (this.parentSellerHierarchies.get(parentId) == null) {
					this.parentSellerHierarchies.put(parentId, new Seller_Hierarchy__c(
						Id = parentId,
						Current_TM1_Assignee__c = null,
						Current_TM2_Assignee__c = null
					));
					parentWithDeletionIds.add(parentId);
				}
			}

			if (!parentWithDeletionIds.isEmpty()) {
				for (Assignee__c sibling : [
					select 
						Assignee__c,
						Territory__c,
						Rep_Designation__c
					from Assignee__c
					where Territory__c in :parentWithDeletionIds
					and Rep_Designation__c in (:TM1, :TM2)
					order by CreatedDate asc
				]) {

					if (this.parentSellerHierarchies.containsKey(sibling.Territory__c)) {

						Seller_Hierarchy__c parent = this.parentSellerHierarchies.get(sibling.Territory__c);
					
						if (sibling.Rep_Designation__c == TM1) {
							parent.Current_TM1_Assignee__c = sibling.Assignee__c;
						}
						else if (sibling.Rep_Designation__c == TM2) {
							parent.Current_TM2_Assignee__c = sibling.Assignee__c;
						}

						this.parentSellerHierarchies.put(sibling.Territory__c, parent);

					}

				}
			}

			//update this.parentSellerHierarchies.values();
			DML.save(this, this.parentSellerHierarchies.values(), false);

		}

	}

}