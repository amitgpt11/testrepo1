/**
* Class for implementing TriggerHandler methods for Opportunity triggers.       
*
* @Author salesforce Services
* @Date 02/10/2014
*/

public virtual with sharing class TriggerHandler implements ITriggerHandler {
    
    /**
    * This method is called prior to execution of a BEFORE trigger. 
    * Use this to cache any data required into maps prior to execution of the trigger.
    * 
    * 
    * @param void
    */
    public virtual void bulkBefore(){}
    
    /**
    * This method is called prior to execution of an AFTER trigger. 
    * Use this to cache any data required into maps prior to execution of the trigger.                                                      
    * 
    * 
    * @param void
    */
    
    public virtual void bulkAfter(){}
   
    /**
    * Purpose: This method is called iteratively for each record to be inserted during 
    * a BEFORE trigger. Never execute any SOQL/SOSL operations in iterative method.
    * 
    * 
    * @param so - sObject
    */
   
    public virtual void beforeInsert(SObject so){}
    
    /**
    * This method is called iteratively for each record to be updated during 
    * a BEFORE trigger. Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param oldSo - SObject
    * @param so - SObject                                                          
    */
   
    public virtual void beforeUpdate(SObject oldSo, SObject so){}
    
    /**
    * This method is called iteratively for each record to be deleted during 
    * a BEFORE trigger.  Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param so - SObject
    *                                                         
    */
    
    public virtual void beforeDelete(SObject so){}
    
    /**
    *
    * This method is called iteratively for each record inserted during an AFTER
    * trigger. Always put field validation in the 'After' methods in case another trigger
    * has modified any values. The record is 'read only' by this point.  
    * Never execute any SOQL/SOSL operations in iterative method.
    *
    * @param so - SObject
    */
    
    public virtual void afterInsert(SObject so){}
 
    /**
    *
    * This method is called iteratively for each record updated during an 
    * AFTER trigger.  Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param oldSo - SObject
    * @param so - SObject
    */
    

   
    public virtual void afterUpdate(SObject oldSo, SObject so){}
    
    /**
    * This method is called iteratively for each record deleted during an 
    * AFTER trigger.  Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param so - SObject
    */ 
     
    public virtual void afterDelete(SObject so){}
    
    /**
    *
    * This method is called once any record is Undeleted.                                                 
    *
    * @param so - SObject 
    *
    */
    
    public virtual void afterUndelete(SObject so){}
    
    /**
    * This method is called once all records have been processed by the trigger.
    * Use this method to accomplish any final operations such as creation or updates of other records.   
    *
    *                                           
    */
    
    public virtual void andFinally(){}
    
}