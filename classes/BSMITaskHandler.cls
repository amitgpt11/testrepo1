public class BSMITaskHandler {
    
    private static Id RecordTypeId;     
    private static string strTemplateName = 'Community_User_Contact_Us_Submitted';                
    private static List<Task> lstTasks;
    private static EmailTemplate templateId;
    @TestVisible private static List<Messaging.SingleEmailMessage> lstMails;
    
    public static void emailAlert_afterInsert(List<Task> lstTasksInserted){
        
        RecordTypeId = [SELECT Id,SobjectType,Name 
                FROM RecordType 
                WHERE DeveloperName ='Shared_Community_Task' 
                AND SobjectType ='Task' 
                LIMIT 1].Id;  
                
        templateId = [Select id, Subject, HtmlValue, Body
                        FROM EmailTemplate 
                        WHERE developername=: strTemplateName];  
        
        lstMails = new List<Messaging.SingleEmailMessage>();
        
        for(Task objTask : lstTasksInserted){           
         
            sendEmailAlert_On_CommunityContactUsSubmit(objTask);
            createTask_On_CommunityContactUsSubmit(objTask);         
        }
        
        system.debug('lstMails================='+lstMails);
        Messaging.sendEmail(lstMails,false);
    }
    
    //BSMI-373
    private static void sendEmailAlert_On_CommunityContactUsSubmit(Task objTask){

        system.debug('UserInfo.getUserType()====='+UserInfo.getUserType());                            
        
        // || UserInfo.getUserType().equalsIgnoreCase('Customer Portal User') ){
       IF( UserInfo.getUserType().equalsIgnoreCase('PowerCustomerSuccess') && objTask.recordTypeId == RecordTypeId){ 
          
            sendEmail(objTask);
       }
    }
    
    
    //BSMI-374
    private static void createTask_On_CommunityContactUsSubmit(Task objTask){
    
        String strMessage = objTask.Message__c;
        if(String.isNotBlank(strMessage)){
            if(strMessage.contains('Division') && 
                strMessage.contains('Name') &&  
                strMessage.contains('Email') &&
                strMessage.contains('Company') &&
                strMessage.contains('City') &&
                strMessage.contains('Subject') && 
                strMessage.contains('Message') &&
                objTask.recordTypeId == RecordTypeId){
                    
                    sendEmail(objTask);
                }
        }    
    }
    
    
    private static void sendEmail(Task objTask){    
        
        String strRequesterName = [SELECT Name 
                                    FROM Contact 
                                    WHERE id =: objTask.WhoId].Name;
    
        Messaging.SingleEmailMessage objMail = new Messaging.SingleEmailMessage();        
        
        objMail.setTemplateID(templateId.Id); 
        objMail.setTargetObjectId(objTask.OwnerId); //set as owner id
        objMail.setSaveAsActivity(false);
        objMail.setSubject('Account Request Submitted for '+strRequesterName);
        objMail.setSenderDisplayName('Salesforce Support');
        
        String strHtmlValue = templateId.htmlValue;
        system.debug('templateId.htmlValue============'+templateId.htmlValue);
        
        strHtmlValue = strHtmlValue.replace('{!Receiving_User.FirstName}',[SELECT Name 
                                                                            FROM User
                                                                            WHERE id =: objTask.OwnerId].Name);
                                                                            
        strHtmlValue = strHtmlValue.replace('{!Task.Link}', Boston_Scientific_Config__c.getInstance('Default').Boston_Document_Content_Base_Url__c +'/'+ objTask.Id);
        
        strHtmlValue = strHtmlValue.replace('{!User.Contact}',strRequesterName);
        
        strHtmlValue = strHtmlValue.replace('{!Task.DueDate}', string.isBlank(String.valueOf(objTask.ActivityDate))?'': String.valueOf(objTask.ActivityDate) );
        strHtmlValue = strHtmlValue.replace('{!Task.Subject}', string.isBlank(objTask.Subject)?'': objTask.Subject);
        strHtmlValue = strHtmlValue.replace('{!Task.Message__c}',objTask.Message__c);                      

        strHtmlValue = strHtmlValue.replace('{!Organization.Name}',[SELECT Name 
                                                                    FROM Organization].Name);
                                                                    
        strHtmlValue = strHtmlValue.replace(']]>','');
                                                                    
        objMail.setHtmlBody(strHtmlValue);
        lstMails.add(objMail);
    }
}