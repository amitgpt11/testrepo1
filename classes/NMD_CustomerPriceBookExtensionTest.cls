@IsTest(SeeAllData=false)
private class NMD_CustomerPriceBookExtensionTest {
	
	static final String ACCTNAME = 'Acct Name';

	@TestSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		Account acct = td.createConsignmentAccount();
		acct.Name = ACCTNAME;
		insert acct;

		Product2 prod = td.newProduct();
		prod.Model_Number__c = 'test';
		prod.Family = 'TestFamily';
    	insert prod;

		Pricebook2 pricebook = td.newPricebook('NMD');
		pricebook.Account__c = acct.Id;
		pricebook.Division__c = 'NM';
		insert pricebook;

		PricebookEntry pbe0 = td.newPricebookEntry(prod.Id);
		PricebookEntry pbe1 = td.newPricebookEntry(pricebook.Id, prod.Id);
		insert new List<PricebookEntry> { pbe0, pbe1 };

	}

	static testMethod void test_CustomerPriceBook() {

		Account acct = [select Id, OwnerId from Account where Name = :ACCTNAME];

		PageReference pr = Page.CustomerPriceBook;
		pr.getParameters().put('id', acct.Id);
		Test.setCurrentPage(pr);

		NMD_CustomerPriceBookExtension ext = new NMD_CustomerPriceBookExtension(new ApexPages.StandardController(acct));

		List<NMD_PricebookPair> pbes = ext.products;
		System.assert(!pbes.isEmpty());

		//test filtering
		ext.productFamilyFilter = 'TestFamily';
		ext.updateProductFilter();
		List<NMD_PricebookPair> pbes2 = ext.products;
		System.assert(!pbes2.isEmpty());

		//test searching
		ext.productFamilyFilter = '';
		ext.searchString = 'te';
		ext.updateProductFilter();
		List<NMD_PricebookPair> pbes3 = ext.products;


		ext.getProductFamilies();
		ext.nextPage();
		ext.prevPage();
		ext.lastPage();
		ext.firstPage();

	}

}