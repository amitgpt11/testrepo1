/**
* Custom extension to handle submitting Prospect Patients to the external "Quincy"
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public with sharing class NMD_SubmitProspectPatientExtension extends NMD_ExtensionBase {

    @TestVisible
    final static String MSG_BADRECTYPE = 'Incorrect RecordType';
    @TestVisible
    final static String MSG_CONTINUE = 'Click Submit to continue.';
    @TestVisible
    final static String MSG_HASERRORS = 'Please correct the listed errors before submitting the Patient.';
    @TestVisible
    final static String MSG_SUBMITTED = 'The Prospect has been submitted to BSN.';
    
    final ApexPages.StandardController sc;

    public Boolean canSubmit { get; private set; }
    public Boolean isSubmitted { get; private set; }
    public Boolean isSalesforce1 { get; private set; }

    public NMD_SubmitProspectPatientExtension(ApexPages.StandardController sc) {

        this.sc = sc;
        this.isSubmitted = false;
        this.isSalesforce1 = ApexPages.currentPage().getParameters().get('isdtp') == 'p1';
        this.canSubmit = validate();

        newInfoMessage(this.canSubmit
            ? MSG_CONTINUE
            : MSG_HASERRORS
        );

    }

    public void submit() {

        if (validate()) {

            try {
                // setting BSN_Update__c to 'Request SAP ID' on Prospects will initiate a worklow/outbound msg
                update new Patient__c(
                    Id = this.sc.getId(),
                    BSN_Update__c = 'Request SAP ID'
                );
                this.isSubmitted = true;
                newInfoMessage(MSG_SUBMITTED);
            }
            catch (System.DmlException de) {
                newErrorMessage(de.getDmlMessage(0));
            }

        }
        else {
            newInfoMessage(MSG_HASERRORS);
        }

    }

    private Boolean validate() {

        Patient__c patient = (Patient__c)this.sc.getRecord();
        Boolean success = true;

        // validations
        if (patient.RecordTypeId != PatientManager.RECTYPE_PROSPECT) {
            newErrorMessage(MSG_BADRECTYPE);
            success = false;
        }
        else {
            if (String.isBlank(patient.Address_1__c)) {
                newErrorMessage(getRequiredString(Schema.SObjectType.Patient__c.fields.Address_1__c));
                success = false;
            }
            if (String.isBlank(patient.City__c)) {
                newErrorMessage(getRequiredString(Schema.SObjectType.Patient__c.fields.City__c));
                success = false;
            }
            /*if (String.isBlank(patient.County__c)) {
                newErrorMessage(getRequiredString(Schema.SObjectType.Patient__c.fields.County__c));
                success = false;
            }*/
            if (String.isBlank(patient.Country__c)) {
                newErrorMessage(getRequiredString(Schema.SObjectType.Patient__c.fields.Country__c));
                success = false;
            }
            if (patient.Patient_Date_of_Birth__c == null) {
                newErrorMessage(getRequiredString(Schema.SObjectType.Patient__c.fields.Patient_Date_of_Birth__c));
                success = false;
            }
            if (String.isBlank(patient.Patient_First_Name__c)) {
                newErrorMessage(getRequiredString(Schema.SObjectType.Patient__c.fields.Patient_First_Name__c));
                success = false;
            }
            if (String.isBlank(patient.Patient_Gender__c)) {
                newErrorMessage(getRequiredString(Schema.SObjectType.Patient__c.fields.Patient_Gender__c));
                success = false;
            }
            if (String.isBlank(patient.Patient_Last_Name__c)) {
                newErrorMessage(getRequiredString(Schema.SObjectType.Patient__c.fields.Patient_Last_Name__c));
                success = false;
            }
            if (String.isBlank(patient.Patient_Phone_Number__c)
                && String.isBlank(patient.Patient_Mobile_Number__c)
            ) {
                newErrorMessage(
                    'Either '
                    + Schema.SObjectType.Patient__c.fields.Patient_Phone_Number__c.getLabel()
                    + ' OR '
                    + Schema.SObjectType.Patient__c.fields.Patient_Mobile_Number__c.getLabel()
                    + ' is required.'
                );
                success = false;
            }
            if (String.isBlank(patient.State__c)) {
                newErrorMessage(getRequiredString(Schema.SObjectType.Patient__c.fields.State__c));
                success = false;
            }
            if (String.isBlank(patient.Zip__c)) {
                newErrorMessage(getRequiredString(Schema.SObjectType.Patient__c.fields.Zip__c));
                success = false;
            }
        }

        return success;

    }

    private String getRequiredString(Schema.DescribeFieldResult field) {
        return String.format('{0} is required.', new List<String> {
            (field.getLabel())
        });
    }

}