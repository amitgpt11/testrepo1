/**
* REST dispatcher to handle toggling of favorite Orders from the custom app
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public with sharing class NMD_OrderTagsDispatcher implements IRestHandler {

	@TestVisible
	final static String KEY_ORDERID = 'oid';
	final static String BSCISYNERGY = 'bscisynergy';
	final static String PERSONAL = 'Personal';
	final static String URI_MAPPING = '/nmd/ordertags';

	public String getURIMapping() {
		return URI_MAPPING;
	}

	public void execute(IRestDispatcher caller, Map<String,String> parameters, Blob requestBody) {

		List<OrderTag> tags = new List<OrderTag>();
		String param = parameters.get(KEY_ORDERID);

		if (String.isNotBlank(param)) {

			Id orderId;

			try {
				orderId = (Id)param;
			}
			catch (System.StringException se) {
				caller.setResponse(400, 'Invalid Order Id');
				return;
			}

			if (orderId.getSObjectType() != Order.getSObjectType()) {
				caller.setResponse(400, 'Invalid Order Id');
				return;
			}

			List<OrderTag> oldTags = new List<OrderTag>();

			for (OrderTag tag : [
				select 
					ItemId
				from OrderTag
				where Name = :BSCISYNERGY
				and Type = :PERSONAL
			]) {
				if (tag.ItemId == orderId) {
					oldTags.add(tag);
				}
				else {
					tags.add(tag);
				}
			}

			if (oldTags.isEmpty()) {
				// create if it doesnt exist
				OrderTag tag = new OrderTag(
					Name = BSCISYNERGY,
					Type = PERSONAL,
					ItemId = orderId
				);
				tags.add(tag);
				DML.save(this, tag, false);
			}
			else {
				// otherwise remove it
				//delete tags;
				DML.remove(this, oldTags, false);
			}

		}

		// retrieve current favs list
		Set<Id> favIds = new Set<Id>();
		for (OrderTag tag : tags) {
			favIds.add(tag.ItemId);
		}

		caller.setResponse(200, JSON.serialize(favIds));

	}

}