/**
* Manger class for sending report emails to Quincy. This class is NOT bulk-safe and
* should not be called from inside a loop!
*
* @author   Salesforce services
* @date     2015-02-06
*/
public without sharing class NMD_SendQuincyEmailManager {

	private Id masterId;
	private Set<Id> childIds;
    private PageReference reportReference;

    public List<Attachment> attachments { get; private set; }
    public Map<String,String> reportParameters { get; private set; }
    public String lastErrorMessage { get; private set; }
    public String templateName { get; set; }
    public Set<String> ccAddresses { get; set; }

	public NMD_SendQuincyEmailManager(Id masterId, Set<Id> childIds) {
        this.masterId = masterId;
        this.childIds = childIds;
        this.lastErrorMessage = '';
        this.ccAddresses = new Set<String> { UserInfo.getUserEmail() };
        this.templateName = templateName;
        this.reportParameters = new Map<String,String>{};
        this.attachments = new List<Attachment>{};

        if (masterId.getSobjectType() == Account.SObjectType) {
            this.reportReference = Page.QuincyProspectReport;
            this.templateName = QuincyReportSettings__c.getInstance().Account_EmailTemplate_DeveloperName__c;
        }
        else if (masterId.getSobjectType() == Contact.SObjectType) {
            this.reportReference = Page.QuincyPhysicianReport;
            this.templateName = QuincyReportSettings__c.getInstance().Contact_EmailTemplate_DeveloperName__c;
        }
    }

	public Boolean sendQuincyEmail() {

		//find/verify the correct template
        List<EmailTemplate> templates = new List<EmailTemplate>([
            select Id, Subject
            from EmailTemplate 
            where DeveloperName = :this.templateName 
            limit 1
        ]);

        if (templates.isEmpty()) {
            return error(Label.NMD_Cannot_Find_Template + ': ' + this.templateName + '.', null);
        }

        EmailTemplate template = templates[0];

        //update account/contact records
        System.Savepoint sp = Database.setSavepoint();

        QuincyReportSettings__c qr = QuincyReportSettings__c.getInstance();
        Contact quincy = new Contact(
            LastName = 'Quincy Contact',
            Email = qr.Quincy_Email__c
        );
        insert quincy;
        String quincyId = quincy.Id;
        
        try {
            
            List<SObject> objects = new List<SObject>{};
            SObject master = this.masterId.getSobjectType().newSObject(this.masterId);
            master.put('Sent_to_Quincy__c', true);
            objects.add(master);

            for (Id cId : childIds) {
                SObject child = cId.getSobjectType().newSObject(cId);
                child.put('Sent_to_Quincy__c', true);
                objects.add(child);
            }
            update objects;

        }
        catch (System.DmlException de) {
            return error(de.getDmlMessage(0), sp);
        }
        
        //generate report as an attachment
        String childIdsStr = '';
        for (Id childId : childIds) {
            childIdsStr += (childIdsStr.length() > 0 ? ',' : '') + childId;
        }

        this.reportParameters.putAll(new Map<String,String> {
            'id' => masterId,
            'cid' => childIdsStr
        });
        this.reportReference.getParameters().putAll(this.reportParameters);
        //add report
        Blob content = Test.isRunningTest() ? Blob.valueOf('X') : this.reportReference.getContent();
        this.attachments.add(new Attachment(
            Name = 'report.pdf', 
            Body = content
        ));

        SendEmailManager emailManager = this.masterId.getSObjectType() == Account.SObjectType
            ? new SendEmailManager(template, quincyId, this.masterId)
            : new SendEmailManager(template, quincyId);
        
        for (String ccAddress : this.ccAddresses) {
            emailManager.addCCAddress(ccAddress);
        }

        for (Attachment attachment : this.attachments) {
            emailManager.addAttachment(attachment);
        }

        Messaging.SendEmailResult result = emailManager.sendEmail();
        if (!result.isSuccess()) {
            return error((result.getErrors()[0]).getMessage(), sp);
        }

        //create our own email history, so we can attach the email attachments as documents
        try {
            Task emailHistory = new Task(
                ActivityDate = System.today(),
                Status = 'Completed',
                Subject = 'Email: ' + template.Subject,
                WhoId = quincyId
            );
            insert emailHistory;

            for (Attachment attachment : this.attachments) {
                attachment.ParentId = emailHistory.Id;
            }
            insert this.attachments;

            delete quincy;

        }
        catch (System.DmlException de) {
            return error(de.getDmlMessage(0), sp);
        }

        return true;

	}

    @TestVisible
    private Boolean error(String errMsg, System.Savepoint sp) {
        this.lastErrorMessage = errMsg;
        if (sp != null) {
            Database.rollback(sp);
        }
        return false;
    }

}