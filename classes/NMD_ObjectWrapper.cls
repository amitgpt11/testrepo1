/**
* Simple Wrapper Model for adding additional attributes to an SObject
*
* @author   Salesforce services
* @date     2015-02-11
*/
public with sharing class NMD_ObjectWrapper {
	
	public SObject data { get; private set; }
	public Integer index { get; private set; }
    public Boolean selected { get; set; }

    public NMD_ObjectWrapper(SObject data) {
        this(data, 0);
    }

    public NMD_ObjectWrapper(SObject data, Integer index) {
    	this.data = data;
    	this.index = index;
        this.selected = false;
    }

}