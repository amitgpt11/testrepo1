@isTest
public class UtilForUnitTestDataSetup { 
     Private Static Integer PersonnelIDCount = 0;
     Private Datetime today = system.today()-1;
        
     public static User getUser(){ 

           String aliasString = 'testA';

          Id sysAdminId = [Select Id from Profile Where Name = 'System Administrator' Limit 1].Id;

          User usr = new User(

          alias = aliasString, 

           email= aliasString+'@boston.com',

          emailencodingkey='UTF-8',

          lastname='Testing', 

           languagelocalekey='en_US',

          localesidkey='en_US',

          profileid = sysAdminId,

          timezonesidkey='Europe/London',

          username= aliasString+'@accenture.com');

          return usr;

        }
     public static User newUser(){
            return newUser('Standard User');
        }
    
     public static User newUser(String userType){
            Profile p = [SELECT Id FROM Profile WHERE Name = :userType]; 
            User user = new User(
                Email = 'suser@boston.com', 
                LastName = 'LNAMETEST',  
                ProfileId = p.Id, 
                UserName = 'UserName' + '@boston.com',
                Alias = 'standt', 
                EmailEncodingKey = 'UTF-8',  
                LanguageLocaleKey = 'en_US', 
                LocaleSidKey = 'en_US',  
                TimeZoneSidKey = 'America/Los_Angeles',
                Cost_Center_Code__c = 'ASDCN', // special forNMD
                IsActive = True
            );
            return user;                            
        }
     public Personnel_ID__c newPersonnelID() {
           return newPersonnelID(UserInfo.getUserId());
        }
     public static Personnel_ID__c newPersonnelID(Id userId) {
            PersonnelIDCount++;
            return new Personnel_ID__c(
                Name = 'PID' + 'Asd12', 
                User_for_account_team__c = userId,
                Personnel_ID__c = 'TESTDATA' + PersonnelIDCount
            );
        }
    
    
     public static Account newAccount() {
         return newAccount('Account');
        }

     public static Account newAccount(String namestrg) {
            return new Account(
                Name = namestrg,
                RecordTypeId = getRECTypeId()
            );
        }
    
    public static Contact newContact(Id acctId) {
        return newContact(acctId, 'LastName' );
    }

    public static Contact newContact(Id acctId, String lname) {
        return new Contact(
            AccountId = acctId,
            LastName = lname
        );
    }
    public static Account_Performance__c NewAccount_Performance(Id accountId){
        return new Account_Performance__c(
            Division__c = 'EP',
            CY__c = '2016',
            Facility_Name__c =accountId
            
        );
    }
    
     public static list<Territory2Type> newTerritory2Type(){
             list<Territory2Type> terriType   = [SELECT id, DeveloperName from Territory2Type limit 1];
             return terriType;
        }   
     public static Id newTerritory2Model(){
            /*return new Territory2Model(
            Name='TM1',
            DeveloperName= 'DevTM1',
            State='Active'
            );*/
            List<Territory2Model>  models = [Select Id from Territory2Model where State = 'Active'];
            Id activeModelId = null;
            if(models.size()  == 1){
                 activeModelId = models.get(0).Id;
                 
             } 
             return activeModelId ;   
        }
     public static Territory2 newTerritory2(String name, Id TempTerritory2ModelId, Id TempTerritory2TypeId){
            return new Territory2(
            Name = name,
            AccountAccessLevel = 'Edit',
            ContactAccessLevel = 'None',
            Territory2ModelId = TempTerritory2ModelId ,
            DeveloperName= 'Dev'+ name,
            Territory2TypeId = TempTerritory2TypeId
            );
        }   
    
     public static ObjectTerritory2Association newObjectTerritory2Association(Id tempObjectId, Id tempTerritory2Id){
        return new ObjectTerritory2Association(
                AssociationCause ='Territory2Manual',
                ObjectId =tempObjectId,
                Territory2Id =tempTerritory2Id 
                
            );
        }   
     public static UserTerritory2Association newUserTerritory2Association(Id tempTerritory2Id, Id tempUserId){
            return new UserTerritory2Association(
                RoleInTerritory2='Reviewer',
                Territory2Id = tempTerritory2Id,
                UserId = tempUserId
                
            );
        }   
     public static Opportunity newOpportunity(Id acctId) {
        return newOpportunity(acctId, 'Opportunity1' );
     }
     public static  Opportunity newOpportunity(Id acctId, String name) {
            return new Opportunity(
                AccountId = acctId,
                CloseDate = System.today().addDays(7),
                Name = name,
                StageName = 'New',
                Type = 'Standard',
                Opportunity_Type__c = 'Standard',
                Competitor_Name__c = 'Other',
                Product_System__c = 'Spectra'
             );   
            
        }
     public static My_Objectives__c newMy_Objectives(String name, Id AccId, Id Terr2ID){
        My_Objectives__c myObj = new  My_Objectives__c();
        myObj.Related_to_Account__c = AccId;
        myObj.Territory__c  = Terr2ID;
        myObj.Name = name;
        myObj.Status__c = 'New';
        return myObj;
     }   
     public String randomString5() {
        return randomString().substring(0, 5);
     }

     public String randomString() {
        //return random string of 32 chars
        return EncodingUtil.convertToHex(Crypto.generateAesKey(128));
     } 
     
     public  static Id getRECTypeId() {
         Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Account.getRecordTypeInfosByName();
          Id RECTYPE_CUSTOMER = RECTYPES.get('Customer').getRecordTypeId();
          return RECTYPE_CUSTOMER;
    }
    
    public static void createCustomSettingFrENDO(){
        TerritoriesForPlum__c plm = new TerritoriesForPlum__c();
         plm.Name = 'PLUM';
         plm.Plum__c = '500';
         insert plm;
         TerritoriesForGI__c gi = new TerritoriesForGI__c();
         gi.Name = 'GI';
         gi.GI__C = '900';
         insert gi;
    }
}