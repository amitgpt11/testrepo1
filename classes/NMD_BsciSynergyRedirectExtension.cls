/**
* Extension class for the VF page BsciSynergyRedirect
*
* @Author salesforce Services
* @Date 2015-07-13
*/
public with sharing class NMD_BsciSynergyRedirectExtension extends NMD_ExtensionBase {
	
    public transient String redirectUrl { get; private set; }

    public NMD_BsciSynergyRedirectExtension() {
        
        this.redirectUrl = '';    
        
        String path = ApexPages.currentPage().getParameters().get(SynergyPageReference.PATH_PARAM);
        if (String.isNotBlank(path)) {
            SynergyPageReference spr = new SynergyPageReference(path);
            this.redirectUrl = spr.getPageReference().getUrl();
        }

    }

}