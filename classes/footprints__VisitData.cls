/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class VisitData {
    global String accountId;
    global String assignedTo;
    global Datetime endTime;
    global String id;
    global String name;
    global Datetime startTime;
    global String visitId;
    global VisitData() {

    }
}
