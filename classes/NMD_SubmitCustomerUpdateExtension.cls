/**
* Controller Class for the SubmitCustomerUpdate.page
*
* @Author salesforce Services
* @Date 02/10/2015
*/
public with sharing class NMD_SubmitCustomerUpdateExtension extends NMD_QuincyExtensionBase {
	
	@testVisible
	final static String QUINCY_FIELDSET = 'Quincy_Field_Updates';
	@testVisible
	final static String BSN_FIELDSET = 'BSN_Field_Updates';

	final Account originalAcct;

	public transient Blob attchbody { get; set; }
	public Boolean isConsignmentAcct { get; private set; }
	public String attchname { get; set; }

	public NMD_SubmitCustomerUpdateExtension(ApexPages.StandardController sc) {
		super(sc);
		
		this.originalAcct = (Account)sc.getRecord().clone(true, true);
		this.isConsignmentAcct = this.originalAcct.RecordTypeId == AccountManager.RECTYPE_CONSIGNMENT;
		this.attchbody = Blob.valueOf('');
		this.attchname = '';

        if (sc.getId() == null) {
        	newErrorMessage(Label.Invalid_Missing_Account);
        }
        else if (this.isConsignmentAcct) {
        	newInfoMessage(Label.NMD_Enter_Updated_Information_BSN);
        }
        else {

        	//retrieve available contacts
	        for (AccContactRelationship__c junction : [
	            select
	                Contact__c,
	                Contact__r.Name
	            from AccContactRelationship__c
	            where Facility__c = :sc.getId()
	        ]) {
	            children.add(new NMD_ObjectWrapper(new Contact(
	                Id = junction.Contact__c,
	                LastName = junction.Contact__r.Name
	            )));
	        }

	        newInfoMessage(Label.NMD_Enter_Updated_Information);

        }

	}

	public override void submit() {

		//collect overwrite fields
		Map<String,String> reportParams = getFieldValueDeltas(
			Schema.SObjectType.Account.fieldSets.getMap().get(this.isConsignmentAcct ? BSN_FIELDSET : QUINCY_FIELDSET),
			this.originalAcct,
			this.sc.getRecord()
		);

		System.debug(reportParams);

		if (!reportParams.isEmpty() && String.isBlank(this.attchname) && !this.isConsignmentAcct) {
			//throw error if there's an update to the account but no upload
			newErrorMessage(Label.NMD_Attachment_Required);
		}
		else {

			NMD_SendQuincyEmailManager emailManager = new NMD_SendQuincyEmailManager(this.sc.getId(), getSelectedIds());
			emailManager.reportParameters.putAll(reportParams);
			
			//add attachment if present
			if (String.isNotBlank(this.attchname)) {
				emailManager.attachments.add(new Attachment(
					Name = this.attchname,
					Body = this.attchbody
				));
			}

			if (this.isConsignmentAcct) {
				emailManager.ccAddresses.add(QuincyReportSettings__c.getInstance().BSN_Email__c);
			}

            isSubmitted = emailManager.sendQuincyEmail();
            
            if (isSubmitted) {
                newInfoMessage(this.isConsignmentAcct
                	? Label.NMD_Report_Emailed_BSN
                	: Label.NMD_Report_Emailed
                );
            }
            else {
            	newErrorMessage(emailManager.lastErrorMessage + ' ' + Label.NMD_Please_Contact_Admin);
            	logError('NMD_SubmitCustomerUpdateExtension.submit', emailManager.lastErrorMessage);
            }
        }

	}

}