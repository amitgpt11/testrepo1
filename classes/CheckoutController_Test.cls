@isTest
public class CheckoutController_Test {
    
    private static testMethod void test() {
    
        //Create Guest User
        User objUser = Test_DataCreator.createCommunityUser();
        
        Test.StartTest();
        
        system.runAs(objUser) {
        
            Test.setCurrentPageReference(new PageReference('Checkout.page')); 
            CheckoutController objCheckoutController = new CheckoutController();
            objCheckoutController.fetchRequestedData();
            
            
            System.currentPageReference().getParameters().put('orderId', 'test');
            objCheckoutController = new CheckoutController();
            objCheckoutController.fetchRequestedData();
        
            //Create Product
            Product2 objProduct = Test_DataCreator.createProduct('Test');
            
            //Create Order
            Shared_Community_Order__c objOrder = Test_DataCreator.createOrder();
            
            //Create Order Line Item
            Shared_Community_Order_Item__c objOrderItem = Test_DataCreator.createOrderItem(objOrder.Id,objProduct.Id);      
                    
            System.currentPageReference().getParameters().put('orderId', objOrder.Id);
            objCheckoutController = new CheckoutController();
            objCheckoutController.init();
            objCheckoutController.submitOrder();
            
            system.assertEquals(null, objCheckoutController.init());
        }
        Test.StopTest();
    }
}