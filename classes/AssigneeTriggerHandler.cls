/**
* Handler class for the Assignee custom object
*
* @Author salesforce Services
* @Date 04/07/2015
*/
public with sharing class AssigneeTriggerHandler extends TriggerHandler {

	final AssigneeManager manager = new AssigneeManager();
	
	/**
    * Called once per record
    * 
    * @param void
    */
	public override void afterInsert(SObject so) {
		Assignee__c assignee = (Assignee__c)so;
		manager.updateParentsCurrentTMAssignees(assignee, false);
	}
   
    /**
    * Called once per object
    * 
    * @param void
    */
	public override void afterUpdate(SObject oldSo, SObject so) {
    	Assignee__c oldAssignee = (Assignee__c)oldSo;
    	Assignee__c newAssignee = (Assignee__c)so;
    	manager.updateParentsCurrentTMAssignees(oldAssignee, newAssignee);
    }
     
    /**
    * Called once per object
    * 
    * @param void
    */
	public override void afterDelete(SObject so) {
    	Assignee__c assignee = (Assignee__c)so;
    	manager.updateParentsCurrentTMAssignees(assignee, true);
    }
    
    /**
    * Called at end of trigger
    * 
    * @param void
    */
	public override void andFinally() {
    	manager.commitParentSellerHierarchies();
    }

}