/**
* Test class for NMD_ObjectTerritoryByContactBatch
*
* @author   Salesforce services
* @date     2014-04-13
*/
@isTest 
private class NMD_ObjectTerritoryByContactBatchTest {

    static testMethod void test_NMD_ObjectTerritoryByContact() {
		
        
        final Integer LENGTH = 1;
        NMD_TestDataManager testData = new NMD_TestDataManager();
        
        //create account
        Account acc = testData.createConsignmentAccount();
        insert acc;     
        
        //create seller hierarchy
        Seller_Hierarchy__c sellerH = testData.newSellerHierarchy('TEST_SH');
        insert sellerH;  
        Seller_Hierarchy__c sellerH1 = testData.newSellerHierarchy('TEST_SH1');
        insert sellerH1;
                
        List<Contact> contacts = new List<Contact>();               
        //create contact
        for(Integer i = 0; i < LENGTH; i++) {
            contacts.add(testData.newContact(acc.Id));
        }
        insert contacts;        
    
        //contact Ids
        Set<String> contactIds = new Set<String>();
        Id tempId;
        for(Contact c : contacts){        
            contactIds.add(c.Id);
            tempId = c.Id;
            c.RecordTypeId = ContactManager.RECTYPE_CUSTOMER;
            c.Territory_ID__c = sellerH.Id;
        }
        update contacts;
        
        //create patient
        Patient__c patient = testData.newPatient();
        insert patient;
        patient.Physician_of_Record__c = tempId;
        patient.RecordTypeId = PatientManager.RECTYPE_CUSTOMER;
        patient.Territory_ID__c = sellerH.Id;
        update patient;                
                
        //create oppo
        Opportunity oppo = testData.newOpportunity(acc.Id);
        insert oppo;
        oppo.Patient__c = patient.Id;
        oppo.Territory_ID__c = sellerH.Id;
        //oppo.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
        oppo.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        update oppo;
        
        patient.Physician_of_Record__c = tempId;
        patient.RecordTypeId = PatientManager.RECTYPE_CUSTOMER;
        patient.Territory_ID__c = sellerH.Id;
        update patient;        
        
		 //run the batch
        System.Test.startTest();         
        	for(Contact c : contacts){        
            
            c.Territory_ID__c = sellerH1.Id;
        }
        update contacts;
           ID batchprocessid = Database.executeBatch(new NMD_ObjectTerritoryByContactBatch());
           NMD_ObjectTerritoryByContactBatch.runNow();
        
        
        Database.BatchableContext bc;
        List<ContactHistory> scope=new List<ContactHistory>();
        ContactHistory ch=new ContactHistory();
            Contact cn=testData.newContact(acc.Id);
			ch.ContactId=cn.id;
       		
        scope.add(ch);

        new NMD_ObjectTerritoryByContactBatch().execute(bc, scope);
       NMD_ObjectTerritoryByContactBatch.objectTerritoryChange(contactIds);
        System.Test.stopTest();
    }
    
}