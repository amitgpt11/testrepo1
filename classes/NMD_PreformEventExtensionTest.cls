/**
* Test class for NMD_PreformEventExtension
*
* @Author Salesforce Services
* @Date 03/24/2015
*/
@isTest(SeeAllData=false)
private class NMD_PreformEventExtensionTest {
	
	static final String FNAME = 'FNAME';
	static final String LNAME = 'LNAME';

	@testSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		Patient__c patient = td.newPatient(FNAME, LNAME);
		insert patient;

	}

	static testMethod void test_PreformEvent() {

		Patient__c patient = [
			select 
				Id, Patient_First_Name__c, Patient_Last_Name__c
			from Patient__c 
			where Patient_First_Name__c = :FNAME 
			and Patient_Last_Name__c = :LNAME
		];

		PageReference pr = Page.PreformEvent;
		pr.getParameters().put('id', patient.Id);
		Test.setCurrentPage(pr);

		NMD_PreformEventExtension ext = new NMD_PreformEventExtension(
			new ApexPages.StandardController(patient)
		);

		ext.preform();

		//for errors
		PageReference errpr = ext.error('error');

	}

}