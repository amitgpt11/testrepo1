/**
* Dispatcher class, to process custom REST requests
*
* @Author salesforce Services
* @Date 2015-08-25
*/
public with sharing class NMD_ProductsDispatcher implements IRestHandler {
	
	@TestVisible
	final static String KEY_PRICEBOOKNAME = 'pricebookName';
	@TestVisible 
	final static String ERR_MISSING_NAME = 'Missing Pricebook2 Name';
	@TestVisible 
	final static String ERR_INVALID_ORDERID = 'Invalid Order Id';
	@TestVisible
	final static String PARAM_ORDERID = 'oid';

	// mapping, ie: /nmd/products/{pricebookName}
	final static String URI_MAPPING = (String.format('/nmd/products/\'{\'{0}\'}\'', new List<String> {
		KEY_PRICEBOOKNAME
	}));

	public String getURIMapping() {
		return URI_MAPPING;
	}

	public void execute(IRestDispatcher caller, Map<String,String> parameters, Blob requestBody) {

		String pbName = parameters.get(KEY_PRICEBOOKNAME);

		if (String.isBlank(pbName)) {

			caller.setResponse(400, ERR_MISSING_NAME);

		}
		else {
			
			Set<Id> acctIds = new Set<Id>();
			String oid = parameters.get(PARAM_ORDERID);
			Id orderId;
			Id custId;
			Date custDate;

			// retrieve order if parameter was provided
			if (String.isNotBlank(oid)) {
			
				//ord = getOrderRecord(oid);
				Id recordId;
		
				try {
					recordId = (Id)oid;
				}
				catch (System.StringException ex) {
					return;
				}

				if (recordId.getSobjectType() == Account.getSObjectType()) {

					custId = recordId;
					acctIds.add(recordId);

				}
				else if (recordId.getSobjectType() == Order.getSObjectType()) {

					List<Order> orders = new List<Order>([
						select 
							AccountId,
							Surgery_Date__c
						from Order
						where Id = :recordId
					]);

					//return orders.isEmpty() ? null : orders[0];

					if (orders.isEmpty()) {
						caller.setResponse(400, ERR_INVALID_ORDERID);
						return;
					}	

					Order ord = orders[0];
					orderId = ord.Id;
					custId = ord.AccountId;
					custDate = ord.Surgery_Date__c;

					acctIds.add(custId);

				}

			}

			List<PricebookEntry> entries = new List<PricebookEntry>();
			NMD_PricebookManager manager = new NMD_PricebookManager(acctIds);
			
			List<NMD_PricebookPair> pairs = custId == null
				? manager.getDefaultPricebookEntries()
				: manager.getAccountPricebookEntries(custId, custDate);

			for (NMD_PricebookPair pair : pairs) {
				
				PricebookEntry pbe = pair.consolidate();
				entries.add(pbe);

				if (pair.isRestricted && manager.hasExemption(pbe.Product2Id)) {
					// this will allow the custom app to show this entry for this user
					pbe.Product2.Material_Group_1__c = '';
				}

			}

			caller.setResponse(200, JSON.serialize(entries));

		}

	}

	//private Order getOrderRecord(String oid) {

	//	Id orderId;
		
	//	try {
	//		orderId = (Id)oid;
	//	}
	//	catch (System.StringException ex) {
	//		return null;
	//	}

	//	List<Order> orders = new List<Order>([
	//		select 
	//			AccountId,
	//			Surgery_Date__c,
	//			OwnerId
	//		from Order
	//		where Id = :orderId
	//	]);

	//	return orders.isEmpty() ? null : orders[0];

	//}

}