/**
* Test Class for TaskManagerNoSharing
*
* @Author salesforce Services
* @Date 2016/04/15
*/
@isTest
private class TaskManagerNoSharingTest {

    static testMethod void test_makeUserTaskOwner_Errors(){

        User nmdFieldRep = new NMD_TestDataManager().newUser('NMD Field Rep');
        User adminUser = new NMD_TestDataManager().newUser('System Administrator');
        insert new List<User>{nmdFieldRep, adminUser};

        Task task;

        System.runAs(nmdFieldRep){

            task = new Task(
                ActivityDate = System.today().addDays(1),
                Subject = 'Owner Change',
                //WhoId = lead.Id,
                OwnerId = nmdFieldRep.Id,
                Status = 'Not Started',
                BluePrint_Bypass_RelatedTo_and_Name__c = true
            );
            insert task;

        }

        Test.startTest();

            String failResult = TaskManagerNoSharing.makeUserTaskOwner(task.Id, nmdFieldRep.Id);
            System.assertEquals(Label.TaskManagerNoSharing_InvalidProfileError, failResult, 'Did not get error for invalid profile.');

            System.runAs(adminUser){

                task.Status = 'Completed';
                update task;

                String results = TaskManagerNoSharing.makeUserTaskOwner(task.Id, adminUser.Id);
                System.assertEquals(Label.TaskManagerNoSharing_CompletedError, results, 'Did not get error for Completed Task.');

            }

        Test.stopTest();

    }
    
    static testMethod void test_makeUserTaskOwner_Success(){

        User nmdFieldRep = new NMD_TestDataManager().newUser('NMD Field Rep');
        User adminUser = new NMD_TestDataManager().newUser('System Administrator');
        insert new List<User>{nmdFieldRep, adminUser};

        Task task;

        System.runAs(nmdFieldRep){

            task = new Task(
                ActivityDate = System.today().addDays(1),
                Subject = 'Owner Change',
                //WhoId = lead.Id,
                OwnerId = nmdFieldRep.Id,
                Status = 'Not Started',
                BluePrint_Bypass_RelatedTo_and_Name__c = true
            );
            insert task;

        }

        Test.startTest();

            System.runAs(adminUser){

                String results = TaskManagerNoSharing.makeUserTaskOwner(task.Id, adminUser.Id);
                System.assertEquals('', results, 'No errors should have been thrown.');

                task = [select Id, OwnerId from Task WHERE Id = :task.Id];
                System.assertEquals(task.OwnerId, adminUser.Id, 'Owner was not changed for new task.');

            }

        Test.stopTest();
    }

    
}