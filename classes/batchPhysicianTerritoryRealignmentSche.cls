/**
* Batch class, to batchPhysicianTerritoryRealignment class  
* @author   Aalok - Accenture
* @date     26APR2016
*/
global class batchPhysicianTerritoryRealignmentSche implements Schedulable{
    
    /**
    *  @desc    Schedulable method, executes the class instance
    */
    global void execute(SchedulableContext context) {
        integer batchSize = Integer.valueOf(15);
        batchPhysicianTerritoryRealignment b = new batchPhysicianTerritoryRealignment();
        database.executebatch(b,batchSize);

    }

}