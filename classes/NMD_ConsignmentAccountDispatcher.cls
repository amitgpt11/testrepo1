/**
* Dispatcher class, search for specific users relative to the context users UserRole
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class NMD_ConsignmentAccountDispatcher implements IRestHandler {

	@TestVisible
	final static String PARAM_QUERY = 'q';
	final static String URI_MAPPING = '/nmd/accounts/search';
	
	public String getURIMapping() {
		return URI_MAPPING;
	}

	public void execute(IRestDispatcher caller, Map<String,String> parameters, Blob requestBody) {

		String query = (parameters.containsKey(PARAM_QUERY) ? parameters.get(PARAM_QUERY) : '')
			.replaceAll('\\+', ' ')  // "Search+String" => "Search String"
			.replaceAll('\\*', '%'); // "Search*" => "Search%"

		String fuzzedQuery = String.format('%{0}%', new List<String> { query });
		Id consigmentRecTypeId = AccountManager.RECTYPE_CONSIGNMENT;
		Id currentUserId = UserInfo.getUserId();

		NMD_UserRoleManager roleManager = new NMD_UserRoleManager();
		Set<Id> roleIds = String.isBlank(query)
			? roleManager.getRolesInRegion()  // region role id and all decendent role ids
			: roleManager.userRoles.keySet(); // neuromod role id and all decendent role ids
		
		String soql = 
			'select ' +
				'Name, ' +
				'ShippingCity, ' +
				'OwnerId ' +
			'from Account ' +
			'where RecordTypeId = :consigmentRecTypeId ' +
			'and OwnerId != :currentUserId ' +
			'and Owner.IsActive = true ' +
			'and Owner.UserRoleId in :roleIds ' +
			(String.isBlank(query) ? '' : 'and Name like :fuzzedQuery ') +
			'order by Name';

		List<Account> accts = (List<Account>)Database.query(soql);

		caller.setResponse(200, JSON.serialize(accts));

	}

}