/**
* Handler class for the Campaign trigger
*
* @Author salesforce Services
* @Date 2016/04/12
*/
public with sharing class CampaignTriggerHandler extends TriggerHandler {

	final CampaignManager manager = new CampaignManager();

    /**
    * method to aggregate inventory data for campaign update
    * 
    * @param void
    */
    public override void bulkAfter() {

    	manager.fetchRecords(trigger.new);

    }

    /**
    * Called once per object
    * 
    * @param void
    */
    public override void afterInsert(SObject obj) {
        Campaign camp = (Campaign)obj;

        manager.createNewMemberStatusRecords(camp);
    
    }      

    /**
    * Called at end of trigger
    * 
    * @param void
    */  
    public override void andFinally() {

    	manager.commitCampaignMemberStatus();

    }

}