/*
-------11/2/2015 - Mike Lankfer - Created TestDataFactory class.  Added createTestAccounts and createTestAPRecords methods.
*/

@isTest
public class TestDataFactory {
    
    //create variable number of Test Accounts and return them as a List
    public static List<Account> createTestAccounts (Integer quantity){        
        
        List<Account> testAccounts = new List<Account>();
        
        for (Integer i = 0; i < quantity; i++){
            Account a = new Account(Name = 'Test Account '+ String.valueOf(i));
            testAccounts.add(a);
        }
        
        insert testAccounts;
        return testAccounts;
        
    }
    
    //create variable number of Account Performance records for the given account and return those records as a List
    public static List<Account_Performance__c> createTestAPRecords (Account a, Integer quantityPerAccount){
        
        List<Account_Performance__c> accountPerformanceRecords = new List<Account_Performance__c>();
        
        for (Integer i = 0; i < quantityPerAccount; i++){
            Account_Performance__c ap = new Account_Performance__c(Facility_Name__c = a.Id, CY__c = String.valueOf(System.Today().year()));
            accountPerformanceRecords.add(ap);
        }
        
        insert accountPerformanceRecords;
        return accountPerformanceRecords;
        
    }
    
    //create variable number of Test contact and return them as a List
    public static List<contact> createTestcontacts (Integer quantity){        
        
        List<contact> testcontacts = new List<contact>();
        
        for (Integer i = 0; i < quantity; i++){
            contact a = new contact(LastName = 'Test contact '+ String.valueOf(i));
            testcontacts.add(a);
        }
        
        insert testcontacts;
        return testcontacts;
        
    }
    // Datetime newInstance(Integer year, Integer month, Integer day, Integer hour, Integer minute, Integer second);
    public static List<Shared_Patient_Case__c> createTestPCRecords (Account a, Integer quantityPerAccount){
        
        //public Datetime newInstance(Integer year, Integer month, Integer day, Integer hour, Integer minute, Integer second);
        datetime startdate = Datetime.newInstance(2016, 9, 15, 02, 30, 0);
        datetime enddate = Datetime.newInstance(2016, 9, 15, 04, 30, 0);
        // e.StartDateTime = datetime.newInstance(2014, 9, 15, 12, 30, 0);
        List<Shared_Patient_Case__c> patientcaseRecords = new List<Shared_Patient_Case__c>();
        
        for (Integer i = 0; i < quantityPerAccount; i++){
            Shared_Patient_Case__c pc = new Shared_Patient_Case__c(Shared_Facility__c = a.Id ,Shared_Start_Date_Time__c =  startdate,Shared_end_Date_Time__c =  enddate);//,Shared_Start_Date_Time__c = datetime.newInstance(2014, 9, 15, 12, 30, 0));
            patientcaseRecords.add(pc);
        }
        
        insert patientcaseRecords;
        return patientcaseRecords;
        
    }
    
    // This method creates sample Product data which is used in Test class
    public static Product2 createTestProduct(String prH,String rtName){
        Product2 pr = new Product2();
        
        list<RecordType> rtList = [Select Id,name From RecordType where sobjecttype = 'Product2' and name = :rtName];
        
        pr.Product_Hierarchy__c = prH;
        if(rtList.size() > 0){
            pr.recordTypeId = rtList.get(0).Id;
        }
        pr.Name = 'Test'+prH;
        insert pr;
        
        return pr;
    }
    
    // This method creates sample Lutonix SRAI which is used in Test class
    public static PI_Lutonix_SRAI__c createTestLutonixHeader(String Month){
        date todaydate = date.Today();
        PI_Lutonix_SRAI__c objInsert = new PI_Lutonix_SRAI__c();
        objInsert.PI_Month__c = Month;
        objInsert.PI_Year__c = String.ValueOf(todaydate.Year());
        objInsert.PI_Partial_Month__c = false;
        insert objInsert;
        return objInsert;
    }
    
    // This method creates sample Single Lutonix SRAI Detail which is used in Test class
    public static PI_Lutonix_SRAI_Detail__c createTestLutonixDetail(String headerId, date dt){
        
        PI_Lutonix_SRAI_Detail__c objInsert;
        
        objInsert = new PI_Lutonix_SRAI_Detail__c();
        objInsert.PI_Date__c = dt;
        objInsert.PI_Lutonix_SRAI__c = headerId;
        objInsert.PI_Recorded_Temperature__c = 12;
        objInsert.PI_Temperature_Scale__c = 'C';
        objInsert.PI_Units_of_Trunk_Stock__c = 100;
        
        insert objInsert;
        
        return objInsert;
    }
    
    public static Map<integer,String> getMapMonthNumber(){
        Map<integer, string> mapMonthNumber = new Map<Integer,string>{
            1 =>'January',
                2 => 'February',
                3 => 'March',
                4 => 'April',
                5 => 'May',
                6 => 'June',
                7 => 'July',
                8 => 'August',
                9 => 'September',
                10 => 'October',
                11 => 'November',
                12 => 'December'
                };
                    return mapMonthNumber;
    }
    
    // This method creates sample list of Lutonix SRAI Detail for whole month which is used in Test class
    public static list<PI_Lutonix_SRAI_Detail__c> createTestLutonixDetail(String headerId, integer daysInMonth){
        list<PI_Lutonix_SRAI_Detail__c> lstToInsert = new list<PI_Lutonix_SRAI_Detail__c>();
        date dt = Date.today();
        dt = dt.toStartOfMonth() - 1;
        PI_Lutonix_SRAI_Detail__c objInsert;
        for(integer i =1;i<=daysInMonth;i++){
            objInsert = new PI_Lutonix_SRAI_Detail__c();
            objInsert.PI_Date__c = dt+i;
            objInsert.PI_Lutonix_SRAI__c = headerId;
            objInsert.PI_Recorded_Temperature__c = 12+i;
            objInsert.PI_Temperature_Scale__c = 'C';
            objInsert.PI_Units_of_Trunk_Stock__c = 100-i;
            lstToInsert.add(objInsert);    
        }
        
        insert lstToInsert;
        return lstToInsert;
    }
    
    //Create product group mapping record
    public static Shared_Product_Group_Mapping__c createProductGrpMapping(String division, Id contrid){
        Shared_Product_Group_Mapping__c grp = new  Shared_Product_Group_Mapping__c();
        grp.Shared_Division__c = division;
        grp.Shared_Contract__c = contrid;        
        return grp;
    } 
    
    
    //create 1 test Account and return it without DML
    public static Account createTestAccount (){        
        
        Account testAccount = new Account(Name = 'Test Account');
        
        return testAccount;
        
    }
    
    
    //create 1 test opty and return without DML
    public static Opportunity createTestOpportunity(Account a){        
        Opportunity opty = new Opportunity(Name = 'Test Opportunity', StageName = 'Plan', AccountId = a.Id, CloseDate = Date.Today());
        
        return opty;           
    }
    
    //create List of Opty Line Items and return without DML
    public static List<OpportunityLineItem> createTestOpportunityLineItems(Opportunity opty, List<PricebookEntry> pricebookEntries){
        List<OpportunityLineItem> testOptyLineItems = new List<OpportunityLineItem>();
        
        for(PricebookEntry pricebookEntry:pricebookEntries){
            OpportunityLineItem optyLineItem = new OpportunityLineItem(OpportunityId = opty.Id, PricebookEntryId = pricebookEntry.Id, Quantity = 1, UnitPrice = 5);
            testOptyLineItems.add(optyLineItem);
        }
        
        return testOptyLineItems;
        
    }
    
    
    //create 1 test Pricebook Entry and return without DML
    public static Pricebook2 createTestPricebook(){
        Pricebook2 pricebook = new Pricebook2(Name = 'Test Pricebook');
        
        return pricebook;
    }
    
    
    //create a List of Pricebook Entries and return without DML
    public static List<PricebookEntry> createTestPricebookEntries(Pricebook2 p, List<Product2> products){
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        
        for(Product2 product:products){
            PricebookEntry testPricebookEntry = new PricebookEntry(Product2Id = product.Id, Pricebook2Id = p.Id, UnitPrice = 0.00, isActive = true);
            pricebookEntries.add(testPricebookEntry);
        }
        
        return pricebookEntries;
    }
    
    
    //create 1 test product and return without DML
    public static Product2 createTestProduct(){        
        Product2 testProduct = new Product2(Name = 'Test Product');
        
        return testProduct;
    }
    
    
    //create 1 test profile and return without DML
    public static Profile createTestProfile(){
        Profile testProfile = new Profile(Name = 'Test Profile');
        
        return testProfile;
    }
    
    
    //create list of test users and return without DML
    public static List<User> createTestUsers(Integer quantity, Profile p){
        List<User> testUsers = new List<User>();
        
        for (Integer i = 1; i <= quantity; i++){
            User user = new User(isActive = true, Alias = 'Endo'+ String.valueOf(i), FirstName = 'EndoTest', LastName = 'EndoUser ' + String.valueOf(i), Email = 'endotestuser'+ String.valueOf(i) +'@test.com', UserName = 'tendoestuser'+ String.valueOf(i) +'@test.com', ProfileId = p.Id, TimeZoneSidKey = 'GMT',
                                 LocaleSidKey = 'en_US',  EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
            testUsers.add(user);
        }
        
        return testUsers;
    }
    
    //create a contact record and return without DML
    public static Contact createTestContact(){
        Contact testContact = new Contact(FirstName = 'Test', LastName = 'Contact');
        
        return testContact;
    }
    
    //create a test patient case record and return without DML
    public static Shared_Patient_Case__c createTestPatientCase(){
        Shared_Patient_Case__c testPatientCase = new Shared_Patient_Case__c();
        
        return testPatientCase;
    }
    
    //create a test implant record and return without DML
    public static LAAC_Implant__c createTestImplant(){
        LAAC_Implant__c testImplant = new LAAC_Implant__c();
        
        return testImplant;
    }
    
}