@isTest
public class BSMIConfigController_Test {
    
    private static testMethod void test() {
     
        User objUser = Test_DataCreator.createCommunityUser();   
        
        Test.StartTest();
        
        Id eventID = 'a2nR000000055WA';
        Test_DataCreator.cerateBostonCSConfigTestData(eventID);
        
        system.runAs(objUser){
            
            BSMIConfigController objBSMIConfigController = new BSMIConfigController();
            system.assertEquals(objBSMIConfigController.save(),null);
            
        }
    }
}