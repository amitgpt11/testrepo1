/*
@CreatedDate     3JUNE2016
@author          Julianne Diamond
@Description     Tests Controller for MyFavorites VF Page
@Requirement Id  User Story : US2674
*/


@isTest

public class MyFavoritesControllerTest {
    
    static testmethod void getAccountFavoriteTest(){
        Test.startTest();
        Account account = new Account(Name = 'Test Account');
        insert account;
        
        MyFavoritesController.getAccountFavorites();
        
        Test.stopTest();
    }  
    
    static testmethod void getContactFavoriteTest(){
        
        Test.startTest();
        Contact contact = new Contact(LastName = 'Test Contact');
        insert contact;
        
        MyFavoritesController.getContactFavorites();
        
        Test.stopTest();
    }
    
    static testmethod void getOpportunityFavoriteTest(){
        
        Test.startTest();
        Opportunity opty = new Opportunity(Name = 'Test Opty', CloseDate = Date.Today(), StageName = 'Prospecting');
        insert opty;
        
        MyFavoritesController.getOpportunityFavorites();
        
        Test.stopTest();
    }

    static testmethod void removeAccountFavoriteTest(){
            User stdUser = UtilForUnitTestDataSetup.getUser(); 
            List<Account> accounts = new List<Account>{};
            List<Account_Favorite__c> accFavs =new List <Account_Favorite__c>{};
            
            Account acc = new Account(Name='test account');
            accounts.add(acc);
            insert accounts;
            
            String accountId;
            accountID = accounts[0].id;
            
            Account_Favorite__c aFav = new Account_Favorite__c(Account__c=accountID,User__c=stdUser.ID);
            accFavs.add(aFav);
            insert accFavs;
            
            string aFavId;
            aFavID = accFavs[0].id;
        
            Test.startTest();
            PageReference pageRef = Page.MyFavorites;
            pageRef.getParameters().put('acctFavoriteId', aFavID);
            Test.setCurrentPage(pageRef);
                MyFavoritesController.removeFromAccountFavorites();
            Test.stopTest();
      }

    static testmethod void removeContactFavoriteTest(){
            User stdUser = UtilForUnitTestDataSetup.getUser(); 
            List<Contact> contacts = new List<Contact>{};
            List<Contact_Favorite__c> conFavs =new List <Contact_Favorite__c>{};
            
            Contact con = new Contact(LastName='test contact');
            contacts.add(con);
            insert contacts;
            
            String contactId;
            contactID = contacts[0].id;
            
            Contact_Favorite__c cFav = new Contact_Favorite__c(Contact__c=contactID,User__c=stdUser.ID);
            conFavs.add(cFav);
            insert conFavs;
            
            string cFavId;
            cFavID = conFavs[0].id;
        
            Test.startTest();
            PageReference pageRef = Page.MyFavorites;
            pageRef.getParameters().put('contactFavoriteId', cFavID);
            Test.setCurrentPage(pageRef);
                MyFavoritesController.removeFromContactFavorites();
            Test.stopTest();
      }
      
    static testmethod void removeOpportunityFavoriteTest(){
            List<Opportunity> opportunities = new List<Opportunity>{};
            List<Opportunity_Favorite__c> oppFavs =new List <Opportunity_Favorite__c>{};
            
            Opportunity opp = new Opportunity(Name = 'Test Opty', CloseDate = Date.Today(), StageName = 'Prospecting');
            opportunities.add(opp);
            insert opportunities;
            
            String opportunityId;
            opportunityID = opportunities[0].id;
            
            Opportunity_Favorite__c oFav = new Opportunity_Favorite__c(Opportunity__c=opportunityID,User__c=UserInfo.getUserId());
            oppFavs.add(oFav);
            insert oppFavs;
            
            string oFavId;
            oFavID = oppFavs[0].id;
           
            Test.startTest();
            PageReference pageRef = Page.MyFavorites;
            pageRef.getParameters().put('optyFavoriteId', oFavID);
            Test.setCurrentPage(pageRef);
                MyFavoritesController.removeFromOpportunityFavorites();
            Test.stopTest();
      }       
      
    static testmethod void removeFromFavoritesTest(){
            User stdUser = UtilForUnitTestDataSetup.getUser(); 
            List<Opportunity> opportunities = new List<Opportunity>{};
            List<Opportunity_Favorite__c> oppFavs =new List <Opportunity_Favorite__c>{};
            
            Opportunity opp = new Opportunity(Name = 'Test Opty', CloseDate = Date.Today(), StageName = 'Prospecting');
            opportunities.add(opp);
            insert opportunities;
            
            String opportunityId;
            opportunityID = opportunities[0].id;
            
            Opportunity_Favorite__c oFav = new Opportunity_Favorite__c(Opportunity__c=opportunityID,User__c=UserInfo.getUserId());
            oppFavs.add(oFav);
            insert oppFavs;
            
            string oFavId;
            oFavID = oppFavs[0].id;
           
            Test.startTest();
            PageReference pageRef = Page.MyFavorites;
            pageRef.getParameters().put('optyFavoriteId', oFavID);
            Test.setCurrentPage(pageRef);
                MyFavoritesController.removeFromFavorites(oFavID);
            Test.stopTest();
      }              
     
}