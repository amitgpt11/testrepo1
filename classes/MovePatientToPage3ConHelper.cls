global class MovePatientToPage3ConHelper {

  @future 
  static public void oppyUpdate(set<id> PatientIds ) {
  
  
  
 System.debug('-----MovePatientToPage3ConHelper --------');
    // Perform long-running code
    List<Opportunity> oppourtunityList = new List<Opportunity>();
    Map<Id, Contact> contactMap = new Map<Id, Contact>();
    Map<Id, Patient__c> PatientMap = new Map<Id, Patient__c>();
    List< Patient__c> patients = new List< Patient__c>();
       Map<id,id> selectdPatientTerriMap = new  Map<id,id>();
    List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>{}; 
    Map<id,id> selectdPatientNewOwnerMap = new  Map<id,id>();   
    
    
                 if(!PatientIds.isEmpty())
                    PatientMap = new Map<Id, Patient__c>([Select Id, Territory_ID__c from Patient__c where Id in :PatientIds ]);
                    patients = new List<Patient__c>([Select Id, Territory_ID__c,Territory_ID__r.Current_TM1_Assignee__c from Patient__c where Id in :PatientIds ]);
    
    
    
              for(Patient__c Pobj : patients){

                selectdPatientTerriMap.put(Pobj.id,Pobj.Territory_ID__c );
                selectdPatientNewOwnerMap.put(Pobj.id,Pobj.Territory_ID__r.Current_TM1_Assignee__c);    

            //patientMap.put();        

            }
    
    
             if(!PatientIds.isEmpty())                  
    {
              for (Opportunity oppo : [SELECT Id, Territory_ID__c, Patient__c, Patient__r.Physician_of_Record__c FROM Opportunity where Patient__c in :PatientIds ]) 
              
              {
                
                   // oppo.Territory_ID__c = PatientMap.get(oppo.Patient__r.Physician_of_Record__c).Territory_ID__c;
                    oppo.Territory_ID__c =  selectdPatientTerriMap.get(oppo.Patient__c);
                    //Code added by amitabh to update owner as per new territory**********
                    if(selectdPatientNewOwnerMap.get(oppo.Patient__c)!= null){
                        oppo.OwnerId = selectdPatientNewOwnerMap.get(oppo.Patient__c);
                    }
                  ////#####  oppo.Territory_ID__c = SelectedItem;
                    oppourtunityList.add(oppo);
                                   
            }   

}           
             system.debug('===oppourtunityList===='+oppourtunityList);    
    
    
        List<Database.SaveResult> oppourtunitySaveResults = Database.update(oppourtunityList, false);
            for (Database.SaveResult result : oppourtunitySaveResults) {
                    //system.debug('====result oppourtunitySaveResults  =='+result );
                if (!result.isSuccess()) {
                    for (Database.Error err : result.getErrors()) {
                        logs.add(new ApplicationLogWrapper(err, result.getId(), 'Error in Update of opportunity Obj On NMD_ObjectTerritoryByContactBatch.execute'));
                    system.debug('***************opprotunity update error'+err);
                    system.debug('***************opprotunity update result'+result);                    
                    }
                }
            }
  }
}