/**
* Controller class for the vf page PatientSharing/SF1
*
* @Author salesforce Services
* @Date 2015-06-08
*/
public with sharing class NMD_PatientSharingExtension extends NMD_ExtensionBase {
	
	final static String ACCESSLEVEL_EDIT = 'Edit';

	final ApexPages.StandardController sc;

	public Boolean isSalesforce1 { get; private set; }

	public NMD_PatientSharingExtension(ApexPages.StandardController sc) {

		this.sc = sc;
		this.isSalesforce1 = ApexPages.currentPage().getParameters().get('isdtp') == 'p1';

		// make sure the value is blank
		Patient__c patient = (Patient__c)sc.getRecord();
		patient.User__c = null;

	}

	public PageReference submit() {

		// fetch patient record
		Patient__c patient = (Patient__c)this.sc.getRecord();

		// create shares
		String result = createShares(patient.Id, patient.User__c);

		PageReference pr;
		if (String.isBlank(result)) {
			// return user to patient detail page
			pr = this.sc.view();
		}
		else {
			// display error
			newErrorMessage(result);
		}
		
		return pr;

	}

	@RemoteAction
	public static Map<String,Object> submitSF1(Id patientId, Id userId) {

		Map<String,Object> response = new Map<String,Object> {
			'success' => false,
			'message' => 'unknown'
		};

		String result = createShares(patientId, userId);
		if (String.isBlank(result)) {
			response.put('success', true);
		}
		else {
			response.put('message', result);
		}

		return response;

	}

	/**
	*  @desc	Remote Action, returns list of matched users per the supplied param
    */
	@RemoteAction
	public static List<Map<String,String>> matchUsers(Id patientId, String param) {

		param = '%' + String.escapeSingleQuotes(param.replaceAll('\\*', '%')) + '%';

		// filter out users that already have a share
		Set<Id> sharedUserIds = new Set<Id>{};
		for (Patient__Share share : [
			select UserOrGroupId
			from Patient__Share
			where ParentId = :patientId
		]) {
			sharedUserIds.add(share.UserOrGroupId);
		}
        
        List<Map<String,String>> users = new List<Map<String,String>>{};
        for (User u : [
        	select Name
        	from User
        	where Name like :param
        	and Id not in :sharedUserIds
        	and IsActive = true
        	order by Name
        ]) {
        	users.add(new Map<String,String> {
                'label' => u.Name,
                'value' => u.Id
            });
        }

        return users;

	}

	/**
	*  @desc	Creates share records for the supplied user, for the Patient record and any related Opportunities and/or Leads
    */
    private static String createShares(Id patientId, Id userId) {

    	Map<Id,Set<Id>> userIdMap = new Map<Id,Set<Id>>{};

    	userIdMap.put(patientId, new Set<Id> { userId });
		new SObjectSharingManager(Patient__Share.getSObjectType(), userIdMap.keySet()).applyNewShares(userIdMap);

		userIdMap.clear();
		for (Opportunity oppty : [
			select Id
			from Opportunity
			where Patient__c = :patientId
		]) {
			userIdMap.put(oppty.Id, new Set<Id> { userId });
		}
		if (!userIdMap.isEmpty()) {
			new SObjectSharingManager(OpportunityShare.getSObjectType(), userIdMap.keySet()).applyNewShares(userIdMap);
		}

		userIdMap.clear();
		for (Lead lead : [
			select Id
			from Lead
			where Patient__c = :patientId
		]) {
			userIdMap.put(lead.Id, new Set<Id> { userId });
		}
		if (!userIdMap.isEmpty()) {
			new SObjectSharingManager(LeadShare.getSObjectType(), userIdMap.keySet()).applyNewShares(userIdMap);
		}

		return '';

	}

}