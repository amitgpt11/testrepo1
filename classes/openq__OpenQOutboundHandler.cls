/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OpenQOutboundHandler {
    global OpenQOutboundHandler() {

    }
    @Future(callout=true)
    global static void checkProfileUpdate() {

    }
    webService static String requestProfileAccess(String contact_id) {
        return null;
    }
    @Future(callout=true)
    global static void retrieveProfileData() {

    }
}
