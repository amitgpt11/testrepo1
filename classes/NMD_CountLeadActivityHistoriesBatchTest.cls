/**
* Test class for NMD_CountLeadActivityHistoriesBatch
*
* @author 	Salesforce services
* @date 	2014-02-03
*/
//SeeAllData needs to be true for the ActivityHistories table to properly return data in the tested batch
@isTest(SeeAllData=true) 
private class NMD_CountLeadActivityHistoriesBatchTest {

	static testMethod void test_CountLeadActivityHistoriesBatch() {

		final Integer HISTLEN = 5;

		NMD_TestDataManager testData = new NMD_TestDataManager();
		List<Lead> leads = new List<Lead>{};
		for(Integer i = 0; i < 10; i++) {
 			leads.add(testData.newLead());
		}
		insert leads;

		//create some closed activities
		List<Task> tasks = new List<Task>{};
		for (Integer i = 0; i < leads.size() * HISTLEN; i++) {
			Lead lead = leads[Math.mod(i, leads.size())];
			tasks.add(testData.newTask(lead.Id, 'Subject' + i, 'Completed', System.today()));
		}
		insert tasks;

		//run the batch
		System.Test.startTest();

			NMD_CountLeadActivityHistoriesBatch.runNow();

		System.Test.stopTest();

		//assert correct number set
		Set<Id> leadIds = new Map<Id,Lead>(leads).keySet();
		for (Lead lead : [
			select Activity_Histories_Count__c 
			from Lead 
			where Id in :leadIds
		]) {
			System.assertEquals(HISTLEN, lead.Activity_Histories_Count__c);
		}

	}
	
}