/*
@CreatedDate     15JUNE2016
@author          Mike Lankfer
@Description     The trigger handler for the OpportunityLineItem standard object.  This class will call all the methods that should fire before and/or after create/update/delete/undelete DML operations.
@Requirement Id  User Story : DRAFT US2511
*/
public class OpportunityLineItemTriggerHandler extends TriggerHandler {
    
    final OpportunityProductCategoryUpdate optyCategoryUpdate = new OpportunityProductCategoryUpdate();
    
    
/*
@CreatedDate     15JUNE2016
@author          Mike Lankfer
@Description     Executes bulk actions before OptyLineItem DML
@Requirement Id  User Story : DRAFT US2511
*/     
    public override void bulkBefore(){
        if (Trigger.isInsert){
        List<OpportunityLineItem> optyLineItems = (List<OpportunityLineItem>) Trigger.new;
        optyCategoryUpdate.updateOptyLineItemProductCategory(optyLineItems);
        }
        
        
    }
    


/*
@CreatedDate     15JUNE2016
@author          Mike Lankfer
@Description     Executes bulk actions after OptyLineItem DML
@Requirement Id  User Story : DRAFT US2511
*/     
    public override void bulkAfter(){
        
        List<OpportunityLineItem> optyLineItems = (List<OpportunityLineItem>) Trigger.new;

      if(Trigger.IsDelete)
        {
            optyLineItems = (List<OpportunityLineItem>) Trigger.Old;
        }
        optyCategoryUpdate.updateOptyHeaderProductCategory(optyLineItems);        
    }

}