/**
* Test class for Campaign object
*
*
* @Author salesforce Services
* @Date 04/12/2016
*/
@isTest
private class CampaignTriggerHandlerTest {
	
	final static NMD_TestDataManager td = new NMD_TestDataManager();
	
	static testMethod void test_createNewMemberStatusRecords_Success() {

		td.setupCampaignMemberStatusSettings();

		Test.startTest();

			Campaign camp = new Campaign(
				Name = 'Test Campaign',
				RecordTypeId = CampaignManager.RECTYPE_FIELD
			);
			insert camp;

		Test.stopTest();

		List<NMD_DefaultCampaignStatus__c> setting = NMD_DefaultCampaignStatus__c.getAll().values();
		List<CampaignMemberStatus> memberStatus = [select Id, Label from CampaignMemberStatus where CampaignId = :camp.Id];
		System.assertEquals(setting.size(), memberStatus.size(), 'Campaign Member Status records were not created for the NMD Campaign');

	}
	
}