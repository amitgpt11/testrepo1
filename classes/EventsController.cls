/**
* Description: This class is used as controller of Events page
*/
public class EventsController extends AuthorizationUtil{
    
    public Community_Event__c objFeaturedEvent {get;set;}
    public List<String> lstMonths    {get;set;}
    public Map<String, List<Community_Event__c>> mapMonthToEvents {get;set;}
    
    /**
        * Description: This method initlizes the list of Community Evnets to display on the Events page
    */
    public override void fetchRequestedData() {
        
        Boston_Scientific_Config__c objBSCustomSetting = Boston_Scientific_Config__c.getValues('Default');
        
        if(objBSCustomSetting != null) {
            
            User objUser = [Select Shared_Community_Division__c 
                   From User 
                   Where Id =: UserInfo.getUserId()];
                   
            String featuredEventFieldAPIName = (objUser.Shared_Community_Division__c == null || objUser.Shared_Community_Division__c == 'Endo') ? 'Boston_AuthHome_Endo_FeaturedEvent_Id__c' : 'Boston_AuthHome_Cardio_FeaturedEvent_Id__c';
            
            if(objBSCustomSetting.get(featuredEventFieldAPIName) != null) {
                
                List<Community_Event__c> lstCommunityEvents = new List<Community_Event__c>([Select Shared_Display_Name__c, Shared_End_Date__c, Shared_Start_Date__c, Shared_Description__c,
                                                                                            Shared_Event_Hyperlink__c, Shared_Image_URL__c
                                                                                            From Community_Event__c 
                                                                                            Where Id =: (Id)objBSCustomSetting.get(featuredEventFieldAPIName)
                                                                                            limit 1]);
                
                if(!lstCommunityEvents.isEmpty()){
                    
                    objFeaturedEvent = lstCommunityEvents[0];
                }
            }
        }
        
        //This list holds the value of months from current month to previous month of next year
        lstMonths = new List<string>();
        
        Map<Integer, String> mapIntToMonth = new Map<Integer, String>{
            1 => 'Boston_Month_January',
            2 => 'Boston_Month_February',
            3 => 'Boston_Month_March',
            4 => 'Boston_Month_April',
            5 => 'Boston_Month_May',
            6 => 'Boston_Month_June',
            7 => 'Boston_Month_July',
            8 => 'Boston_Month_August',
            9 => 'Boston_Month_September',
            10 => 'Boston_Month_October',
            11 => 'Boston_Month_November',
            12 => 'Boston_Month_December'};
                
        Integer intcurrentMonth = Date.Today().month();
        
        for(Integer intCM = intcurrentMonth; intCM <=12; intCM++) {
            
            lstMonths.add(mapIntToMonth.get(intCM));
        }
        
        for(Integer intFmToCm = 1; intFmToCm < intcurrentMonth; intFmToCm++) {
            
            lstMonths.add(mapIntToMonth.get(intFmToCm));
        }
        
       mapMonthToEvents  = new Map<String, List<Community_Event__c>>{
            'Boston_Month_January' => new List<Community_Event__c>(),
            'Boston_Month_February' => new List<Community_Event__c>(),
            'Boston_Month_March' => new List<Community_Event__c>(),
            'Boston_Month_April' => new List<Community_Event__c>(),
            'Boston_Month_May' => new List<Community_Event__c>(),
            'Boston_Month_June' => new List<Community_Event__c>(),
            'Boston_Month_July' => new List<Community_Event__c>(),
            'Boston_Month_August' => new List<Community_Event__c>(),
            'Boston_Month_September' => new List<Community_Event__c>(),
            'Boston_Month_October' => new List<Community_Event__c>(),
            'Boston_Month_November' => new List<Community_Event__c>(),
            'Boston_Month_December' => new List<Community_Event__c>()
            };
        User objUser = [Select Shared_Community_Division__c 
            From User 
            Where Id =: UserInfo.getUserId()];
                
        for(Community_Event__c objCommunityEvent : [SELECT Shared_Event_Quarter__c, Shared_Display_Name__c, Shared_City__c,  Shared_Start_Date__c,
                                                        Shared_End_Date__c, Shared_Event_Hyperlink__c, Shared_Image_URL__c, Shared_Description__c, 
                                                        Shared_Event_Month__c, Shared_Division1__c
                                                    FROM Community_Event__c
                                                    Where Shared_End_Date__c >= Today
                                                    AND Shared_End_Date__c <= NEXT_N_MONTHS:12
                                                    AND Shared_Division1__c =: objUser.Shared_Community_Division__c
                                                    ORDER BY Shared_Start_Date__c ASC]) {
                                                        
                                                        mapMonthToEvents.get('Boston_Month_'+objCommunityEvent.Shared_Event_Month__c).add(objCommunityEvent);
                                                    }
    }
}