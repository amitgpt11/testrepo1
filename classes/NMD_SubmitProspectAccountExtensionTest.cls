/**
* Test class for the page/class SubmitProspectAccount
*
* @author   Salesforce services
* @date     2015-02-03
*/
@isTest(SeeAllData=false)
private class NMD_SubmitProspectAccountExtensionTest {

    static final String PROSPECTACCOUNT = 'ProspectAccount';
    static final NMD_TestDataManager testData = new NMD_TestDataManager();

    @testSetup
    static void setup() {
        //setup test data
        Account acct = testData.newAccount(PROSPECTACCOUNT);
        acct.RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        insert acct;

        List<Contact> contacts = new List<Contact>{};
        for (Integer i = 0; i < 5; i++) {
            contacts.add(testData.newContact(acct.Id));
        }
        insert contacts;

        testData.setupQuincyContact();

    }
    static testMethod void test_SubmitProspectAccount1() {
        //grab data
        Account acct = [select Id from Account where Name = :PROSPECTACCOUNT];

        Seller_Hierarchy__c seller = testData.newSellerHierarchy('TERRITORY');
        insert seller;
        
         //populate required fields
        acct.Sold_to_Street__c = '123 fake street';
        acct.Sold_to_City__c = 'FakeCity';
        acct.Sold_to_State__c = 'FakeState';
        acct.Sold_to_Zip_Postal_Code__c = '12345';
        acct.Sold_to_Country__c = 'USA';
        
        acct.BillingStreet = '123 fake street';
        acct.BillingCity = 'FakeCity';
        acct.BillingState = 'FakeState';
        acct.BillingPostalCode = '12345';
        acct.BillingCountry = 'USA';

        acct.NMD_Territory__c = seller.Id;

        acct.Phone = '123456789';
        acct.Fax = '123456789';
        acct.Classification__c ='GPO';
        acct.Type= 'Sold to';
        update acct;

        //send successfully
        ApexPages.StandardController sc2 = new ApexPages.StandardController(acct);
        NMD_SubmitProspectAccountExtension extension2 = new NMD_SubmitProspectAccountExtension(sc2);

        extension2.submit();
        
    }
    static testMethod void test_SubmitProspectAccount() {

        //grab data
        Account acct = [select Id from Account where Name = :PROSPECTACCOUNT];

        Seller_Hierarchy__c seller = testData.newSellerHierarchy('TERRITORY');
        insert seller;

        
        System.debug('Account id retrieved from quincy before processing in test_SubmitProspectAccount '+acct);
        
        
        //setup page
        PageReference pr = Page.SubmitProspectAccount;
        pr.getParameters().put('id', acct.Id);
        Test.setCurrentPage(pr);
        ApexPages.StandardController sc = new ApexPages.StandardController(acct);
        NMD_SubmitProspectAccountExtension extension = new NMD_SubmitProspectAccountExtension(sc);
		
        
        
        //send and verify error message received
        extension.submit();

        //verify error received
        System.assertequals(Label.NMD_Quincy_Prospect_Account_Required_Fields,ApexPages.GetMessages().get(1).getSummary());

        //verify Sent_to_Quincy
        acct = [select Sent_to_Quincy__c from Account where Id = :acct.Id];
      	System.debug('Account id retrieved from quincy in method test_SubmitProspectAccount'+acct);
        
    }
       
    
}