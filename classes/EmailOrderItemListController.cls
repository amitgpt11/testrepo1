/**
 * Used to provide a list of OrderItem records to be used in an email.
 *      Used for EmailOrderItemList.Component
 *
 * @Author salesforce Services
 * @Date 2015-09-02
 */
public with sharing class EmailOrderItemListController {

    public String orderId { get; set; }
    
    public List<OrderItem> getOrderItems() { 

        return String.isBlank(this.orderId)
            ? new List<OrderItem>()
            : new List<OrderItem>([
                    select
                         Pricebookentry.Product2.Name,
                         Pricebookentry.Product2.ProductCode, 
                         Model_Number__c, 
                         Lot_Number__c, 
                         Serial_Number__c, 
                         Quantity, 
                         Inventory_Location__c,
                         Inventory_Location__r.Name,
                         Sales_Order_Number__c
                    from OrderItem 
                    where OrderId = :orderId
                ]);

    }

}