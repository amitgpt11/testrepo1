@isTest
private class ProductsController_Test {

    private static testMethod void test() {
        
        User usr = Test_DataCreator.createCommunityUser();
        
        system.Test.setCurrentPage(Page.Products);
        ProductsController objProductsController = new ProductsController();
            
        test.startTest();
        
        system.runAs(usr){
            
            objProductsController.init();
            system.assertNotEquals(objProductsController.errorMessage,null);
            system.assertEquals(objProductsController.lstProductWrapper.size(),0);
        }
        
        Test_DataCreator.createProducts();
        List<Product2> objProducts = new List<Product2>([SELECT Id FROM Product2]);
        
        system.runAs(usr){

            ApexPages.currentPage().getParameters().put('productFamily','test');
            objProductsController = new ProductsController();
            objProductsController.init();
            system.assertEquals(objProductsController.errorMessage,null);
            system.assertNotEquals(objProductsController.lstProductWrapper.size(),0);
        }
        
         User objLocaleUser = Test_DataCreator.createCommunityUserWithLocale();
        
        system.runAs(objLocaleUser){
            
            ApexPages.currentPage().getParameters().put('productFamily','test');
            objProductsController = new ProductsController();
            
            Shared_Community_Product_Translation__c objSharedTranslation = Test_DataCreator.createProductTranslationRecord(objProducts[0].Id,objLocaleUser.Shared_Community_Division__c);
            objSharedTranslation.Shared_Language__c = objProductsController.mapLocaleToLanguage.get(objLocaleUser.LanguageLocaleKey);
            update objSharedTranslation;
                           
            
            
            objProductsController.fetchRequestedData();
        }
        
        test.stopTest();
    }
}