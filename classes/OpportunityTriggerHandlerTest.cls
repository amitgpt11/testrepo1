/**
* Test Class for the TriggerHandler methods for Opportunity triggers.       
*
* @Author salesforce Services
* @Date 02/10/2015
*/
@isTest(SeeAllData=false)
private class OpportunityTriggerHandlerTest {

    static final String ACCT_MASTER = 'MasterAccount';
    static final String ACCT_TRIALING = 'TrialingAccount';
    static final String ACCT_PROCEDURE = 'ProcedureAccount';
    static final String OPPTY_NAME_TRIAL = '/-.-NMD SCS Trial';
    static final String OPPTY_NAME_IMPLANT = '/-.-NMD SCS Implant';
    static final String LNAME = 'LName';
    static final String LEAD_SOURCE = 'Care Card';
    static final String OPPORTUNITY_TYPE = 'STANDARD';
    static Opportunity trialOpty = null;
    static Opportunity implantOpty = null;
    static String OptyStatus;
    @testSetup
    static void setup() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        Id pricebookId = Test.getStandardPricebookId();

        Pricebook2 pb = new Pricebook2(Name = 'NMD', Description = 'NMD Products', IsActive = true);
        insert pb;

        Product2 prod0 = td.newProduct('Trial');
        Product2 prod1 = td.newProduct('Implant');
        insert new List<Product2> { prod0, prod1 };

        PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod0.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry standardPrice1 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod1.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        insert new List<PricebookEntry> { standardPrice0, standardPrice1 };

        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod0.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod1.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
        insert new List<PricebookEntry> { pbe0, pbe1 };

        Seller_Hierarchy__c territory = td.newSellerHierarchy();
        territory.ASP_Trial__c = 100;
        territory.ASP_Implant__c = 120;
        insert territory;
        
        Account acctMaster = td.createConsignmentAccount();
        acctMaster.Name = ACCT_MASTER;

        Account acctTrialing = td.newAccount(ACCT_TRIALING);
        acctTrialing.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acctTrialing.Type = 'Ship To';
        acctTrialing.NMD_Territory__c = territory.Id;
        
        Account acctProcedure = td.newAccount(ACCT_PROCEDURE);
        acctProcedure.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        //acctProcedure.Sold_to_Attn__c = 'Attn';
        acctProcedure.Sold_to_City__c = 'City';
        acctProcedure.Sold_to_State__c = 'State';
        acctProcedure.Sold_to_Zip_Postal_Code__c = 'Zip';
        acctProcedure.Sold_to_Country__c = 'Country';
        acctProcedure.Sold_to_Phone__c = 'Phone';
        acctProcedure.NMD_Territory__c = territory.Id;

        insert new List<Account> {
            acctMaster, acctTrialing, acctProcedure
        };
        
     
        Contact trialing = td.newContact(acctTrialing.Id, LNAME);
        trialing.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        Contact prodcedure = td.newContact(acctProcedure.Id, LNAME);
        prodcedure.AccountId = acctMaster.Id;
        prodcedure.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        prodcedure.MailingStreet = '123 Main St';
       
         //Added By Ashish --Start--
        prodcedure.MailingCity = 'test';
        prodcedure.MailingState = 'test';
        prodcedure.MailingPostalCode = 'test';
        prodcedure.MailingCountry = 'test';
        prodcedure.Fax = 'test';
        //Added By Ashish --End--
       
        prodcedure.Territory_ID__c = territory.Id;
        prodcedure.Phone = '5556667788';

        insert new List<Contact> { 
            trialing, prodcedure 
        };
    
        //initializing trail and implant opportunities & assigning same master account to both opportunities.
        trialOpty = td.newOpportunity(acctTrialing.Id, OPPTY_NAME_TRIAL);
            trialOpty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
            trialOpty.Trialing_Account__c = acctTrialing.Id;
            trialOpty.Trialing_Physician__c = trialing.Id;
            trialOpty.Procedure_Account__c = acctProcedure.Id;
            trialOpty.Procedure_Physician__c = prodcedure.Id;
            trialOpty.Territory_ID__c = territory.Id;
            trialOpty.closeDate = System.today().addDays(30);
            trialOpty.LeadSource = LEAD_SOURCE;
            trialOpty.Opportunity_Type__c = OPPORTUNITY_TYPE;
        insert trialOpty;
        
        
        implantOpty = td.newOpportunity(acctTrialing.Id, OPPTY_NAME_IMPLANT);
            implantOpty.Territory_ID__c = territory.Id;
            implantOpty.Opportunity_Type__c = OPPORTUNITY_TYPE;
            implantOpty.closeDate = System.today().addDays(30);
            implantOpty.LeadSource = LEAD_SOURCE;
            implantOpty.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
            implantOpty.Procedure_Account__c = acctProcedure.Id;
            implantOpty.Procedure_Physician__c = prodcedure.Id;
        insert implantOpty;
    

    }
   public static String opportunityTriggerStatus()
    {
      ApexTrigger apx = [Select id,Status,Name From ApexTrigger where Name = 'OpportunityTrigger'];
      return apx.status;
    }
   
    
    static testMethod void test_UpdateAccountId_Standard() {
 
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        Opportunity oppty = [
            select 
                Id,
                AccountId,
                Opportunity_Type__c,
                Trialing_Physician__c,
                Procedure_Account__c
            from Opportunity
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_TRIAL_NEW
        ];

        oppty.Opportunity_Type__c = 'OMG';
        //oppty.Trial_Status__c = 'Successful';
        update oppty;

        //assert correct account is now parent
        Account acctTrialing = [select Id from Account where Name = :ACCT_TRIALING];
        Id acctId = [select AccountId from Opportunity where Id = :oppty.Id].AccountId;
        System.assertEquals(acctTrialing.Id, acctId);

    }
    
    //static testMethod void test_UpdateAccountId_Swap() {

    //  Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

    //  Opportunity oppty = [
    //      select 
    //          Id,
    //          AccountId,
    //          Opportunity_Type__c,
    //          Trialing_Account__c,
    //          Procedure_Account__c
    //      from Opportunity 
    //      where AccountId = :acct.Id
    //  ];

    //  oppty.Opportunity_Type__c = 'BSC Swap';
    //  update oppty;

    //  //assert correct account is now parent
    //  Account acctProdcedure = [select Id from Account where Name = :ACCT_PROCEDURE];
    //  Id acctId = [select AccountId from Opportunity where Id = :oppty.Id].AccountId;
    //  System.assertEquals(acctProdcedure.Id, acctId);

    //}

    //static testMethod void test_UpdateAccountId_Revision() {

    //  Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

    //  Opportunity oppty = [
    //      select 
    //          Id,
    //          AccountId,
    //          RecordTypeId,
    //          Trialing_Account__c,
    //          Procedure_Account__c
    //      from Opportunity 
    //      where AccountId = :acct.Id
    //  ];

    //  oppty.RecordTypeId = OpportunityManager.RECTYPE_EXPLANT;
    //  oppty.I_Acknowledge__c = true;
    //  update oppty;

    //  //assert correct account is now parent
    //  Account acctProdcedure = [select Id from Account where Name = :ACCT_PROCEDURE];
    //  Id acctId = [select AccountId from Opportunity where Id = :oppty.Id].AccountId;
    //  System.assertEquals(acctProdcedure.Id, acctId);

    //}
    
    
    static testMethod void test_OpportunityRevenueUpdates(){
        
    Test.StartTest();    
        NMD_TestDataManager td = new NMD_TestDataManager();
        td.setupNuromodUserRoleSettings();
         User ibu_User = td.newUser('NMD IBU');
        insert new List<User> {ibu_User};
      system.runAs(ibu_User){ 
        Seller_Hierarchy__c territory = td.newSellerHierarchy();
        territory.ASP_Trial__c = 100;
        territory.ASP_Implant__c = 120;
        insert territory;
        
       Account newTrialingAccount = td.newAccount('TrialingAccount');
        newTrialingAccount.Account_Number__c = '11111111';
        newTrialingAccount.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
        newTrialingAccount.Type = 'Ship To';
        
        Account newProcedureAccount = td.newAccount('ProcedureAccount');
        newProcedureAccount.Account_Number__c = '22222222';
        newProcedureAccount.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;

        insert new List<Account> { newTrialingAccount, newProcedureAccount };

        //Create new Contacts to populate Order Surgeon
        Contact newTrialingContact = td.newContact(newTrialingAccount.Id, 'TestLastTrialing');
        newTrialingContact.SAP_ID__c = '111111';
        newTrialingContact.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        Contact newProcedureContact = td.newContact(newProcedureAccount.Id, 'TestLastProcedure');
        newProcedureContact.SAP_ID__c = '222222';
        newProcedureContact.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        insert new List<Contact> { newTrialingContact, newProcedureContact };
        
       
        
        Opportunity testImplant = td.newOpportunity(newTrialingAccount.Id, OPPTY_NAME_IMPLANT);
            testImplant.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
            testImplant.Trialing_Account__c = newTrialingAccount.Id;
            testImplant.Trialing_Physician__c = newTrialingContact.Id;
            testImplant.Procedure_Account__c = newProcedureAccount.Id;
            testImplant.Procedure_Physician__c = newProcedureContact.Id;
            testImplant.Territory_ID__c = territory.Id;
            testImplant.closeDate = System.today().addDays(30);
            testImplant.LeadSource = LEAD_SOURCE;
            testImplant.Opportunity_Type__c = OPPORTUNITY_TYPE;
            
             Opportunity testRevision = td.newOpportunity(newTrialingAccount.Id,'/-.-NMD SCS Revision' );
            testRevision.RecordTypeId = OpportunityManager.RECTYPE_REVISION; 
            testRevision.Procedure_Account__c = newProcedureAccount.Id;
            testRevision.Procedure_Physician__c = newProcedureContact.Id;
            testRevision.Territory_ID__c = territory.Id;
            testRevision.closeDate = System.today().addDays(30);
            testRevision.LeadSource = LEAD_SOURCE;
            testRevision.I_Acknowledge__c= true;
            testRevision.Procedure_Type__c = 'BSC IPG Swap';
            testRevision.Opportunity_Type__c = 'BSC IPG Swap';
            
            Opportunity testTrial = td.newOpportunity(newTrialingAccount.Id, OPPTY_NAME_TRIAL);
            testTrial.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
            testTrial.Trialing_Account__c = newTrialingAccount.Id;
            testTrial.Trialing_Physician__c = newTrialingContact.Id;
            testTrial.Procedure_Account__c = newProcedureAccount.Id;
            testTrial.Procedure_Physician__c = newProcedureContact.Id;
            testTrial.Territory_ID__c = territory.Id;
            testTrial.closeDate = System.today().addDays(30);
            testTrial.LeadSource = LEAD_SOURCE;
            testTrial.Opportunity_Type__c = OPPORTUNITY_TYPE;
        
           OptyStatus = opportunityTriggerStatus(); 
              
            //TRIAL
            insert testTrial;
             if(OptyStatus == 'Active'){
            Opportunity opty1 = [select Trial_Revenue__c from Opportunity where id =:testTrial.id];
            //check the ASP Value 
            System.assertEquals(opty1.Trial_Revenue__c,100);
            //IMPLANT
            insert testImplant;
             opty1 = [select Procedure_Revenue__c from Opportunity where id =:testImplant.id];
            //check the ASP Value 
            System.assertEquals(opty1.Procedure_Revenue__c,120);
            //REVISION
            insert testRevision;
             opty1 = [select Procedure_Revenue__c from Opportunity where id =:testRevision.id];
            //check the ASP Value 
            System.assertEquals(opty1.Procedure_Revenue__c,120);
            //cleanup
            opty1=null;}
        }
            
            
    Test.stopTest();    
    }
    

    static testMethod void test_AutosubmitQuincy_Procedure() {

        new NMD_TestDataManager().setupQuincyContact();
test.startTest();
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        Opportunity oppty = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_IMPLANT
        ];

        // update related records so they will submit
        update new Account(
            Id = oppty.Procedure_Account__c,
            Sold_to_Fax__c = 'Fax'
        );
        update new Contact(
            Id = oppty.Procedure_Physician__c,
            Fax = '5556662233'
        );

        oppty.Scheduled_Procedure_Date_Time__c = System.now().addDays(7);
        update oppty;
test.stopTest();
    }

    static testMethod void test_AutosubmitQuincy_Trial() {
test.startTest();
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        Opportunity oppty = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_TRIAL_NEW
        ];

        // update related records so they will submit
        update new Account(
            Id = oppty.Procedure_Account__c,
            Sold_to_Fax__c = 'Fax'
        );
        update new Contact(
            Id = oppty.Procedure_Physician__c,
            Fax = '5556662233'
        );

        oppty.Scheduled_Trial_Date_Time__c = System.now().addDays(7);
        update oppty;
test.stopTest();
    }

    static testMethod void test_AutoReminder_Procedure() {
        
        
        new NMD_TestDataManager().setupQuincyContact(); //Added by Ashish
        
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        Opportunity oppty = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_IMPLANT
        ];

        oppty.Scheduled_Procedure_Date_Time__c = System.now().addDays(7);
        update oppty;

    }

    static testMethod void test_AutoReminder_Trial() {

        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        Opportunity oppty = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_TRIAL_NEW
        ];

        oppty.Scheduled_Trial_Date_Time__c = System.now().addDays(7);
        update oppty;

    }

    static testmethod void Test_preventOpportunityWithOrderDelete(){
        NMD_TestDataManager td = new NMD_TestDataManager();
        td.setupNuromodUserRoleSettings();
        Test.startTest();
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        Opportunity oppty = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
            and RecordTypeId = :OpportunityManager.RECTYPE_TRIAL_NEW
        ];

        User fieldRep = td.newUser('NMD Field Rep');
        User sysAdmin = td.newUser('System Administrator');
        
        insert new List <User>{
            fieldRep, sysAdmin
        };

        //insert fieldRep;

        Order order = new Order(
            //Pricebook2Id = pb0.Id,
            AccountId = oppty.AccountId,
            OpportunityId = oppty.Id,
            EffectiveDate = System.today(),
            Surgery_Date__c = System.today() - 1,
            Status = 'Draft',
            RecordTypeId = OrderManager.RECTYPE_PATIENT,
            OwnerId = fieldRep.Id
        );
        insert order;

        oppty.OwnerId = fieldRep.Id;
        update oppty;

        

            //test for error with NMD profile
            System.runAs(fieldRep){

                boolean errorCaught = false;
                try{
                    delete oppty;
                } catch (Exception e){
                    errorCaught = e.getMessage().contains(Label.Prevent_Delete_Opportunity_With_Orders) ? true : false;
                }
               OptyStatus = opportunityTriggerStatus(); 
               if(OptyStatus == 'Active'){
                System.assertEquals(true, errorCaught, 'NMD User should receive error for deleting an Opportunity with an Order.  No error received!');
               }
            }

            //test for no error with api user/admin
            System.runAs(sysAdmin){
                boolean adminError = false;
                try{
                    delete oppty;
                } catch (Exception e){
                    adminError = e.getMessage().contains(Label.Prevent_Delete_Opportunity_With_Orders) ? true : false;
                }
                System.assertEquals(false, adminError, 'Admin should not receive error for deleting an Opportunity with orders.');
            }

        Test.stopTest();

    }

    static testMethod void test_UpdateOrderAccountOrContact(){
    
        NMD_TestDataManager td = new NMD_TestDataManager();
         Order order1;
        td.setupNuromodUserRoleSettings();
Test.startTest();
        Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];

        List<Opportunity> opptys = [
            select 
                Id,
                AccountId,
                RecordTypeId,
                Trialing_Account__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Procedure_Physician__c
            from Opportunity 
            where AccountId = :acct.Id
        ];
        
        system.debug(opptys+'size-->@@');
        for(Opportunity op: opptys){
            if(op.RecordTypeId == OpportunityManager.RECTYPE_TRIAL_NEW)
                trialOpty = op;
            else
                implantOpty = op;
        }
    SYSTEM.DEBUG(trialOpty+'trialOpty-->'+implantOpty+'implantOpty-->');
        if(trialOpty!=null || implantOpty !=null){
         order1 = new Order(
            //Pricebook2Id = pb0.Id,
            AccountId = trialOpty.AccountId,
            OpportunityId = trialOpty.Id,
            EffectiveDate = System.today(),
            Surgery_Date__c = System.today() - 1,
            Status = 'Draft',
            RecordTypeId = OrderManager.RECTYPE_PATIENT,
            Procedure_Type__c = 'Trial',
            Stage__c = 'New'
        );
        Order order2 = new Order(
            //Pricebook2Id = pb0.Id,
            AccountId = implantOpty.AccountId,
            OpportunityId = implantOpty.Id,
            EffectiveDate = System.today(),
            Surgery_Date__c = System.today() - 1,
            Status = 'Draft',
            RecordTypeId = OrderManager.RECTYPE_PATIENT,
            Procedure_Type__c = 'Implant',
            Stage__c = 'New'
        );
        insert new List<Order>{ order1, order2 };
        
        //Create new Contacts to populate Order Account
        Account newTrialingAccount = td.newAccount('TrialingAccount');
        newTrialingAccount.Account_Number__c = '11111111';
        newTrialingAccount.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        newTrialingAccount.Type = 'Ship To';
        
        Account newProcedureAccount = td.newAccount('ProcedureAccount');
        newProcedureAccount.Account_Number__c = '22222222';
        newProcedureAccount.RecordTypeId = AccountManager.RECTYPE_PROSPECT;

        insert new List<Account> { newTrialingAccount, newProcedureAccount };

        //Create new Contacts to populate Order Surgeon
        Contact newTrialingContact = td.newContact(newTrialingAccount.Id, 'TestLastTrialing');
        newTrialingContact.SAP_ID__c = '111111';
        newTrialingContact.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        Contact newProcedureContact = td.newContact(newProcedureAccount.Id, 'TestLastProcedure');
        newProcedureContact.SAP_ID__c = '222222';
        newProcedureContact.RecordTypeId = ContactManager.RECTYPE_PROSPECT;

        insert new List<Contact> { newTrialingContact, newProcedureContact };
        
            trialOpty.Trialing_Account__c = newTrialingAccount.Id; 
            trialOpty.Trialing_Physician__c = newTrialingContact.Id;
            trialOpty.Procedure_Account__c = newProcedureAccount.Id;
            trialOpty.Procedure_Physician__c = newProcedureContact.Id;
            
             
            implantOpty.Procedure_Account__c = newProcedureAccount.Id;
            implantOpty.Procedure_Physician__c = newProcedureContact.Id;
            
            
            System.debug('Order Account Id BEFORE :'+Order1.accountId);
            System.debug('trialOpty Account Id BEFORE :'+trialOpty.Trialing_Account__c);
            System.debug('implantOpty Account Id BEFORE :'+implantOpty.Procedure_Account__c);
            //update new List<Opportunity> {trialOpty, implantOpty};
            update trialOpty;
            System.debug('Trial Opty Account ID: '+trialOpty.Trialing_Account__c +' & Order Account Id AFTER :'+Order1.accountId);
            update implantOpty;
            System.debug('implantOpty Account Id after:'+implantOpty.Procedure_Account__c);
            //order1 = [SELECT Id, AccountId, Surgeon__c FROM Order WHERE Procedure_Type__c =: 'Trial'];
            System.assertEquals(trialOpty.AccountId, order1.AccountId, 'Order1 AccountId not set by trigger.');
            //System.assertEquals(newTrialingContact.Id, order1.Surgeon__c, 'Order1 Surgeon__c not set by trigger.');

            order2 = [SELECT Id, AccountId, Surgeon__c FROM Order WHERE Id = :order2.Id];
           // System.assertEquals(implantOpty.Id, order2.AccountId, 'Order2 AccountId not set by trigger.');
            //System.assertEquals(newProcedureContact.Id, order2.Surgeon__c, 'Order2 Surgeon__c not set by trigger.');

        Test.stopTest();
        }
    }
    
    //Added By Ashish --Start--
    static testMethod void test_CreateAccountOrContact(){
        
        NMD_TestDataManager td = new NMD_TestDataManager();
            
        Account newTrialingAccount = td.newAccount('TrialingAccount');
        newTrialingAccount.Account_Number__c = '11111111';
        newTrialingAccount.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
        newTrialingAccount.Type = 'Ship To';
        
        Account newProcedureAccount = td.newAccount('ProcedureAccount');
        newProcedureAccount.Account_Number__c = '22222222';
        newProcedureAccount.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;

        insert new List<Account> { newTrialingAccount, newProcedureAccount };
        
        Seller_Hierarchy__c territory = td.newSellerHierarchy();
        territory.ASP_Trial__c = 100;
        territory.ASP_Implant__c = 120;
        insert territory;

        Contact newProcedureContact = td.newContact(newProcedureAccount.Id, 'TestLastProcedure');
        newProcedureContact.SAP_ID__c = '222222';
        newProcedureContact.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
      /*  newProcedureContact.MailingStreet = 'test';
        newProcedureContact.MailingCity = 'test';
        newProcedureContact.MailingState = 'test';
        newProcedureContact.MailingPostalCode = 'test';
        newProcedureContact.MailingCountry = 'test';
        newProcedureContact.Phone = '123456789';
        newProcedureContact.Fax = 'test';
        newProcedureContact.Territory_ID__c = territory.id;*/
        insert newProcedureContact;
        
         test.startTest();   
         list<opportunity> lstInsertopp = new list<opportunity>();
            Opportunity testRevision = td.newOpportunity(newTrialingAccount.Id,'/-.-NMD SCS Revision' );
            testRevision.RecordTypeId = OpportunityManager.RECTYPE_REVISION; 
            testRevision.Procedure_Account__c = newProcedureAccount.Id;
            testRevision.Procedure_Physician__c = newProcedureContact.Id;
            testRevision.Territory_ID__c = territory.Id;
            testRevision.closeDate = System.today().addDays(30);
            testRevision.LeadSource = LEAD_SOURCE;
            testRevision.I_Acknowledge__c= true;
            testRevision.Procedure_Type__c = 'BSC IPG Swap';
            testRevision.Opportunity_Type__c = 'BSC IPG Swap'; 
            lstInsertopp.add(testRevision);
            
            Opportunity testExp = td.newOpportunity(newTrialingAccount.Id,'/-.-NMD SCS Revision' );
            testExp.RecordTypeId = OpportunityManager.RECTYPE_EXPLANT; 
            testExp.Procedure_Account__c = newProcedureAccount.Id;
            testExp.Procedure_Physician__c = newProcedureContact.Id;
            testExp.Territory_ID__c = territory.Id;
            testExp.closeDate = System.today().addDays(30);
            testExp.LeadSource = LEAD_SOURCE;
            testExp.I_Acknowledge__c= true;
            testExp.Procedure_Type__c = 'BSC IPG Swap';
            testExp.Opportunity_Type__c = 'BSC IPG Swap'; 
            
            lstInsertopp.add(testExp);
            
            insert lstInsertopp;
            
            
            OpportunityManager manager = OpportunityManager.getInstance();
            manager.createOpportunityProductFromOrder(testExp);
            manager.createOpportunityProductFromOrder(testRevision);
            test.StopTest();
    }
    //Added By Ashish --End--
    
    //Chris: I can't find a way to get passed this.productsCommitted = true returning out of the 
    //      commitOpportunityProducts method.  It seems to always be set to true even before my update
    //static testMethod void test_commitOpportunityProducts(){
    //  Account acct = [select Id from Account where Name = :ACCT_TRIALING limit 1];
    //
    //  Opportunity oppty = [
    //      select 
    //          Id,
    //          AccountId,
    //          RecordTypeId,
    //          Trialing_Account__c,
    //          Trialing_Physician__c,
    //          Procedure_Account__c,
    //          Procedure_Physician__c
    //      from Opportunity 
    //      where AccountId = :acct.Id
    //  ];
    //
    //  Test.startTest();
    //
    //      oppty.Opportunity_Type__c = 'Standard';
    //      oppty.Trial_Status__c = 'Successful';
    //      oppty.Scheduled_Procedure_Date_Time__c = System.today();
    //      update oppty;
    //
    //  Test.stopTest();
    //
    //}
}