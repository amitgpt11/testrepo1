/*
 @CreatedDate     8 MAR 2016                                                                   
 @author         Sipra-Accenture
 @Description    This  is the test class checks for the territories on account and users, compares the territories , and assign to the opportunity records while Updation of the record.

*/


@isTest(SeeAllData=False)
private class AssignTerritorytoOpportunityHandlerTest{

    Private Set<Id> accountOnOpportunity = new Set<Id>();
    public boolean isAccountChangeonOpty =false;
    public Id activeModelId = null;
    public Set<Id> ownerOnOpportunity = new Set<Id>();    
    public List<Id> territoryOnOpportunity = new List<Id>();
    public List<Id> matchedTerritories = new List<Id>();  
    //public list<Account> alist = new List<Account>();
      
    
    @TestSetup
    public static void setupMockData(){
        User sysAdmin= UtilForUnitTestDataSetup.getUser();
        
        System.runAs(sysAdmin){
        
            Account a1 = UtilForUnitTestDataSetup.newAccount('ACCTNAME1');
            Account a2 = UtilForUnitTestDataSetup.newAccount('ACCTNAME2');
            list<Account> alist= new List<Account>{a1, a2};
            insert alist;
            
            //Prepare Users as Owner to Opportunity
            User u1 = UtilForUnitTestDataSetup.newUser();
            u1.Username = 'UserNameC@boston.com';
            u1.IsActive= True;
            User u2 = UtilForUnitTestDataSetup.newUser();
            u2.Username = 'UserNameR@boston.com';
            u2.IsActive= True;
             List<User> ulist=new list<User>{u1,u2};
            insert ulist;
            
            
            Id urId = [SELECT DeveloperName,Id,Name FROM UserRole WHERE Name = 'US Endo - Northeast PULM Region' limit 1].Id;
            
            User userC = UtilForUnitTestDataSetup.newUser('US ENDO User');
            userC.Username = 'EndoUserC1@xyz.com';
            userC.UserRoleId = urId;
            insert userC;
            
            List<Territory2Type> Ttype = UtilForUnitTestDataSetup.newTerritory2Type();
            Id Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
            
            
            Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('TO1',Territory2ModelIDActive, Ttype[0].Id); //Ttype[0].Id
            
            Territory2 t2 = UtilForUnitTestDataSetup.newTerritory2('TO2',Territory2ModelIDActive, Ttype[0].Id); //0M5P00000008OIeKAM
            
            Territory2 t3 = UtilForUnitTestDataSetup.newTerritory2('900000A',Territory2ModelIDActive, Ttype[0].Id); //For ENDO GI
            t3.ParentTerritory2Id = [Select Id FROM Territory2 where Name LIKE : 'EN0%' limit 1].Id; //Cannot create another territory which starts from EN0 because it already exists. And we need this territory only, since custom label includes name of this territory.
            Territory2 t4 = UtilForUnitTestDataSetup.newTerritory2('500000B',Territory2ModelIDActive, Ttype[0].Id); //For ENDO PULM
            t4.ParentTerritory2Id = [Select Id FROM Territory2 where Name LIKE : 'BT%' limit 1].Id;
            List<Territory2> trList=new List<Territory2> {t1,t2,t3,t4 };
            insert trList;
            
            ObjectTerritory2Association OTAsso =UtilForUnitTestDataSetup.newObjectTerritory2Association(alist[0].Id, trList[0].Id);
            ObjectTerritory2Association OTAsso1 =UtilForUnitTestDataSetup.newObjectTerritory2Association(alist[1].Id, trList[1].Id);
            ObjectTerritory2Association OTAsso2 =UtilForUnitTestDataSetup.newObjectTerritory2Association(alist[0].Id, trList[1].Id);
            ObjectTerritory2Association OTAsso3 =UtilForUnitTestDataSetup.newObjectTerritory2Association(alist[1].Id, trList[0].Id);
            insert new List<ObjectTerritory2Association> {OTAsso, OTAsso1,OTAsso2,OTAsso3};
            
            UserTerritory2Association UTAsso = UtilForUnitTestDataSetup.newUserTerritory2Association(trList[0].Id, ulist[0].Id);
            
            UserTerritory2Association UTAsso1 = UtilForUnitTestDataSetup.newUserTerritory2Association(trList[1].Id, ulist[1].Id);
            UserTerritory2Association UTAsso2 = UtilForUnitTestDataSetup.newUserTerritory2Association(trList[0].Id, ulist[1].Id);
            UserTerritory2Association UTAsso3 = UtilForUnitTestDataSetup.newUserTerritory2Association(trList[1].Id, ulist[0].Id);
            
            insert new List<UserTerritory2Association> {UTAsso, UTAsso1 };
            
            Opportunity oppty1 =UtilForUnitTestDataSetup.newOpportunity(alist[0].Id, 'Opty1');
            oppty1.territory2Id =trList[0].Id;
            oppty1.OwnerId =ulist[0].Id;
            insert new List<Opportunity> {oppty1};
               
        }
    }
    
    static TestMethod void TestAssignTerritorytoOpportunityHandler_OwnerAndAccountChange(){
        String Name ='To' +'%';
        String DeveloperName ='Dev' + '%';
        List<Territory2> t2list =[Select Id from Territory2 Where (Name LIKE : Name OR DeveloperName LIKE :DeveloperName) ];
        List<User> ulist=[Select Id From User Where Username Like : '%boston%'];
        List<Account> alist=[Select Id from Account Where Name Like : 'ACCTNAME%'];
        
        
       List<ObjectTerritory2Association> OTAssoList = [Select Id,  ObjectId, Territory2Id From ObjectTerritory2Association Where ( AssociationCause ='Territory2Manual' And ObjectId In :alist)];
        List<UserTerritory2Association> UTAssoList = [Select Id,  Territory2Id, UserId From  UserTerritory2Association Where RoleInTerritory2= 'Reviewer'];
        
        List<Opportunity> oldOpptyList = [
            select 
                Id,
                AccountId,
                OwnerId,
                Opportunity_Type__c,
                Trialing_Physician__c,
                Procedure_Account__c
            from Opportunity 
            where AccountId = :alist[0].Id
        
        ];


              //update the oppty list
              List<Id> oldOptyIds = new List<Id>();
              List<Opportunity> updatedOptyList = new List<Opportunity>();
              for( Opportunity o: oldOpptyList ){
                  if(o.AccountId == alist[0].Id){             
                      
                      o.AccountId = alist[1].Id;
                      o.OwnerId =ulist[1].Id;
                      
                      }else
                      {
                          o.AccountId = alist[0].Id;
                          o.OwnerId =ulist[1].Id;  
                      }
                      oldOptyIds.add(o.Id);
                      updatedOptyList .add(o);
                }
            Update  updatedOptyList;
            
            //System.assertEquals('0MIP000000000LOOAY',updatedOptyList[0].territory2Id);

    }
static testMethod void test_ENDOOppty_1(){
    Map<String,Schema.RecordTypeInfo> RECTYPES_Oppty = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
    Id RECTYPE_ENDO = RECTYPES_Oppty.get('Endo New Disposable Business').getRecordTypeId();
    String NameGI ='900' +'%'+'A';
    String NamePULM ='500' +'%'+'B';
    Territory2 GITerr = [Select Id,Name,ParentTerritory2.Name from Territory2 Where Name LIKE : NameGI limit 1];
    Territory2 PULMTerr = [Select Id,Name,ParentTerritory2.Name from Territory2 Where Name LIKE : NamePULM limit 1];
   
    list<Opportunity> opptyLst = new list<Opportunity>(); 
     list<Account> acctlst= new list<Account>();
      User userC = [Select name,Id,Profile.name,UserRole.Name From User where UserRole.Name Like : 'US Endo - Northeast%'  limit 1];
    system.debug('userC ....'+userC+'--'+userC.UserRoleId);
     
            Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
            acctlst.add(acct1);
            insert acctlst;
            
            ObjectTerritory2Association OTAsso =UtilForUnitTestDataSetup.newObjectTerritory2Association(acctlst[0].Id, GITerr.Id);
            ObjectTerritory2Association OTAsso1 =UtilForUnitTestDataSetup.newObjectTerritory2Association(acctlst[0].Id, PULMTerr.Id);      
            insert new List<ObjectTerritory2Association> {OTAsso, OTAsso1};
            
            List<Account> alist=[Select Id from Account Where Name Like : 'ACCTNAME%'];
            for(integer i=0;i<10;i++){
                Opportunity oppty = new Opportunity(
                    Name = 'Test Oppty'+'1',
                    AccountId = acctlst[0].Id,
                    OwnerId = userC.Id,
                    CloseDate = system.today()+30,
                    recordtypeId = RECTYPE_ENDO,
                    StageName = 'Target Prospect'
                );
                opptyLst.add(oppty);
            }
           
                 UtilForUnitTestDataSetup.createCustomSettingFrENDO();                 
                insert opptyLst;
          
    }
    
    static testMethod void test_ENDOOppty_2(){
        Map<String,Schema.RecordTypeInfo> RECTYPES_Oppty = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
        Id RECTYPE_ENDO = RECTYPES_Oppty.get('Endo New Disposable Business').getRecordTypeId();
        //User userC;
        list<Opportunity> opptyLst = new list<Opportunity>(); 
        list<Account> acctlst= new list<Account>();
            
         User userC = [Select name,Id,Profile.name,UserRole.Name From User where UserRole.Name Like : 'US Endo - Northeast%'  limit 1];
         
    
            list<Account> alist=[Select Id from Account Where Name Like : 'ACCTNAME%'];
           
            
            for(integer i=0;i<10;i++){
                Opportunity oppty = new Opportunity(
                    Name = 'Test Oppty'+'1',
                    AccountId = alist[0].Id,
                    OwnerId = userC.Id,
                    CloseDate = system.today()+30,
                    recordtypeId = RECTYPE_ENDO,
                    StageName = 'Target Prospect'
                );
                opptyLst.add(oppty);
            }
            
                 UtilForUnitTestDataSetup.createCustomSettingFrENDO();                 
                insert opptyLst;
    }
    
    }