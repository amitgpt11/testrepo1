public class ProductsPageController {

    public List<Product2> lstProducts {get; set;}
    public String productFamily {get; set;}
    
    /**
      Description: Method to check authenticated user
    */
    public PageReference init(){
        
        if(UserInfo.getUserType() == 'Guest'){
            
            return new PageReference('/apex/UnAuthenticatedHomePage');
        }
        
        fetchProducts();
        
        return null;
    }
    
    /**
     * Description: method to query product information
    */
    private void fetchProducts() {
        
        productFamily = ApexPages.currentPage().getParameters().get('productFamily');
        lstProducts = new List<Product2>();
        
        List<User> lstCurrentUser = new List<User>([SELECT Shared_Community_Division__c
                FROM User
                WHERE Id =: UserInfo.getUserId()
        ]);
        
        if(productFamily != null && productFamily != '' && !lstCurrentUser.isEmpty()) {
            
            lstProducts = [SELECT Id, Product_Label__c, Family, Shared_Product_Image_URL__c
                FROM Product2
                WHERE Family =: productFamily
                AND Shared_Top_Level_Parent__c = True
                AND Division__c =: lstCurrentUser[0].Shared_Community_Division__c
                ];
        }
        
        system.debug('#'+lstProducts);
    }
}