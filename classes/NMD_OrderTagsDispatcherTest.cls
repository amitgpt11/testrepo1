@IsTest(SeeAllData=false)
private class NMD_OrderTagsDispatcherTest {

	final static NMD_TestDataManager td = new NMD_TestDataManager();
	final static RestDispatcherV1 dispatcher = new RestDispatcherV1(null);
	final static String ACCTNAME = 'ACCTNAME';

	static {
		RestContext.response = new RestResponse();
	}
	
	@testSetup
	static void setup() {
		td.setupNuromodUserRoleSettings();

		Account acct = td.createConsignmentAccount();
		acct.Name = ACCTNAME;
		insert acct;

		Opportunity oppty = td.newOpportunity(acct.Id);
		insert oppty;

		Order order = td.newOrder(acct.Id, oppty.Id);
		insert order;

	}

	static testMethod void test_GetUriMapping() {

		NMD_OrderTagsDispatcher agent = new NMD_OrderTagsDispatcher();
		String uri = agent.getURIMapping();

	}

	static testMethod void test_OrderTagsDispatcher_Get() {

		Map<String,String> parameters = new Map<String,String>();
		NMD_OrderTagsDispatcher agent = new NMD_OrderTagsDispatcher();
		agent.execute(dispatcher, parameters, null);

	}

	static testMethod void test_OrderTagsDispatcher_BadId1() {

		Map<String,String> parameters = new Map<String,String> {
			NMD_OrderTagsDispatcher.KEY_ORDERID => 'foo'
		};
		NMD_OrderTagsDispatcher agent = new NMD_OrderTagsDispatcher();
		agent.execute(dispatcher, parameters, null);

	}

	static testMethod void test_OrderTagsDispatcher_BadId2() {

		Order ord = [select AccountId from Order where Account.Name = :ACCTNAME];
		Map<String,String> parameters = new Map<String,String> {
			NMD_OrderTagsDispatcher.KEY_ORDERID => ord.AccountId
		};
		NMD_OrderTagsDispatcher agent = new NMD_OrderTagsDispatcher();
		agent.execute(dispatcher, parameters, null);

	}

	static testMethod void test_OrderTagsDispatcher_Toggle() {

		Order ord = [select Id from Order where Account.Name = :ACCTNAME];
		Map<String,String> parameters = new Map<String,String> {
			NMD_OrderTagsDispatcher.KEY_ORDERID => ord.Id
		};
		NMD_OrderTagsDispatcher agent = new NMD_OrderTagsDispatcher();
		agent.execute(dispatcher, parameters, null);

		agent.execute(dispatcher, parameters, null);

	}

}