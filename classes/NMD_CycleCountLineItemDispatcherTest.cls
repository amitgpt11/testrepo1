@IsTest(SeeAllData=false)
private class NMD_CycleCountLineItemDispatcherTest {
	
	final static RestDispatcherV1 dispatcher = new RestDispatcherV1(null);
	final static String CCRANAME = 'CCRANAME';

	static {
		RestContext.response = new RestResponse();
	}

	@TestSetup
	static void setup() {

		Cycle_Count_Response_Analysis__c ccra = new Cycle_Count_Response_Analysis__c(
			Name = CCRANAME,
			Status__c = 'Scanning'
		);
		insert ccra;

	}

	static testMethod void test_CycleCountLineItemDispatcher_GetURIMapping() {

		NMD_CycleCountLineItemDispatcher handler = new NMD_CycleCountLineItemDispatcher();
		String uri = handler.getURIMapping();

	}

	static testMethod void test_CycleCountLineItemDispatcher_NoAction() {

		Map<String,String> parameters = new Map<String,String>();
		NMD_CycleCountLineItemDispatcher handler = new NMD_CycleCountLineItemDispatcher();

		handler.execute(dispatcher, parameters, null);

	}

	static testMethod void test_CycleCountLineItemDispatcher_MissingBody() {

		Map<String,String> parameters = new Map<String,String> {
			NMD_CycleCountLineItemDispatcher.KEY_ACTION => 'insert'
		};
		NMD_CycleCountLineItemDispatcher handler = new NMD_CycleCountLineItemDispatcher();

		handler.execute(dispatcher, parameters, null);

	}

	static testMethod void test_CycleCountLineItemDispatcher_Dml() {

		Cycle_Count_Response_Analysis__c ccra = [select Id from Cycle_Count_Response_Analysis__c where Name = :CCRANAME];

		Map<String,String> parameters = new Map<String,String> {
			NMD_CycleCountLineItemDispatcher.KEY_ACTION => 'insert'
		};
		NMD_CycleCountLineItemDispatcher handler = new NMD_CycleCountLineItemDispatcher();

		List<Cycle_Count_Line_Item__c> items = new List<Cycle_Count_Line_Item__c> {
			new Cycle_Count_Line_Item__c(
				Cycle_Count_Response_Analysis__c = ccra.Id
			)
		};

		handler.execute(dispatcher, parameters, Blob.valueOf(JSON.serialize(items)));

		parameters.put(NMD_CycleCountLineItemDispatcher.KEY_ACTION, 'upsert');
		handler.execute(dispatcher, parameters, Blob.valueOf(JSON.serialize(items)));

		items = [select Id from Cycle_Count_Line_Item__c where Cycle_Count_Response_Analysis__c = :ccra.Id];
		parameters.put(NMD_CycleCountLineItemDispatcher.KEY_ACTION, 'update');
		handler.execute(dispatcher, parameters, Blob.valueOf(JSON.serialize(items)));

	}

	static testMethod void test_CycleCountLineItemDispatcher_Verify() {

		Cycle_Count_Response_Analysis__c ccra = [select Id from Cycle_Count_Response_Analysis__c where Name = :CCRANAME];

		insert new Cycle_Count_Line_Item__c(
			Cycle_Count_Response_Analysis__c = ccra.Id
		);

		Map<String,String> parameters = new Map<String,String> {
			NMD_CycleCountLineItemDispatcher.KEY_ACTION => 'verify',
			NMD_CycleCountLineItemDispatcher.PARAM_CCRAID => ccra.Id
		};
		NMD_CycleCountLineItemDispatcher handler = new NMD_CycleCountLineItemDispatcher();

		handler.execute(dispatcher, parameters, null);

	}

}