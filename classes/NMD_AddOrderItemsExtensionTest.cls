/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class NMD_AddOrderItemsExtensionTest {

	final static String ACCTNAME = 'Account Name';
	
	@TestSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		td.setupNuromodUserRoleSettings();

		Seller_Hierarchy__c territory = td.newSellerHierarchy();
		territory.Current_TM1_Assignee__c = UserInfo.getUserId();
		insert territory;

		Account acct = td.createConsignmentAccount();
		acct.Name = ACCTNAME;
		insert acct;

        Pricebook2 pb = new Pricebook2(Name = 'NMD', Description = 'NMD Products', IsActive = true);
    	insert pb;

    	Pricebook2 acctPb = new Pricebook2(Name = 'Account Pricebook', Description = 'Account Products', IsActive = true);
    	acctPb.Account__c = acct.Id;
    	insert acctPb;

    	Product2 prod = td.newProduct('A Product');
    	insert prod;

        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
    	PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false, Start_Date__c = System.today().addDays(-1), End_Date__c = System.today().addDays(1));
    	PricebookEntry accountPbe = new PricebookEntry(Pricebook2Id = acctPb.Id, Product2Id = prod.Id, UnitPrice = 4, IsActive = true, UseStandardPrice = false, Start_Date__c = System.today().addDays(-1), End_Date__c = System.today().addDays(1));
    	insert new List<PricebookEntry> { standardPrice, pbe, accountPbe };

		Opportunity oppty = td.newOpportunity(acct.Id);
		oppty.Territory_ID__c = territory.Id;
		insert oppty;

		Inventory_Data__c invData = new Inventory_Data__c(
			Account__c = acct.Id,
			RecordTypeId = Schema.SObjectType.Inventory_Data__c.getRecordTypeInfosByName().get('Consignment Inventory').getRecordTypeId()
		);
		insert invData;

		Inventory_Location__c invLoc = new Inventory_Location__c(
			Inventory_Data_ID__c = invData.Id
		);
		insert invLoc;
		
		// a process builder will create the location
		//Inventory_Location__c invLoc = [select Id from Inventory_Location__c where Inventory_Data_ID__c = :invData.Id];

		Inventory_Item__c invItem = new Inventory_Item__c(
			Inventory_Location__c = invLoc.Id,
			Model_Number__c = prod.Id
		);
		insert invItem;

		Inventory_Transaction__c invTran = new Inventory_Transaction__c(
			RecordTypeId = Schema.SObjectType.Inventory_Transaction__c.getRecordTypeInfosByName().get('NMD Inventory Transaction').getRecordTypeId(),
			Inventory_Item__c = invItem.Id,
			Quantity__c = 10
		);
		insert invTran;

		Order ord = new Order(
			AccountId = acct.Id,
			OpportunityId = oppty.Id,
			EffectiveDate = System.today(),
			Status = 'Draft',
			Pricebook2Id = pb.Id,
			Shipping_Location__c = invLoc.Id,
			RecordTypeId = OrderManager.RECTYPE_PATIENT,
			Surgery_Date__c = System.today().addDays(3)
		);
		insert ord;

	}

	static testMethod void test_AddOrderItems() {

		Order ord = [
			select 
				Pricebook2Id, 
				CurrencyIsoCode, 
				RecordTypeId, 
				Status,
				AccountId,
				Shipping_Location__r.Inventory_Data_ID__r.Account__c,
				Surgery_Date__c
			from Order 
			where Account.Name = :ACCTNAME
		];

		PageReference pr = Page.AddOrderItems;
		pr.getParameters().put('id', ord.Id);
		Test.setCurrentPage(pr);

		NMD_AddOrderItemsExtension ext = new NMD_AddOrderItemsExtension(new ApexPages.StandardController(ord));

		Integer totalProducts = ext.totalProducts;

		ext.firstPage();
		ext.prevPage();
		ext.nextPage();
		ext.lastPage();

		ext.saveSelected();

		ext.products[0].selected = true;
		ext.products[0].item.Quantity = null;
		ext.saveSelected();
		ext.products[0].item.Quantity = 1.5;
		ext.saveSelected();
		ext.products[0].item.Quantity = 10000;
		ext.saveSelected();
		ext.products[0].item.Quantity = 0;
		ext.saveSelected();
		ext.products[0].item.Quantity = 1;
		ext.saveSelected();
		ext.searchStr = 'A Product';

		ext.updateProductList();
		ext.products[0].selected = true;
		ext.saveSelectedMore();

	}

	static testMethod void test_AddOrderItems_ErrOnActivatedOrder() {
		Account acct = [SELECT Id FROM Account LIMIT 1];
		Opportunity oppty = [SELECT Id FROM Opportunity LIMIT 1];
		Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE Name = 'NMD' LIMIT 1];

		Order ord = new Order(
			AccountId = acct.Id,
			OpportunityId = oppty.Id,
			EffectiveDate = System.today(),
			Status = 'Draft',
			Pricebook2Id = pb.Id,
			RecordTypeId = OrderManager.RECTYPE_PATIENT
		);
		insert ord;

		OrderItem oi = new OrderItem(
			OrderId = ord.Id,
			PricebookEntryId = [SELECT Id FROM PricebookEntry WHERE Pricebook2Id = :pb.Id LIMIT 1].Id,
			Quantity = 1,
			UnitPrice = 1,
			Is_Scanned__c = true
		);
		insert oi;

		ord.Status = 'Activated';
		update ord;

		PageReference pr = Page.AddOrderItems;
		pr.getParameters().put('id', ord.Id);
		Test.setCurrentPage(pr);

		NMD_AddOrderItemsExtension ext = new NMD_AddOrderItemsExtension(new ApexPages.StandardController(ord));

	}
}