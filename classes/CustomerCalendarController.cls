global with sharing class CustomerCalendarController extends AuthorizationUtil{
    
    public static list<calMeeting> meetings {get;set;}
    public static Meeting__c objMeeting {get;set;}
    public static BusinessHours objBusinessHr {get;set;}
    public static  List<Holiday> holidayList {get;set;}
    public String[] holidayActualDate {get;set;}
    public static calBusinessHour objCalBusinessHour {get; set;}
    public Boston_Scientific_Config__c objBostonConfig {get;set;}
    public static map<DateTime, Integer> mapObjHoursAgainstDate {get;set;}
    public static map<DateTime, Boolean> mapObjAvailabilityAgainstDate {get;set;}
    public static map<String, Integer> mapObjHoursAgainstDayOfWeek = new map<String, Integer>();
    public static String[] nonAvailableDates {get;set;}
    public static User loggedInUserContactId{get;set;}
    public String strAppointmentBookingSummaryURL {get; set;}
    // public String isEducation {get; set;}
    
    /**
        Description: Constructor functionality placed here
    */
   public override void fetchRequestedData() {
        
        // isEducation = ApexPages.currentPage().getParameters().get('isEducation');
        objMeeting = new Meeting__c();
        //BSMI-129
        String strUserDivision = [SELECT Shared_Community_Division__c
                                        FROM User
                                        WHERE Id =: UserInfo.getUserId()].Shared_Community_Division__c;
                                        
        strUserDivision = String.isBlank(strUserDivision) ? 'Endo':strUserDivision;
        
        objMeeting.recordTypeId = Schema.getGlobalDescribe().get('Meeting__c').getDescribe().getRecordTypeInfosByName().get(strUserDivision+' Meetings').getRecordTypeId();
        
        
        //BSMI-129
        // if(isEducation != null && isEducation != '') {
            
        //     objMeeting.AppointmentTopic__c = 'Training';
        // }
        
        //set URL based on UserType.
        if(UserInfo.getUserType() == 'PowerCustomerSuccess') {
            
            strAppointmentBookingSummaryURL = '/endoconnected/AppointmentBookingSummary';
        }
        else {
            
            strAppointmentBookingSummaryURL = '/apex/AppointmentBookingSummary';
        }
        
        //variables
        holidayList = new List<Holiday>();
        holidayActualDate = new List<String>();
        objBostonConfig = Boston_Scientific_Config__c.getOrgDefaults();
        
        //system.debug('objBostonConfig----------------->>'+objBostonConfig);
        
        
        //Get Business Hours for the Organization
        objBusinessHr = [SELECT SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime, WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime
            FROM BusinessHours 
            WHERE Name = 'VTM_Availability'];
        //System.debug('VTM_Availability----------->>'+String.valueOf(objBusinessHr.MondayStartTime).substring(0,5));
        
        
        objCalBusinessHour = new calBusinessHour();
        objCalBusinessHour.monStartBH = objBusinessHr.MondayStartTime != NULL ? String.valueOf(objBusinessHr.MondayStartTime).substring(0,5) : '09:00';
        objCalBusinessHour.monEndBH = objBusinessHr.MondayEndTime != NULL ? String.valueOf(objBusinessHr.MondayEndTime).substring(0,5) : '09:00';
        objCalBusinessHour.mondowBH = '1';
        objCalBusinessHour.tueStartBH = objBusinessHr.TuesdayStartTime != Null ? String.valueOf(objBusinessHr.TuesdayStartTime).substring(0,5) : '09:00';
        objCalBusinessHour.tueEndBH = objBusinessHr.TuesdayEndTime != NULL ? String.valueOf(objBusinessHr.TuesdayEndTime).substring(0,5) : '09:00';
        objCalBusinessHour.tuedowBH = '2';
        objCalBusinessHour.wedStartBH = objBusinessHr.WednesdayStartTime != NULL ? String.valueOf(objBusinessHr.WednesdayStartTime).substring(0,5) : '09:00';
        objCalBusinessHour.wedEndBH = objBusinessHr.WednesdayEndTime != NULL ? String.valueOf(objBusinessHr.WednesdayEndTime).substring(0,5) : '09:00';
        objCalBusinessHour.weddowBH = '3';
        objCalBusinessHour.thuStartBH = objBusinessHr.ThursdayStartTime != NULL ? String.valueOf(objBusinessHr.ThursdayStartTime).substring(0,5) : '09:00';
        objCalBusinessHour.thuEndBH = objBusinessHr.ThursdayEndTime != NULL ? String.valueOf(objBusinessHr.ThursdayEndTime).substring(0,5) : '09:00';
        objCalBusinessHour.thudowBH = '4';
        objCalBusinessHour.friStartBH = objBusinessHr.FridayStartTime != NULL ? String.valueOf(objBusinessHr.FridayStartTime).substring(0,5) : '09:00';
        objCalBusinessHour.friEndBH = objBusinessHr.FridayEndTime != NULL ? String.valueOf(objBusinessHr.FridayEndTime).substring(0,5) : '09:00';
        objCalBusinessHour.fridowBH = '5';
               
        
        //Get Holidays of Organization
        holidayList = [SELECT ActivityDate
            FROM Holiday WHERE ActivityDate >= Today];


        //get the array List from holidayList
        for(Holiday objHoliday: holidayList) {
            
            DateTime holidayDateTime = objHoliday.ActivityDate;
            //System.debug('holidayDateTime------------>>'+holidayDateTime);
            String holidayDate = holidayDateTime.addDays(1).format('YYYY-MM-dd');
            //System.debug('holidayDate------------>>'+holidayDate);
            holidayActualDate.add(holidayDate);
        }
        
        //system.debug('holidayActualDate=========>>'+holidayActualDate);
    }
    

    /**
        Description: Called from the PageLoad method to get corresponding Meetings
    */
    @RemoteAction
    public static map<String, Object> getMeetings() {
        
        //Variables
        meetings = New List<calMeeting>();
        mapObjHoursAgainstDate = new map<DateTime, Integer>();
        mapObjAvailabilityAgainstDate = new map<DateTime, Boolean>();
        nonAvailableDates = new List<String>();
        String dtFormat = 'EEE, d MMM yyyy HH:mm:ss z';
        DateTime fetchMeetingsTillDate = DateTime.now();
        Boston_Scientific_Config__c objBostonConfig = Boston_Scientific_Config__c.getOrgDefaults();
        //system.debug('mapObjHoursAgainstDayOfWeek----------->>'+mapObjHoursAgainstDayOfWeek);
        
        
        //Get Business Hours for the Organization
        objBusinessHr = [SELECT SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime, WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime
            FROM BusinessHours 
            WHERE Name = 'VTM_Availability'];
        //System.debug('VTM_Availability----------->>'+objBusinessHr);
        
        
        //get the Schema of the BusinessHours Object to get fieldsAPI names and Fill the Map mapObjHoursAgainstDayOfWeek
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.BusinessHours.fields.getMap();
        for(String fieldName: schemaFieldMap.keySet()) {
            
            if(fieldName == 'mondaystarttime') {
                
                //system.debug('Monday End Time -------------------->>'+Integer.valueof(String.valueOf(objBusinessHr.MondayEndTime).substring(0,2)));
                //system.debug('Monday Start Time---------------------->>'+Integer.valueof(String.valueOf(objBusinessHr.MondayStartTime).substring(0,2)));
                Integer hours = Integer.valueof(String.valueOf(objBusinessHr.MondayEndTime).substring(0,2)) - Integer.valueof(String.valueOf(objBusinessHr.MondayStartTime).substring(0,2));
                mapObjHoursAgainstDayOfWeek.put('Monday',hours);
            }
            
            if(fieldName == 'tuesdaystarttime') {
                
                Integer hours = Integer.valueof(String.valueOf(objBusinessHr.TuesdayEndTime).substring(0,2)) - Integer.valueof(String.valueOf(objBusinessHr.TuesdayStartTime).substring(0,2));
                mapObjHoursAgainstDayOfWeek.put('Tuesday',hours);
            }
            
            if(fieldName == 'wednesdaystarttime') {
                
                Integer hours = Integer.valueof(String.valueOf(objBusinessHr.WednesdayEndTime).substring(0,2)) - Integer.valueof(String.valueOf(objBusinessHr.WednesdayStartTime).substring(0,2));
                mapObjHoursAgainstDayOfWeek.put('Wednesday',hours);
            }
            
            if(fieldName == 'thursdaystarttime') {
                
                Integer hours = Integer.valueof(String.valueOf(objBusinessHr.ThursdayEndTime).substring(0,2)) - Integer.valueof(String.valueOf(objBusinessHr.ThursdayStartTime).substring(0,2));
                mapObjHoursAgainstDayOfWeek.put('Thursday',hours);
            }
            
            if(fieldName == 'fridaystarttime') {
                
                Integer hours = Integer.valueof(String.valueOf(objBusinessHr.FridayEndTime).substring(0,2)) - Integer.valueof(String.valueOf(objBusinessHr.FridayStartTime).substring(0,2));
                mapObjHoursAgainstDayOfWeek.put('Friday',hours);
            }
        }
        //system.debug('mapObjHoursAgainstDayOfWeek=================>>'+mapObjHoursAgainstDayOfWeek);
        
        
        //Get loggedIn users ContactId
        loggedInUserContactId = new User();
        loggedInUserContactId = [SELECT Id, Shared_Community_Division__c, Contact.OwnerId
            FROM User
            WHERE Id =: UserInfo.getUserId() LIMIT 1];
        
        
        //fetch Meeting Till date
        //System.debug('noOfMonths============================>>'+objBostonConfig.Boston_No_of_Month_to_Fetch_Meetings__c);
        Integer noOfMonths = Integer.valueOf(objBostonConfig.Boston_No_of_Month_to_Fetch_Meetings__c);
        fetchMeetingsTillDate = fetchMeetingsTillDate.addMonths(noOfMonths);
        
        //system.debug('loggedInUserContactId.Contact.OwnerId-------------->>'+loggedInUserContactId.Contact.OwnerId);
        
        //get Meeting Records
        for(Meeting__c objFetchMeeting: [SELECT Id, Name, isAllDayMeeting__c, Start_Time__c, End_Time__c, Meeting_Request_Status__c, AssignedTo__c, Contact__c
                FROM Meeting__c
                WHERE AssignedTo__c =: loggedInUserContactId.Contact.OwnerId
                AND Start_Time__c >= TODAY  
                AND Start_Time__c <=: fetchMeetingsTillDate
                AND Meeting_Request_Status__c != 'Rejected']) {
                    
                    if(objFetchMeeting != null) {
                        
                        
                        //Get hours of Meeting Record
                        //system.debug('objFetchMeeting Object --------->>'+objFetchMeeting);
                        DateTime startDT = objFetchMeeting.Start_Time__c;
                        DateTime endDT = objFetchMeeting.End_Time__c;
                        Integer tempHours = Integer.valueof((endDT.getTime() - startDT.getTime())/(60*60));
                        Integer hours = Integer.valueOf(tempHours/1000);
                        Date startDateFormat = date.newinstance(startDT.year(), startDT.month(), startDT.day());

                        //Check date and put Total Hours agianst the Meeting date in Map     
                        if(mapObjHoursAgainstDate.containsKey(startDateFormat)) {
                        
                            //system.debug('hours Object --------->>'+hours+'=====>StarDate=======>>'+startDT+'=========>>EndDate=========>>'+endDT);
                            mapObjHoursAgainstDate.put(startDateFormat,mapObjHoursAgainstDate.get(startDateFormat) + hours);
                        }
                        else {
                            
                            //system.debug('hours Object --------->>'+hours+'=====>StarDate=======>>'+startDT+'=========>>EndDate=========>>'+endDT);
                            mapObjHoursAgainstDate.put(startDateFormat, hours);
                        }
                        
                        //system.debug('mapObjHoursAgainstDate=================>>'+mapObjHoursAgainstDate);
                        
                       
                        //Bind the Meeting data to the Inner class to hold data for passing to the javascript.
                        calMeeting myMeeting = New calMeeting();
                        myMeeting.title = objFetchMeeting.Name;
                        myMeeting.allDay = objFetchMeeting.isAllDayMeeting__c;
                        myMeeting.start = startDT.format(dtFormat);
                        myMeeting.endString = endDT.format(dtFormat);
                        myMeeting.ClassName = 'event-personal';
                        
                
                        //Add Inner class instance into the List
                        meetings.add(myMeeting);
                        //system.debug('List of Meetings--------->>'+meetings);
                    }
                }
                
                
                //Create the NonAvailableDates List to block date slots.
                //system.debug('mapObjHoursAgainstDate out from loop==============>>'+mapObjHoursAgainstDate);
                for(DateTime dt: mapObjHoursAgainstDate.keySet()) {
                            
                    String getDayOfWeek = dt.addDays(1).format('EEEE');
                    //system.debug('------------------------getDayOfWeek=====================>>'+dt.addDays(1).format('EEEE'));
                    //System.debug('-------------dt------------------->>'+dt);
                    //System.debug('---------->>mapObjHoursAgainstDate.get(dt)----------->>'+mapObjHoursAgainstDate.get(dt));
                    //System.debug('---------->>mapObjHoursAgainstDayOfWeek.get(getDayOfWeek)----------->>'+mapObjHoursAgainstDayOfWeek.get(getDayOfWeek));
                    if(mapObjHoursAgainstDate.get(dt) >= mapObjHoursAgainstDayOfWeek.get(getDayOfWeek)) {
                        
                        System.debug('-------------dt------------------->>'+dt);
                        String nonAvailableDateStr = dt.addDays(1).format('YYYY-MM-dd');
                        nonAvailableDates.add(nonAvailableDateStr);
                        mapObjAvailabilityAgainstDate.put(dt, true);
                    }
                }
                
                //system.debug('==========nonAvailableDates==================>>'+nonAvailableDates);
                map<String, Object> singleReturnMap = new map<String, Object>();
                singleReturnMap.put('meetings',meetings);
                singleReturnMap.put('nonAvailableDates', nonAvailableDates);
                
        return singleReturnMap;
        
    }
    
    
    /**
        Description: Insert the Meeting record Invoked from the Javascript. 
    */
    @RemoteAction
    global static Meeting__c insertMeeting(String objMeetingToSave) {
        
        //system.debug('objMeetingToSave-------------------->>>>>'+objMeetingToSave);
        
        //Get loggedIn users ContactId
        loggedInUserContactId = [SELECT Id, Shared_Community_Division__c, Contact.OwnerId, ContactId
            FROM User
            WHERE Id =: UserInfo.getUserId() LIMIT 1];
        
        system.debug('loggedInUserContactId==========='+loggedInUserContactId.Contact.OwnerId);
        //get the deserialized Data of meeting come from the javascript.
        jsonToApexForMeeting objJToA = New jsonToApexForMeeting();
        objJToA = (jsonToApexForMeeting)JSON.deserialize(objMeetingToSave, jsonToApexForMeeting.class);
        system.debug('Called fRom Insert Meeting objMeeting-------------------->>>>>'+objJToA);
        
        
        //change the start End DateTime according to the Orgs Timezone.
        TimeZone tz = UserInfo.getTimeZone();
        Double offSet = tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
        DateTime meetingStartDate = objJToA.Start_Time - offSet ;
        DateTime meetingEndDate = objJToA.End_Time - offSet;
        
        Meeting__c objInsertMeeting = new Meeting__c();
        
        if(objJToA != null) {
            
            //System.debug('=================objJToA.AppointmentTopic====================>>'+objJToA.AppointmentTopic);
            
                
                
                
                //Create the Instance of the Meeting Object.
                
                objInsertMeeting.AppointmentTopic__c = objJToA.AppointmentTopic;
                //objInsertMeeting.Name = objJToA.Name;
                objInsertMeeting.Start_Time__c = meetingStartDate;
                objInsertMeeting.End_Time__c = meetingEndDate;
                objInsertMeeting.Contact__c = loggedInUserContactId.ContactId;
                objInsertMeeting.AssignedTo__c = loggedInUserContactId.Contact.OwnerId;
                objInsertMeeting.OwnerId = loggedInUserContactId.Contact.OwnerId;
                objInsertMeeting.Name = 'Community Site Appointment Request';
                objInsertMeeting.Meeting_Request_Status__c = 'Pending';
                objInsertMeeting.Shared_Division__c = loggedInUserContactId.Shared_Community_Division__c;
                
                //BSMI-413
                /*
                This was added as Test class was failing as the process builder-flow threw exception                
                */
                if(Test.isRunningTest()){
                    objInsertMeeting.EventId__c = objJToA.EventId;
                } else {
                    
                    // to prevent test coverage failure while running test code, as Contact OWD is private
                    Insert objInsertMeeting;
                }
                
                //Insert Meeting Record.
                return objInsertMeeting;
        }
        return null;
    }
    
    
    /**
        Description: Inner Class to hold the values came from the Javascript to Save in Meeting Object.
    */
    private class jsonToApexForMeeting {
        
        public string AppointmentTopic{get; set;}
        public string ContactId{get; set;}
        public string Description{get; set;}
        public DateTime End_Time{get; set;}
        public string EventId{get; set;}
        public Boolean isAllDayMeeting{get; set;}
        public string Meeting_Request_Status{get; set;}
        public DateTime Start_Time{get; set;}
        public string Name{get; set;}
    }
    
    
    /**
        Description: Class to hold calendar event data
    */
    private class calMeeting{
        
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String start {get;set;}
        public String endString {get;set;}
        public String className {get;set;}
        
        
    }
    
    
     /**
        Description: Inner Class to hold the values for Business hours to bind with Javascript.
    */
    private class calBusinessHour{
    
        public String monStartBH {get;set;}
        public String monEndBH {get;set;}
        public String mondowBH {get;set;}
        public String tueStartBH {get;set;}
        public String tueEndBH {get;set;}
        public String tuedowBH {get;set;}
        public String wedStartBH {get;set;}
        public String wedEndBH {get;set;}
        public String weddowBH {get;set;}
        public String thuStartBH {get;set;}
        public String thuEndBH {get;set;}
        public String thudowBH {get;set;}
        public String friStartBH {get;set;}
        public String friEndBH {get;set;}
        public String fridowBH {get;set;}
    }
}