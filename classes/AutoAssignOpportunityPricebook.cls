/*
@CreatedDate     16JUNE2016
@author          Mike Lankfer
@Description     Contains the logic to automatically associate a Pricebook to an Opty based on the Pricebook/Profile associations in the Pricebook/Profile mapping custom setting
@Requirement Id  User Story : DRAFT US2511
*/
public class AutoAssignOpportunityPricebook {
    
    
    private static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();    
    private static final Id RECTYPE_NMD_COOL_OPTY = RECTYPES.get('NMD Cool').getRecordTypeId();
    private static final Id RECTYPE_NMD_REPROGRAMMING_OPTY = RECTYPES.get('NMD Reprogramming').getRecordTypeId();
    private static final Id RECTYPE_NMD_SCS_EXPLANT_OPTY = RECTYPES.get('NMD SCS Explant').getRecordTypeId();
    private static final Id RECTYPE_NMD_SCS_IMPLANT_OPTY = RECTYPES.get('NMD SCS Implant').getRecordTypeId();
    private static final Id RECTYPE_NMD_SCS_REVISION_OPTY = RECTYPES.get('NMD SCS Revision').getRecordTypeId();
    private static final Id RECTYPE_NMD_SCS_TRIAL_OPTY = RECTYPES.get('NMD SCS Trial').getRecordTypeId();
    private static final Id RECTYPE_NMD_SCS_TRIAL_IMPLANT_OPTY = RECTYPES.get('NMD SCS Trial-Implant').getRecordTypeId();
    
    private static Set<Id> NMD_ALL_OPTY = new Set<Id>{
        RECTYPE_NMD_COOL_OPTY, RECTYPE_NMD_REPROGRAMMING_OPTY, RECTYPE_NMD_SCS_EXPLANT_OPTY,
        RECTYPE_NMD_SCS_IMPLANT_OPTY, RECTYPE_NMD_SCS_REVISION_OPTY, RECTYPE_NMD_SCS_TRIAL_OPTY, 
        RECTYPE_NMD_SCS_TRIAL_IMPLANT_OPTY
    };
    
    private List<Shared_Pricebook_Profile_Mapping__c> profileToPricebookList = Shared_Pricebook_Profile_Mapping__c.getall().values();
    private Map<Id,Id> profileToPricebookMap = new Map<Id,Id>();
    private Map<String, Id> profileMap = new Map<String, Id>();
    private List<Profile> profileList = new List<Profile>();
    private Map<String, Id> pricebookMap = new Map<String, Id>();
    private List<Pricebook2> pricebookList = new List<Pricebook2>();
    private Set<Id> userIdSet = new Set<Id>();
    private Map<Id,Id> userToProfileMap = new Map<Id,Id>();
    private List<User> userList = new List<User>();
    private static String nmdSalesOrg = 'US10';
    
    
/*
@CreatedDate     16JUNE2016
@author          Mike Lankfer
@Description     Contains the logic to automatically associate a Pricebook to an Opty based on the Pricebook/Profile associations in the Pricebook/Profile mapping custom setting
@Requirement Id  User Story : DRAFT US2511
*/    
    public void assignPricebook(List<Opportunity> optys){
        
        //build list of profiles
        profileList = [SELECT Name, Id FROM Profile];
        
        //build list of pricebooks, excluding NMD
        pricebookList = [SELECT Name, Id FROM Pricebook2 WHERE Sales_Organization__c != :nmdSalesOrg];
        
        //build Map of Profile Names to Ids
        for(Profile p:profileList){
            profileMap.put(p.Name, p.Id);
        }
        
        //build Map of Pricebook Names to Ids
        for(Pricebook2 p:pricebookList){
            pricebookMap.put(p.Name, p.Id);
        }
        
        //build Map of Profile Ids to Pricebook Ids
        for(Shared_Pricebook_Profile_Mapping__c p2pMap: profileToPricebookList){
            profileToPricebookMap.put(profileMap.get(p2pMap.Name), pricebookMap.get(p2pMap.Shared_Pricebook__c));            
        }
        
        //build Set of Opportunity Owners
        for (Opportunity opty:optys){
            userIdSet.add(opty.OwnerId);
        }
        
        //build List of Opportunity Owners
        userList = [SELECT Id, ProfileId FROM User WHERE Id IN :userIdSet];
        
        //build Map of User Ids to Profile Ids
        for(User u:userList){
            userToProfileMap.put(u.Id, u.ProfileId);
        }
        
        //Loop through Opportunity and assign Pricebook based on user profile to pricebook mapping
        for(Opportunity opty:optys){
            if(!NMD_ALL_OPTY.contains(opty.RecordTypeId)){
                   opty.Pricebook2Id = profileToPricebookMap.get(userToProfileMap.get(opty.OwnerId));
               }
        }       
    }    
}