@IsTest(SeeAllData=false)
private class NMD_SubmitProspectPatientExtensionTest {
	
	final static String NAME = 'NAME';

	@TestSetup
	static void setup() {

		Seller_Hierarchy__c territory = new Seller_Hierarchy__c(
			Name = NAME,
			Current_TM1_Assignee__c = UserInfo.getUserId()
		);
		insert territory;

		Patient__c patient1 = new Patient__c(
			RecordTypeId = PatientManager.RECTYPE_PROSPECT,
			Territory_ID__c = territory.Id
		);
		Patient__c patient2 = new Patient__c(
			RecordTypeId = PatientManager.RECTYPE_CUSTOMER,
			Territory_ID__c = territory.Id
		);
		insert new List<Patient__c> { patient1, patient2 };

	}

	static testMethod void test_SubmitProspectPatient_BadRecType() {

		Patient__c patient = [
			select
				RecordTypeId
			from Patient__c 
			where Territory_ID__r.Name = :NAME
			and RecordTypeId = :PatientManager.RECTYPE_CUSTOMER
		];

		PageReference pr = Page.SubmitProspectPatient;
		pr.getParameters().put('id', patient.Id);
		Test.setCurrentPage(pr);

		NMD_SubmitProspectPatientExtension ext = new NMD_SubmitProspectPatientExtension(
			new ApexPages.StandardController(patient)
		);

		ext.submit();

		System.assertEquals(NMD_SubmitProspectPatientExtension.MSG_BADRECTYPE, ApexPages.getMessages()[0].getDetail());

	}

	static testMethod void test_SubmitProspectPatient_Errors() {

		Patient__c patient = [
			select
				RecordTypeId,
				Address_1__c,
				City__c,
				Country__c,
				County__c,
				Patient_Date_of_Birth__c,
				Patient_First_Name__c,
				Patient_Gender__c,
				Patient_Last_Name__c,
				Patient_Phone_Number__c,
				Patient_Mobile_Number__c,
				State__c,
				Zip__c
			from Patient__c 
			where Territory_ID__r.Name = :NAME
			and RecordTypeId = :PatientManager.RECTYPE_PROSPECT
		];

		PageReference pr = Page.SubmitProspectPatient;
		pr.getParameters().put('id', patient.Id);
		Test.setCurrentPage(pr);

		NMD_SubmitProspectPatientExtension ext = new NMD_SubmitProspectPatientExtension(
			new ApexPages.StandardController(patient)
		);

		System.assertNotEquals(NMD_SubmitProspectPatientExtension.MSG_CONTINUE, ApexPages.getMessages()[0].getDetail());

	}

	static testMethod void test_SubmitProspectPatient_Good() {

		Patient__c patient = [
			select
				RecordTypeId,
				Address_1__c,
				City__c,
				Country__c,
				County__c,
				Patient_Date_of_Birth__c,
				Patient_First_Name__c,
				Patient_Gender__c,
				Patient_Last_Name__c,
				Patient_Phone_Number__c,
				Patient_Mobile_Number__c,
				State__c,
				Zip__c
			from Patient__c 
			where Territory_ID__r.Name = :NAME
			and RecordTypeId = :PatientManager.RECTYPE_PROSPECT
		];

		patient.Address_1__c = 'Address_1__c';
		patient.City__c = 'City__c';
		patient.County__c = 'County__c';
		patient.Country__c = 'Country__c';
		patient.Patient_Date_of_Birth__c = System.today().addYears(-30);
		patient.Patient_First_Name__c = 'Patient_First_Name__c';
		patient.Patient_Gender__c = 'Male';
		patient.Patient_Last_Name__c = 'Patient_Last_Name__c';
		patient.Patient_Phone_Number__c = '5556667788';
		patient.Patient_Mobile_Number__c = '1234567890';
		patient.State__c = 'ST';
		patient.Zip__c = '12345';

		PageReference pr = Page.SubmitProspectPatient;
		pr.getParameters().put('id', patient.Id);
		Test.setCurrentPage(pr);

		NMD_SubmitProspectPatientExtension ext = new NMD_SubmitProspectPatientExtension(
			new ApexPages.StandardController(patient)
		);

		System.assertEquals(NMD_SubmitProspectPatientExtension.MSG_CONTINUE, ApexPages.getMessages()[0].getDetail());

		ext.submit();

		//System.assertEquals(NMD_SubmitProspectPatientExtension.MSG_SUBMITTED, ApexPages.getMessages()[0].getDetail());

	}
   
    
    //Added by Tejas Ghalsasi
    
    static testMethod void test_method_1() {
        
        Patient__c patient = [
			select
				RecordTypeId,
				Address_1__c,
				City__c,
				Country__c,
				County__c,
				Patient_Date_of_Birth__c,
				Patient_First_Name__c,
				Patient_Gender__c,
				Patient_Last_Name__c,
				Patient_Phone_Number__c,
				Patient_Mobile_Number__c,
				State__c,
				Zip__c
			from Patient__c 
			where Territory_ID__r.Name = :NAME
			and RecordTypeId = :PatientManager.RECTYPE_PROSPECT
		];

		//patient.Address_1__c = 'Address_1__c';
		patient.City__c = 'City__c';
		patient.County__c = 'County__c';
		patient.Country__c = 'Country__c';
		patient.Patient_Date_of_Birth__c = System.today().addYears(-30);
		patient.Patient_First_Name__c = 'Patient_First_Name__c';
		patient.Patient_Gender__c = 'Male';
		patient.Patient_Last_Name__c = 'Patient_Last_Name__c';
		patient.Patient_Phone_Number__c = '5556667788';
		patient.Patient_Mobile_Number__c = '1234567890';
		patient.State__c = 'ST';
		patient.Zip__c = '12345';

		PageReference pr = Page.SubmitProspectPatient;
		pr.getParameters().put('id', patient.Id);
		Test.setCurrentPage(pr);

		NMD_SubmitProspectPatientExtension ext = new NMD_SubmitProspectPatientExtension(
			new ApexPages.StandardController(patient)
		);

		//System.assertEquals(NMD_SubmitProspectPatientExtension.MSG_CONTINUE, ApexPages.getMessages()[0].getDetail());

		ext.submit();

		//System.assertEquals(NMD_SubmitProspectPatientExtension.MSG_SUBMITTED, ApexPages.getMessages()[0].getDetail());

    
    
        
        
    
    
    }
    
    
    

}