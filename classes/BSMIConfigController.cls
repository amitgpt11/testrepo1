public class BSMIConfigController {

    public Boston_Scientific_Config__c ObjBostonScientificConfig{get;set;}
    
    public BSMIConfigController(){
        
        ObjBostonScientificConfig = Boston_Scientific_Config__c.getInstance('Default');
    }

    public PageReference save(){
        
        update ObjBostonScientificConfig;
        return null;
    }
}