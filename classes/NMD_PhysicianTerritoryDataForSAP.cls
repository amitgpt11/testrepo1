/**
 * New Wrapper class for Physician Territory Integration 
 * Data to be sent over to SAP
 * 
 * Developer - Dipil Jain - Accenture
 **/
 
public class NMD_PhysicianTerritoryDataForSAP {
    
    public String Sales_Organization {get; set;}
    public String Customer_Number {get; set;}
    public String Profit_Center {get; set;}
    public String Sales_Rep_ID {get; set;}
    public String Valid_From_Date {get; set;}
    public String Valid_To_Date {get; set;}
    public String Sales_Rep_Sold_To {get; set;}
    public String Physician_SAP_ID {get; set;}
    public String Sales_Territory_ID {get; set;}
   // public String Sales_Region_ID {get; set;}
   // public String Sales_Area_ID {get; set;}
   // public String Sales_Division_ID {get; set;}
   // public String Sales_Rep_Type {get; set;}
    public String Field_Support_Rep {get; set;}
    public String Field_Support_Area {get; set;}
    public String Field_Support_Region {get; set;}
    public String Business_Unit {get; set;}
    public String Change_Indicator {get; set;}
    
    public NMD_PhysicianTerritoryDataForSAP(){
        //hardcoding a few values, unless their actual mapping is received. 
        this.sales_Organization = 'US10';
        this.valid_From_Date= Datetime.now().format('yyyy-MM-dd');
        this.profit_Center= '40000';
        this.business_Unit= 'NMD';
        customer_Number='';
        sales_Rep_ID='';
        field_Support_Rep='';
        field_Support_Area='';
        field_Support_Region='';
        valid_To_Date='';
        sales_Rep_Sold_To='';
        physician_SAP_ID='';
        sales_Territory_ID='';
        // sales_Region_ID='';
        // sales_Area_ID='';
        // sales_Division_ID='';
        // sales_Rep_Type='';
    }
    
}