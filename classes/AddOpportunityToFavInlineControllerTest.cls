/*
@CreatedDate     3JUNE2016
@author          Julianne Diamond
@Description     Tests Controller for AddOpportunityToFavInline VF Page
@Requirement Id  User Story : US2674
*/

@isTest

public class AddOpportunityToFavInlineControllerTest {
    
    static testmethod void addOpportunityToFavorites(){
        
        Opportunity oppt= new Opportunity(Name = 'Test Opty', StageName = 'Prospecting', CloseDate = Date.today());
        insert oppt;
        
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(oppt);
        
        AddOpportunityToFavInlineController controller = new AddOpportunityToFavInlineController(sc);
        
        controller.showOpportunityFavorites();
        
        controller.messageText = 'txt';
        System.assertEquals('txt', controller.messageText);
        
        Test.stopTest();
    }
    
    static testmethod void addOpportunityToFavorites2(){
        
        Opportunity oppt= new Opportunity(Name = 'Test Opty', StageName = 'Prospecting', CloseDate = Date.today());
        insert oppt;
        
        Opportunity_Favorite__c optyFavorite = new Opportunity_Favorite__c(Opportunity__c = oppt.Id, User__c = UserInfo.getUserId());
        insert optyFavorite;
        
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(oppt);
        
        AddOpportunityToFavInlineController controller = new AddOpportunityToFavInlineController(sc);
        
        controller.showOpportunityFavorites();
        
        controller.messageText = 'txt';
        System.assertEquals('txt', controller.messageText);
        
        Test.stopTest();
    }
}