/**
* Manager class for the Task object
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class TaskManager {
	
	final static SObjectType OBJECT_TYPE = Lead.SObjectType;

    private static String userProfileName;

	@testVisible
	final static String RAW_PATIENT_EDUCATION_SUBJECT = 'Patient Education+Physician+Patient Initials';

	public static void updateNMDLeadPatientEducationTasks(List<Task> newTasks) {

		List<Task> patientTasks = new List<Task>{};
		Map<Id,Lead> nmdLeads = new Map<Id,Lead>{};
		Map<Id,Contact> physicians = new Map<Id,Contact>{};
		Map<Id,Patient__c> patients = new Map<Id,Patient__c>{};

		for (Task task : newTasks) {
			if (task.WhoId != null
			 && task.WhoId.getSObjectType() == OBJECT_TYPE
			 && task.Subject == RAW_PATIENT_EDUCATION_SUBJECT
			) {

				patientTasks.add(task);
				nmdLeads.put(task.WhoId, null);

			}
		}

		if (patientTasks.isEmpty()) {
			return;
		}

		for (Lead lead : [
			select
				Id,
				Patient__c,
				Trialing_Physician__c
			from Lead
			where Id in :nmdLeads.keySet()
		]) {
			nmdLeads.put(lead.Id, lead);
			physicians.put(lead.Trialing_Physician__c, null);
			patients.put(lead.Patient__c, null);
		}

		physicians.putAll(new Map<Id,Contact>([
			select Id, Name
			from Contact
			where Id in :physicians.keySet()
		]));

		patients.putAll(new Map<Id,Patient__c>([
			select Id, Patient_First_Name__c, Patient_Last_Name__c
			from Patient__c
			where Id in :patients.keySet()
		]));

		for (Task task : patientTasks) {

			Lead parentLead = nmdLeads.get(task.WhoId);
			if (parentLead == null) continue;

			Patient__c patient = patients.get(parentLead.Patient__c);
			Contact physician = physicians.get(parentLead.Trialing_Physician__c);
			//if (patient == null || physician == null) continue;

			task.Subject = formatSubject(
				physician == null ? 'Physician' : physician.Name, 
				patient.Patient_First_Name__c, 
				patient.Patient_Last_Name__c
			);

		}

	}

	@testVisible
	private static String formatSubject(String physicianName, String patientFirstName, String patientLastName) {
		return String.format('Patient Education - {0} - {1}', new List<String> {
			physicianName,
			String.format('{0}.{1}.', new List<String> {
				patientFirstName.left(1).toUpperCase(),
				patientLastName.left(1).toUpperCase()
			})
		});
	}

	//public void fetchUserProfileName(){
	//	Id profileId = userInfo.getProfileId();
	//	userProfileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name; 
	//}

	//public static void makeUserTaskOwner(Task oldTask, Task newTask) {

	//	TaskManagerNoSharing.makeUserTaskOwner(oldTask, newTask, userProfileName);

		//if(oldTask.Make_Me_Owner__c != newTask.Make_Me_Owner__c
  //          && newTask.Make_Me_Owner__c == true
  //      ){
  //      	if(profileNames.contains(userProfileName)){
	 //       	if(newTask.Status != 'Completed'){
  //          		newTask.OwnerId = UserInfo.getUserId();
  // 		        } else {
  // 		    		newTask.addError('Completed tasks cannot be reassigned.');
		//        }
  //      	} else {
  //      		newTask.addError('Only users in the NMD IBU-PC user profile can make themselves the owner.');
  //      	}
  //      }
  //     	newTask.Make_Me_Owner__c = false;

	//}

}