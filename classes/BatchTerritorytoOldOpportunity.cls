/*
 @CreatedDate     11APR2016                                  
 @ModifiedDate    11APR2016                                  
 @author          Amitabh-Accenture
 @Description     This is the batch will be executed one time in order to set the territory on old Opportunities which were created before implementing this code.
 @Methods         batchTerritorytoOldOpportunityOneTime 
 @Requirement Id  
 */
 
 global class BatchTerritorytoOldOpportunity implements Database.Batchable<sObject> { 
     
    global Database.QueryLocator start(Database.BatchableContext BC) {
    Date dt = system.today();
    String query =  'select ' +
                    'Id,OwnerId,name, ' +
                    'AccountId, ' +
                    'StageName, ' +
                    'Opportunity_Type__c, ' +
                    'Competitor_Name__c, ' +
                    'Product_System__c, ' +
                    'territory2Id '+
                    'From Opportunity ' +
                    'where LastModifiedDate <: dt';    
        
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        
        list<Opportunity> OptyList = new list<Opportunity>();
        //list<Id> myObjIdLst = new list<Id>();
        boolean isInsert = True;
        //boolean isAssignSuccess = false;
        for(Opportunity o : scope){
            OptyList.add(o);
            
        }
        if(OptyList!=null && OptyList.size()> 0){ 
            UtilForAssignTerritorytoOpportunity util = new UtilForAssignTerritorytoOpportunity();
             util.utilassignTerritoryForOpportunity(isInsert ,OptyList);
             system.debug('Inside execute after util call==>');
        }
        
    }   
    
    global void finish(Database.BatchableContext BC) {
        system.debug('Batch completed');
    }

}