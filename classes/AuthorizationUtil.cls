public abstract class AuthorizationUtil {   
                                        
    public Map<String,String> mapLocaleToLanguage{
        get {
            return new Map<String, String> {
                'fr' => 'French', 
                'de' => 'German',
                'sv' => 'Swedish'
            };
        }
        set;
    }

    public PageReference init() {
    
        //if usertype is not GUEST and requested page is RequestAccount redirect it to ContactUs
        if(ApexPages.currentPage().getUrl().contains('RequestAccount') && UserInfo.getUserType() != 'Guest'){
           
                return new PageReference('/ContactUs');
        }
        
        // Redirect to UnAuthenticatedHome Page if the user is 'Guest' 
        if(UserInfo.getUserType() == 'Guest' && !ApexPages.currentPage().getUrl().contains('RequestAccount')){
            
            return new PageReference('/UnAuthenticatedHomePage');
        }
        
        fetchRequestedData();
        
        return null;
    }
    
    public abstract void fetchRequestedData();    
   
}