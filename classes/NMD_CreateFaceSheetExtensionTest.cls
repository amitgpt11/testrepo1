/**
* Test class for the CreateFaceSheet vf extension
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class NMD_CreateFaceSheetExtensionTest {
	
	final static String NAME = 'NAME';
	final static String LNAME = 'LNAME';
	final static String REPROGRAMMING = 'Reprogramming';
	final static String IMPLANTIMAGE = 'Implant Image';

	@testSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		Account acct = td.createConsignmentAccount();
		acct.Name = NAME;
		insert acct;

		Patient__c patient = td.newPatient();
		insert patient;

		Opportunity oppty = td.newOpportunity(acct.Id, NAME);
		oppty.Patient__c = patient.Id;
		insert oppty;

		//List<Attachment> attachments = new List<Attachment>{
		//	td.newAttachment(patient.Id, td.randomString5(), REPROGRAMMING),
		//	td.newAttachment(patient.Id, td.randomString5(), IMPLANTIMAGE)
		//};
		//insert attachments;

		List<PatientImageClassifications__c> pics = new List<PatientImageClassifications__c> {
			td.newPatientImageClassification(REPROGRAMMING, 0),
			td.newPatientImageClassification(IMPLANTIMAGE, 10)
		};
		insert pics;

	}

	static testMethod void test_FaceSheetReport() {

		Opportunity oppty = [select Id, Patient__c, RecordTypeId from Opportunity where Account.Name = :NAME limit 1];

		String cids = '';
		for (Attachment attach : [select Id from Attachment where ParentId = :oppty.Id]) {
			cids += attach.Id + ',';
		}

		PageReference pr = Page.FaceSheet;
		pr.getParameters().putAll(new Map<String,String> {
			'id' => oppty.Id,
			'cid' => cids
		});
		Test.setCurrentPage(pr);

		NMD_QuincyReportExtension ext = new NMD_QuincyReportExtension(new ApexPages.StandardController(oppty));

	}

	static testMethod void test_CreateFaceSheet() {
		
		Opportunity oppty = [select Id, Patient__c from Opportunity where Account.Name = :NAME limit 1];

		PageReference pr = Page.CreateFaceSheet;
		pr.getParameters().put('id', oppty.Id);
		Test.setCurrentPage(pr);

		NMD_CreateFaceSheetExtension ext = new NMD_CreateFaceSheetExtension(new ApexPages.StandardController(oppty));

		for (NMD_ObjectWrapper item : ext.attachments) {
			item.selected = true;
		}

		ext.submit();

		System.assert(String.isNotBlank(ext.pdfUrl));

	}

	static testMethod void test_CreateFaceSheetSF1() {
		
		Opportunity oppty = [select Id, Patient__c from Opportunity where Account.Name = :NAME limit 1];

		PageReference pr = Page.CreateFaceSheet;
		pr.getParameters().put('id', oppty.Id);
		Test.setCurrentPage(pr);

		NMD_CreateFaceSheetExtension ext = new NMD_CreateFaceSheetExtension(new ApexPages.StandardController(oppty));

		List<Id> imageIds = new List<Id>{};
		for (NMD_ObjectWrapper item : ext.attachments) {
			imageIds.add(item.data.Id);
		}

		NMD_CreateFaceSheetExtension.generatePdf(oppty.Id, imageIds);

	}

	static testMethod void test_redirectCheck_Desktop() {

		Opportunity oppty = [select Id, Patient__c from Opportunity where Account.Name = :NAME limit 1];

		PageReference pr = Page.CreateFaceSheet;
		pr.getParameters().put('id', oppty.Id);
		Test.setCurrentPage(pr);

		NMD_CreateFaceSheetExtension ext = new NMD_CreateFaceSheetExtension(new ApexPages.StandardController(oppty));
		PageReference result = ext.checkRedirect();

		System.assertNotEquals(null, result);

	}

	static testMethod void test_redirectCheck_SF1() {

		Opportunity oppty = [select Id, Patient__c from Opportunity where Account.Name = :NAME limit 1];

		PageReference pr = Page.CreateFaceSheetSF1;
		pr.getParameters().putAll(new Map<String,String> {
			'id' => oppty.Id,
			'isdtp' => 'p1'
		});
		Test.setCurrentPage(pr);

		NMD_CreateFaceSheetExtension ext = new NMD_CreateFaceSheetExtension(new ApexPages.StandardController(oppty));
		PageReference result = ext.checkRedirect();

		System.assertEquals(null, result);

	}

}