/*
 @CreatedDate     14 APR 2016                                  
 @author          Mayuri-Accenture
 @Description     Test class for 'AssignedUsersExt'. -This class Displays list of Users  assigned to the territories which is Used in the VF page to display.
 
 */



@isTest(SeeAllData=false)

private class AssignedUsersExtTest{
    public static User userC;
    public static List<Territory2> trList=new List<Territory2>(); 
    public static List<Territory2Type> Ttype; 
    public static Id Territory2ModelIDActive;  
    
    @testSetup
    static void setupTerritoryMockData(){
        userC = UtilForUnitTestDataSetup.newUser('Standard User');
        userC.Username = 'UserNameC1@xyz.com';
        insert userC;
        
        
        Ttype = UtilForUnitTestDataSetup.newTerritory2Type();        
        Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
        Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('AA1234',Territory2ModelIDActive,Ttype[0].Id);   
        trList.add(t1);              
        Territory2 t2 = UtilForUnitTestDataSetup.newTerritory2('AA1245',Territory2ModelIDActive,Ttype[0].Id); 
        trList.add(t2); 
        insert trList; 
        System.assertEquals(trList[0].Territory2ModelId,Territory2ModelIDActive);        
        
        
        UserTerritory2Association nAssc = new UserTerritory2Association();
        nAssc.RoleInTerritory2 = 'Territory Manager';
        nAssc.Territory2Id = trList[0].Id;
        nAssc.UserId = userC.Id;
        insert nAssc;
        
        UserTerritory2Association nAssc1 = new UserTerritory2Association();
        nAssc1.RoleInTerritory2 = 'Area Manager';
        nAssc1.Territory2Id = trList[1].Id;
        nAssc1.UserId = userC.Id;
        insert nAssc1;
        
    } 
    static testMethod void assignUser_method1()
    {
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'AA%'  ]; 
        list<TerritoryUserRoles__c> csLst = new list<TerritoryUserRoles__c>();
        
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        insert acct1;
        
        ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
        accountAssociation.ObjectId = acct1.Id;
        accountAssociation.Territory2Id = trLst[0].Id;
        accountAssociation.AssociationCause = 'Territory2Manual';
        insert accountAssociation;
        
        TerritoryUserRoles__c cs1 = new TerritoryUserRoles__c();
        cs1.Name='Territory Manager';
        cs1.Priority__c = 1;
        csLst.add(cs1);
        TerritoryUserRoles__c cs2 = new TerritoryUserRoles__c();
        cs2.Name='Area Manager';
        cs2.Priority__c = 2;
        csLst.add(cs2);
        
        insert csLst;
        
        ApexPages.currentPage().getParameters().put('id', acct1.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(acct1);
        AssignedUsersExt acctControlleer  =  new  AssignedUsersExt(ctr);
        
        Test.startTest();
        Test.setCurrentPage(Page.Assigned_Users);
        Test.stopTest();
    
    
    }

}