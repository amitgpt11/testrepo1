/*
 @CreatedDate     16 June 2016                                  
 @author          Ashish-Accenture
 @Description     Test class for Batch Apex class 'batchSendDailyReminder'
 
 */

@isTest
public class batchSendDailyReminderTest{

    static TestMethod void TestbatchSendDailyReminder(){ 
        
        date todayDate = date.today();
        
        Map<integer,String> mapMonthNumber = TestDataFactory.getMapMonthNumber();        
        PI_Lutonix_SRAI__c headObj = TestDataFactory.createTestLutonixHeader(mapMonthNumber.get(todayDate.Month()));
        TestDataFactory.createTestLutonixDetail(headObj.Id,todayDate);
        TestDataFactory.createTestLutonixDetail(headObj.Id,todayDate);
        TestDataFactory.createTestLutonixDetail(headObj.Id,todayDate.toStartOfMonth());
        PI_Lutonix_SRAI__c headObj2 = TestDataFactory.createTestLutonixHeader( mapMonthNumber.get(todayDate.Month()));
        //TestDataFactory.createTestLutonixDetail(headObj2.Id);
        
        
        Test.startTest();
            //Execute Batch Class
            batchSendDailyReminder batchObj = new batchSendDailyReminder();
            ID batchprocessid = Database.executeBatch(batchObj,200);
            System.assertNotEquals(batchprocessid,null);
            
            //Execute Schedulable Method
            batchSendDailyReminder sh1 = new batchSendDailyReminder();
            String sch = '0 0 23 * * ?'; 
            String jobId = system.schedule('Daily Reminders', sch, sh1); 
            System.assertNotEquals(jobId,null);  
            
        Test.stopTest();
            
               
    }
}