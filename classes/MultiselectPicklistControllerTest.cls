@IsTest(SeeAllData=false)
private class MultiselectPicklistControllerTest {
	
	static testMethod void test_MultiselectPicklistController() {

		MultiselectPicklistController con = new MultiselectPicklistController();

		con.leftSOptions = new List<SelectOption>();
		con.rightSOptions = new List<SelectOption>();

		con.leftOptionsHidden = 'left&left';
		con.rightOptionsHidden = 'right&right';

		System.assertEquals('left', con.leftSOptions[0].getValue());
		System.assertEquals('right', con.rightSOptions[0].getValue());

	}

}