/** 
*   Description: This class id Controller for MyContactDetails page
*/
public class MyContactDetailsController extends AuthorizationUtil{
    
    public Boolean isEndoUser                                   {get;set;}
    public User objUser                                         {get;set;}
    public List<FeedItemWrapper> lstFeedItemWrapper             {get;set;} 
    public List<Shared_Community_Order__c> lstCommunityOrder    {get;set;}
    public Boolean isEuropeanUser                               {get;set;}
    
    /*
* Description: This wrapper class holds the FeetItem and its DownloadUrl String
*/
    private class FeedItemWrapper {
        
        public String downloadUrl              {get;set;} 
        public FeedItem objFeedItem            {get;set;}
        public String strContentSize            {get;set;}      
        
        public FeedItemWrapper(){
            
            objFeedItem = new FeedItem();
        }
    }
    
    /**
* Description: This method fteches the User details along with it's contact and account fields.
*/
    public override void fetchRequestedData() {
        
        String strDocumentDownloadUrlprefix;
        FeedItemWrapper objFeedItemWrapper;
        List<FeedItem> lstFeedItem = new List<FeedItem>();
        lstFeedItemWrapper = new List<FeedItemWrapper>();
        lstCommunityOrder = new List<Shared_Community_Order__c>();
        isEuropeanUser = false;
        
        objUser = [Select Firstname, Lastname, Email, Phone, ContactId, Contact.firstname, Contact.lastname, Contact.email, Contact.phone, 
                   Contact.Account.BillingStreet, Contact.Account.BillingState, Contact.Account.BillingPostalCode,
                   Contact.Account.BillingCity, Contact.Account.BillingCountry, Contact.Account.ShippingCity,
                   Contact.Account.ShippingCountry, Contact.Account.ShippingPostalCode, Contact.Account.ShippingState, Contact.Account.ShippingStreet,
                   Shared_Community_Division__c, TimeZoneSidKey
                   From User 
                   Where Id =: Userinfo.getUserId()];
        
        List<User> loggedInUser = new List<User>([SELECT TimeZoneSidKey 
                                           FROM User 
                                           WHERE TimeZoneSidKey like '%Europe%' 
                                           AND Id =: UserInfo.getUserID()]);
                                           
        isEuropeanUser = (!loggedInUser.isEmpty())?true:false;
        
        if(objUser.Shared_Community_Division__c == null || objUser.Shared_Community_Division__c  == 'Endo') {
            
            isEndoUser = true;     
        }
        
        if(objUser.ContactId != null && !Test.isRunningTest()) {
            
            lstFeedItem = [Select RelatedRecordId, Title, ContentSize
                           From FeedItem
                           Where parentId =: objUser.ContactId
                           AND Visibility = 'AllUsers'];  
        }
        
        if(Test.isRunningTest()){
            
            Account objAccount = Test_DataCreator.createAccount('test Account');
            Contact objContact = Test_DataCreator.createContact('testLastname', objAccount.Id);
            
            FeedItem testFeed = new FeedItem (
                ParentId = objContact.Id,
                Body = 'Hello',
                Visibility = 'AllUsers' 
            );
            
            insert testFeed;
            
            lstFeedItem.add([Select RelatedRecordId, Title, ContentSize
                           From FeedItem 
                           WHERE id=: testFeed.Id]);
                           
           
        }
        
        if(Boston_Scientific_Config__c.getValues('Default') != null) {
            
            strDocumentDownloadUrlprefix = Boston_Scientific_Config__c.getValues('Default').Boston_Document_Download_Url__c;
            for(FeedItem objFeedItem : lstFeedItem){
                
                Decimal contentsize;
                objFeedItemWrapper = new FeedItemWrapper();
                objFeedItemWrapper.objFeedItem = objFeedItem;
                
                if(objFeedItem.ContentSize < 1000000) {
                    
                    
                    contentsize  = objFeedItem.ContentSize / 1000;
                    contentsize  = (contentsize).setscale(1);
                    objFeedItemWrapper.strContentSize = contentsize + ' '+'KB';
                } else {
                    
                    contentsize  = (objFeedItem.contentSize) / 1000000;
                    contentsize  = (contentsize).setscale(1);
                    objFeedItemWrapper.strContentSize = contentsize + ' '+'MB';
                }
                
                if(objFeedItem.RelatedRecordId != null) {
                    
                    objFeedItemWrapper.downloadUrl = strDocumentDownloadUrlprefix+objFeedItem.RelatedRecordId;    
                }
                lstFeedItemWrapper.add(objFeedItemWrapper);
            }
        }
        
        //Fetch the Community Orders
        lstCommunityOrder = [Select Name, Shared_Date_Submitted__c, Status__c, Shared_Total_Quantity__c,
                             (Select id From Community_List_Item__r)
                             From Shared_Community_Order__c
                             Where (Status__c = 'Completed' OR Status__c = 'Submitted')
                             AND Contact__c =: objUser.ContactId];
        system.debug('====lstCommunityOrder==='+lstCommunityOrder); 
        
    }
    
    /**
*  This method Updates the Contact and User Details
*/ 
    public void save(){
        
        system.debug('#Save Method');
        //Rollback to this DataBAse state if any exception occurs in DML's
        Savepoint sp = Database.setSavepoint();
        try{
            
            Contact objContact = new Contact(Id = objUser.ContactId, firstname = objUser.contact.firstName, lastname = objUser.contact.lastname,
                                             email = objUser.contact.email, phone = objuser.contact.phone);
            User objUser = new User(Id= Userinfo.getUserId(), firstname = objUser.contact.firstName, lastname = objUser.contact.lastname,
                                    phone = objuser.contact.phone, email = objUser.contact.email);
            
            Update objContact;
            Update objUser;
            
            fetchRequestedData();
        }
        catch(Exception ex){
            system.debug('ex========='+ex.getMessage());
            Database.rollback( sp );
        }
    }
}