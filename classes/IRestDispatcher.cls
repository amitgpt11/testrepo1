/**
* Interface for the REST Dispatcher
*
* @Author salesforce Services
* @Date 06/12/2015
*/
global interface IRestDispatcher {

	String getApiVersion();

	void setResponse(Integer statusCode);
	void setResponse(Integer statusCode, String responseBody);
	
	void execute();

}