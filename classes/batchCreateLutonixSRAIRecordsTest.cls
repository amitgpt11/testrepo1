/*
 @CreatedDate     16 June 2016                                  
 @author          Ashish-Accenture
 @Description     Test class for Batch Apex class 'batchCreateLutonixSRAIRecords'
 
 */

@isTest
public class batchCreateLutonixSRAIRecordsTest{

    static TestMethod void TestbatchCreateLutonixSRAIRecords(){ 
        
       
        Test.startTest();
            //Execute Batch Class
            batchCreateLutonixSRAIRecords batchObj = new batchCreateLutonixSRAIRecords();
            ID batchprocessid = Database.executeBatch(batchObj,200);
            System.assertNotEquals(batchprocessid,null);
            
            //Execute Schedulable Method
            batchCreateLutonixSRAIRecords sh1 = new batchCreateLutonixSRAIRecords();
            String sch = '0 0 23 * * ?'; 
            String jobId = system.schedule('Daily Reminders', sch, sh1); 
            System.assertNotEquals(jobId,null);  
            
        Test.stopTest();
        
        /*
        list<PI_Lutonix_SRAI__c> lstHeadRec = [Select id from PI_Lutonix_SRAI__c];
        System.assert(lstHeadRec.size() > 0);
*/
           
               
    }
}