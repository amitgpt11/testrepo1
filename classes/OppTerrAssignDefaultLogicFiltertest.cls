@istest
public class OppTerrAssignDefaultLogicFiltertest{
Private Set<Id> accountOnOpportunity = new Set<Id>();
    public boolean isAccountChangeonOpty =false;
    public Id activeModelId = null;
    public Set<Id> ownerOnOpportunity = new Set<Id>();    
    public List<Id> territoryOnOpportunity = new List<Id>();
    public List<Id> matchedTerritories = new List<Id>(); 
    public List<Id> opportunityId = new List<Id>();
    public static List<Territory2> trList;
    public static list<Account> alist;
    
  @TestSetup
    public static void setupMockData(){
        User sysAdmin= UtilForUnitTestDataSetup.getUser();
        
        System.runAs(sysAdmin){
        
            Account a1 = UtilForUnitTestDataSetup.newAccount('ACCTNAME1');
            Account a2 = UtilForUnitTestDataSetup.newAccount('ACCTNAME2');
            alist= new List<Account>{a1, a2};
            insert alist;
            
            //Prepare Users as Owner to Opportunity
            User u1 = UtilForUnitTestDataSetup.newUser();
            u1.Username = 'UserNameC@boston.com';
            u1.IsActive= True;
            User u2 = UtilForUnitTestDataSetup.newUser();
            u2.Username = 'UserNameR@boston.com';
            u2.IsActive= True;
             List<User> ulist=new list<User>{u1,u2};
            insert ulist;
            
            
            Opportunity opp =new Opportunity(name='testOpportunity');
            opp.AccountId=a1.Id;
            opp.StageName='Customer Awareness';
            opp.ForecastCategoryName='Pipeline';
            opp.CloseDate=System.Today();
            insert opp;
            
            
            List<Territory2Type> Ttype = UtilForUnitTestDataSetup.newTerritory2Type();           
             Id Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
                
                
                Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('TO1',Territory2ModelIDActive, Ttype[0].Id ); //Ttype[0].Id               
                Territory2 t2 = UtilForUnitTestDataSetup.newTerritory2('TO2',Territory2ModelIDActive, Ttype[0].Id ); //0M5P00000008OIeKAM
                
                trList=new List<Territory2> {t1,t2};
                insert trList;
                           
                ObjectTerritory2Association OTAsso =UtilForUnitTestDataSetup.newObjectTerritory2Association(alist[0].Id, trList[0].Id);
                ObjectTerritory2Association OTAsso2 =UtilForUnitTestDataSetup.newObjectTerritory2Association(alist[0].Id, trList[1].Id);
                ObjectTerritory2Association OTAsso1 =UtilForUnitTestDataSetup.newObjectTerritory2Association(alist[1].Id, trList[1].Id);
                insert new List<ObjectTerritory2Association> {OTAsso, OTAsso1,OTAsso2};
                
                UserTerritory2Association UTAsso = UtilForUnitTestDataSetup.newUserTerritory2Association(trList[0].Id, ulist[0].Id);
                UserTerritory2Association UTAsso2 = UtilForUnitTestDataSetup.newUserTerritory2Association(trList[1].Id, ulist[0].Id);               
                UserTerritory2Association UTAsso1 = UtilForUnitTestDataSetup.newUserTerritory2Association(trList[1].Id, ulist[1].Id);              
                insert new List<UserTerritory2Association> {UTAsso, UTAsso1,UTAsso2  };
                
                    
                Opportunity oppty1 =UtilForUnitTestDataSetup.newOpportunity(alist[0].Id, 'Opty1');
                oppty1.territory2Id =trList[0].Id;
                oppty1.OwnerId =ulist[0].Id;
                insert new List<Opportunity> {oppty1};
               
        }
      
    
     
}

    public static testmethod void oppTerrAssigntest(){
    
     List<Territory2> trLst = [Select name,Id From Territory2 where name Like : 'TO%'  ]; 
     List<Account> aLst = [Select name,Id From Account where name Like : 'AC%'  ];
      list<User> urLst = new list<User>([Select name,Id From User limit 1 ]); 
    
    Opportunity opp =new Opportunity(name='testOpportunity');
    opp.AccountId=aLst[0].Id;
    opp.StageName='Customer Awareness';
    opp.ForecastCategoryName='Pipeline';
    opp.CloseDate=System.Today();
    opp.Territory2Id = trLst[0].Id;
    opp.OwnerId = urLst[0].Id;
    
    
    
    List<Opportunity> opplistid=new List<Opportunity>();
    opplistid.add(opp);
    insert opplistid;
    
    map<Id,Opportunity> Opptymap = new map<Id,Opportunity>([
            select 
                Id,
                AccountId,
                OwnerId,
                Opportunity_Type__c,
                Trialing_Physician__c,
                Procedure_Account__c,
                Territory2Id 
            from Opportunity 
            where Id= :opplistid[0].Id]);
    if(opplistid!=null&&opplistid.size()>0){
    OppTerrAssignDefaultLogicFilter  objfilter = new OppTerrAssignDefaultLogicFilter();
    List<id> temLst = new List<Id>();
    temLst.addAll(Opptymap.keyset());
    Map<Id,Id> mapid= new map<Id,Id>();
    objfilter.getOpportunityTerritory2Assignments(temLst);
    //Id id=objfilter.getActiveModelId();
    }
      }






}