/**
@Author:Sipra Das
* Name : MyObjectiveController
* Description : Displays list of Territories  on click of the custom Lookup Icon
* Date : 11/04/2016
*/

public with sharing class MyObjectiveController{
    public List<Territory2> results{get;set;} // search results
    public string searchString{get;set;} // search keyword   
    public List<ID> terrId{get;set;}
    public List<Territory2> lstterr{get;set;}
    public List<ObjectTerritory2Association> objT2A{get;set;}
    public My_Objectives__c myobj;
    public String aId{get;set;}
    public String terrSelected{get;set;}
    
    public MyObjectiveController(ApexPages.StandardController stdController) { 
        lstterr=new List<Territory2>();
        terrId=new List<Id>();
        objT2A =new List<ObjectTerritory2Association>();
        
        String objid=  ApexPages.currentPage().getParameters().get('id');    //Get the MyObjective Id
        system.debug('objid : '+objid);
        if(objid!=null){
            String accId=[select Id,Related_to_Account__c from My_Objectives__c where id=:objid].Related_to_Account__c ;//Get the accountId related to myobjective id
            
            system.debug('accId : '+accId);
            myobj = (My_Objectives__c)stdController.getRecord();
            
            objT2A=  [SELECT Id,ObjectId,Territory2Id FROM ObjectTerritory2Association WHERE ObjectId =: accId];//Get the list of territories related to the account
            for(ObjectTerritory2Association  accgrp:objT2A)//Loop through the list of territories
            {
                terrId.add(accgrp.Territory2Id);
            }
            
            lstterr=[SELECT Id, Name,DeveloperName,ParentTerritory2Id,Description,Territory2TypeId FROM Territory2 where id in:terrId];
            results=lstterr;
            System.debug('******'+lstterr);
            
        }
    }
    
    public PageReference save(){
        system.debug('terrSelected:'+terrSelected);
        myobj.Territory__c  = terrSelected;
        upsert myobj ; 
        
        pagereference ref = new pagereference('/'+myobj.id);
        ref.setredirect(true);
        return ref;            
    }
    public void dummyAction(){
        
    }
    public MyObjectiveController() {
        // get the current search string
        searchString = System.currentPageReference().getParameters().get('lksrch');
        results=lstterr;
        runSearch();  
    }
    
    public PageReference showDetail() {
        pagereference ref = new pagereference('/apex/MyObjectiveTerritory?Id={!My_Objectives__c.Id}');
        ref.setredirect(true);
        return ref;
    }
    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }
    
    // prepare the query and issue the search command
    private void runSearch() {
        results = performSearch(searchString);               
    } 
    
    // run the search and return the records found. 
    private List<Territory2> performSearch(string searchString) {
        String terr = ' SELECT Id, Name,DeveloperName,ParentTerritory2Id,Territory2TypeId,Description  FROM Territory2';
        if(searchString != '' && searchString != null)
            terr = terr+  ' where name LIKE \'%' + searchString +'%\'';
        terr = terr + ' limit 200'; 
        System.debug(terr);
        return database.query(terr);        
    }
    
    public PageReference ReturntoParent() {
        PageReference pr = new PageREference('/apex/MyObjectiveTerritory');
        return pr;
    }
    
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }    
}