@isTest
private class ProductCategoriesController_Test {

    private static testMethod void test() {
    
        //Create Comm User
        User objUser = Test_DataCreator.createCommunityUser();
        
        Test_DataCreator.createProducts();
        Product2 objProd = Test_DataCreator.createProduct('test');
        objProd.Shared_Product_Image_URL__c = 'http://www.google.com';
        update objProd;
        
        system.runAs(objUser){
            
            system.Test.setCurrentPage(Page.ProductCategories);
            ProductCategoriesController objProductCategoriesController = new ProductCategoriesController();
            objProductCategoriesController.init();
            system.assert(objProductCategoriesController.lstProductCategories.size()>0,'true');
        }   
    }
}