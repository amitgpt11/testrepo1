/**
* Test class for the NMD_AccountNamingBatch
*
* @Author salesforce Services
* @Date 01/28/2016
*/
@IsTest(SeeAllData=false)
private class NMD_AccountNamingBatchTest {

	static testMethod void test_AccountNamingBatch() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		Account acct = td.newAccount();
		acct.Name = td.randomString5();
		acct.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
		acct.Sold_to_City__c = td.randomString5();
		acct.Sold_to_State__c = 'CA';
		insert acct;

		// nullify the mailing name so the record will be caught by the batch
		acct = [select Mailing_Name__c from Account where Id = :acct.Id];
		acct.Mailing_Name__c = '';
		update acct;

		// show that the account's mailing name is blank
		acct = [select Mailing_Name__c from Account where Id = :acct.Id];
		System.assert(String.isBlank(acct.Mailing_Name__c));

		Test.startTest();
		{

			// execute the batch
			NMD_AccountNamingBatch.runNow();

		}
		Test.stopTest();

		// show that the mailing name was set after the batch finished
		acct = [select Mailing_Name__c from Account where Id = :acct.Id];
		System.assert(String.isNotBlank(acct.Mailing_Name__c));

	}

}