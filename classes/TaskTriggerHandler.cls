/**
* Handler class for the Task trigger
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class TaskTriggerHandler extends TriggerHandler {
    
    private Boolean isDeletePermissible;

    final TaskManager manager = new TaskManager();
    
    public override void bulkBefore() {
        
        if(!trigger.IsDelete){
            
            List<Task> newTasks = (List<Task>)trigger.new;
            
            TaskManager.updateNMDLeadPatientEducationTasks( newTasks );

            //if(trigger.IsUpdate){
            //    manager.fetchUserProfileName();
            //}
        }
        else{
            // The following profiles only must be able to delete the Task
            // In the Before Delete, prevent deletion, if the profile is not an admin
            Set<String> profilesExempt = new Set<String> {
                'EU Sys Admin', 
                    'BSci System Admin', 
                    'System Administrator'
                    };
                        String profileName = [
                            select Name
                            from Profile
                            where Id = :UserInfo.getProfileId()
                        ].Name;
            
            this.isDeletePermissible = profilesExempt.contains(profileName);
        }               
        
    } 

    public override void beforeInsert(sObject obj){
        Task newTask = (Task)obj;

        //default to false on new, only check on update
        //newTask.Make_Me_Owner__c = false;
    }

    /**
    * @desc before Update method
    */
    public override void beforeUpdate(sObject oldObj, sObject newObj){
        Task oldTask = (Task)oldObj;
        Task newTask = (Task)newObj;
        //TaskManager.makeUserTaskOwner(oldTask, newTask);

    }
    
    public override void beforeDelete(sObject obj){
        
        if(!this.isDeletePermissible){
            obj.addError(Label.Can_t_Delete_Activities);
        }
        
    } 
    
}