/**
* Controller Class for the PreFormLead.page  
*
* @Author salesforce Services
* @Date 02/20/2015
*/
public with sharing class NMD_PreformEventExtension {
	
	final ApexPages.StandardController sc;

	public NMD_PreformEventExtension(ApexPages.StandardController sc) {
		this.sc = sc;
	}

	public PageReference preform() {

		Event newEvent = new Event(
			RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('NMD Patient Events').getRecordTypeId(),
		    ActivityDateTime = System.now(),
			DurationInMinutes = 60,
			WhatId = this.sc.getId(),
			Subject = '' //a trigger will update the subject
		);

		try {
			insert newEvent;
		}
		catch (System.DmlException de) {
			return error(de.getDmlMessage(0));
		}

		//send user to newly edit page of newly created task
		ApexPages.StandardController lsc = new ApexPages.StandardController(newEvent);
		PageReference pr = lsc.edit();
		pr.getParameters().put('retURL', lsc.view().getUrl());
		return pr;

	}

	@testVisible
	private PageReference error(String errMsg) {
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errMsg + ' ' + Label.NMD_Please_Contact_Admin));
		Logger.error(
            'NMD_PreformEventExtension', 
            'preform', 
            this.sc.getId(),                                              //id of object
            this.sc.getRecord().getSObjectType().getDescribe().getName(), //name of object 
            errMsg, 
            '', 
            null, 
            0
        );
        return null;
	}

}