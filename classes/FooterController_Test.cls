@isTest
private class FooterController_Test {

	private static testMethod void test() {
	    
	    //Creates the header footer custom settings
	    Test_DataCreator.cerateBostonCSHeaderFooterTestData();
	     
	    // The Parameter Date is the start date of the event. (viz. not used in this test class need only ) 
        Community_Event__c objEvent = Test_DataCreator.createCommunityEvent(Date.Today().addDays(2));
        
        //Creates the Config Custom settings
        Test_DataCreator.cerateBostonCSConfigTestData(objEvent.Id);
        
        //Set the page to get the value of ApexPages.currentPage().getUrl() in controller
	    PageReference pageRef = Page.BSC_SiteTemplate;
        Test.setCurrentPage(pageRef);
        
        FooterController objFooterController1 = new FooterController();
        
        //Creates the Shared_Community_Page__c which is master to 'Shared_Community_METIS_Code__c'
        Shared_Community_Page__c objShared_Community_Page = Test_DataCreator.createCommunityPage();
	    
	    Shared_Community_METIS_Code__c objMETISCode = Test_DataCreator.createCommunityMETISCode(objShared_Community_Page.Id); 
	        
	    //Create Community user 
	    User objUser = Test_DataCreator.createCommunityUser();
	    
        system.Test.setCurrentPage(Page.Events);
        FooterController objFooterController = new FooterController();
        FooterController.currentPageName('BSC_SiteTemplate');
	    
	   
        objMETISCode.Shared_Division__c = ObjUser.Shared_Community_Division__c;
        objMETISCode.Shared_Country__c = 'Default';
        update objMETISCode;
   
	    system.runAs(objUser){
	        
	        FooterController.currentPageName('BSC_SiteTemplate');
	    }
	        
        objMETISCode.Shared_Country__c = objUser.country;
        update objMETISCode;
        FooterController.currentPageName('BSC_SiteTemplate');
        
        system.runAs(objUser){
            
            system.assertEquals(FooterController.pageName, 'BSC_SiteTemplate');
            system.debug('footerCode============='+FooterController.footerCode);
        }
            
    	    //Check the footerImageurl, footerCode is not null
    	   // system.assertNotEquals(null, FooterController.footerImageUrl);
    	   // system.assertNotEquals(null, FooterController.footerCode);
	   // }
	}
}