/**
* REST dispatcher for the Inventory_Settings__c custom setting
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public with sharing class NMD_InventorySettingsDispatcher implements IRestHandler {

	final static String URI_MAPPING = '/nmd/settings';

	public String getURIMapping() {
		return URI_MAPPING;
	}

	public void execute(IRestDispatcher caller, Map<String,String> parameters, Blob requestBody) {

		// only need to return the record
		caller.setResponse(200, JSON.serialize(NMD_Inventory_Settings__c.getInstance()));

	}

}