/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ScheduleMirfBatch implements System.Schedulable {
    global List<Id> userId;
    global ScheduleMirfBatch() {

    }
    global ScheduleMirfBatch(List<Id> userId) {

    }
    global void execute(System.SchedulableContext ctx) {

    }
    global void processBatch(List<Id> userId) {

    }
    global void scheduleNext() {

    }
}
