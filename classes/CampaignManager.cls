/**
* Manager class for Campaign object
*
*
* @Author salesforce Services
* @Date 04/12/2016
*/
public with sharing class CampaignManager {

	static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Campaign.getRecordTypeInfosByName();

	public static final Id RECTYPE_CORPORATE = RECTYPES.get('NMD Corporate Event').getRecordTypeId();
	public static final Id RECTYPE_FIELD = RECTYPES.get('NMD Field Event').getRecordTypeId();

	public static final Set<Id> RECTYPES_NMD = new Set<Id> {
		RECTYPE_CORPORATE, RECTYPE_FIELD
	};

	private boolean defaultStatusFound = false;
	private List<CampaignMemberStatus> newStatusList = new List<CampaignMemberStatus>();
	private List<CampaignMemberStatus> statusesToDelete = new List<CampaignMemberStatus>();
	private Map<Id, List<CampaignMemberStatus>> existingStatuses = new Map<Id, List<CampaignMemberStatus>>();
	private List<NMD_DefaultCampaignStatus__c> campDefaultSettingList = new List<NMD_DefaultCampaignStatus__c>();


    /**
    * Fetches records to be used by later methods
    * 
    * @param List<Campaign> camps list of new campains
    */    
	public void fetchRecords(List<Campaign> camps){

		Set<Id> campIds = new Set<Id>();
		for(Campaign camp : camps){
			if(RECTYPES_NMD.contains(camp.RecordTypeId)){
				campIds.add(camp.Id);
			}
		}

		if(!campIds.isEmpty()){

			//query for existing default campaign statuses to remove
			for(CampaignMemberStatus cms : [
				select Id,
					Label,
					CampaignId,
					IsDefault
				from CampaignMemberStatus
				where CampaignId in :campIds
			]){
				if(!existingStatuses.containsKey(cms.CampaignId)){
					existingStatuses.put(cms.CampaignId, new List<CampaignMemberStatus>());
				}
				existingStatuses.get(cms.CampaignId).add(cms);

				//keep track of existing records that will be deleted
				statusesToDelete.add(cms);
			}

			//pull in the new status values from the custom setting
			campDefaultSettingList = NMD_DefaultCampaignStatus__c.getAll().values();

		}

	}

    /**
    * Loop through the Statuses specified in the custom setting and create a CampainMemberStatuses
    *	for each status found.  If no default is found, throw a nice error before a dml error occurs 
    *
    * @param Campaign newCamp new campain
    */    
	public void createNewMemberStatusRecords(Campaign newCamp){
		if(RECTYPES_NMD.contains(newCamp.RecordTypeId)
			&& !campDefaultSettingList.isEmpty()
		){
			Integer sortOrder = 0;

			if(existingStatuses.containsKey(newCamp.Id)){
				sortOrder = existingStatuses.get(newCamp.Id).size();
			}

			for(NMD_DefaultCampaignStatus__c setting : campDefaultSettingList){

				if(setting.Default__c == true){
					defaultStatusFound = true;
				}

				CampaignMemberStatus cms = new CampaignMemberStatus();
				cms.CampaignId = newCamp.Id;
				cms.Label = setting.Name;
				cms.HasResponded = setting.Responded__c;
				cms.IsDefault = setting.Default__c;
				cms.SortOrder = sortOrder + (Integer)setting.Sort_Order__c;

				newStatusList.add(cms);

			}

			if(defaultStatusFound == false){
				newCamp.addError(Label.NMD_CampaignMemberStatus_No_Defailt);
			}

		}
	}

    /**
    * Insert new CampainMemberStatus records, remove existing ones
    *
    * @param void
    */    
	public void commitCampaignMemberStatus(){
		System.debug('***New: ' + newStatusList);
		System.debug('***Del: ' + statusesToDelete);
		if(!newStatusList.isEmpty() && defaultStatusFound){
			DML.save(this, newStatusList, false);
			
			//Only remove existing statuses if new statuses are being inserted
			if(!statusesToDelete.isEmpty()){
				DML.remove(this, statusesToDelete);
			}

		}

	}


}