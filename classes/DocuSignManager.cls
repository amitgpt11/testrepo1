/**
* Manager class for the Docu Sign Managed Package
*
* @Author Vikas Malik
* @Date 2016-07-05
*/
public with sharing class DocuSignManager {
    
    // Singleton pattern
    private static DocuSignManager instance;
    
    public static DocuSignManager getInstance() {
        if (instance == null) {
            instance = new DocuSignManager();
        }
        return instance;
    }
    
    private static final String COMPLETE_STATUS = 'Completed';
    private static final String DECLLINE_STATUS = 'Declined';
    private static final String OPTIN_STATUS_VAL = 'Opt in';
    private static final String OPTOUT_STATUS_VAL = 'Opt out';
    
    private static final Map<String,String> remoteConsentStausMap = new Map<String,String>{COMPLETE_STATUS => OPTIN_STATUS_VAL,DECLLINE_STATUS => OPTOUT_STATUS_VAL};
    
    private Map<Id,Contact> updateContactMap = new Map<Id,Contact>();
    
    
    public void processRecepientStatus(dsfs__DocuSign_Recipient_Status__c newObj, dsfs__DocuSign_Recipient_Status__c oldObj){
        String optinStatus = getOptinStatus(newObj,oldObj);
        System.debug('#### Calling optinStatus ###'+optinStatus);
        if (null != optinStatus){
            updateContactMap.put(newObj.dsfs__Contact__c,new Contact(Id = newObj.dsfs__Contact__c,  Remote_Consent__c = remoteConsentStausMap.get(optinStatus)));
        }
    }
    
    
    private String getOptinStatus(dsfs__DocuSign_Recipient_Status__c newObj, dsfs__DocuSign_Recipient_Status__c oldObj){
        System.debug('#### Calling getOptinStatus Method ###');
        if (null != newObj.dsfs__Contact__c && (COMPLETE_STATUS.equals(newObj.dsfs__Recipient_Status__c) || DECLLINE_STATUS.equals(newObj.dsfs__Recipient_Status__c) && (null == oldObj || newObj.dsfs__Recipient_Status__c != oldObj.dsfs__Recipient_Status__c))){
            return newObj.dsfs__Recipient_Status__c;
        }
        
        return null;
    }
    
    
    public void updateContacts(){
        if (updateContactMap.values().isEmpty()){
            return;
        }
        try {
            DML.save(this, this.updateContactMap.values());
        }
        catch (System.DmlException de) {
            
        }
    }
    
}