public class ProductsController extends AuthorizationUtil{
    
    
    public List<ProductsWrapper> lstProductWrapper{get;set;}
    public String productFamily {get; set;}
    public String errorMessage {get; set;}
    
    
    
    /**
* Description: method to query product information
*/
    public override void fetchRequestedData() {
        
        lstProductWrapper = new List<ProductsWrapper>();
        
        productFamily = ApexPages.currentPage().getParameters().get('productFamily');
        
        if(String.isBlank(productFamily)) {
            
            errorMessage = 'No product family specified.';
        }
        
        List<User> lstCurrentUser = new List<User>([SELECT Shared_Community_Division__c, LanguageLocaleKey
                                                    FROM User
                                                    WHERE Id =: UserInfo.getUserId()
                                                   ]);
                                                   
       List<Product2> lstProducts = new List<Product2>([SELECT Id, Name, Product_Label__c, Family, Shared_Product_Image_URL__c
                       FROM Product2
                       WHERE Family =: productFamily
                       AND Division__c =: lstCurrentUser[0].Shared_Community_Division__c
                       AND Shared_Parent_Product__c = null
                       AND IsActive = TRUE
                       order by Name
                      ]);
        
        if(lstProducts.isEmpty()) {
            
            errorMessage = 'No active product found under product family - ' + productFamily;
        } else{ //BSMI-253
            
            Set<Id> setProductIds = new Set<Id>();
            Map<Id,String> MapProductIdsToLabel = new Map<Id,String>();
            for(Product2 objProduct: lstProducts){
                
                setProductIds.add(objProduct.Id);                
            }
            
            // 
            if(lstCurrentUser[0].LanguageLocaleKey != 'en_US'){
            
                for(Shared_Community_Product_Translation__c objProductTranslation : [SELECT Id, Name, Shared_Product_Label__c, Shared_Product_Family__c , Shared_Document_URL__c, Shared_Product__c
                           FROM Shared_Community_Product_Translation__c
                           WHERE Shared_Product__c =: setProductIds
                           AND Shared_Language__c =: mapLocaleToLanguage.get(lstCurrentUser[0].LanguageLocaleKey)
                           ORDER BY Name]){
                           
                      MapProductIdsToLabel.put(objProductTranslation.Shared_Product__c, objProductTranslation.Shared_Product_Label__c); 
               }
           }
                         
           for(Product2 objProduct: lstProducts){
                
                ProductsWrapper objProductWrapper;
                if(MapProductIdsToLabel.containsKey(objProduct.Id)){                                       

                  objProductWrapper = new ProductsWrapper(
                                                    MapProductIdsToLabel.get(objProduct.Id),
                                                    objProduct.Id,
                                                    objProduct.Shared_Product_Image_URL__c); 
                } else{
                
                   objProductWrapper = new ProductsWrapper(objProduct.Product_Label__c,
                                                           objProduct.Id,
                                                           objProduct.Shared_Product_Image_URL__c);                
                }
               
               lstProductWrapper.add(objProductWrapper);
           }//BSMI-253
        }
    }
    
    private class ProductsWrapper{
        
        public String strProdName{get;set;}
        public String strProdImageURL{get;set;}
        public String strProdId{get;set;}
        
        public ProductsWrapper(String name, String Id, String imageURL){
            
            this.strProdName = name;
            this.strProdImageURL = imageURL;
            this.strProdId = Id;
        }
    }
}