@isTest
public class EducationController_Test {

    private static testMethod void test() {
    
        //Create Guest User
        User objUser = Test_DataCreator.createCommunityUser();
        
        Test.StartTest();
        
        system.runAs(objUser) {
            
            system.Test.setCurrentPage(Page.Education);
            EducationController objEducationController = new EducationController();
            objEducationController.init();
        }
        Test.stopTest();
    }
}