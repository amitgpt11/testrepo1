/**
* REST Dispatcher, utility class to process inbound REST requests and route them 
* to the appropriate internal method to handle to request
*
* @Author salesforce Services
* @Date 2015/07/15
*/
@RestResource(urlMapping = '/v1/*')
global class RestDispatcherV1 implements IRestDispatcher {

	// requestTypes
	enum RequestType { HTTPGET, HTTPPOST, HTTPPUT, HTTPDELETE, HTTPPATCH }

	// api version
	final static String API_VERSION = 'v1';
	// register handlers
	final static Map<RequestType,List<Type>> dispatchers = new Map<RequestType,List<Type>> {
		RequestType.HTTPGET => new List<Type> {
			NMD_ApprovalsDispatcher.class, // path: /nmd/approvals/{recordId}/{action}
			NMD_ConsignmentAccountDispatcher.class, // path: /nmd/accounts/search?{q}
			NMD_CycleCountLineItemDispatcher.class, // path: /nmd/ccitems/verify?{cid}
			NMD_InventorySettingsDispatcher.class, // path: /nmd/settings
			NMD_OrdersDispatcher.class, // path: /nmd/orders/{orderId}/{action}
			NMD_OrdersHistoryDispatcher.class, // path: /nmd/orders/history
			NMD_OrderTagsDispatcher.class, // path: /nms/ordertags?{oid}
			NMD_ProductsDispatcher.class // path: /nmd/products/{pricebookName}
		},
		RequestType.HTTPDELETE => new List<Type>(),
		RequestType.HTTPPATCH => new List<Type> {
			NMD_CycleCountLineItemDispatcher.class, // path: /nmd/ccitems/insert|update
			NMD_SObjectDispatcher.class // path: /nmd/sobjects/update
		},
		RequestType.HTTPPOST => new List<Type>(),
		RequestType.HTTPPUT => new List<Type>()
	};

	/**
	* @description HTTP methods
	*/
	@HTTPGET
	global static void doGET() {
		new RestDispatcherV1(RequestType.HTTPGET).execute();
	}

	@HTTPPOST
	global static void doPOST() {
		new RestDispatcherV1(RequestType.HTTPPOST).execute();
	}
	
	@HTTPPATCH
	global static void doPATCH() {
		new RestDispatcherV1(RequestType.HTTPPATCH).execute();
	}
	
	@HTTPPUT
	global static void doPUT() {
		new RestDispatcherV1(RequestType.HTTPPUT).execute();
	}
	
	@HTTPDELETE
	global static void doDELETE() {
		new RestDispatcherV1(RequestType.HTTPDELETE).execute();
	}

	public String getApiVersion() {
		return API_VERSION;
	}
	
	final RequestType requestType;

	public RestDispatcherV1(RequestType reqType) {

		this.requestType = reqType;

	}

	public void setResponse(Integer statusCode) {
		setResponse(statusCode, '');
	}

	public void setResponse(Integer statusCode, String responseBody) {
		RestContext.response.statusCode = statusCode;
		RestContext.response.responseBody = Blob.valueOf(responseBody);
	}

	public void execute() {

		// default to 404 NOT_FOUND. if a resource match is found below it will update the response
		setResponse(404);
		
		for (Type t : dispatchers.get(this.requestType)) {

			Object obj = t.newInstance();
			if (!(obj instanceOf IRestHandler)) continue;

			IRestHandler handler = (IRestHandler)obj;
			String uriMapping = '/' + getApiVersion() + handler.getURIMapping();

			// find a matching dispatcher and call its executor
			if (match(RestContext.request.requestURI, uriMapping)) {

				// retrieve parameters
				Map<String,String> parameters = getParameters(RestContext.request.requestURI, uriMapping);
				parameters.putAll(RestContext.request.params);

				// execute the handler's method
				handler.execute(this, parameters, RestContext.request.requestBody);

				// only one match is valid for a resource, so first come first serve. the order that
				// the IRestHandlers are loaded is important
				break;

			}

		}

	}

	private Boolean match(String requestURI, String dispachableURI) {
		
		List<String> dispachableURIList = dispachableURI.split('/');
		List<String> requestURIList = requestURI.split('/');

		if (dispachableURIList.size() != requestURIList.size()) {
			return false;
		}
		else {
			for (Integer i = 0; i < dispachableURIList.size(); i++) {
				
				String uri = dispachableURIList[i];
				if (!uri.startsWith('{') && uri != requestURIList[i]) {
					return false;
				}

			}
		}

		return true;
	}

	private Map<String,String> getParameters(String requestURI, String dispachableURI) {
		
		List<String> dispachableURIList = dispachableURI.split('/');
		List<String> requestURIList = requestURI.split('/');
		Map<String,String> result = new Map<String,String>{};

		for (Integer i = 0; i < dispachableURIList.size(); i++) {

			String uri = dispachableURIList[i];
			if (uri.startsWith('{')) {
				result.put(uri.subString(1, uri.length()-1), requestURIList[i]);
			}

		}

		return result;
	}

}