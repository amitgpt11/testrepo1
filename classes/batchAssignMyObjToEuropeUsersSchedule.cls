/**
* Batch class, to share My Objective record to all the Users from Territory mantioned on the record and the users from its Parent Territory
* @author   Mayuri - Accenture
* @date     26APR2016
*/
global class batchAssignMyObjToEuropeUsersSchedule implements Schedulable{
    
    /**
    *  @desc    Schedulable method, executes the class instance
    */
    global void execute(SchedulableContext context) {
        integer batchSize = Integer.valueOf(system.Label.ETM_Generic_Batch_Size);
        batchAssignMyObjectivesToEuropeUsers b = new batchAssignMyObjectivesToEuropeUsers();
        database.executebatch(b,batchSize);

    }

}