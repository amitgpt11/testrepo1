@isTest
public class CopyrightTermsTest{
    
    @isTest static void test_1(){
    
        Test.startTest();
            
        User objUser = Test_DataCreator.createCommunityUser();
        System.runAs(objUser){
        
            Shared_community_Translation__c objSharedTranslation = new Shared_community_Translation__c(Shared_Page__c = 'Copyright',
                                                                                                        Shared_Language__c = objUser.LanguageLocaleKey,
                                                                                                        Shared_Text__c = 'Test text');
            insert objSharedTranslation;
            
            Shared_community_Translation__c objSharedTranslation2 = new Shared_community_Translation__c(Shared_Page__c = 'Terms of Use',
                                                                                                        Shared_Language__c = objUser.LanguageLocaleKey,
                                                                                                        Shared_Text__c = 'Test text');
            insert objSharedTranslation2;
            
            Test.setCurrentPageReference(new PageReference('Page.copyright')); 
            CopyrightTerms objCopyRightTerms = new CopyrightTerms();
            objCopyRightTerms.init();
            System.assertNotEquals(objCopyRightTerms.getpageText(),null);
            
            Test.setCurrentPageReference(new PageReference('Page.TermsofUse')); 
            objCopyRightTerms = new CopyrightTerms();
            objCopyRightTerms.init();
            System.assertNotEquals(objCopyRightTerms.getpageText(),null);
            
        }
        Test.stopTest();
    }    
}