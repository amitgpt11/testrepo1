/**
* Dispatcher class, to process incoming REST requests
*
* @Author salesforce Services
* @Date 2016/04/11
*/
public without sharing class NMD_ApprovalsDispatcher implements IRestHandler {
    
    // url keys/parts
    final static String KEY_RECORDID = 'recordId';
    final static String KEY_ACTION = 'action';

    private string lastError = '';

    // mapping, ie: /nmd/approvals/{recordId}/{action}
    final static String URI_MAPPING = (String.format('/nmd/approvals/\'{\'{0}\'}\'/\'{\'{1}\'}\'', new List<String> {
        KEY_RECORDID, KEY_ACTION
    }));

    // actions
    final static String ACTION_RECALL = 'recall';
    final static String ACTION_APPROVE = 'approve';
    final static String ACTION_REJECT = 'reject';

    public String getURIMapping() {
        return URI_MAPPING;
    }

    public void execute(IRestDispatcher caller, Map<String,String> parameters, Blob requestBody) {

        // retrieve the Order Ids
        Id recordId;
        try {
            recordId = parameters.get(KEY_RECORDID);
        }
        catch (System.StringException se) {
            // return error
            caller.setResponse(400, 'Invalid record Id');
            return;
        }

        String action = parameters.get(KEY_ACTION);
        Boolean success = false;

        // call the action's internal method
        if ((new Set<String> { ACTION_RECALL, ACTION_APPROVE, ACTION_REJECT }).contains(action)) {

            success = approvalAction(action, recordId);

        }
        else {
            caller.setResponse(400, 'Invalid Action');
            return;
        }

        if (success) {
            caller.setResponse(200);
        }
        else {
            caller.setResponse(500, lastError);
        }   

    }

    public boolean approvalAction(String action, Id recordId){

        System.Savepoint sp = Database.setSavepoint();

        try {

            List<ProcessInstanceWorkitem> approvalsList = new List<ProcessInstanceWorkitem>([               
                select Id, 
                	ActorId,
                    ProcessInstance.TargetObjectId
                from ProcessInstanceWorkitem 
                where ProcessInstance.TargetObjectId = :recordId 
                    and ProcessInstance.Status = 'Pending'
            ]);

            if(approvalsList.isEmpty()){
                lastError = 'No Approvals found';
            } else {
                Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
                ProcessInstanceWorkitem workItem = approvalsList[0];

                if(action == ACTION_RECALL){
                    pwr.setComments('Recalled');
                    pwr.setAction('Removed');
                } else if(action == ACTION_APPROVE){
                    pwr.setComments('Approved');
                    pwr.setAction('Approve');
                } else if(action == ACTION_REJECT){
                    pwr.setComments('Rejected');
                    pwr.setAction('Reject');
                }

                pwr.setWorkitemId(workItem.Id);

				Approval.ProcessResult pr = Approval.process(pwr);

                if(!pr.isSuccess()){
                    lastError = pr.getErrors()[0].getMessage();
                }
            }
            
        }
        catch (System.DmlException de) {
        	if(action == ACTION_RECALL && de.getDmlType(0) == StatusCode.INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY){
        		lastError = Label.Recall_Approval_By_Other_User_Error;
        	} else {
            	lastError = de.getDmlMessage(0);
        	}
        }
        catch (System.Exception ex) {
            lastError = ex.getMessage();
        }

        if (String.isNotBlank(lastError)){
            Database.rollback(sp);
            return false;
        }

        return true;
    }

}