@isTest
private class CustomerCalendarController_Test {
    
    private static testMethod void test() {
        
        system.Test.setCurrentPage(Page.CustomerCalendar);
        CustomerCalendarController objCustomerCalendarController = new CustomerCalendarController();
        
        //Create Guest User
        User objUser = Test_DataCreator.createCommunityUser();
        objUser = [SELECT Id, Contact.ownerId 
                    FROM User
                    WHERE id =: objUser.Id];
        
        Test.StartTest();
        
        objCustomerCalendarController.init();
    system.debug('## I am in test code'+objUser);        
        system.runAs(objUser){
          system.debug('## I am inside run as test code');
          //create Boston scientific cofig test data using Test_DataCreator class
          Id eventID = 'a2nR000000055WA';
            Test_DataCreator.cerateBostonCSConfigTestData(eventID); 
            
            DateTime start_Date = datetime.now();
            DateTime end_Date = datetime.now();
            Meeting__c objMeeting = Test_DataCreator.createMeetingForCalendar('TestEvent', start_Date, end_Date.addDays(5), 'Accepted', objUser.Contact.ownerId);
            String dataInJSONString = '{"AppointmentTopic":null,"ContactId":"'+ objUser.ContactId +'","Description":"Test event","End_Time":"'+String.valueOf(end_Date.date())+'T'+String.valueOf(end_Date.time())+'","EventId":"'+ eventID +'","isAllDayMeeting":false,"Meeting_Request_Status":"test","Start_Time":"'+String.valueOf(start_Date.date())+'T'+String.valueOf(start_Date.time())+'","Name":"test"}';
            //2016-05-11T13:00:00
            
            Holiday hn= new holiday();
            hn.name='Test';
            hn.activitydate=date.today();
            insert hn;
            
            objCustomerCalendarController = new CustomerCalendarController();
            objCustomerCalendarController.init();
            system.assertNotEquals(CustomerCalendarController.getMeetings().size(),0);            
            system.assertNotEquals(CustomerCalendarController.insertMeeting(dataInJSONString),null);
            
            // ApexPages.currentPage().getParameters().put('isEducation','true');
            // objCustomerCalendarController = new CustomerCalendarController();
            // objCustomerCalendarController.init();
            // system.assertEquals(CustomerCalendarController.objMeeting.AppointmentTopic__c,'Training');
        }
        
        Test.StopTest();
    }
}