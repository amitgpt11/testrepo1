/*
 @CreatedDate     14 June 2016                                  
 @author          Ashish-Accenture
 @Description     This class is used to send mail after clicking on 'Generate Email' Custom button on Lutonix SRAI object
       
*/
public class GenerateEmailLutonixTempController{
    public boolean isPartial{get;set;}
    public boolean isMailSent{get;set;}
    list<PI_Lutonix_SRAI__c> lstHeaderRec;
    Id recId;
    Static map<String,Integer> MapNoOfMonth;
    Static{
         MapNoOfMonth = new map<String,Integer>{
        'January' => 1,
        'February' => 2,
        'March' => 3,
        'April' => 4,
        'May' => 5,
        'June' => 6,
        'July' => 7,
        'August' => 8,
        'September' => 9,
        'October' => 10,
        'November' => 11,
        'December' => 12
        };
    }
    
    // fetch records and check whether it is partial month details or not
    public GenerateEmailLutonixTempController(ApexPages.StandardController controller) {
        
        recId = controller.getId();
        lstHeaderRec = [Select id,CreatedDate, ownerid,PI_Partial_Month__c,PI_Month__c,PI_Year__c from PI_Lutonix_SRAI__c where id=: recId];
        Integer daysInMonth;
        isPartial = false;
        isMailSent = false;
        
        if(lstHeaderRec.size() > 0){
            daysInMonth = date.daysInMonth(Integer.ValueOf(lstHeaderRec.get(0).PI_Year__c), MapNoOfMonth.get(lstHeaderRec.get(0).PI_Month__c));
        
            list<PI_Lutonix_SRAI_Detail__c> lstDetailRec = [Select id, PI_Date__c,PI_Lutonix_SRAI__c,PI_Recorded_Temperature__c,PI_Temperature_Scale__c,PI_Units_of_Trunk_Stock__c from PI_Lutonix_SRAI_Detail__c where PI_Lutonix_SRAI__c=: recId];
            if(lstDetailRec.size() == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'No Temperature log records added. Please add records before sending the log report.'));   
                isMailSent = true;
                return;
            }
            
            if(daysInMonth != lstDetailRec.size() && daysInMonth > lstDetailRec.size()){
                isPartial = true;
            }else{
                Map<integer, PI_Lutonix_SRAI_Detail__c> mapDayDetailRec = new Map<integer, PI_Lutonix_SRAI_Detail__c>();
                For(PI_Lutonix_SRAI_Detail__c detail : lstDetailRec){
                    
                    mapDayDetailRec.put(detail.PI_Date__c.day(), detail);
                }
              
                for(integer i=1;i<=daysInMonth; i++){
                    
                    if(mapDayDetailRec.get(i) == null){
                        
                        isPartial = true;
                        break;
                    }/*else{
                        if(mapDayDetailRec.get(i).PI_Recorded_Temperature__c == null || mapDayDetailRec.get(i).PI_Units_of_Trunk_Stock__c == null){
                            isPartial = true;
                            break;
                        }
                    }*/
                }
            }
        
        
            If(isPartial){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'The details of all the days of the month are missing. Do you want to mail Partial Details?'));
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Are you sure you want to send email?'));   
                
            }
        }
        
    }
    
    // call send mail method and update partial month checkbox
    public void sendMailOnPartialData(){
        try{
        boolean isSuccess = SendMail();
        if(isSuccess && isPartial){
            PI_Lutonix_SRAI__c objToUpdate = new PI_Lutonix_SRAI__c();
            objToUpdate.id = recId;
            objToUpdate.PI_Partial_Month__c = true;
            update objToUpdate;
        }
        }catch(exception e){
            System.debug('#####====e==='+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Internal Server Error. Please contact your Administrator.'));
            isMailSent = true;
        }
    }
    
    public pagereference closeWindow(){
        return new pagereference('/'+recId);
    }
    
    // sending mail
    public boolean SendMail(){
    
        list<EmailTemplate> emailTempl = [ select id, name, body, subject, HTMLValue  from EmailTemplate where name='SRAI Temperature Log'];
        boolean returnFlag;
        if(emailTempl.size() > 0){    
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(emailTempl.get(0).id);
            mail.setTargetObjectId(lstHeaderRec.get(0).OwnerId);
            mail.setwhatId(recId);
            mail.setSaveAsActivity(false);
            mail.setSenderDisplayName('Bsci Admin');
            if(!Label.Lutonix_Temperature_Email_id.equalsIgnoreCase('no') && Label.Lutonix_Temperature_Email_id.contains('@') && Label.Lutonix_Temperature_Email_id.contains('.')){
                mail.setCCAddresses(new list<String>{Label.Lutonix_Temperature_Email_id});    
            }
           
            List<Messaging.SendEmailResult> results;
            try{
                results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                System.debug('======results ===='+results );
                
            }Catch(exception e){
                System.debug('####===e==='+e);
                returnFlag = false;
            }
            
            If(results.size() > 0){
                returnFlag = results.get(0).isSuccess();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Email Sent Successfully.'));
                isMailSent = true;
            }
            
       }
       return returnFlag;
    }
    
    // check whether this page opened on SF1 or not
    public Boolean isSF1 {
        get{
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
                String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
                ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
                (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
            ) {
                return true;
            }else{
                return false;
            }
        }
   } 

}