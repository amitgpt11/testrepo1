/** 
    Description: This class works as Controller for UnAuthenticatedHomePage
*/
public class UnAuthenticatedHomePageController {
    
    public String imageUrl     {get;set;}
    
    /** 
     * Description: This method initilizes the imageUrl to the UnAuthenticated Banner Image
    */
    public PageReference init(){
/** Commented out the redirect for non-guest users to the Authenticated page so the 
 * configuraiton can be shown in the demo for BSC        
 *      if(userinfo.getUserType() != 'Guest'){
 *           
 *           return new PageReference('/apex/AuthenticatedHomePage');
 *      }
*/ 
        system.debug('========Boston_Scientific_Config__c.getValues(Default)======='+Boston_Scientific_Config__c.getValues('Default'));
        if(Boston_Scientific_Config__c.getValues('Default').Boston_UnAuthenticatedpage_BannerImageId__c != null &&
            Boston_Scientific_Config__c.getValues('Default').Boston_Document_Content_Base_Url__c != null){
            
            imageUrl = Boston_Scientific_Config__c.getValues('Default').Boston_Document_Content_Base_Url__c+'/servlet/servlet.ImageServer?id='+Boston_Scientific_Config__c.getValues('Default').Boston_UnAuthenticatedpage_BannerImageId__c+
                '&oid='+userinfo.getOrganizationId();                
        }
        return null;
    }
}