/**
* Unit test for logger utility.
*
* @Author salesforce Services
* 
*/

@IsTest(SeeAllData=false)
public without sharing class LoggerTest {

    @TestSetup
    static void init() {
        Log_Settings__c settings = new Log_Settings__c(Debug__c = true, Info__c = true, Error__c = true, Warning__c = true);
        insert settings;
    }
    
    static testMethod void testCoreLoggingFunctionality() {
        //init();
        System.assert(Log_Settings__c.getInstance().Debug__c == true);
        System.assert(Log_Settings__c.getInstance().Info__c == true);
        System.assert(Log_Settings__c.getInstance().Warning__c == true);
        System.assert(Log_Settings__c.getInstance().Error__c == true);
        
        System.assertEquals(0, Database.countQuery('select count() from application_log__c'));
        String s = createString(33000);
        Logger.logMessage(Logger.ERROR, s, s, s, s, s, s, null, 19);
        System.assertEquals(1, Database.countQuery('select count() from application_log__c'));
        
        Application_Log__c log = [select source_class__c, source_function__c, record_id__c, record_object_type__c, message__c, integration_payload__c, severity__c, log_code__c, timer__c from application_log__c];
        
        System.assertEquals(Logger.ERROR, log.Severity__c);
        System.assertEquals(255, log.Source_Class__c.length());
        System.assertEquals(200, log.Source_Function__c.length());
        System.assertEquals(18, log.Record_id__c.length());
        System.assertEquals(255, log.record_object_type__c.length());
        System.assertEquals(255, log.Message__c.length());
        System.assertEquals(32768, log.Integration_Payload__c.length());
        System.assertEquals(19, log.Timer__c);
        
        // TODO - add log.Timestamp
    }
    
    static testMethod void testDebug() {
        //init();
        System.assertEquals(0, Database.countQuery('select count() from application_log__c'));
        Logger.debug('LoggerTest', 'testDebug', null, 'Application_Log__c', 'Test class');
        System.assertEquals(1, Database.countQuery('select count() from application_log__c'));
    }
    
    static testMethod void testDebugFull() {
        //init();
        System.assertEquals(0, Database.countQuery('select count() from application_log__c'));
        Logger.debug('LoggerTest', 'testDebug', null, 'Application_Log__c', 'Test class', 'payload', null, 0);
        System.assertEquals(1, Database.countQuery('select count() from application_log__c'));
    }
    
    static testMethod void testInfo() {
        //init();
        System.assertEquals(0, Database.countQuery('select count() from application_log__c'));
        Logger.info('LoggerTest', 'testInfo', null, 'Application_Log__c', 'Test class', 'payload', null, 0);
        System.assertEquals(1, Database.countQuery('select count() from application_log__c'));
    }
    
    static testMethod void testWarn() {
        init();
        System.assertEquals(0, Database.countQuery('select count() from application_log__c'));
        Logger.warn('LoggerTest', 'testWarn', null, 'Application_Log__c', 'Test class', 'payload', null, 0);
        System.assertEquals(1, Database.countQuery('select count() from application_log__c'));
    }
    
    static testMethod void testError() {
        //init();
        System.assertEquals(0, Database.countQuery('select count() from application_log__c'));
        Logger.error('LoggerTest', 'testError', null, 'Application_Log__c', 'Test class', 'payload', null, 0);
        System.assertEquals(1, Database.countQuery('select count() from application_log__c'));
    }
    
    static testMethod void testExceptionLogging() {
        //init();
        System.assertEquals(0, Database.countQuery('select count() from application_log__c'));
        Exception e = createException();
        System.assert(e != null);
        Logger.exception('LoggerTest', 'testExceptionLogging', null, 'Application_Log__c', 'payLoad', e);
        System.assertEquals(1, Database.countQuery('select count() from application_log__c'));
        
        Application_Log__c log = [select source_class__c, source_function__c, message__c, stack_trace__c, severity__c, log_code__c, timer__c from application_log__c];
        System.assertEquals(Logger.ERROR, log.Severity__c);
        System.assert(log.Stack_Trace__c != null);
        System.assert(log.Stack_Trace__c.length() > 1);
        System.assert(log.Message__c != null);
        System.assert(log.Message__c.length() > 1);
    }
    
    static testMethod void testFutureLogging() {
        //init();
        List<ApplicationLogWrapper> futureLogs = createLogWrappers(7);
        System.assertEquals(0, Database.countQuery('select count() from application_log__c'));
        Test.startTest();
        System.assertEquals(7, futureLogs.size());
        Logger.logFutureMessages(Logger.logWrappersToJSON(futureLogs));
        Test.stopTest();
        System.assertEquals(futureLogs.size(), Database.countQuery('select count() from application_log__c'));
    }

    static testMethod void testDatabaseErrorAplicationWrapper() {

        //insert account w/no name to generate an error
        Account acct = new Account(
            RecordTypeId = AccountManager.RECTYPE_CONSIGNMENT
        );

        Database.SaveResult result = Database.insert(acct, false);

        ApplicationLogWrapper alw = new ApplicationLogWrapper(result.getErrors()[0], UserInfo.getUserId(), '');

    }
    
    private static String createString(integer length) {
        String[] chars = new String[]{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
            String s = '';
        for(integer i = 0; i < length; i++) {
            s += chars[Math.mod(i, chars.size())];
        }
        return s;
    }
    
    private static Exception createException() {
        Exception e = null;
        try {
            String s = null;
            Integer i = s.length();
        } catch(Exception ex) {
            e = ex;
        }
        return e;
    }
    
    private static List<ApplicationLogWrapper> createLogWrappers(integer count) {
        List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>();
        for(integer i = 0; i < count; i++) {
            ApplicationLogWrapper log = new ApplicationLogWrapper(Logger.ERROR, 'LoggerTest', 'futureLogging'); 
            log.logMessage = 'Log wrapper ' + i;
            log.timer = 1000 + i;
            logs.add(log);
        }
        
        return logs;    
    }
    
}