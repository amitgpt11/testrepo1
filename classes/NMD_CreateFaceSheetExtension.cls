/**
* Extension class for the vf page Create Face Sheet
*
* @Author salesforce Services
* @Date 03/01/2015
*/
public without sharing class NMD_CreateFaceSheetExtension extends NMD_ExtensionBase {

	final static String MISC = 'Word Excel PDF';

	final ApexPages.StandardController sc;

	public List<NMD_ObjectWrapper> attachments { get; private set; }
	public String pdfUrl { get; set; }

	public NMD_CreateFaceSheetExtension(ApexPages.StandardController sc) {

		this.sc = sc;
		this.attachments = new List<NMD_ObjectWrapper>{};
		this.pdfUrl = '';

		Opportunity oppty = (Opportunity)sc.getRecord();

		Set<String> classifications = new Set<String>{};
		for (PatientImageClassifications__c pic : PatientImageClassifications__c.getAll().values()) {
			if (pic.Is_Active__c && pic.Name != MISC) {
				classifications.add(pic.Name);
			}
		}

		for(FeedItem item : [
            select 
            	CreatedDate,
            	RelatedRecordId,
            	Title, 
            	ContentDescription
            from FeedItem
            where ParentId = :oppty.Patient__c
            and Type = 'ContentPost'
            order by CreatedDate desc
        ]) {
        	// ContentDescription and RelatedRecordId cannot be filtered in a query call
        	if (classifications.contains(item.ContentDescription) && item.RelatedRecordId != null) {
				this.attachments.add(new NMD_ObjectWrapper(item));
			}
		}

	}

	public PageReference checkRedirect() {
		PageReference pr;

		if (ApexPages.currentPage().getParameters().get('isdtp') != 'p1') {
			pr = Page.CreateFaceSheet;
			pr.getParameters().put('id', this.sc.getId());
		}

		return pr;
	}

	public void submit() {

		String selectedIds = '';
        for (NMD_ObjectWrapper item : this.attachments) {
            if (item.selected) {
                selectedIds += item.data.Id + ',';
            }
        }

        PageReference pr = Page.FaceSheet;
        pr.getParameters().putAll(new Map<String,String> {
        	'id' => this.sc.getId(),
        	'cid' => selectedIds
        });

        this.pdfUrl = pr.getUrl();

	}

	@RemoteAction
	public static Map<String,Object> generatePdf(Id opptyId, List<Id> childIds) {

		String errorMessage = '';
		Map<String,Object> result = new Map<String,Object>{
			'is_success' => false,
			'message' => 'Unknown'
		};

		Opportunity oppty = [select Name, Patient__r.Name from Opportunity where Id = :opptyId];

		String childIdsStr = '';
		for (Id childId : childIds) {
			childIdsStr += childId + ',';
		}

		PageReference pr = Page.FaceSheet;
		pr.getParameters().putAll(new Map<String,String> {
            'id' => oppty.Id,
            'cid' => childIdsStr
        });

		//EmailTemplate template = templates[0];
		String subject = String.format('Face Sheet for {0} - {1}', new List<String> {
			oppty.Name,
			oppty.Patient__r.Name
		});
		String textBody = URL.getSalesforceBaseUrl().toExternalForm() + pr.getUrl();

		SendEmailManager emailManager = new SendEmailManager(subject, textBody, UserInfo.getUserEmail());
        Blob content = Test.isRunningTest() ? Blob.valueOf('X') : pr.getContent();
        if (content.size() < 5242880) {
        	emailManager.addAttachment('facesheet.pdf', content);
        }
        else {
        	content = null;
        }
		
		try {
			Messaging.SendEmailResult emailResult = emailManager.sendEmail();
			if (!emailResult.isSuccess()) {
				errorMessage = (emailResult.getErrors()[0]).getMessage();
			}
		}
		catch (System.EmailException ee) {
			errorMessage = ee.getMessage();
		}


		if (String.isNotBlank(errorMessage)) {
			
			result.putAll(new Map<String,Object> {
				'is_success' => false,
				'message' => errorMessage
			});

			Logger.error(
	            'NMD_CreateFaceSheetExtension', 
	            'generatePdf', 
	            opptyId,
	            'Opportunity',
	            errorMessage, 
	            '', 
	            null, 
	            0
	        );

		}
		else {
			result.putAll(new Map<String,Object> {
				'is_success' => true,
				'message' => Label.NMD_Email_Sent
			});
		}

		return result;

	}

}