/*****  
        Class:  DemoRequestFormExtension  (Controller for Page - DemoRequestForm)
        Created By: Accenture Team
        Created Date: 16th-June-2016
        Last Modified By: Narendra Accenture
        Last Modify Date: 29th-July-2016
        Description: This page is used to create Demo Request form and send email to quincy as pdf.
****/
public class DemoRequestFormExtension {
    public String loggedInUserName{get;set;}
      
    public String SelectedItem{get;set;}
    
    public String shippingAddress{get;set;}
    public String facilityID{get;set;}
    public Shared_Request_Form__c requestForm{get;set;}
    public List<Shared_Request_Form_Line_Item__c> requestFormItemList{get;set;}
    public List<Product2> products_LEVEL4{get;set;}
    public List<Product2> products_LEVEL5{get;set;}
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_OPPTY = Schema.SObjectType.Product2.getRecordTypeInfosByName();
    public static final Id RECTYPE_LEVEL4 = RECTYPES_OPPTY.get('Standard Product Hierarchy - Level 4').getRecordTypeId();
    public static final Id RECTYPE_LEVEL5 = RECTYPES_OPPTY.get('Standard Product Hierarchy - Level 5').getRecordTypeId();
    static final Map<String,Schema.RecordTypeInfo> RECTYPES_RequestForm = Schema.SObjectType.Shared_Request_Form__c.getRecordTypeInfosByName();
    public static final Id RECTYPE_Demo_Request = RECTYPES_RequestForm.get('Demo Request').getRecordTypeId();
    public User userRecord;
    Set<String> productLevel5Set = new Set<String>();
    List<String> productLevel5List = new List<String>();
    
    public DemoRequestFormExtension(){
        requestForm = new Shared_Request_Form__c();
        requestFormItemList = new List<Shared_Request_Form_Line_Item__c>();
        //products_LEVEL4 = [SELECT EAN_UPN__c,Id,Name,RecordTypeId,Division__c,UPN_Material_Number__c FROM Product2 WHERE RecordTypeId =:RECTYPE_LEVEL4 AND Division__c='PI' ];
        products_LEVEL4 = [SELECT EAN_UPN__c,Id,Name,RecordTypeId,Division__c,UPN_Material_Number__c,Shared_Custom_Product_Group_Level_5__c  FROM Product2 WHERE RecordTypeId =:RECTYPE_LEVEL5 AND Division__c='PI' AND Shared_Custom_Product_Group_Level_5__c !=''];
        
        for(Product2 prd : products_LEVEL4){
        productLevel5Set.add(prd.Shared_Custom_Product_Group_Level_5__c );
        }
        
        productLevel5List.addAll(productLevel5Set); 
        productLevel5List.sort();
        if(products_LEVEL4.Size() > 0 ){
        //products_LEVEL5 = [SELECT EAN_UPN__c,Id,Name,RecordTypeId,UPN_Material_Number__c, Description FROM Product2 WHERE Shared_Parent_Product__c =:products_LEVEL4[0].Id ];
        
        products_LEVEL5 = [SELECT EAN_UPN__c,Id,Name,RecordTypeId,UPN_Material_Number__c, Description FROM Product2 WHERE Shared_Custom_Product_Group_Level_5__c =:productLevel5List[0] ];
       
        }else{
        
        products_LEVEL5 = new List<Product2>();
        }
        
        for(Product2 prt : products_LEVEL5){
           Shared_Request_Form_Line_Item__c  rItem = new Shared_Request_Form_Line_Item__c(); 
            rItem.Full_UPN__c = prt.UPN_Material_Number__c;
            rItem.Description__c = prt.Description;
            rItem.Quantity__c = 0;
            rItem.Unit_Of__c = 'EA';
           requestFormItemList.add(rItem); 
        }
        
        userRecord = [Select ID,Name,Cost_Center_Code__C from User Where Id =:UserInfo.getUserId() limit 1];
        loggedInUserName = UserInfo.getName();
        requestForm.Cost_Center__c = userRecord.Cost_Center_Code__C;
    }
    public DemoRequestFormExtension.DemoRequestFormExtension(ApexPages.StandardController stdController){
       
    
    }
    
    public pagereference save () {
        return null;
    }
    
    
    public PageReference sendPdf(){
        System.debug('Request Form : '+this.requestForm);
        System.debug('Facility ID '+facilityID);
        requestForm = this.requestForm;
        requestForm.Facility__C = facilityID;
        requestForm.RecordTypeId = RECTYPE_Demo_Request; 
        
        //requestForm.Name = loggedInUserName;
        requestForm.Requesters_Name__c = UserInfo.getUserId();

        try{

        insert this.requestForm;
        }catch(Exception ex){
      
        System.debug('Error '+ex.getMessage());
        }
        
       if(this.requestForm.Id != null){

        for(Shared_Request_Form_Line_Item__c r : requestFormItemList){
            
            r.Request_Form__c = this.requestForm.Id;
            
        }     
        
        try{   
        System.debug('<<<<<<Inserting requestFormItemList :'+requestFormItemList);
        
        List<Shared_Request_Form_Line_Item__c> requestFormItemFilteredList = new List<Shared_Request_Form_Line_Item__c> ();
            
            if(requestFormItemList.size() > 0){
               // insert requestFormItemList;
               for(Shared_Request_Form_Line_Item__c reqFrmLineItem : requestFormItemList ){
                    if(reqFrmLineItem.Quantity__c > 0){
                        requestFormItemFilteredList.add(reqFrmLineItem);
                    }//End of If
                }//End of Loop

            Insert requestFormItemFilteredList;
               
            }//End of If
            }catch(Exception ex){
            System.debug('Error '+ex.getMessage());
            }
            
           // sendRequestFormToQuincy(this.requestForm.Id);
           
           
            Shared_Request_Form__c reqForm = [Select Id,Name,Requesters_Name__c,Requesters_Name__r.Name from Shared_Request_Form__c  Where Id =:this.requestForm.Id limit 1];
       
        //PageReference pdf = ApexPages.currentPage();//Page.DemoRequestFormpdf;
        //S PageReference pdf = Page.DemoRequestFormPDFA;
         PageReference pdf = Page.callPdfPage;
        // add parent id to the parameters for standardcontroller
        system.debug('<<<>>>' + pdf);
        pdf.getParameters().put('id',this.requestForm.Id);
        system.debug('<<<>>>>>>>' + pdf);
        
      
        System.debug('PDF url : '+pdf);
        // the contents of the attachment from the pdf
        Blob body;
        pdf.setRedirect(true);
         return pdf;
       /* try {
    
          // returns the output of the page as a PDF
           
          //body = pdf.getContent();
          
          
          body = pdf.getContentAsPDF();
          System.debug('===body==='+body);      
    
        // need to pass unit test -- current bug  
        } catch (Exception e) {
          body = Blob.valueOf('Some Text');
          system.debug('<<>>>'+ e);
        }
        
         Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        attach.setContentType('application/pdf');
        attach.setFileName(reqForm.Name+'.pdf');
        attach.setInline(false);
        attach.Body = body;
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setToAddresses(new String[] { 'Susannah.st-germain@bsci.com' });//{ 'Quincy-DemoRequests@bsci.com' });
        mail.setCcAddresses(new String[] { UserInfo.getUserEmail() });
        mail.setSubject('Demo Request Form');
        mail.setHtmlBody('Hello Quincy, <br/><br/>Please find attachment for demo request form - '+reqForm.Name+' <br/><br/> Thanks <br/>'+reqForm.Requesters_Name__r.Name);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 

    // Send the email
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/

        }
    
        
        return new PageReference('/'+this.requestForm.id);
    }
 
    public PageReference ValidateValues(){
            facilityID = requestForm.Facility__c;
            List<Account> acclist = [SELECT Id,Name,Account_Number__c,ShippingCity,ShippingPostalCode,ShippingState,ShippingStreet,Sold_to_Attn__c,Sold_to_City__c,Sold_to_Country__c,Sold_to_Fax__c,Sold_to_Phone__c,Sold_to_State__c,Sold_to_Street__c,Sold_to_Zip_Postal_Code__c FROM Account where id =:requestForm.Facility__c ];
            //List<Account> acclist = [Select id,Name,ShippingAddress,ShippingCity,ShippingPostalCode,ShippingState,ShippingStreet  from Account where id =:requestForm.Facility__c ];
           if(acclist.Size() > 0  ){
                requestForm.Shipping_Address__c = (acclist[0].ShippingStreet == null ? acclist[0].Sold_to_Street__c: acclist[0].ShippingStreet);
                requestForm.City__c = (acclist[0].ShippingCity == null ? acclist[0].Sold_to_City__c: acclist[0].ShippingCity);
                requestForm.State__c = (acclist[0].ShippingState == null ? acclist[0].Sold_to_State__c : acclist[0].ShippingState);
                requestForm.Zip_Code__c = (acclist[0].ShippingPostalCode == null ? acclist[0].Sold_to_Zip_Postal_Code__c : acclist[0].ShippingPostalCode);
                requestForm.SAP_Account_Number__c = acclist[0].Account_Number__c;
           }
       return null;
    }
    
    public PageReference changeProductLEVEL4(){
        
        products_LEVEL5 = [SELECT EAN_UPN__c,Id,Name,RecordTypeId, UPN_Material_Number__c, Description FROM Product2 WHERE Shared_Custom_Product_Group_Level_5__c  =:SelectedItem ];
        requestFormItemList.clear();
        for(Product2 prt : products_LEVEL5){
           Shared_Request_Form_Line_Item__c  rItem = new Shared_Request_Form_Line_Item__c(); 
            rItem.Full_UPN__c = prt.UPN_Material_Number__c;
            rItem.Description__c = prt.Description;
            rItem.Quantity__c = 0;
            rItem.Unit_Of__c = 'EA';
           requestFormItemList.add(rItem); 
        }
        return null;
    } 
 
    public List<SelectOption> getLEVEL4Products(){
        List<SelectOption> options = new List<SelectOption>();
        
        for(String prd : productLevel5List){
            options.add(new SelectOption(prd, prd));
        }
        return options;
    }//End of Method    
    
    
    
        public PageReference changeCostCenter(){
       if(requestForm.Requesters_Name__c!=null){
        userRecord = [Select ID,Name,Cost_Center_Code__C from User Where Id =:requestForm.Requesters_Name__c limit 1];
        requestForm.Cost_Center__c = userRecord.Cost_Center_Code__C;
        
    } 
    return null;
    }
}