/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class NMD_OrdersDispatcherTest {

	static final String ACCTNAME = 'ACCTNAME';
	static final RestDispatcherV1 dispatcher = new RestDispatcherV1(null);

	static {
		RestContext.response = new RestResponse();
	}
	
	@testSetup 
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		td.setupNuromodUserRoleSettings();

		Account acct = td.createConsignmentAccount();
		acct.Name = ACCTNAME;
		insert acct;

		Opportunity oppty = td.newOpportunity(acct.Id);
		insert oppty;

		Order order = new Order(
			AccountId = acct.Id,
			OpportunityId = oppty.Id,
			EffectiveDate = System.today(),
    		Status = 'Draft',
    		Order_Method__c = 'Approval'
		);
		insert order;

	}

	static testMethod void test_URI() {

		NMD_OrdersDispatcher handler = new NMD_OrdersDispatcher();
		String uri = handler.getURIMapping();

	}

	static testMethod void test_Order_SubmitAction() {

		Order order = [select Id from Order where Opportunity.Account.Name = :ACCTNAME];

		NMD_OrdersDispatcher handler = new NMD_OrdersDispatcher();
		handler.execute(dispatcher, new Map<String,String>{
			'orderId' => order.Id,
			'action' => 'submit'
		}, null);

	}

	static testMethod void test_Order_ValidateAction() {

		Order order = [select Id from Order where Opportunity.Account.Name = :ACCTNAME];

		NMD_OrdersDispatcher handler = new NMD_OrdersDispatcher();
		handler.execute(dispatcher, new Map<String,String>{
			'orderId' => order.Id,
			'action' => 'validate'
		}, null);

	}

	static testMethod void test_Order_VerifyAction() {

		Order order = [select Id from Order where Opportunity.Account.Name = :ACCTNAME];

		NMD_OrdersDispatcher handler = new NMD_OrdersDispatcher();
		handler.execute(dispatcher, new Map<String,String>{
			'orderId' => order.Id,
			'action' => 'verify'
		}, null);

	}

	static testMethod void test_Order_InvalidAction() {

		Order order = [select Id from Order where Opportunity.Account.Name = :ACCTNAME];

		NMD_OrdersDispatcher handler = new NMD_OrdersDispatcher();
		handler.execute(dispatcher, new Map<String,String>{
			'orderId' => order.Id,
			'action' => 'foo'
		}, null);

	}

	static testMethod void test_Order_InvalidOrder() {

		Order order = [select Id from Order where Opportunity.Account.Name = :ACCTNAME];

		NMD_OrdersDispatcher handler = new NMD_OrdersDispatcher();
		handler.execute(dispatcher, new Map<String,String>{
			'orderId' => 'foo'
		}, null);

	}

}