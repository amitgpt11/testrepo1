/**
* Extension class for the ProductBillingForm vf page
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public with sharing class NMD_ProductBillingFormExtension {
	
	public transient List<OrderItem> firstItems { get; private set; }
	public transient List<OrderItem> secondItems { get; private set; }

	public NMD_ProductBillingFormExtension(ApexPages.StandardController sc) {
		Boolean foundVirtualKits = false;

		this.firstItems = new List<OrderItem>{};
		this.secondItems = new List<OrderItem>{};
		for (OrderItem item : [
			select
				Item_Status__c,
				Quantity,
				UnitPrice,
				PricebookEntry.Product2.Description,
				PricebookEntry.ProductCode,
				No_Charge_Reason__c,
				Serial_Number__c,
				Lot_Number__c,
				Virtual_Kit__c,
				IsVirtualKit__c
			from OrderItem
			where OrderId = :sc.getId()
			and Item_Status__c != 'Explanted'
			order by 
				Order_Item_Id__c ASC
			//	PricebookEntry.ProductCode,
			//	Quantity desc
		]) {

			if(item.IsVirtualKit__c){
				foundVirtualKits = true;
			}

			if (item.No_Charge_Reason__c != 'Pre Purchase') {
				this.firstItems.add(item);
			}
			else {
				this.secondItems.add(item);
			}

		}

		if(foundVirtualKits){
			firstItems = sortListForVirtualKits(firstItems);
			secondItems = sortListForVirtualKits(secondItems);
		}
		
	}

    /**
    * Adjusts the order of the Order items and makes sure that the Virtual Kit children
    *   are after the parent record.  
    * 
    * @param List<Orderitem> itemList List of OrderItem records
    */   
	private List<OrderItem> sortListForVirtualKits(List<OrderItem> itemList){
		List<OrderItem> sortedList = new List<OrderItem>();

		List<OrderItem> kits = new List<OrderItem>();
		Map<Id, List<OrderItem>> kitItemMap = new Map<Id, List<OrderItem>>();

		//Filter virtual kit items into a map with the kit record id as the key
		//	Separate kits and non-virtual-kit records into a separate list
		for(OrderItem item : itemList){
			if(item.Virtual_Kit__c != null){
				if(!kitItemMap.containsKey(item.Virtual_Kit__c)){
					kitItemMap.put(item.Virtual_Kit__c, new List<OrderItem>());
				}

				kitItemMap.get(item.Virtual_Kit__c).add(item);
			} else {
				kits.add(item);
			}
		}

		//loop through separated list and add items to a new list in order.
		//	When you find an item with a kit, add the kit and its related items
		for(OrderItem item : kits){
			sortedList.add(item);
			if(kitItemMap.containsKey(item.Id)){
				for(OrderItem kitItem : kitItemMap.get(item.Id)){
					sortedList.add(kitItem);
				}
			}
		}

		return sortedList;
	}

}