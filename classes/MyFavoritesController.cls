/*
@CreatedDate     3JUNE2016
@author          Julianne Diamond
@Description     Controller for MyFavorites VF Page
@Requirement Id  User Story : US2674
*/


//Gets current user's Favorites
public class MyFavoritesController {

    public static List<Opportunity_Favorite__c> getOpportunityFavorites(){
        List<Opportunity_Favorite__c> results = [SELECT Id, Opportunity__r.Id, Opportunity__r.Name, Opportunity__r.Account.Id,Opportunity__r.Account.Name, Opportunity__r.Amount, Opportunity__r.CloseDate, Opportunity__r.Owner.Name,Opportunity__r.StageName,Opportunity__r.LastModifiedDate 
                                                FROM Opportunity_Favorite__c WHERE User__r.Id = :UserInfo.getUserId() order by Opportunity__r.Account.Name ASC];
        
        return results;       
    }
    
    public static List<Account_Favorite__c> getAccountFavorites(){
        List<Account_Favorite__c> results = [SELECT Id, Account__r.Id,Account__r.Name,Account__r.GPO_Name__c,Account__r.GPO_ID__c,Account__r.IDN_Name__c,Account__r.IDN_ID__c,Account__r.Favorite__c 
                                             FROM Account_Favorite__c WHERE User__r.Id = :UserInfo.getUserId()  order by Account__r.Name ASC];
        return results;
    }
    
    public static List<Contact_Favorite__c> getContactFavorites(){
        List<Contact_Favorite__c> results = [SELECT Id, contact__r.Id, contact__r.Name, contact__r.Lastname,contact__r.FirstName,contact__r.Account.Name
                                             FROM Contact_Favorite__c WHERE User__r.Id = :UserInfo.getUserId() order by contact__r.Lastname ASC];
        
        return results;
    }

    
//Remove record selected from MyFavorites page

    public static void removeFromAccountFavorites(){
        Id acctFavoriteId = Apexpages.currentPage().getParameters().get('acctFavoriteId');
        Account_Favorite__c myAcctFav = [SELECT Id FROM Account_Favorite__c WHERE Id = :acctFavoriteId ];
                                        

        try {
            delete myAcctFav;
        }
        catch (DMLException e){
            System.debug('The following DML exception was thrown: '+e);
        }
    }
    
    public static void removeFromContactFavorites(){
        Id contactFavoriteId = Apexpages.currentPage().getParameters().get('contactFavoriteId');
        Contact_Favorite__c myContFav = [SELECT Id FROM Contact_Favorite__c WHERE Id = :contactFavoriteId ];
                                        
        try {
            delete myContFav;
        }
        catch (DMLException e){
            System.debug('The following DML exception was thrown: '+e);
        }
    }
    
    public static void removeFromOpportunityFavorites(){
        Id optyFavoriteId = Apexpages.currentPage().getParameters().get('optyFavoriteId');
        Opportunity_Favorite__c myOptyFav = [SELECT Id FROM Opportunity_Favorite__c WHERE Id = :optyFavoriteId ];

        try {
            delete myOptyFav;
        }
        catch (DMLException e){
            System.debug('The following DML exception was thrown: '+e);
        }
    }
    
// Inline vfPage Remove favorites
    Public Static boolean removeFromFavorites(Id favoriteId){
        //sObject sObj = Schema.getGlobalDescribe().get(ObjectName).newSObject() ;
        boolean returnFlag = true;
        try{
            String sObjName = favoriteId.getSObjectType().getDescribe().getName();
            String query = 'Select id from '+String.escapeSinglequotes(sObjName)+' where id =:favoriteId';
            System.debug('==query ==='+query );
            
            list<sobject> listToDelete = Database.query(query);
            System.debug('==listToDelete==='+listToDelete);
            if(listToDelete.size() > 0){
                delete listToDelete;
            }
        }Catch(Exception e){
            System.debug('###==e==='+e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Please Contact your Administrator'));
            returnFlag = false;
        }
        return returnFlag;
    }
      
}