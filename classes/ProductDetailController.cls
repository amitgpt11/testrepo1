public without sharing class ProductDetailController extends AuthorizationUtil{
    
    public Product2 objParentProduct {get;set;}
    public Product2 objFilteredProduct {get;set;}
    public Integer quantity {get; set;}
    public Boolean hasVideoURLAttached {get; set;}
    public String errorMessage {get;set;}
    public Map<String,ProductAttributes> mapAttributeToValues {get;set;}
    public List<String> lstProductAttributes {get;set;}
    
    private Map<String,String> mapProductAttributeFieldAPINameToFieldType;
    public Map<String,List<SelectOption>> mapProductAttributeFieldAPINameToAllPossibleOptions;
    private Boolean hasAttributes;
    
    public class ProductAttributes {
        
        public List<SelectOption> attributeOptions {get;set;}
        public String attributeSelectedOption {get;set;}
        public Set<Double> lstNumericOptions {get;set;}
        
        public ProductAttributes(){
            
            attributeOptions = new List<SelectOption>();
            attributeSelectedOption = '';
            lstNumericOptions = new Set<Double>();
        }
    }
    
    public override void fetchRequestedData() {
        
        errorMessage = '';
        quantity = 1;
        hasVideoURLAttached = hasAttributes = false;
        mapAttributeToValues = new Map<String,ProductAttributes>();
        mapProductAttributeFieldAPINameToFieldType = new Map<String,String>();
        mapProductAttributeFieldAPINameToAllPossibleOptions = new Map<String,List<SelectOption>>();
        
        //BSMI-253
        List<User> lstCurrentUser = new List<User>([SELECT LanguageLocaleKey
                                                    FROM User
                                                    WHERE Id =: UserInfo.getUserId()
                                                   ]);
        //BSMI-253
        
        if(!ApexPages.currentPage().getParameters().containsKey('Id')) {
            
            errorMessage = 'Id parameter is missing in URL.';
        }
        
        String productId = ApexPages.currentPage().getParameters().get('Id');
        
        if(productId == null || productId == '') {
            
            errorMessage = 'Id value is missing in URL.';
        } else {
            
            List<Product2> lstProducts = new List<Product2>([SELECT Id, Name, Product_Label__c, Shared_Product_Image_URL__c, Shared_Video_URL__c, Shared_Community_Description__c, Family, ProductCode,
                                                             Shared_Includes_Handle_Break__c, Shared_Nose__c, Shared_Shape__c, Shared_Stiffness__c, Shared_Style__c, Shared_With_Needle__c, Shared_Product_Documentation__c
                                                             FROM Product2
                                                             WHERE Id =: productId]);
            
            if(lstProducts.isEmpty()) {
                
                errorMessage = 'Incorrect id parameter in URL.';
            } else {
                
                objParentProduct =  lstProducts[0];
                
                if(objParentProduct.Shared_Video_URL__c != null && objParentProduct.Shared_Video_URL__c != '') {
                    
                    hasVideoURLAttached = true;
                    objParentProduct.Shared_Video_URL__c = objParentProduct.Shared_Video_URL__c.replace('watch?v=','embed/');
                }
                
                Set<String> setAcceptedAttributeFieldType = new Set<String>{'BOOLEAN', 'DOUBLE', 'PICKLIST', 'STRING'};
                List<Schema.FieldSetMember> attributeFields = Schema.SObjectType.Product2.fieldSets.Product_Attribute_Fields.getFields();
                
                for(Schema.FieldSetMember attributeField : attributeFields) {
                    
                    String fieldType = String.valueOf(attributeField.getType());
                    
                    if(setAcceptedAttributeFieldType.contains(fieldType)) {
                        
                        mapProductAttributeFieldAPINameToFieldType.put(String.valueOf(attributeField.getFieldPath()),fieldType);
                    }
                }
                
                // call only if field set has fields
                if(!mapProductAttributeFieldAPINameToFieldType.isEmpty()) {
                    
                    filterProductAttribute();   
                } else {
                    
                    objFilteredProduct = objParentProduct;
                }
                
                //BSMI-253
                if(lstCurrentUser[0].LanguageLocaleKey != 'en_US'){
                    
                    for(Shared_Community_Product_Translation__c objProductTranslation : [SELECT Shared_Product_Label__c, Shared_Description__c,Shared_Document_URL__c
                           FROM Shared_Community_Product_Translation__c
                           WHERE Shared_Product__c =: productId
                           AND Shared_Language__c =: mapLocaleToLanguage.get(lstCurrentUser[0].LanguageLocaleKey)]){
                        
                        objParentProduct.Product_Label__c = objProductTranslation.Shared_Product_Label__c;
                        objParentProduct.Shared_Community_Description__c = objProductTranslation.Shared_Description__c;
                        objParentProduct.Shared_Product_Documentation__c = objProductTranslation.Shared_Document_URL__c;
                        
                    }
                }
                //BSMI-253
            }
        }
    }
    
    public void filterProductAttribute() {
        
        Boolean hasValueFilteredForAllAttribute = true;
        
        Integer totalFilterApplied = 0;
        
        String attributeName = ApexPages.currentPage().getParameters().get('attributeName');
        
        objFilteredProduct = new Product2();
        
        List<String> lstProductAttributePicklistAPINames = new List<String>();
        lstProductAttributePicklistAPINames.addAll(mapProductAttributeFieldAPINameToFieldType.keySet());
        
        //get filtered product
        String query = 'SELECT Id, ' + String.join(lstProductAttributePicklistAPINames,',') + ', ProductCode ' +
                'FROM Product2 ' +
                'WHERE Shared_Parent_Product__c = \'' + objParentProduct.Id + '\'' + 
                'AND IsActive = true ';
            
        if(hasAttributes) {
            
            attributeName = String.isBlank(mapAttributeToValues.get(attributeName).attributeSelectedOption) ? '' : attributeName;
            
            for(String attribute : lstProductAttributes) {
                
                ProductAttributes objParentProductAttribute = mapAttributeToValues.get(attribute);
                
                if(attributeName == attribute && String.isBlank(objParentProductAttribute.attributeSelectedOption)) {
                    
                    attributeName = lstProductAttributes[0];
                }
                
                if(String.isBlank(objParentProductAttribute.attributeSelectedOption)) {
                    
                    hasValueFilteredForAllAttribute = false;
                } else {
                    
                    totalFilterApplied++;
                    
                    attributeName = attribute;
                    
                    String fieldType = mapProductAttributeFieldAPINameToFieldType.get(attribute);
                    
                    if(fieldType == 'BOOLEAN' || fieldType == 'DOUBLE'){
                     
                       query += 'AND ' + attribute + ' = ' + objParentProductAttribute.attributeSelectedOption + ' ';
                    }else{
                    
                        query += 'AND ' + attribute + ' = \'' + String.escapeSingleQuotes(objParentProductAttribute.attributeSelectedOption) + '\' ';
                    }
                }
            }
            
            if(String.isBlank(attributeName)) {
                
                attributeName = ApexPages.currentPage().getParameters().get('attributeName');
            }
        }
        
        if(hasAttributes && hasValueFilteredForAllAttribute) {
            
            query += 'limit 1';
            
            system.debug('## query 1 : '+query);
            //query = String.escapeSingleQuotes(query);
            //system.debug('## query 2 : '+query);
            
            for(Product2 filteredProduct : Database.query(query)) {
                
                objFilteredProduct = filteredProduct;
            }
        } else {
            
            lstProductAttributes = new List<String>();
            Set<String> filteredAttributeFieldAPIName = new Set<String>();
            Map<String,Set<String>> mapAttributeToOptions = new Map<String,Set<String>>();
            
            //get available attributes
            for(Product2 childProduct : Database.query(query)) {
                
                system.debug('## childProduct : '+childProduct);
                
                for(String attribute : lstProductAttributePicklistAPINames) {
                    
                    String attributeValue = String.valueOf(childProduct.get(attribute));
                    
                    if(attributeValue != null) {
                        
                        ProductAttributes objParentProductAttribute = mapAttributeToValues.get(attribute);
                        Set<String> attributeOptions = mapAttributeToOptions.get(attribute);
                        String fieldType = mapProductAttributeFieldAPINameToFieldType.get(attribute);
                        
                        
                        system.debug('attributeOptions====1========'+attributeOptions);
                        attributeOptions = attributeOptions == null ? new Set<String>() : attributeOptions;
                        
                        if(objParentProductAttribute != null && attributeOptions.isEmpty()) {
                            
                            objParentProductAttribute.attributeOptions = new List<SelectOption>();
                            objParentProductAttribute.lstNumericOptions = new Set<Double>();
                            
                            if(fieldType != 'BOOLEAN') {
                                
                                objParentProductAttribute.attributeOptions.add(new SelectOption('',''));
                            }
                            
                            system.debug('attributeOptions====2========'+objParentProductAttribute.attributeOptions);
                            system.debug('attributeOptions====2=2======='+objParentProductAttribute.lstNumericOptions);
                        }
                        
                        
                        if(objParentProductAttribute == null) {
                            
                            objParentProductAttribute = new ProductAttributes();
                            
                            if(fieldType != 'BOOLEAN') {
                                
                                objParentProductAttribute.attributeOptions.add(new SelectOption('',''));
                                objParentProductAttribute.lstNumericOptions = new Set<Double>();
                            }
                        }
                        
                        system.debug('attributeOptions====3========'+objParentProductAttribute.attributeOptions);
                        system.debug('fieldType====3========'+fieldType);
                        if(!attributeOptions.contains(attributeValue)) {
                            
                            attributeOptions.add(attributeValue);
                            mapAttributeToOptions.put(attribute,attributeOptions);
                            
                            if(fieldType != 'DOUBLE') {
                                
                                objParentProductAttribute.attributeOptions.add(new SelectOption(attributeValue,attributeValue));
                            } else {
                                
                                objParentProductAttribute.lstNumericOptions.add(Double.valueOf(attributeValue));
                            }
                            
                            mapAttributeToValues.put(attribute,objParentProductAttribute);
                            
                            if(String.isBlank(attributeName)) {
                                
                                system.debug('objParentProductAttribute.attributeOptions=========='+objParentProductAttribute.attributeOptions);
                                mapProductAttributeFieldAPINameToAllPossibleOptions.put(attribute,objParentProductAttribute.attributeOptions);
                            }
                            
                            filteredAttributeFieldAPIName.add(attribute);
                        }
                        
                        system.debug('attributeOptions====4========'+objParentProductAttribute.attributeOptions);
                    }
                }
            }
            
            lstProductAttributes.addAll(filteredAttributeFieldAPIName);
            hasAttributes = !lstProductAttributes.isEmpty();
            
            if(!hasAttributes) {
                
                objFilteredProduct = objParentProduct;
            } 
            
            system.debug('mapProductAttributeFieldAPINameToAllPossibleOptions=================='+mapProductAttributeFieldAPINameToAllPossibleOptions);
            system.debug('totalFilterApplied==========='+totalFilterApplied);
            if(attributeName != null && totalFilterApplied == 1) {
                
                mapAttributeToValues.get(attributeName).attributeOptions = new List<SelectOption>();
                mapAttributeToValues.get(attributeName).attributeOptions.addAll(mapProductAttributeFieldAPINameToAllPossibleOptions.get(attributeName));
            }
            
            system.debug('mapAttributeToValues====5========'+mapAttributeToValues);
            for(String attribute : mapAttributeToValues.keySet()) {
                
                ProductAttributes objParentProductAttribute = mapAttributeToValues.get(attribute);
                
                if(objParentProductAttribute.lstNumericOptions.isEmpty()) {
                    
                    objParentProductAttribute.attributeOptions.sort();
                } else {
                    
                    List<Double> lstNumericOptionsTemp = new List<Double>();
                    lstNumericOptionsTemp.addAll(objParentProductAttribute.lstNumericOptions);
                    lstNumericOptionsTemp.sort();
                    Boolean isPresent = false;
                    
                    for(Double numericOption : lstNumericOptionsTemp) {
                        
                        String attributeValue = String.valueOf(numericOption);
                        
                        for(SelectOption option: objParentProductAttribute.attributeOptions){
                            
                            if(option.getValue().equalsIgnoreCase(attributeValue)){
                                
                                isPresent = true;
                                break;
                            }
                        }
                        
                        if(!isPresent){
                            objParentProductAttribute.attributeOptions.add(new SelectOption(attributeValue,attributeValue));
                        }
                    }
                    
                    mapAttributeToValues.put(attribute,objParentProductAttribute);
                }
                system.debug('attributeOptions====5========'+mapAttributeToValues.get(attribute));
            }
        }
    }
    
    public void AddItems(){
        
        List<User> lstUser = new List<User>([Select ContactId, Contact.OwnerId 
                                            From User
                                            Where Id =: Userinfo.getUserId()]);
        
        if(!lstUser.isEmpty() && lstUser[0].ContactId != null) {
            
            List<Shared_Community_Order__c> lstOrder = new List<Shared_Community_Order__c>();
            Shared_Community_Order__c objShared_Community_Order;
            
            lstOrder = [Select Contact__c, Shared_Date_Submitted__c, Status__c 
                        From Shared_Community_Order__c 
                        Where Contact__c =: lstUser[0].ContactId
                        AND Status__c = 'In Progress'];
            
            if(lstOrder.isEmpty()) {
                
                objShared_Community_Order = new Shared_Community_Order__c(Contact__c = lstUser[0].ContactId, Shared_Date_Submitted__c = DateTime.Now(),
                                                                          Status__c = 'In Progress');
                Insert objShared_Community_Order;    
            } else {
                
                objShared_Community_Order = lstOrder[0];
            }
           
           //check if the product item is already in the order, if yes update that else create new
           List<Shared_Community_Order_Item__c> lst_objShared_Community_Order_Item = new List<Shared_Community_Order_Item__c>([SELECT Id, Shared_Quantity__c 
                              FROM Shared_Community_Order_Item__c 
                              WHERE Product__c =: objFilteredProduct.Id 
                              AND Shared_Order__c =: objShared_Community_Order.Id 
                              LIMIT 1]); 
            
           Shared_Community_Order_Item__c objShared_Community_Order_Item = new Shared_Community_Order_Item__c(); 
                                                                                          
           IF(!lst_objShared_Community_Order_Item.isEmpty()){
               
                objShared_Community_Order_Item = lst_objShared_Community_Order_Item[0];
                objShared_Community_Order_Item.Shared_Quantity__c = objShared_Community_Order_Item.Shared_Quantity__c + quantity;       
           }else{
               
               objShared_Community_Order_Item = new Shared_Community_Order_Item__c(Shared_Order__c = objShared_Community_Order.Id,
                                                        Product__c = objFilteredProduct.Id,
                                                        Shared_Quantity__c = quantity);
               
           }                                     
       
            Upsert objShared_Community_Order_Item;
            
            insert new Shared_Community_Order__Share(ParentId = objShared_Community_Order.Id,
                UserOrGroupId = lstUser[0].Contact.OwnerId,
                AccessLevel = 'Edit',
                RowCause = Schema.Shared_Community_Order__Share.RowCause.VTM__c
            );
        }
    } 
}