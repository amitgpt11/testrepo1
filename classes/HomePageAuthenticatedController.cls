/** 
Description: This class works as Controller for HomePageAuthenticated page
*/
global without sharing class HomePageAuthenticatedController extends AuthorizationUtil{
    
    public User objUser {get;set;}
    public Community_Event__c objFeaturedEvent {get;set;}
    public Product2 objFeaturedProduct {get;set;}
    public Boston_Scientific_Config__c objBSCustomSetting {get;set;}
    
    public override void fetchRequestedData() {
        
        objFeaturedEvent = new Community_Event__c();
        objFeaturedProduct = new Product2();
        
        String featuredEventFieldAPIName = '';
        String featuredProductFieldAPIName = '';
        
        List<Community_Event__c> lstCommunityEvents = new List<Community_Event__c>();
        List<Product2> lstProducts = new List<Product2>();
        
        if(Boston_Scientific_Config__c.getValues('Default') == null) return; 
        
        objUser = [Select Name, Contact.Owner.SmallPhotoUrl, Contact.Owner.FullPhotoUrl,Contact.Owner.Representative_Intro__c, Shared_Community_Division__c 
                   From User 
                   Where Id =: UserInfo.getUserId()];
        
        featuredEventFieldAPIName = (objUser.Shared_Community_Division__c == 'Endo') ? 'Boston_AuthHome_Endo_FeaturedEvent_Id__c' : 'Boston_AuthHome_Cardio_FeaturedEvent_Id__c';
        featuredProductFieldAPIName = (objUser.Shared_Community_Division__c == 'Endo') ? 'Boston_AuthHome_Endo_FeaturedPrduct_Id__c' : 'Boston_AuthHome_Cardio_FeaturedPrduct_Id__c';
        
        objBSCustomSetting = Boston_Scientific_Config__c.getValues('Default');
        
        lstCommunityEvents = [Select Shared_Display_Name__c, Shared_End_Date__c, Shared_Start_Date__c, Shared_Description__c, Shared_Event_Hyperlink__c, Shared_Image_URL__c
                              From Community_Event__c 
                              Where Id =: String.valueOf(objBSCustomSetting.get(featuredEventFieldAPIName))
                              limit 1];
        
        if(!lstCommunityEvents.isEmpty()){
            
            objFeaturedEvent = lstCommunityEvents[0];
        }
        
        lstProducts = [Select Id, Product_Label__c, Shared_Community_Description__c, Shared_Product_Image_URL__c
                       From Product2 
                       Where Id =: String.valueOf(objBSCustomSetting.get(featuredProductFieldAPIName))
                       Limit 1];
        
        if(!lstProducts.isEmpty()){
            
            objFeaturedProduct = lstProducts[0];
        }
    }
}