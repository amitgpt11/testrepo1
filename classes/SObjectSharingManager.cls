/**
* Class to manage object sharing.  This class will automatically find the deltas
* between what is requested to share and what already is.
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public without sharing class SObjectSharingManager {

	final static String EDIT = 'Edit';
	final static Set<String> includedProfiles = (NMD_Sharing_Profile__c.getAll().keySet());
	final static Set<String> excludedCauses = new Set<String> { 
		'ImplicitChild',
		'ImplicitParent',
		'Owner',
		'Rule',
		'Team',
		'TerritoryRule'
	};

	final Map<Id,Map<Id,SObject>> existingShares = new Map<Id,Map<Id,SObject>>(); // ParentId => { UserOrGroupId => Share }
	final Set<Id> profileIds = new Set<Id>(); // only shares created from certain profiles are removed
	final SObjectType shareObjectType;
	final String shareObjectTypeName;

	private String fieldAccessLevel = 'AccessLevel';
	private String fieldParentId = 'ParentId';
	private String fieldRowCause = 'RowCause';
	private String fieldUserOrGroupId = 'UserOrGroupId';
	private String fieldLastModifiedByProfileName = 'LastModifiedBy.Profile.Name';

	public SObjectSharingManager(SObjectType shareObjectType, Set<Id> parentIds) {

		this.shareObjectType = shareObjectType;
		this.shareObjectTypeName = shareObjectType.getDescribe().getName();

		if (!shareObjectTypeName.endsWith('Share')) {
			throw new SObjectSharingManagerException('SObjectType must be a Sharing Object.');
		}

		// if this is a standard object share then it will have a standard name and field api
		if (!shareObjectTypeName.endsWith('__Share')) {
			String objName = shareObjectTypeName.replace('Share', '');
			// overwrite w/the standard field names
			fieldAccessLevel = objName + fieldAccessLevel;
			fieldParentId = objName + 'Id';
		}

		fetchExistingShares(parentIds);

	}

	public void applyNewShares(Map<Id,Set<Id>> userIdsByParentId) {
		
		Set<Id> parentIds = userIdsByParentId.keySet();
		List<SObject> newShares = new List<SObject>();

		for (Id parentId : parentIds) {
			for (Id userId : userIdsByParentId.get(parentId)) {
				if (!existingShares.containsKey(parentId) || !this.existingShares.get(parentId).containsKey(userId)) {
					newShares.add(newShareObject(parentId, userId));
				}
			}
		}

		if (!newShares.isEmpty()) {
			create(newShares);
		}

	}

	public void applyDeltaShares(Map<Id,Set<Id>> userIdsByParentId) {

		Set<Id> parentIds = userIdsByParentId.keySet();
		List<SObject> newShares = new List<SObject>();
		List<SObject> oldShares = new List<SObject>();

		for (Id parentId : parentIds) {
			if (!this.existingShares.containsKey(parentId)) continue;

			Map<Id,SObject> shares = this.existingShares.get(parentId);
			Set<Id> uIds = userIdsByParentId.get(parentId);

			// add shares that do not exist
			for (Id userId : uIds) {
				// create a share if neccesary
				if (!shares.containsKey(userId)) {
					newShares.add(newShareObject(parentId, userId));
				}
			}

			// remove existed shares that are not listed
			for (Id userId : shares.keySet()) {
				// only remove the share if it wasnt added by a system RowCause
				if (shares.containsKey(userId) && !uIds.contains(userId)) {
					// get share
					SObject share = shares.get(userId);
					// only remove share if it was manually added
					// by someone in a non-exempt sharing role.
					if (!excludedCauses.contains(SObjectManager.getFieldAsString(share, this.fieldRowCause))
					 && includedProfiles.contains(SObjectManager.getFieldAsString(share, this.fieldLastModifiedByProfileName))
					) {
						// remove from the existing share pool
						oldShares.add(shares.remove(userId));
					}
				}
			}

		}

		if (!newShares.isEmpty()) {
			create(newShares);
		}

		if (!oldShares.isEmpty()) {
			//delete oldShares;
			DML.remove(this, oldShares, false);
		}

	}

	private void fetchExistingShares(Set<Id> parentIds) {

		this.existingShares.clear();

		SObjectType userType = User.getSObjectType();

		// retrieve existing shares
		for (SObject share : Database.query(
			'select ' +
				this.fieldAccessLevel + ', ' +
				this.fieldLastModifiedByProfileName + ', ' +
				this.fieldParentId + ', ' +
				this.fieldRowCause + ', ' +
				this.fieldUserOrGroupId + ' ' +
			'from ' + this.shareObjectTypeName + ' ' +
			'where ' + this.fieldParentId + ' in :parentIds'
		)) {

			Id parentId = (Id)share.get(this.fieldParentId);
			Id userId = (Id)share.get(this.fieldUserOrGroupId);

			// ignore group shares
			if (userId.getSObjectType() != userType) continue;

			if (this.existingShares.containsKey(parentId)) {
				this.existingShares.get(parentId).put(userId, share);
			}
			else {
				this.existingShares.put(parentId, new Map<Id,SObject> { userId => share });
			}

		}

	}

	private SObject newShareObject(Id parentId, Id userId) {
		SObject share = this.shareObjectType.newSObject();
		share.put(this.fieldAccessLevel, EDIT);
		share.put(this.fieldParentId, parentId);
		share.put(this.fieldUserOrGroupId, userId);
		return share;
	}

	private void create(List<SObject> newShares) {
		//Database.insert(newShares, false);
		DML.save(this, newShares, false);
		for (SObject share : newShares) {
			if (share.Id != null) {
				Id parentId = (Id)String.valueOf(share.get(this.fieldParentId));
				Id userId = (Id)String.valueOf(share.get(this.fieldUserOrGroupId));
				this.existingShares.get(parentId).put(userId, share);
			}
		}
	}

	public class SObjectSharingManagerException extends System.Exception {}

}