/**
 * Description: This class is used as controller for the SearchResults page.
*/
public class SearchController extends AuthorizationUtil{
    
    public Boolean isEnglishUser           {get;set;}
    public Boolean isOtherUser             {get;set;}
    public List<Product2> lstProducts      {get;set;}
    public List<Shared_Community_Product_Translation__c> lstProductTranslation       {get;set;}
    
    /*
    * Description: The construtor initilizes the lstProducts or lstProductTranslation based on User's language to be displayed on Documents page
    */
     public override void fetchRequestedData() {
        
        isEnglishUser = false;
        isOtherUser = false;
        String strSearch = apexpages.currentpage().getparameters().get('query');
        List<List<sObject>> lstsObjects = new List<List<sObject>>();
        lstProducts = new List<Product2>();
        lstProductTranslation = new List<Shared_Community_Product_Translation__c>();
        Set<Id> setParentProdIds = new Set<Id>();
      
        User objUser = [Select Shared_Community_Division__c, LanguageLocaleKey 
            From User 
            Where Id =: UserInfo.getUserId()];
        //Check if user's Shared_Community_Division__c field is not null or blank
        if(objUser.Shared_Community_Division__c != null && objUser.Shared_Community_Division__c != '') {
            
            //SOSL on product2 object if user language is english else it SOSL on Shared_Community_Product_Translation__c object
            if(objUser.LanguageLocaleKey == 'en_US') {
                
                lstsObjects = [FIND :strSearch IN ALL FIELDS 
                    RETURNING Product2 
                    (Id, Product_Label__c, Division__c, ProductCode, Family, Shared_Community_Description__c, Shared_Parent_Product__c,
                    Shared_Parent_Product__r.Id, Shared_Parent_Product__r.Product_Label__c, Shared_Parent_Product__r.Division__c, Shared_Parent_Product__r.ProductCode, Shared_Parent_Product__r.Family, Shared_Parent_Product__r.Shared_Community_Description__c
                    Where Shared_Parent_Product__c != null and 
                    IsActive = True and
                    Division__c =: objUser.Shared_Community_Division__c limit 1000)];
                
                for(Product2 objProduct : (List<Product2>)lstsObjects[0]){
                    
                    if(!setParentProdIds.contains(objProduct.Shared_Parent_Product__c)){
                        
                        setParentProdIds.add(objProduct.Shared_Parent_Product__c);
                        lstProducts.add(objProduct);
                    }
                }
                
                isEnglishUser = true;
            }else {
                
                lstsObjects = [FIND :strSearch IN ALL FIELDS 
                    RETURNING Shared_Community_Product_Translation__c (Name, Shared_Product_Label__c,Shared_Division__c, Shared_Product__r.ProductCode, Shared_Product__r.Shared_Parent_Product__c, Shared_Description__c, Shared_Document_URL__c, Shared_Language__c, Shared_Product__c, Shared_Product_Family__c
                    Where Shared_Product__r.IsActive = True and
                    Shared_Product__r.Shared_Parent_Product__c != null and 
                    Shared_Division__c =: objUser.Shared_Community_Division__c and 
                    Shared_Language__c =: mapLocaleToLanguage.get(objUser.LanguageLocaleKey) limit 1000)];
                
                for(Shared_Community_Product_Translation__c objProductTranslation : (List<Shared_Community_Product_Translation__c>)lstsObjects[0]){
                   
                    if(!setParentProdIds.contains(objProductTranslation.Shared_Product__r.Shared_Parent_Product__c)){
                        
                        setParentProdIds.add(objProductTranslation.Shared_Product__r.Shared_Parent_Product__c);
                    }
                }
                
                for(Shared_Community_Product_Translation__c objProduct : [SELECT Id, Name,Shared_Product_Label__c,Shared_Division__c, Shared_Product__r.ProductCode, 
                                                                          Shared_Product__r.Shared_Parent_Product__c, Shared_Description__c, Shared_Document_URL__c, 
                                                                          Shared_Language__c, Shared_Product__c, Shared_Product_Family__c 
                                                                          FROM Shared_Community_Product_Translation__c 
                                                                          WHERE Shared_Product__c IN: setParentProdIds
                                                                          AND Shared_Division__c =: objUser.Shared_Community_Division__c 
                                                                          AND Shared_Language__c =: mapLocaleToLanguage.get(objUser.LanguageLocaleKey) 
                                                                          limit 1000]){
            
                    lstProductTranslation.add(objProduct);
                }
                
                isOtherUser = true;
            }    
        }
    }    
}