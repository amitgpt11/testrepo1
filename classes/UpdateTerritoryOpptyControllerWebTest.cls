/**
 * Name : UpdateTerritoryOpptyControllerWebTest
 * Author : Accenture - Mayuri
 * Description : Test class used for testing the UpdateTerritoryOpptyController_Web
 * Date : 27APR2016
 */
@isTest
private class UpdateTerritoryOpptyControllerWebTest { 
     @testSetup
    static void setupTerritoryMockData(){
        BatchAccountTerritoryUpdateTest.setupTerritoryMockData();
    }
    
    static testMethod void  testSave_Scenario1(){ 
        //User stdUser = UtilForUnitTestDataSetup.getUser(); 
        
        Territory2 trLst = [Select name From Territory2 where name Like : 'AA%' LIMIT 1 ];  
        
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        insert acct1;        
        
        ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
        accountAssociation.ObjectId = acct1.Id;
        accountAssociation.Territory2Id = trLst.Id;
        accountAssociation.AssociationCause = 'Territory2Manual';
        insert accountAssociation;   
         
         Opportunity oppty = new Opportunity();
         oppty.Name = 'test oppty';
         oppty.AccountId = acct1.Id;
         oppty.CloseDate = system.today()+30;
         oppty.StageName = 'Target Prospect';
         insert oppty;
          
        ApexPages.currentPage().getParameters().put('id', oppty.Id);
        ApexPages.currentPage().getParameters().put('lksrch','AA');
        ApexPages.StandardController ctr = new ApexPages.StandardController(oppty);
        UpdateTerritoryOpptyController_Web opptyController=  new  UpdateTerritoryOpptyController_Web(ctr);
        opptyController.terrSelected = trLst.Id;
        Test.startTest();
        Test.setCurrentPage(Page.MyObjectiveTerritoryPage);
        
        //System.runAs(stdUser) {
            
            opptyController.search();            
            opptyController.dummyAction();
            opptyController.saveOppty();
            opptyController.redirectToPage();
            Test.stopTest();
            // System.assert(pagereferenceTest!=null);
        //}
        
    }
    static testMethod void  testSave_Scenario2(){ 
        UpdateTerritoryOpptyController_Web opptyController=  new  UpdateTerritoryOpptyController_Web();
        
    }

}