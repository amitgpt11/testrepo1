/**
* NMD_SubmitProspectAccountExtension.class
*
* @author   Salesforce services
* @date     2015-02-03
* @desc     Extension class to the SubmitProspectAccount.page
*/
public with sharing class NMD_SubmitProspectAccountExtension extends NMD_QuincyExtensionBase {

    public NMD_SubmitProspectAccountExtension(ApexPages.StandardController sc) {
        super(sc);
        newInfoMessage('Click Submit to continue.');
    }

    /**
    * Submit Account to Quincy.  Throw error if required fields are missing.
    *
    * @param    void
    */
    public override void submit() {

        NMD_SendQuincyEmailManager emailManager = new NMD_SendQuincyEmailManager(this.sc.getId(), new Set<Id>{});

        Account acct = [SELECT Id,
                    Name,
                    Classification__c,
                    Type,
                    Sold_to_Street__c, 
                    Sold_to_City__c, 
                    Sold_to_State__c, 
                    Sold_to_Zip_Postal_Code__c,
                    Sold_to_Country__c,
                    BillingStreet,
                    BillingCity,
                    BillingState,
                    BillingPostalCode,
                    BillingCountry,
                    NMD_Territory__c
                FROM Account
                WHERE Id = :this.sc.getId()
                ];

        //verify required fields are populated and throw error if they are missing
        //Changes as per US2471 to add Type Classification, and Name fields to required fields 
        if(String.isBlank(acct.Sold_to_Street__c)
            || String.isBlank(acct.Sold_to_City__c)
            || String.isBlank(acct.Sold_to_State__c)
            || String.isBlank(acct.Sold_to_Zip_Postal_Code__c)
            || String.isBlank(acct.Sold_to_Country__c)
            || String.isBlank(acct.BillingStreet)
            || String.isBlank(acct.BillingCity)
            || String.isBlank(acct.BillingState)
            || String.isBlank(acct.BillingPostalCode)
            || String.isBlank(acct.BillingCountry)
            || acct.NMD_Territory__c == null
            || String.isBlank(acct.Type)
            || String.isBlank(acct.Classification__c)
            || String.isBlank(acct.Name)
        ){
            newErrorMessage(Label.NMD_Quincy_Prospect_Account_Required_Fields);
        }
        else {
            Boolean sentOK = emailManager.sendQuincyEmail();
            if (sentOK) {
                newInfoMessage(Label.NMD_Report_Emailed);
                isSubmitted = true;
            }
            else {
                String errorMessage = emailManager.lastErrorMessage + ' ' + Label.NMD_Please_Contact_Admin;
                newErrorMessage(errorMessage);
                logError('NMD_SubmitProspectAccountExtension.submit', emailManager.lastErrorMessage);
            }
        }
    }

}