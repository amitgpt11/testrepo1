/**
* Test class for the PatientTriggerHandler class
*
* @Author salesforce Services
* @Date 02/10/2015
*/
@isTest(SeeAllData=false)
private class PatientTriggerHandlerTest {

	final static String TERRITORYNAME = 'TERRITORYNAME';
	final static String ZIPCODE = '12345';
	final static String REPCODE = 'REPC'; //4 char max
	final static String SOURCE = 'CARE Online';
	
	@testSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();
		td.setupLeadPreformSettings();

		Seller_Hierarchy__c territory = new Seller_Hierarchy__c(
			Name = TERRITORYNAME,
			Rep_Code__c = REPCODE,
			Current_TM1_Assignee__c = UserInfo.getUserId()
		);
		insert territory;

		Postal_Code_Reference__c pcRef = new Postal_Code_Reference__c(
			Postal_Code__c = ZIPCODE,
			Country__c = 'US'
		);
		insert pcRef;

		Postal_Code_Relationship__c pcRel = new Postal_Code_Relationship__c(
			Postal_Code_Reference__c = pcRef.Id,
			Territory_ID__c = territory.Id,
			Division__c = 'NM'
		);
		insert pcRel;

	}

	static testMethod void test_PatientTerritory_RepCode() {

		Seller_Hierarchy__c territory = [select Id from Seller_Hierarchy__c where Name = :TERRITORYNAME];

		Patient__c patient = new Patient__c(
			RecordTypeId = PatientManager.RECTYPE_CUSTOMER,
			Patient_Phone_Number__c = '555-444-3322',
			Rep_Code__c = REPCODE
		);
		insert patient;

		patient = [select Territory_Id__c from Patient__c where Id = :patient.Id];
		System.assertEquals(territory.Id, patient.Territory_ID__c);

	}

	static testMethod void test_PatientTerritory_PostalCode() {

		Seller_Hierarchy__c territory = [select Id from Seller_Hierarchy__c where Name = :TERRITORYNAME];

		Patient__c patient = new Patient__c(
			RecordTypeId = PatientManager.RECTYPE_CUSTOMER,
			Zip__c = ZIPCODE
		);
		insert patient;

		patient = [select Territory_Id__c from Patient__c where Id = :patient.Id];
		System.assertEquals(territory.Id, patient.Territory_ID__c);

	}

	static testMethod void test_PatientNmdLead() {

		Patient__c patient = new Patient__c(
			Source__c = SOURCE
		);
		insert patient;

		List<Lead> leads = new List<Lead>([select Id from Lead where Patient__c = :patient.Id]);
		System.assertEquals(1, leads.size());

	}

}