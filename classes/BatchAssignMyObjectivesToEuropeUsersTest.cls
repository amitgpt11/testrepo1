/*
 @CreatedDate     08 APR2 016                                                                
 @author          Mayuri-Accenture
 @Description     This  is test Class for 'BatchAssignMyObjectivesToEuropeUsers' , the batch which runs everyday to recalculate the Territory sharing for My_Objectives__c records.
   
 */


@isTest(SeeAllData=false)
@testVisible
private class BatchAssignMyObjectivesToEuropeUsersTest{
    public static list<User> usrLst = new list<User>();
    public static list<Personnel_ID__c> pesrnIdlst = new list<Personnel_ID__c>();
    public static list<Account> accList = new list<Account>();
    public static list<Contact> contactList = new list<Contact>();
    public static List<Territory2> trList=new List<Territory2>();
    public static Territory2 t3,t4;
    
    @testSetup
    static void setupMockData(){
        User userC = UtilForUnitTestDataSetup.newUser('EU Sales Rep'); 
        userC.UserName = 'Europe1@xyz.com';
        User userD = UtilForUnitTestDataSetup.newUser('EU Sales Rep'); 
        userD.UserName = 'Europe2@xyz.com';
        usrLst.add(userC);
        usrLst.add(userD);
        insert usrLst;
        
        List<Territory2Type> Ttype = UtilForUnitTestDataSetup.newTerritory2Type();
        
        Id Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
       
        Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('AA6789',Territory2ModelIDActive, Ttype[0].Id); //Ttype[0].Id
        trList.add(t1);        
        Territory2 t2 = UtilForUnitTestDataSetup.newTerritory2('AA9876',Territory2ModelIDActive, Ttype[0].Id); 
        trList.add(t2);        
        insert trList; 
        
        
        t4 = UtilForUnitTestDataSetup.newTerritory2('AA9999',Territory2ModelIDActive, Ttype[0].Id); //0M5P00000008OIeKAM
        t4.ParentTerritory2Id = trList[1].Id;
        insert t4;
        
        UserTerritory2Association nAssc = new UserTerritory2Association();
        nAssc.RoleInTerritory2 = 'Territory Manager';
        nAssc.Territory2Id = trList[0].Id;
        nAssc.UserId = usrLst[0].Id;
        insert nAssc; 
        
        UserTerritory2Association nAssc1 = new UserTerritory2Association();
        nAssc1.RoleInTerritory2 = 'Territory Manager';
        nAssc1.Territory2Id = trList[1].Id;
        nAssc1.UserId = usrLst[1].Id;
        insert nAssc1;      
     
    }
    
    static TestMethod void TestbatchAssignMyObjectivesToEuropeUsers(){
        
        //setupMockData();
        list<User> urLst = new list<User>([Select name,Id From User where LastName Like : 'LNA%' ]);
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'AA%'  ];
        
        accList= new List<Account>();
        for(Integer i=0;i<10;i++){
            Account acct = new Account(Name = 'ACCTNAME'+i,RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId());
            accList.add(acct);
        }
        insert accList;
        List<My_Objectives__c> MyObjList = new List<My_Objectives__c>();
        //system.runAs(urLst[0]){
            For(Integer i=0; i<accList.size(); i++){
                My_Objectives__c a = new My_Objectives__c(
                    OwnerId = urLst[0].Id,
                    Division__c = 'EP',
                    Related_to_Account__c=accList[i].Id,
                    Status__c = 'New',
                    Territory__c = trLst[0].name, 
                    LastModifiedDate = system.today(),
                    createdDate = system.today()
                    
                );
                MyObjList.add(a);
            }
            
            insert MyObjList;
        //}
         
        
        Test.startTest();
            ID batchprocessid = Database.executeBatch(new batchAssignMyObjectivesToEuropeUsers());
        Test.stopTest();
      
    }
    
    static TestMethod void TestbatchAssignMyObjectivesToEuropeUsers_delete(){
        List<Territory2> trLst = [Select name,ParentTerritory2Id,No_Of_Parents_1_To_6__c  From Territory2 where name Like : 'AA%'  ];
        list<My_Objectives__c> myUpdateObjLst = new list<My_Objectives__c>();
        list<User> urLst = new list<User>([Select name,Id From User where LastName Like : 'LNA%' ]);
        list<Account> accList1 = new list<Account>();
        for(Integer i=0;i<10;i++){
            Account acct = new Account(Name = 'ACCTNAME'+i,RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId());
            accList1.add(acct);
        }
        insert accList1;
        List<My_Objectives__c> MyObjList = new List<My_Objectives__c>();  
         For(Integer i=0; i<accList1.size(); i++){
                My_Objectives__c a = new My_Objectives__c(
                    OwnerId = urLst[0].Id,
                    Division__c = 'EP',
                    Related_to_Account__c=accList1[i].Id,
                    Status__c = 'New',
                    Territory__c = trLst[0].name, 
                    LastModifiedDate = system.today(),
                    createdDate = system.today()
                    
                );
                MyObjList.add(a);
            }
            
            insert MyObjList;
        for(My_Objectives__c m : MyObjList){
            m.Territory__c = trLst[2].name; 
            myUpdateObjLst.add(m);
        }
        
        update myUpdateObjLst;
        system.debug('myUpdateObjLst--'+myUpdateObjLst);
        system.debug('terry--'+trLst);
        Test.startTest();
            ID batchprocessid = Database.executeBatch(new batchAssignMyObjectivesToEuropeUsers());
        Test.stopTest();
    }
}