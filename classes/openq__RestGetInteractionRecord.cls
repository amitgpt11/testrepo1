/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/getinteractionrecord/*')
global class RestGetInteractionRecord {
    global RestGetInteractionRecord() {

    }
    @HttpPost
    global static openq.OpenMSL.INTERACTION_RECORD_RETRIEVE_RESPONSE_element getInteractionRecord(openq.OpenMSL.INTERACTION_RECORD_RETRIEVE_REQUEST_element request_x) {
        return null;
    }
}
