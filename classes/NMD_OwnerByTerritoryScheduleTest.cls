/**
* Test class for NMD_ContactOwnerByTerritorySchedule, NMD_LeadOwnerByTerritorySchedule,
				 NMD_OpportunityOwnerByTerritorySchedule, NMD_PatientOwnerByTerritorySchedule
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class NMD_OwnerByTerritoryScheduleTest {
	
	static testMethod void test_ContactOwnerByTerritorySchedule() {
		new NMD_ContactOwnerByTerritorySchedule().execute(null);
	}

	static testMethod void test_LeadOwnerByTerritorySchedule() {
		new NMD_LeadOwnerByTerritorySchedule().execute(null);
	}

	static testMethod void test_OpportunityOwnerByTerritorySchedule() {
		new NMD_OpportunityOwnerByTerritorySchedule().execute(null);
	}

	static testMethod void test_PatientOwnerByTerritorySchedule() {
		new NMD_PatientOwnerByTerritorySchedule().execute(null);
	}

}