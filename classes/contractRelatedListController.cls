/*
 @CreatedDate     24 June 2016                                  
 @author          Ashish-Accenture
 @Description     This class is used display active contracts as related list in Page layout
       
*/

public with sharing class contractRelatedListController{
    Public list<Contract> lstContractRec {get;set;}
    public list<String> lstOfFields {get;set;}
    public Map<String,string> mapLabelAPI{get;set;}
    
    // To fetch Active and visible contracts for Account
    public contractRelatedListController(ApexPages.StandardController controller) {
        try{
            id accId = controller.getId();
            
            ContractConfiguration__c CS;
            if(ContractConfiguration__c.getValues(UserInfo.getUserId()) != null){
                CS = ContractConfiguration__c.getValues(UserInfo.getUserId());
            }else if(ContractConfiguration__c.getValues(UserInfo.getProfileId()) != null){
                CS = ContractConfiguration__c.getValues(UserInfo.getProfileId());
            }else{
                CS = ContractConfiguration__c.getOrgDefaults();
            }
            mapLabelAPI = new Map<String,string>();
            
            
        Schema.Sobjecttype objType = Contract.getSObjectType();
        Schema.Describesobjectresult sobjRes = objType.getDescribe();
        Map<String, Schema.Sobjectfield> fieldMap = sobjRes.fields.getMap();
        List<String> sortSelectOption = new List<String>(fieldMap.keyset());
        sortSelectOption.sort();
        String fieldType;
        String fieldName;
        String fieldLabel;
        for(String a : sortSelectOption) {
            Schema.Sobjectfield field = fieldMap.get(a);
            fieldName = field.getDescribe().getName().toLowerCase();
            fieldType = field.getDescribe().getType().name().toLowerCase();
            fieldLabel = field.getDescribe().getlabel();
            
            mapLabelAPI.put(fieldName,fieldLabel);
        } 
            
            lstOfFields = new list<String>();
            if(CS != null && String.isNotBlank(cs.Visible_Fields__c)){
                
                for(String field : cs.Visible_Fields__c.Split(',')){
                    if(String.isNotBlank(field)){
                        lstOfFields.add(field.tolowercase().trim());
                    }
                }
                 
            }
            
            Set<String> setDivisions = new Set<String>();
            String divisionString= '';
            if(CS != null && String.isNotBlank(cs.Contract_Divisions__c)){
                for(String div : cs.Contract_Divisions__c.split(',')){
                    if(String.isNotBlank(div)){
                        setDivisions.add(div.trim());
                        divisionString = divisionString + '\''+div.trim() + '\',';
                    }
                }
            }
            divisionString = divisionString.removeEnd(',');
            
            if(lstOfFields.size() == 0){
                lstOfFields = new list<String>{'Name'};
            }
            System.debug('===divisionString==='+divisionString);
           
           //Set<String> setStatus  = new Set<String>{'Active','Activated'};
           Set<String> setStatus  = new Set<String>();
           if(CS != null && String.isNotBlank(cs.Contract_Status__c)){
                
                for(String status: cs.Contract_Status__c.Split(',')){
                    if(String.isNotBlank(status)){
                        setStatus.add(status.tolowercase().trim());
                    }
                }
                 
            }
            String query = 'Select ' + String.join(lstOfFields, ',')+ ' from Contract where AccountId =: accId AND status in : setStatus AND Shared_Divisions__c includes ('+divisionString+')';
           
            lstContractRec = database.query(query);
            
        }Catch(exception e){
            System.debug('###==e===='+e);
        }
      
    }
      
    
    
}