/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class RestDispatcherV1Test {
	
	static testMethod void test_Dispatcher_GET() {

		// create the REST response that the dispatcher will use
		RestContext.response = new RestResponse();

		// create the REST request
		RestContext.request = new RestRequest();
		RestContext.request.requestURI = '/v1/nmd/orders/orderId/action';
		RestContext.request.httpMethod = 'GET';
		RestContext.request.requestBody = Blob.valueOf('');

		// simulate call
		RestDispatcherV1.doGET();

	}

	static testMethod void test_Dispatcher_PUT() {

		// create the REST response that the dispatcher will use
		RestContext.response = new RestResponse();

		// create the REST request
		RestContext.request = new RestRequest();
		RestContext.request.requestURI = '/v1/foo';
		RestContext.request.httpMethod = 'PUT';
		RestContext.request.requestBody = Blob.valueOf('');

		// simulate call
		RestDispatcherV1.doPUT();

	}

	static testMethod void test_Dispatcher_POST() {

		// create the REST response that the dispatcher will use
		RestContext.response = new RestResponse();

		// create the REST request
		RestContext.request = new RestRequest();
		RestContext.request.requestURI = '/v1/foo';
		RestContext.request.httpMethod = 'POST';
		RestContext.request.requestBody = Blob.valueOf('');

		// simulate call
		RestDispatcherV1.doPOST();

	}

	static testMethod void test_Dispatcher_PATCH() {

		// create the REST response that the dispatcher will use
		RestContext.response = new RestResponse();

		// create the REST request
		RestContext.request = new RestRequest();
		RestContext.request.requestURI = '/v1/foo';
		RestContext.request.httpMethod = 'PATCH';
		RestContext.request.requestBody = Blob.valueOf('');

		// simulate call
		RestDispatcherV1.doPATCH();

	}

	static testMethod void test_Dispatcher_DELETE() {

		// create the REST response that the dispatcher will use
		RestContext.response = new RestResponse();

		// create the REST request
		RestContext.request = new RestRequest();
		RestContext.request.requestURI = '/v1/foo';
		RestContext.request.httpMethod = 'DELETE';
		RestContext.request.requestBody = Blob.valueOf('');

		// simulate call
		RestDispatcherV1.doDELETE();

	}

}