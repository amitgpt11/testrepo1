/**
* Batch class, Change Territory on Account and User based on Account Team Members values
* @author   Mayuri - Accenture
* @date     26APR2016
*/
global class batchAccountTerritoryUpdateSchedule implements Schedulable{
    
    /**
    *  @desc    Schedulable method, executes the class instance
    */
    global void execute(SchedulableContext context) {
    integer batchSize = Integer.valueOf(system.Label.ETM_Account_Territory_Update_Batch_Size);
    
    try{
        batchAccountTerritoryUpdate b = new batchAccountTerritoryUpdate();
        database.executebatch(b,batchSize);
    }catch(Exception e){
        system.debug(e+'e');
    }

    }

}