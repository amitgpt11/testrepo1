/*
 @CreatedDate     16 June 2016                                  
 @author          Ashish-Accenture
 @Description     Test class for Apex class 'GenerateEmailLutonixTempController'
 
 */

@isTest
public class GenerateEmailLutonixTempControllerTest{

    static TestMethod void TestGenerateEmailLutonixTempController(){ 
         date todayDate = date.today();
        
        
        Map<integer,String> mapMonthNumber = TestDataFactory.getMapMonthNumber(); 
        //Lutonix SRAI record without any Lutonix SRAI Detail record       
        PI_Lutonix_SRAI__c headObj = TestDataFactory.createTestLutonixHeader(mapMonthNumber.get(todayDate.Month()));
        
        //For Partial month records
        PI_Lutonix_SRAI__c headObj1 = TestDataFactory.createTestLutonixHeader(mapMonthNumber.get(todayDate.Month()));
        TestDataFactory.createTestLutonixDetail(headObj1.Id,todayDate);
        
        //for full monthe records
        integer daysInMonth = date.daysInMonth(todayDate.Year(), todayDate.Month());
        PI_Lutonix_SRAI__c headObj2 = TestDataFactory.createTestLutonixHeader(mapMonthNumber.get(todayDate.Month()));
        list<PI_Lutonix_SRAI_Detail__c> detailListRec = TestDataFactory.createTestLutonixDetail(headObj2.id,daysInMonth);
        
        Test.StartTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(headObj);
            GenerateEmailLutonixTempController controller = new GenerateEmailLutonixTempController(sc);
            
            sc = new ApexPages.StandardController(headObj1);
            controller = new GenerateEmailLutonixTempController(sc);
            controller.sendMailOnPartialData();
            boolean isMobile = controller.isSF1;
            controller.closeWindow();
            
            System.Assertequals(controller.isMailSent,true);
            
            list<PI_Lutonix_SRAI__c> LutonixSRAIlst = [Select id, PI_Partial_Month__c from PI_Lutonix_SRAI__c where id = :headObj1.id];
            
            if(LutonixSRAIlst.size() > 0){
                System.assertNotEquals(LutonixSRAIlst.get(0).PI_Partial_Month__c, false);
            }
            
            ApexPages.currentPage().getParameters().put('isdtp','p1');
            sc = new ApexPages.StandardController(headObj2);
            controller = new GenerateEmailLutonixTempController(sc);
            controller.isPartial = null;
            controller.sendMailOnPartialData();
            isMobile = controller.isSF1;
            
        Test.StopTest();
    }
}