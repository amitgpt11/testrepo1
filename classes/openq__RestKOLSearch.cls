/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/kolSearch/*')
global class RestKOLSearch {
    global RestKOLSearch() {

    }
    @HttpPost
    global static openq.OpenMSL.PROFILE_SEARCH_RESULT_RESPONSE_element kolSearch(openq.OpenMSL.KOL_SEARCH_REQUEST_element request_x) {
        return null;
    }
}
