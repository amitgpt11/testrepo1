@IsTest(SeeAllData=false)
private class InventoryTransactionTriggerHandlerTest {

	final static String ACCTNAME = 'Account Name';
	final static String SERIAL1 = '1234567890';
	final static String SERIAL2 = '0987654321';
	
	@TestSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		td.setupNuromodUserRoleSettings();

		User testUser = td.newUser('NMD Field Rep');
		insert testUser;

		System.runAs(new User(Id = UserInfo.getUserId())) {

			Seller_Hierarchy__c territory = td.newSellerHierarchy();
			insert territory;

			Patient__c patient = new Patient__c(
				RecordTypeId = PatientManager.RECTYPE_CUSTOMER,
				Territory_ID__c = territory.Id,
				SAP_ID__c = td.randomString5()
			);
			insert patient;

			// create two products, pricebook, and entries
			Id pricebookId = Test.getStandardPricebookId();

			Pricebook2 pb = new Pricebook2(Name = 'NMD', Description = 'NMD Products', IsActive = true);
	    	insert pb;

			// create one consignment acct for context user, one for a test user
			Account acct1 = td.createConsignmentAccount();
			acct1.Name = ACCTNAME;

			Account acct2 = td.newAccount();
			acct2.RecordTypeId = AccountManager.RECTYPE_INSPROVIDER;
			acct2.Status__c = 'Active';
			acct2.OwnerId = testUser.Id;

			Account acctTrialing = td.newAccount();
			acctTrialing.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
			acctTrialing.Type = 'Ship To';
			acctTrialing.Account_Number__c = td.randomString5();

			Account acctProdcedure = td.newAccount();
			acctProdcedure.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
			acctProdcedure.Account_Number__c = td.randomString5();

			insert new List<Account> { acct1, acct2, acctTrialing, acctProdcedure };

			Contact contTrialing = td.newContact(acctTrialing.Id);
			contTrialing.RecordTypeId = ContactManager.RECTYPE_CUSTOMER;
			contTrialing.SAP_ID__c = td.randomString5();

			Contact contProcedure = td.newContact(acctProdcedure.Id);
			contProcedure.RecordTypeId = ContactManager.RECTYPE_CUSTOMER;
			contProcedure.SAP_ID__c = td.randomString5();

			insert new List<Contact> { contTrialing, contProcedure };

			Opportunity oppty = td.newOpportunity(acct1.Id);
			oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
			oppty.Pricebook2Id = pb.Id;
			oppty.Primary_Insurance__c = acct2.Id;
			oppty.Territory_ID__c = territory.Id;
			oppty.StageName = 'Implant';
			oppty.CloseDate = System.today().addDays(1);
			oppty.Patient__c = patient.Id;
			oppty.Patient_SAP_ID__c = patient.SAP_ID__c;
			oppty.Trialing_Account__c = acctTrialing.Id;
			oppty.Trialing_Account_SAP_ID__c = acctTrialing.Account_Number__c;
			oppty.Trialing_Physician__c = contTrialing.Id;
			oppty.Trialing_Physician_SAP_ID__c = contTrialing.SAP_ID__c;
			oppty.Procedure_Account__c = acctProdcedure.Id;
			oppty.Procedure_Account_SAP_ID__c = acctProdcedure.Account_Number__c;
			oppty.Procedure_Physician__c = contProcedure.Id;
			oppty.Procedure_Physician_SAP_ID__c = contProcedure.SAP_ID__c;
			oppty.Pain_Area__c = 'None';
			insert oppty;

	    	Product2 prod0 = td.newProduct('Trial');
	    	insert prod0;

	        PricebookEntry standardPrice0 = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod0.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
	        PricebookEntry pbe0 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod0.Id, UnitPrice = 1, IsActive = true, UseStandardPrice = false);
	    	insert new List<PricebookEntry> { standardPrice0, pbe0 };

			// create one inv data per acct
			Inventory_Data__c invData1 = new Inventory_Data__c(
				Account__c = acct1.Id,
				Customer_Class__c = 'YY'
			);
			insert invData1;

			// create one inv location per inv data
			Inventory_Location__c invLoc1 = new Inventory_Location__c(
				Inventory_Data_ID__c = invData1.Id
			);
			insert invLoc1;

			// create one inventory item per product, alternative between inv locations
			Inventory_Item__c invItem1 = new Inventory_Item__c(
				Inventory_Location__c = invLoc1.Id,
				Model_Number__c = prod0.Id,
				Serial_Number__c = SERIAL1
			);
			insert invItem1;

			Inventory_Transaction__c invTrans1 = new Inventory_Transaction__c(
				Inventory_Item__c = invItem1.Id,
				Quantity__c = 10
			);
			insert invTrans1;

			Order order = new Order(
				RecordTypeId = OrderManager.RECTYPE_PATIENT,
				AccountId = acct1.Id,
				OpportunityId = oppty.Id,
	            EffectiveDate = System.today(),
	            Pricebook2Id = pb.Id,
	            Status = 'Draft'
			);
			insert order;

			OrderItem item = new OrderItem(
				OrderId = order.Id,
				Quantity = 1,
				UnitPrice = 1,
				PricebookEntryId = pbe0.Id
			);
			insert item;

			//Order transfer = order.clone(false, true);
			//transfer.RecordTypeId = OrderManager.RECTYPE_TRANSFER;
			//transfer.Shipping_Location__c = invLoc1.Id;
			//transfer.Originating_Order__c = order.Id;
			//insert transfer;

			//OrderItem item2 = new OrderItem(
			//	Inventory_Source__c = invItem1.Id,
			//	OrderId = transfer.Id,
			//	Quantity = 1,
			//	UnitPrice = 1,
			//	PricebookEntryId = pbe0.Id
			//);
			//insert item2;

		}


	}

	static testMethod void test_ZTKAB_Matching() {
		
		Test.startTest();
			
			Inventory_Item__c invItem = [select Id from Inventory_Item__c where Serial_Number__c = :SERIAL1];

			// create transaction record 
			Order order = [
				select 
					(
						select
							PricebookEntry.Product2Id
						from OrderItems
					)
			 	from Order 
			 	limit 1
			];

			String ztkaOrderNum = 'o48ej4oc8';

			Inventory_Transaction__c invTransZTKA = new Inventory_Transaction__c(
				Inventory_Item__c = invItem.Id,
				Model_Number__c = order.OrderItems[0].PricebookEntry.Product2Id,
				Order__c = order.Id,
				Order_Item__c = order.OrderItems[0].Id,
				Order_Type__c = InventoryTransactionManager.ZTKA,
				Sales_Order_No__c = ztkaOrderNum,
				RecordTypeId = InventoryTransactionManager.RECTYPE_TRANSACTION,
				Lot_Number__c = 'Lot',
				Serial_Number__c = '12345'
			);
			insert invTransZTKA;

			Inventory_Transaction__c invTransZTKB = new Inventory_Transaction__c(
				Inventory_Item__c = invItem.Id,
				ZTKA_No__c = ztkaOrderNum,
				Model_Number__c = order.OrderItems[0].PricebookEntry.Product2Id,
				Order_Type__c = InventoryTransactionManager.ZTKB,
				RecordTypeId = InventoryTransactionManager.RECTYPE_TRANSACTION,
				Lot_Number__c = 'Lot',
				Serial_Number__c = '12345',
				Order_Item__c = order.OrderItems[0].Id
			);
			insert invTransZTKB;	

		Test.stopTest();

	}

	static testMethod void test_ZTKBA_Matching() {
		
		Test.startTest();

			Inventory_Item__c invItem = [select Id from Inventory_Item__c where Serial_Number__c = :SERIAL1];

			// create transaction record 
			Order order = [
				select 
					(
						select
							PricebookEntry.Product2Id
						from OrderItems
					)
			 	from Order 
			 	limit 1
			];

			String ztkaOrderNum = 'o48ej4oc8';

			Inventory_Transaction__c invTransZTKB = new Inventory_Transaction__c(
				Inventory_Item__c = invItem.Id,
				ZTKA_No__c = ztkaOrderNum,
				Model_Number__c = order.OrderItems[0].PricebookEntry.Product2Id,
				Order_Type__c = InventoryTransactionManager.ZTKB,
				RecordTypeId = InventoryTransactionManager.RECTYPE_TRANSACTION,
				Lot_Number__c = 'Lot',
				Serial_Number__c = '12345'
			);
			insert invTransZTKB;

			Inventory_Transaction__c invTransZTKA = new Inventory_Transaction__c(
				Inventory_Item__c = invItem.Id,
				Model_Number__c = order.OrderItems[0].PricebookEntry.Product2Id,
				Order__c = order.Id,
				Order_Item__c = order.OrderItems[0].Id,
				Order_Type__c = InventoryTransactionManager.ZTKA,
				Sales_Order_No__c = ztkaOrderNum,
				RecordTypeId = InventoryTransactionManager.RECTYPE_TRANSACTION,
				Lot_Number__c = 'Lot',
				Serial_Number__c = '12345'
			);
			insert invTransZTKA;

		Test.stopTest();

	}

	static testMethod void test_InventoryItem_Rollups() {
		
		Test.startTest();

			Inventory_Item__c item = [select Id from Inventory_Item__c where Serial_Number__c = :SERIAL1];

			List<Inventory_Transaction__c> trans = new List<Inventory_Transaction__c> {
				new Inventory_Transaction__c(
					Inventory_Item__c = item.Id,
					Quantity__c = 1
				),
				new Inventory_Transaction__c(
					Inventory_Item__c = item.Id,
					Quantity__c = 1
				),
				new Inventory_Transaction__c(
					Inventory_Item__c = item.Id,
					Quantity__c = 1
				)
			};
			insert trans;

	        TriggerHandlerManager.clearTriggerHashes();

			update new List<Inventory_Transaction__c> {
				new Inventory_Transaction__c(
					Id = trans[0].Id,
					Quantity__c = 2
				),
				new Inventory_Transaction__c(
					Id = trans[1].Id,
					Delivery_Quantity__c = 1
				),
				new Inventory_Transaction__c(
					Id = trans[2].Id,
					Delivery_Quantity__c = 1,
					Disposition__c = InventoryTransactionManager.IN_TRANSIT
				)
			};

			item = [
				select
					Available_Quantity__c,
					In_Transit__c,
					SAP_Calculated_Quantity__c
				from Inventory_Item__c
				where Id = :item.Id
			];

			System.assertEquals(14, item.Available_Quantity__c);
			System.assertEquals(2, item.SAP_Calculated_Quantity__c);
			System.assertEquals(1, item.In_Transit__c);

		Test.stopTest();

	}

	static testMethod void test_InventoryItem_Delete_and_Undelete(){
		
		Test.startTest();
		
			Inventory_Item__c item = [select Id from Inventory_Item__c where Serial_Number__c = :SERIAL1];

			List<Inventory_Transaction__c> trans = new List<Inventory_Transaction__c> {
				new Inventory_Transaction__c(
					Inventory_Item__c = item.Id,
					Quantity__c = 1
				),
				new Inventory_Transaction__c(
					Inventory_Item__c = item.Id,
					Delivery_Quantity__c = 1
				),
				new Inventory_Transaction__c(
					Inventory_Item__c = item.Id,
					Delivery_Quantity__c = 1,
					Disposition__c = InventoryTransactionManager.IN_TRANSIT
				)
			};
			insert trans;

			//Verify initial quantity values
			item = [
				select
					Available_Quantity__c,
					In_Transit__c,
					SAP_Calculated_Quantity__c
				from Inventory_Item__c
				where Id = :item.Id
			];

			System.assertEquals(11, item.Available_Quantity__c);
			System.assertEquals(2, item.SAP_Calculated_Quantity__c);
			System.assertEquals(1, item.In_Transit__c);

			//Delete Inventory Transaction records and verify quantities
			delete trans;
			item = [
				select
					Available_Quantity__c,
					In_Transit__c,
					SAP_Calculated_Quantity__c
				from Inventory_Item__c
				where Id = :item.Id
			];

			System.assertEquals(10, item.Available_Quantity__c);
			System.assertEquals(0, item.SAP_Calculated_Quantity__c);
			System.assertEquals(0, item.In_Transit__c);

			//Undelete Inventory Transaction records and verify quantities
			undelete trans;
			item = [
				select
					Available_Quantity__c,
					In_Transit__c,
					SAP_Calculated_Quantity__c
				from Inventory_Item__c
				where Id = :item.Id
			];

			System.assertEquals(11, item.Available_Quantity__c);
			System.assertEquals(2, item.SAP_Calculated_Quantity__c);
			System.assertEquals(1, item.In_Transit__c);

		Test.stopTest();

	}

	static testMethod void test_updateOrderStatusToActivated(){

		Test.startTest();

			Account acct = [SELECT Id, Name FROM Account WHERE Name = :ACCTNAME];
			Inventory_Item__c invItem = [SELECT Id, Inventory_Location__c FROM Inventory_Item__c LIMIT 1];
			
			Order order = [
				select 
					Pricebook2Id,
					(
						select
							PricebookEntryId,
							PricebookEntry.Product2Id
						from OrderItems
					)
			 	from Order 
			 	limit 1
			];

			Order returnOrder = order.clone(false, true);
			returnOrder.RecordTypeId = OrderManager.RECTYPE_RETURN;
			returnOrder.AccountId = acct.Id;
			returnOrder.EffectiveDate = System.today();
			returnOrder.Status = 'Draft';
			returnOrder.Pricebook2Id = order.Pricebook2Id;
			returnOrder.Shipping_Location__c = invItem.Inventory_Location__c;
			insert returnOrder;

			OrderItem item = new OrderItem(
				OrderId = returnOrder.Id,
				Quantity = 1,
				UnitPrice = 1,
				PricebookEntryId = order.OrderItems[0].PricebookEntryId
			);

			OrderItem item2 = new OrderItem(
				OrderId = returnOrder.Id,
				Quantity = 3,
				UnitPrice = 1,
				PricebookEntryId = order.OrderItems[0].PricebookEntryId
			);

			insert new List<OrderItem>{
				item, item2
			};

			List<Inventory_Transaction__c> trans = new List<Inventory_Transaction__c> {
				new Inventory_Transaction__c(
					Inventory_Item__c = invItem.Id,
					Delivery_Quantity__c = -1,
					Order__c = returnOrder.Id
				),
				new Inventory_Transaction__c(
					Inventory_Item__c = invItem.Id,
					Delivery_Quantity__c = -1,
					Order__c = returnOrder.Id
				),
				new Inventory_Transaction__c(
					Inventory_Item__c = invItem.Id,
					Delivery_Quantity__c = -2,
					Order__c = returnOrder.Id
				)
			};
			insert trans;

			returnOrder = [SELECT Id, Status FROM Order WHERE Id = :returnOrder.Id];
			System.assertEquals('Activated', returnOrder.Status, 'Return order should be Activated!');

		Test.stopTest();

	}

}