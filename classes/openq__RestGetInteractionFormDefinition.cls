/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/getinteractionformdefinition/*')
global class RestGetInteractionFormDefinition {
    global RestGetInteractionFormDefinition() {

    }
    @HttpPost
    global static openq.OpenMSL.INTERACTION_FORM_DEFINITION_RESPONSE_element getInteractionDataDefinition(openq.OpenMSL.INTERACTION_FORM_DEFINITION_REQUEST_element request_x) {
        return null;
    }
}
