/**
* MultiselectController synchronizes the values of the hidden elements to the SelectOption lists.
*
* @Author open sourced
* @Date 2015-04-22
*/
public with sharing class MultiselectPicklistController {

    final static String UTF8 = 'UTF-8';

    // SelectOption lists for public consumption
    public List<SelectOption> leftSOptions { get; set; }
    public List<SelectOption> rightSOptions { get; set; }

    /**
    *  @desc    Backing for hidden text field containing the options from the left list
    */
    public String leftOptionsHidden { 
        get; 
        set {
           leftOptionsHidden = value;
           setOptions(leftSOptions, value);
        }
    }

    /**
    *  @desc    Backing for hidden text field containing the options from the right list
    */
    public String rightOptionsHidden { 
        get; 
        set {
           rightOptionsHidden = value;
           setOptions(rightSOptions, value);
        }
    }

    /**
    *  @desc    Parse &-separated values and labels from value and put them in option
    */
    private void setOptions(List<SelectOption> options, String value) {

        options.clear();
        List<String> parts = value.split('&');
        
        for (Integer i = 0; i < parts.size() / 2; i++) {
            Integer i2 = i * 2;
            options.add(new SelectOption(
                EncodingUtil.urlDecode(parts[i2], UTF8), 
                EncodingUtil.urlDecode(parts[i2 + 1], UTF8)
            ));
        }
    }

}