@isTest(SeeAllData=False)

public class MyObjectivesTriggerHandlerTest{
    public static list<User> usrLst;
    public static list<Personnel_ID__c> pesrnIdlst = new list<Personnel_ID__c>();
    public static list<Account> acctlst = new list<Account>();
    public static list<Account_Team_Member__c> ATMList = new list<Account_Team_Member__c>();
    public static list<My_Objectives__c> MyObjList = new list<My_Objectives__c>();
    public static list<My_Objectives__c> MyObjUpdateList = new list<My_Objectives__c>();
    public static list<My_Objectives__c> MyObjUpdateList1 = new list<My_Objectives__c>();
    public static list<Opportunity> opptyLst = new list<Opportunity>(); 
    public static List<Territory2Type> Ttype;  
    public static Id Territory2ModelIDActive;
    public static List<Territory2> trList;  
     @testSetup
     static void setupMockData(){
 
            User sysAdmin= UtilForUnitTestDataSetup.getUser();
            
            User u1 = UtilForUnitTestDataSetup.newUser('EU Sales Rep');
            u1.Username = 'UserNameC@boston.com';
            u1.IsActive= True;
            User u2 = UtilForUnitTestDataSetup.newUser();
            u2.Username = 'UserNameR@boston.com';
            u2.IsActive= True;
            usrLst =new list<User>{u1,u2};
            insert usrLst;  
        
            Ttype = UtilForUnitTestDataSetup.newTerritory2Type();
            Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
                
                
            Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('TO1',Territory2ModelIDActive, Ttype[0].Id);
            Territory2 t2 = UtilForUnitTestDataSetup.newTerritory2('TO2',Territory2ModelIDActive, Ttype[0].Id);
            trList=new List<Territory2> {t1,t2 };
            insert trList;
                
            UserTerritory2Association UTAsso = UtilForUnitTestDataSetup.newUserTerritory2Association(trList[0].Id, usrLst[0].Id);            
            UserTerritory2Association UTAsso1 = UtilForUnitTestDataSetup.newUserTerritory2Association(trList[1].Id, usrLst[1].Id);            
            insert new List<UserTerritory2Association> {UTAsso, UTAsso1 };
    }

    static testMethod void test_myObjTrigger_1()
    {
            List<Territory2> trLst = [Select name,Id From Territory2  where name LIKE : 'TO%' ]; //Since calling the above method here gives MIXED_DML exception, query the necessary data
            list<User> urLst = new list<User>([Select name,Id From User where Username LIKE : '%@boston.com' ]);
            
            Account a1 = UtilForUnitTestDataSetup.newAccount('ACCTNAME1');
            Account a2 = UtilForUnitTestDataSetup.newAccount('ACCTNAME2');
            list<Account> alist= new List<Account>{a1, a2};
            insert alist;
            Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.My_Objectives__c.getRecordTypeInfosByName();
            Id RECTYPE_Master = RECTYPES.get('Master').getRecordTypeId();
            system.debug(RECTYPE_Master+'RECTYPE_Master --');
            
            list<Account> acLst = new list<Account>([Select name,Id From Account where name LIKE : 'AC%' ]);
            
            ObjectTerritory2Association OTAsso =UtilForUnitTestDataSetup.newObjectTerritory2Association(acLst[0].Id, trLst[0].Id);
            ObjectTerritory2Association OTAsso1 =UtilForUnitTestDataSetup.newObjectTerritory2Association(acLst[1].Id, trLst[1].Id);
            insert new List<ObjectTerritory2Association> {OTAsso, OTAsso1};
            MyObjectivesRecordType__c cs = new MyObjectivesRecordType__c();
            cs.Name = 'ANZ Objective';
            insert cs;
            User EUuser = urLst[0];
            system.runAs(urLst[0]){
                for(Integer i=0; i<acLst.size(); i++){
                    My_Objectives__c a = new My_Objectives__c(
                        Name = 'test',
                        OwnerId = urLst[0].Id, 
                        Division__c = 'EP',
                        Related_to_Account__c=acLst[0].Id,
                        Status__c = 'New',
                        Territory__c = trLst[0].name                
                    );
                    MyObjList.add(a);
                }
                insert  MyObjList;
             }
             system.debug(MyObjList+'--@@@--');
             //system.assertEquals(MyObjList[0].RecordTypeId,RECTYPE_MASTER); 
             for(Integer i=0; i<MyObjList.size(); i++){
                 My_Objectives__c a = new My_Objectives__c(
                     Id = MyObjList[i].Id,                     
                     Related_to_Account__c = acLst[1].Id
                );
                MyObjUpdateList.add(a); 
             }  
             update MyObjUpdateList;
             
             for(Integer i=0; i<MyObjList.size(); i++){
                 My_Objectives__c a = new My_Objectives__c(
                     Id = MyObjList[i].Id, 
                     Status__c = 'In Progress',                    
                     Related_to_Account__c = acLst[0].Id
                );
                MyObjUpdateList1.add(a); 
             }  
             update MyObjUpdateList1;
                       
    }
    static testMethod void test_myObjTrigger_2()
    {
        list<User> urLst = new list<User>([Select name,Id From User where Username LIKE : '%@boston.com' ]);
            
            Account a1 = UtilForUnitTestDataSetup.newAccount('ACCTNAME1');
            Account a2 = UtilForUnitTestDataSetup.newAccount('ACCTNAME2');
            list<Account> alist= new List<Account>{a1, a2};
            insert alist;
            Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.My_Objectives__c.getRecordTypeInfosByName();
            Id RECTYPE_ANZ = RECTYPES.get('ANZ Objective').getRecordTypeId();
            
            list<Account> acLst = new list<Account>([Select name,Id From Account where name LIKE : 'AC%' ]);
            for(Integer i=0; i<acLst.size(); i++){
                My_Objectives__c a = new My_Objectives__c(
                    Name = 'test',
                    OwnerId = urLst[0].Id, 
                    Division__c = 'EP',
                    Related_to_Account__c=acLst[0].Id,
                    Status__c = 'New',
                    RecordTypeId = RECTYPE_ANZ              
                );
                MyObjList.add(a);
            }
            insert  MyObjList;
            system.debug(MyObjList+'123--@@@--');
            for(Integer i=0; i<MyObjList.size(); i++){
                 My_Objectives__c a = new My_Objectives__c(
                     Id = MyObjList[i].Id,                     
                     Related_to_Account__c = acLst[1].Id
                );
                MyObjUpdateList.add(a); 
             }  
             update MyObjUpdateList;
             
             for(Integer i=0; i<MyObjList.size(); i++){
                 My_Objectives__c a = new My_Objectives__c(
                     Id = MyObjList[i].Id, 
                     Status__c = 'In Progress',                    
                     Related_to_Account__c = acLst[0].Id
                );
                MyObjUpdateList1.add(a); 
             }  
             update MyObjUpdateList1;
    
    }

}