/**
* Trigger for the Account to create Custom Object
*
* @Author salesforce Services
* @Date 04/07/2015
*/
@isTest(SeeAllData=false)
private class AccountTriggerHandlerTest {

    final static NMD_TestDataManager td = new NMD_TestDataManager();

    static testMethod void test_Prospect() {

        Account acct = td.newAccount();
        acct.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acct.Sold_to_City__c = 'City';
        acct.Sold_to_State__c = 'ST';
        insert acct;

    }
    
    static testMethod void test_Consignment() {        

        Account acc = td.createConsignmentAccount();
        insert acc;
        //check if obj got created.
        System.assertEquals(1, [Select Count() from Inventory_Data__c where Account__c = :acc.Id]);
        
        acc.BillingCity = 'New York';
        update acc;
        System.assertEquals(1, [Select Count() from Inventory_Data__c where Account__c = :acc.Id]);
        //add more checkes against customer fields      
    }

    static testMethod void test_ConsignmentSwitch() {


        Account acct = td.createConsignmentAccount();
        acct.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
        insert acct;

        TriggerHandlerManager.clearTriggerHashes();

        acct.RecordTypeId = AccountManager.RECTYPE_CONSIGNMENT;
        update acct;

        acct = [SELECT Id, RecordTypeId FROM Account WHERE Id = :acct.Id];

        System.assertEquals(1, [select count() from Inventory_Data__c where Account__c = :acct.Id]);

    }

    static testMethod void test_ConsignmentOwnerFromPersonnelID() {
        
        User user = td.newUser('System Administrator');
        insert user;

        Account acc = td.createConsignmentAccount();

        Personnel_ID__c pid = [SELECT Id, User_for_account_team__c FROM Personnel_ID__c WHERE Id = :acc.Personnel_ID__c LIMIT 1];
        pid.User_for_account_team__c = user.Id;
        update pid;

        System.Test.startTest();

            insert acc;

            Account testAcc = [SELECT Id, OwnerId FROM Account WHERE Id = :acc.Id];
            System.assertEquals(user.Id, testAcc.OwnerId, 'Account owner should be the Personnel ID User');

            Inventory_Data__c iData = [SELECT Id, OwnerId FROM Inventory_Data__c WHERE Account__c = :acc.Id LIMIT 1];
            System.assertEquals(user.Id, iData.OwnerId, 'Inventory Data owner should be the Personnel ID User!');

        System.Test.stopTest();
    }

    static testMethod void test_NameFormatting() {

        Account acct = td.newAccount();
        acct.RecordTypeId = AccountManager.RECTYPE_CUSTOMER;
        acct.Account_Number__c = td.randomString5();
        acct.ShippingCity = 'City';
        acct.ShippingState = 'MA';
        acct.Account_Number__c = '101022123131';
        insert acct;

        acct.ShippingState = 'CA';
        update acct;

    }

    static testMethod void test_CascadeSAPInfo() {

        Account acct = td.createConsignmentAccount();
        Account trial = td.newAccount();
        trial.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        trial.Type = 'Sold to';
        Account proc = td.newAccount();
        proc.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        insert new List<Account> { acct, trial, proc };

        Opportunity oppty = td.newOpportunity(acct.Id);
        oppty.Trialing_Account__c = trial.Id;
        oppty.Procedure_Account__c = proc.Id;
        insert oppty;

        Test.startTest();
        {
            update new Account(
                Id = trial.Id,
                Account_Number__c = td.randomString5()
            );
        }
        Test.stopTest();

    }

}