/**
* Extension class for the vf page PainMap
*
* @Author salesforce Services
* @Date 03/25/2015
*/
public with sharing class NMD_PainMapExtension extends NMD_ExtensionBase {
	
	final ApexPages.StandardController sc;

	public String summaryId { get; private set; }
	public List<Map<String,String>> layers { get; private set; }

	public NMD_PainMapExtension(ApexPages.StandardController sc) {
		this.sc = sc;
		this.layers = new List<Map<String,String>>{};

		List<Clinical_Data_Summary__c> cdss = new List<Clinical_Data_Summary__c>([
			select Id 
			from Clinical_Data_Summary__c
			where RecordTypeId = :ClinicalDataSummaryManager.RECTYPE_PAINMAP
			and Opportunity__c = :sc.getId()
		]);
		if (!cdss.isEmpty()) {

			this.summaryId = cdss[0].Id;

			for (Clinical_Data__c cdata : [
				select
					Name,
					Enabled__c,
					Index__c,
					Average_Pain_Intensity__c,
					Worst_Pain_Intensity__c,
					Continuous_Pain_Intensity__c,
					(select Id from Attachments)
				from Clinical_Data__c
				where Clinical_Data_Summary__c = :this.summaryId
			]) {

				Map<String,Object> metadata = new Map<String,Object>{
					'name' => cdata.Name,
					'enabled' => cdata.Enabled__c,
					'index' => cdata.Index__c,
					'avg_pain' => cdata.Average_Pain_Intensity__c,
					'max_pain' => cdata.Worst_Pain_Intensity__c,
					'con_pain' => cdata.Continuous_Pain_Intensity__c
				};

				this.layers.add(new Map<String,String>{
					'image_id' => cdata.Attachments[0].Id,
					'metadata' => JSON.serialize(metadata)
				});
			}

		}

	}

	public PageReference checkRedirect() {

		PageReference pr;
		if (ApexPages.currentPage().getParameters().get('isdtp') != 'p1') {
			pr = Page.PainMap;
			pr.getParameters().put('id', this.sc.getId());
		}

		return pr;
	}

	@RemoteAction
	public static Map<String,Object> savePainMap(Id summaryId, String painMapBase64, List<Map<String,Object>> layers) {

		Map<String,Object> result = new Map<String,Object>{
			'is_success' => false,
			'message' => 'unknown'
		};

		//System.debug(summaryId);
		//System.debug(painMapBase64.length());
		//System.debug(layers[0].get('name'));

		System.Savepoint sp = Database.setSavepoint();

		//remove old painmap
		List<Attachment> oldPainMap = new List<Attachment>([select Id from Attachment where ParentId = :summaryId]);
		if (!oldPainMap.isEmpty()) {
			delete oldPainMap;
		}

		//attach pain map
		List<Attachment> attachments = new List<Attachment>{
			new Attachment(
				ParentId = summaryId,
				Name = 'painmap.png',
				Body = EncodingUtil.base64Decode(painMapBase64)
			)
		};
		painMapBase64 = null;

		//remove old child records
		List<Clinical_Data__c> oldLayers = new List<Clinical_Data__c>([select Id from Clinical_Data__c where Clinical_Data_Summary__c = :summaryId]);
		if (!oldLayers.isEmpty()) {
			delete oldLayers;
		}

		//create child records
		List<Clinical_Data__c> newLayers = new List<Clinical_Data__c>{};
		for (Map<String,Object> layer : layers) {
			newLayers.add(new Clinical_Data__c(
				Clinical_Data_Summary__c = summaryId,
				Name = String.valueOf(layer.get('name')),
				Enabled__c = Boolean.valueOf(layer.get('enabled')),
				Index__c = Decimal.valueOf(String.valueOf(layer.get('index'))),
				Average_Pain_Intensity__c = Decimal.valueOf(String.valueOf(layer.get('avg_pain'))),
				Worst_Pain_Intensity__c = Decimal.valueOf(String.valueOf(layer.get('max_pain'))),
				Continuous_Pain_Intensity__c = Decimal.valueOf(String.valueOf(layer.get('con_pain')))
			));
		}
		insert newLayers;

		//attach images
		for (Integer i = 0; i < layers.size(); i++) {
			Map<String,Object> layer = layers[i];
			Id cdataId = newLayers[i].Id;

			attachments.add(new Attachment(
				ParentId = cdataId,
				Name = 'layer.png',
				Description = String.valueOf(layer.get('name')),
				Body = EncodingUtil.base64Decode(String.valueOf(layer.get('image')))
			));
		}
		insert attachments;

		result = new Map<String,Object>{
			'is_success' => true
		};

		return result;

	}


}