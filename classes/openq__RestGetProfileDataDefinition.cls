/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/getprofiledatadefinition/*')
global class RestGetProfileDataDefinition {
    global RestGetProfileDataDefinition() {

    }
    @HttpPost
    global static openq.OpenMSL.PROFILE_DATA_DEFINITION_RESPONSE_element getProfileDataDefinition(openq.OpenMSL.PROFILE_DATA_DEFINITION_REQUEST_element request_x) {
        return null;
    }
}
