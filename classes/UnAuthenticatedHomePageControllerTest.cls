@isTest
private class UnAuthenticatedHomePageControllerTest {

	private static testMethod void test() {
	    
	    //Create Community_Event__c with display name as 'test Community Event'
	    Community_Event__c objCommunity_Event = Test_DataCreator.createCommunityEvent('test Display Name');
	    //Create Boston scientific cofig test data using Test_DataCreator class
	    Test_DataCreator.cerateBostonCSConfigTestData(objCommunity_Event.Id); 
        UnAuthenticatedHomePageController objUnAuthenticatedHomePageController = new UnAuthenticatedHomePageController(); 
	    objUnAuthenticatedHomePageController.init();
	    
	    //Create Guest User
        User objUser = Test_DataCreator.createGuestUser();
        
        Test.StartTest();
        
        system.runAs(objUser){
            
            system.assertEquals(null, objUnAuthenticatedHomePageController.init());
        }
        Test.StopTest();
    }
}