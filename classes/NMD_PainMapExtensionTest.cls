/**
* Test class for the PainMap vf extension
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class NMD_PainMapExtensionTest {
    
    final static String NAME = 'NAMETest';

    @TestSetup
    static void setup() {

        NMD_TestDataManager td = new NMD_TestDataManager();

        Account acct = td.createConsignmentAccount();
        acct.Name = NAME;
        insert acct;

        Opportunity oppty = td.newOpportunity(acct.Id);
        //oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
        oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        insert oppty;

        /*Clinical_Data_Summary__c cds = [
            select Id 
            from Clinical_Data_Summary__c 
            where Opportunity__c = :oppty.Id
            and RecordTypeId = :ClinicalDataSummaryManager.RECTYPE_PAINMAP
        ];*/
        
        Clinical_Data_Summary__c cds = new Clinical_Data_Summary__c();
        cds.Opportunity__c = oppty.Id;
        cds.RecordTypeId = ClinicalDataSummaryManager.RECTYPE_PAINMAP;
        insert cds;
   
        Clinical_Data__c cd = new Clinical_Data__c(
            Clinical_Data_Summary__c = cds.Id,
            Name = 'TP',
            Enabled__c = true,
            Index__c = 0,
            Average_Pain_Intensity__c = 5,
            Worst_Pain_Intensity__c = 7,
            Continuous_Pain_Intensity__c = 3
        );
        insert cd;

        insert new List<Attachment> {
            new Attachment(
                ParentId = cds.Id,
                Name = 'painmap.png',
                Body = Blob.valueOf('foo')
            ),
            new Attachment(
                ParentId = cd.Id,
                Name = 'layer.png',
                Body = Blob.valueOf('bar')
            )
        };

    }

    static testMethod void test_PainMap() {

        Opportunity oppty = [select Id from Opportunity where Account.Name = :NAME];

        PageReference pr = Page.PainMapSF1;
        pr.getParameters().put('id', oppty.Id);
        Test.setCurrentPage(pr);

        NMD_PainMapExtension ext = new NMD_PainMapExtension(new ApexPages.StandardController(oppty));

        ext.checkRedirect();

    }

    static testMethod void test_SavePainmap() {

        Opportunity opty = [select Id,Name from Opportunity where Account.Name = :NAME];
        List<Clinical_Data_Summary__c> cds = [select id,Opportunity__c,RecordTypeId From Clinical_Data_Summary__c where Opportunity__c =:opty.id AND RecordTypeId =:ClinicalDataSummaryManager.RECTYPE_PAINMAP];
    

        String painMapBase64 = EncodingUtil.base64Encode(Blob.valueOf('foo'));
        String layerBase64 = EncodingUtil.base64Encode(Blob.valueOf('bar'));
        List<Map<String,Object>> layers = new List<Map<String,Object>> {
            new Map<String,Object> {
                'image' => layerBase64,
                'name' => 'TP',
                'enabled' => true,
                'index' => 0,
                'avg_pain' => 5,
                'max_pain' => 7,
                'con_pain' => 4
            }
        };
       for(Clinical_Data_Summary__c cds1 : cds){
  
        NMD_PainMapExtension.savePainMap(cds1.Id, painMapBase64, layers);}

    }

}