@isTest
private class DocumentsController_Test {

	private static testMethod void test() {
	    
	        //create Boston scientific cofig test data using Test_DataCreator class
            Community_Event__c objEvent = Test_DataCreator.createCommunityEvent(Date.Today().adddays(2));
            Test_DataCreator.cerateBostonCSConfigTestData(objEvent.Id);
            User objUser = Test_DataCreator.createCommunityUser();
            
        system.runAs(objUser) {
            
            Account objAccount = Test_DataCreator.createAccount('test Account');
            Contact objContact = Test_DataCreator.createContact('testLastname', objAccount.Id);
            
            FeedItem post = new FeedItem(ParentId = objContact.Id, Body = 'test post', Visibility = 'AllUsers');
            insert post;
            DocumentsController objDocumentsController = new DocumentsController();
            //system.assertNotEquals(0, objDocumentsController.lstFeedItemWrapper.size());
        }
    }
}