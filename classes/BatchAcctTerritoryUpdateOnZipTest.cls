@isTest(SeeAllData=false)
@testVisible
private class BatchAcctTerritoryUpdateOnZipTest{
    public static List<Territory2Type> Ttype;
    public static Id Territory2ModelIDActive;
    public static List<Territory2> trList=new List<Territory2>();
    
    @testSetup
    static void setupTerritoryMockData(){
        Ttype = UtilForUnitTestDataSetup.newTerritory2Type();        
        Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
        Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('AA1234',Territory2ModelIDActive,Ttype[0].Id);   
        trList.add(t1);              
        Territory2 t2 = UtilForUnitTestDataSetup.newTerritory2('AA5678',Territory2ModelIDActive,Ttype[0].Id); 
        trList.add(t2);
        Territory2 t3 = UtilForUnitTestDataSetup.newTerritory2('AA9012',Territory2ModelIDActive,Ttype[0].Id); 
        trList.add(t3);
        Territory2 t4 = UtilForUnitTestDataSetup.newTerritory2('AA3456',Territory2ModelIDActive,Ttype[0].Id); 
        trList.add(t4); 
        insert trList; 
        
        User userR = UtilForUnitTestDataSetup.newUser('Uro Sales/Marketing User');
        userR.Username = 'UroUser1@abc.com';
        insert userR;
    }
    
    static TestMethod void TestbatchAccountTerritoryUpdate_1(){ 
        
        list<User> urLst = new list<User>([Select name,Id,Profile.name From User where Username Like : 'Uro%'  limit 1]);
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'AA%'  ]; 
        Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Zip_To_Territory__c.getRecordTypeInfosByName();
        Id RECTYPE_URO = RECTYPES.get('UroPH').getRecordTypeId();
        Map<String,Schema.RecordTypeInfo> RECTYPES_ACCCT = Schema.SObjectType.Account.getRecordTypeInfosByName();
        Id RECTYPE_ACCT = RECTYPES_ACCCT.get('Non-Buying').getRecordTypeId();
        
        list<Account> actLst = new list<Account>();
        list<Zip_To_Territory__c> ztLst = new list<Zip_To_Territory__c>();
        list<ObjectTerritory2Association> objLst = new list<ObjectTerritory2Association>();
       
        for(integer i=0;i<50;i++){
            Account a = new Account();
            a.name = 'testAcct'+i;
            a.ShippingPostalCode = '9000'+i;
            a.RecordTypeId = RECTYPE_ACCT;
            a.UroPH_Account__c = true;
            actLst.add(a);            
        }
        insert actLst;
        for(integer i=0;i<25;i++){
            ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
            accountAssociation.ObjectId = actLst[i].Id;
            accountAssociation.Territory2Id = trLst[2].Id;
            accountAssociation.AssociationCause = 'Territory2Manual';
            objLst.add(accountAssociation);
        }
        
        for(integer i=25;i<50;i++){
            ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
            accountAssociation.ObjectId = actLst[i].Id;
            accountAssociation.Territory2Id = trLst[3].Id;
            accountAssociation.AssociationCause = 'Territory2Manual';
            objLst.add(accountAssociation);
        }
        
        insert objLst;
        
        
            Zip_To_Territory__c a = new Zip_To_Territory__c();
            a.UroPH_Territory_Name__c = trLst[0].name;
            string temp;
            for(integer j=0;j<25;j++){
             temp = '9000'+j+',';                        
            }            
            a.UroPH_Zip_Codes__c = temp.removeEnd(',');
            //a.UroPH_Zip_Codes__c = '90001;90002;90003;';
            a.UroPH_Delete__c = false;
            a.RecordTypeId = RECTYPE_URO;    
            a.LastModifiedDate = system.today();
            a.createdDate = system.today();        
            ztLst.add(a);
            
            Zip_To_Territory__c b = new Zip_To_Territory__c();
            b.UroPH_Territory_Name__c = trLst[1].name;
            string tempb;
            for(integer j=25;j<50;j++){
             tempb = '9000'+j+',';                        
            }            
            b.UroPH_Zip_Codes__c = tempb.removeEnd(',');
            b.UroPH_Delete__c = false;
            b.RecordTypeId = RECTYPE_URO;
            b.LastModifiedDate = system.today();
            b.createdDate = system.today(); 
            ztLst.add(b);   
            
            
                       
        insert ztLst;
        //system.assertEquals(ztLst[0].RecordTypeId,RECTYPES.get('UroPH').getRecordTypeId());
        //system.debug(ztLst+'ztLst-->');
        
        test.startTest();
        BatchAcctTerritoryUpdateOnZip bc = new BatchAcctTerritoryUpdateOnZip();       
        RECTYPE_URO = RECTYPES.get('UroPH').getRecordTypeId();
        Id processiD = Database.executeBatch(bc);
           
        test.stopTest();
    
    }
    static TestMethod void TestbatchAccountTerritoryUpdate_2(){ 
         list<User> urLst = new list<User>([Select name,Id,Profile.name From User where Username Like : 'Uro%'  limit 1]);
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'AA%'  ]; 
        Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Zip_To_Territory__c.getRecordTypeInfosByName();
        Id RECTYPE_URO = RECTYPES.get('UroPH').getRecordTypeId();
        Map<String,Schema.RecordTypeInfo> RECTYPES_ACCCT = Schema.SObjectType.Account.getRecordTypeInfosByName();
        Id RECTYPE_ACCT = RECTYPES_ACCCT.get('Non-Buying').getRecordTypeId();
        
        list<Account> actLst = new list<Account>();
        list<Zip_To_Territory__c> ztLst = new list<Zip_To_Territory__c>();
        list<ObjectTerritory2Association> objLst = new list<ObjectTerritory2Association>();
       
        for(integer i=0;i<50;i++){
            Account a = new Account();
            a.name = 'testAcct'+i;
            a.ShippingPostalCode = '9000'+i;
            a.RecordTypeId = RECTYPE_ACCT;
            a.UroPH_Account__c = true;
            actLst.add(a);            
        }
        insert actLst;
        for(integer i=0;i<25;i++){
            ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
            accountAssociation.ObjectId = actLst[i].Id;
            accountAssociation.Territory2Id = trLst[2].Id;
            accountAssociation.AssociationCause = 'Territory2Manual';
            objLst.add(accountAssociation);
        }
        
        for(integer i=25;i<50;i++){
            ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
            accountAssociation.ObjectId = actLst[i].Id;
            accountAssociation.Territory2Id = trLst[3].Id;
            accountAssociation.AssociationCause = 'Territory2Manual';
            objLst.add(accountAssociation);
        }
        
        insert objLst;
        
        
            
            
            Zip_To_Territory__c c = new Zip_To_Territory__c();
            c.UroPH_Territory_Name__c = trLst[2].name;
            string tempc;
            for(integer j=0;j<25;j++){
             tempc = '9000'+j+',';                        
            }            
            c.UroPH_Zip_Codes__c = tempc.removeEnd(',');
            c.UroPH_Delete__c = true;
            c.RecordTypeId = RECTYPE_URO;
            c.LastModifiedDate = system.today();
            c.createdDate = system.today(); 
            ztLst.add(c); 
            
            Zip_To_Territory__c d= new Zip_To_Territory__c();
            d.UroPH_Territory_Name__c = trLst[3].name;
            string tempd;
            for(integer j=25;j<50;j++){
             tempd = '9000'+j+',';                        
            }            
            d.UroPH_Zip_Codes__c = tempd.removeEnd(',');
            d.UroPH_Delete__c = true;
            d.RecordTypeId = RECTYPE_URO;
            d.LastModifiedDate = system.today();
            d.createdDate = system.today(); 
            ztLst.add(d);  
                       
        insert ztLst;
        //system.assertEquals(ztLst[0].RecordTypeId,RECTYPES.get('UroPH').getRecordTypeId());
        //system.debug(ztLst+'ztLst-->');
        
        test.startTest();
        BatchAcctTerritoryUpdateOnZip bc = new BatchAcctTerritoryUpdateOnZip();       
        RECTYPE_URO = RECTYPES.get('UroPH').getRecordTypeId();
        Id processiD = Database.executeBatch(bc);
           
        test.stopTest();
    }

}