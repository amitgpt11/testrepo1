/*
 @CreatedDate     1 July 2016                                  
 @author          Ashish-Accenture
 @Description     This class is contains trigger methods of ProductGroupMapping
       
*/


Public class ProductGroupMappingTriggerMethods{
    
    //update Contract division(Multiselect picklist) based on its child Product Group Mapping divisions
    public static void updateContractDivision(){ 
        try{
            Map<Id,set<String>> mapContractDivisionToUpdate = new Map<Id,set<String>>();
            
            Map<String, String> mapDivMapping = new Map<String, String>();
            List<ContractDivisionMapping__c> lstDivMapping = ContractDivisionMapping__c.getall().values();
            
            for(ContractDivisionMapping__c divmap : lstDivMapping){
                mapDivMapping.put(divmap.Name.tolowercase(),divmap.Division__c); 
            }
           
           if(trigger.IsDelete){
               for(sObject sobj: trigger.old){
                   Shared_Product_Group_Mapping__c grp = (Shared_Product_Group_Mapping__c) sobj;
                   mapContractDivisionToUpdate.put(grp.Shared_Contract__c,new Set<String>{null});
               } 
            }else{
           
                for(sObject sobj: trigger.new){
                    Shared_Product_Group_Mapping__c grp = (Shared_Product_Group_Mapping__c) sobj;
                    
                    String grpDivision = grp.Shared_Division__c;
                    if(mapDivMapping != null && grp.Shared_Division__c != null && mapDivMapping.get(grp.Shared_Division__c.tolowercase()) != null){
                        grpDivision = mapDivMapping.get(grp.Shared_Division__c.tolowercase());
                    }
                    
                    if(mapContractDivisionToUpdate.get(grp.Shared_Contract__c) != null){
                      
                        Set<String> setTempdivision = mapContractDivisionToUpdate.get(grp.Shared_Contract__c);
                        setTempdivision.add(grpDivision);
                        mapContractDivisionToUpdate.put(grp.Shared_Contract__c,setTempdivision);
                    }else{
                        mapContractDivisionToUpdate.put(grp.Shared_Contract__c,new Set<String>{grpDivision});
                    }
                    
                }
            }    
            
            list<Shared_Product_Group_Mapping__c> lstAllGroupMappingContracts = [Select id,Shared_Division__c,Shared_Contract__c from Shared_Product_Group_Mapping__c where Shared_Contract__c in: mapContractDivisionToUpdate.keySet()];
            for(Shared_Product_Group_Mapping__c grp : lstAllGroupMappingContracts){
                 if(mapContractDivisionToUpdate.get(grp.Shared_Contract__c) != null){
                    
                    String grpDivision = grp.Shared_Division__c;
                    if(mapDivMapping != null && grp.Shared_Division__c != null && mapDivMapping.get(grp.Shared_Division__c.tolowercase()) != null){
                        grpDivision = mapDivMapping.get(grp.Shared_Division__c.tolowercase());
                    }
                      
                    Set<String> setTempdivision = mapContractDivisionToUpdate.get(grp.Shared_Contract__c);
                    setTempdivision.add(grpDivision);
                    mapContractDivisionToUpdate.put(grp.Shared_Contract__c,setTempdivision);
                }      
            }
            
            list<Contract> lstContractToUpdate = new list<Contract>();
            Contract objContrToUpdate;
            Set<String> setTempDiv;
            for(id contrId : mapContractDivisionToUpdate.keyset()){
                objContrToUpdate = new Contract();
                objContrToUpdate.Id = contrId;
                
                setTempDiv = new Set<String>();
                setTempDiv.addall(mapContractDivisionToUpdate.get(contrId));
                
                list<String> listTempDiv = new list<String>();
                listTempDiv.addall(setTempDiv);
                
                objContrToUpdate.Shared_Divisions__c = String.Join(listTempDiv,';');
                
                lstContractToUpdate.add(objContrToUpdate);
            }
            
            
            if(lstContractToUpdate.size() > 0){
                update lstContractToUpdate;
            } 
        }Catch(exception e){
            System.debug('####==e====='+e);
        }      
    }
}