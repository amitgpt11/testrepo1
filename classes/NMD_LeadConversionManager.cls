/**
* NMD_LeadConversionManager
*
* @author   Salesforce services
* @date     2014-03-01
*/
public without sharing class NMD_LeadConversionManager {

    final Lead convert;
    final String convertStatus;

    public String lastErrorMessage { get; private set; }
    public Id newOpportunityId { get; private set; }

    public NMD_LeadConversionManager(Id leadId) {

        this.lastErrorMessage = '';
        this.newOpportunityId = null;

        try {
            
            this.convert = [
                select
                    Company,
                    ConvertedDate,
                    CreatedDate,
                    LeadSource,
                    Account_Facility__c,
                    Communication_Preference__c,
                    DVD_Received__c,
                    Educated_Date__c,
                    First_Contacted_Date__c,
                    Patient__c,
                    Patient__r.Pain_Area__c,
                    Patient__r.Physician_of_Record__r.AccountId,
                    Patient__r.Primary_Insurance__c,
                    Patient__r.Secondary_Insurance__c,
                    Territory_ID__c,
                    Trialing_Physician__c,
                    Trialing_Physician__r.AccountId,
                    CARE_Card_Physician__c,
                    Secondary_Source__c,
                    Referring_Physician__c
                
                from Lead 
                where Id = :leadId
            ];

            List<LeadStatus> ls = new List<LeadStatus>([select MasterLabel from LeadStatus where IsConverted = true]);
            this.convertStatus = Test.isRunningTest() ? 'Converted' : ls[0].MasterLabel;

        }
        catch (System.QueryException qe) {
            this.lastErrorMessage = qe.getMessage();
        }

        if (this.convert != null && this.convert.ConvertedDate != null) {
            this.lastErrorMessage = Label.NMD_Lead_Already_Converted + ' ' + this.convert.ConvertedDate.format();
            throw new ConvertLeadException(this.lastErrorMessage);
        }

    }

    // returns the Id of the created Opportunity
    public Boolean convertLead() {

        Id acctId = this.convert.Trialing_Physician__c != null
            ? this.convert.Trialing_Physician__r.AccountId
            : this.convert.Account_Facility__c;

        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setAccountId(acctId);
        lc.setConvertedStatus(this.convertStatus); 
        lc.setLeadId(this.convert.Id);

        System.Savepoint sp = Database.setSavepoint();
        DML.deferLogs();
        Boolean success = true;

        try {

            //terminates any pending time-based workflow actions
            Lead l = new Lead(
                Id = this.convert.Id,
                Disable_Workflow__c = true
            );
            DML.save(this, l);
            
            Database.LeadConvertResult lcr = Database.convertLead(lc);

            // catch reason why if it failed
            if (!lcr.isSuccess()) {
                return error(lcr.getErrors()[0].getMessage(), sp);
            }
            
            //find the ASP_Trial_amount value of Territory_ID__c, if it is present. 
            Double aspAmount = 0;
            if(this.convert.Territory_ID__c!=null){   
                Seller_Hierarchy__c shc;      
                try{
                    shc = [select id, ASP_Trial__c from Seller_Hierarchy__c where id= :this.convert.Territory_ID__c limit 1];
                    aspAmount = shc.ASP_Trial__c;
                }catch(QueryException qe){
                    System.debug(qe);
                }
                 
            }

            this.newOpportunityId = lcr.getOpportunityId();
            system.debug('Required Trail Id=='+ OpportunityManager.RECTYPE_TRIAL_NEW);
            //map our custom fields
            Opportunity newOppty = new Opportunity(
                Id = this.newOpportunityId,
                Name = this.convert.Company,
                //changes for US2436 started
                //RecordTypeId = OpportunityManager.RECTYPE_TRIAL,
                
                RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW,
                //changes for US2436 ends
                DVD_Received__c = this.convert.DVD_Received__c,
                First_Contacted_Date__c = safeDate(this.convert.First_Contacted_Date__c),
                Lead_Created_Date__c = this.convert.CreatedDate,
                Lead_Educated_Date__c = this.convert.Educated_Date__c,
                Patient__c = this.convert.Patient__c,
                Territory_ID__c = this.convert.Territory_ID__c,
                ASP_Trial_Amount__c = aspAmount,
                //changes for US2436 started
                //Trialing_Account__c = this.convert.Account_Facility__c,
                //Trialing_Account__c = this.convert.Patient__r.Physician_of_Record__r.AccountId,
                Pain_Area__c =  this.convert.Patient__r.Pain_Area__c,
                Primary_Insurance__c = this.convert.Patient__r.Primary_Insurance__c,
                Secondary_Insurance__c = this.convert.Patient__r.Secondary_Insurance__c,
                Opportunity_Type__c = 'Standard',                   
            //changes for US2436 ends
            
            //changes for US2458 started      
                Referral_MD__c = this.convert.Referring_Physician__c,
            //changes for US2458 ends  

                Trialing_Physician__c = this.convert.Trialing_Physician__c,
                CARE_Card_Physician__c = this.convert.CARE_Card_Physician__c,
                Secondary_Source__c = this.convert.Secondary_Source__c
            );
            //update newOppty;
            DML.save(this, newOppty);
            system.debug('Leas conversion oppy  '+newOppty);
            system.debug('Required Trail Id=='+newOppty.RecordTypeId+' required RT'+OpportunityManager.RECTYPE_TRIAL_NEW);
            //do not want the contact that was created by the conversion process
            Contact c = new Contact(
                Id = lcr.getContactId()
            );
            DML.remove(this, c);

            // create share records for associated territory assignees
            new OpportunityManager().opportunitySharing(new List<Opportunity> { newOppty });
            System.debug('Calling Method to Update Opty Line Item Price :'+newOppty);
            new OpportunityManager().updateOptyLineItemPrice(new List<Opportunity> { newOppty });
            System.debug('Called Method to Update Opty Line Item Price :'+newOppty);
            
            //new OpportunityManager().opportunitySharing(new List<Opportunity> { newOppty });
            //added as per Q3 Sprint 1 -Amitabh
           // new OpportunityManager().createClinicalDataSummary(newOppty);
            //new OpportunityManager().commitClinicalDataSummaries();
            

        }
        catch (System.DmlException de) {
            success = error(de.getDmlMessage(0), sp);
        }

        DML.flushLogs(false);

        return success;

    }

    private Date safeDate(DateTime dt) {
        return dt == null ? null : dt.date();
    }

    @testVisible
    private Boolean error(String errMsg, System.Savepoint sp) {
        this.lastErrorMessage = errMsg;
        if (sp != null) {
            Database.rollback(sp);
        }
        return false;
    }

    public class ConvertLeadException extends Exception {}

}