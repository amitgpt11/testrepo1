/**
 * Batch class,  Recalculate sharing rules
 *
 * @author  Salesforce services
 * @date    2014-05-01
 */
global without sharing class NMD_ObjectSharingBatch implements Database.Batchable<SObject>, Schedulable {
    
    //static final List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>();
    
    public String query;

    /*
    * @desc Immediately executes an instance of this batch for the object mentioned
    */
    public static void runNowPatients(){
        Database.executeBatch(new NMD_ObjectSharingBatch(Patient__c.getSObjectType()));
    }

    /*
    * @desc Immediately executes an instance of this batch for the object mentioned
    */
    public static void runNowContacts(){
        Database.executeBatch(new NMD_ObjectSharingBatch(Contact.getSObjectType()));
    }

    /*
    * @desc Immediately executes an instance of this batch for the object mentioned
    */
    public static void runNowLeads(){
        Database.executeBatch(new NMD_ObjectSharingBatch(Lead.getSObjectType()));
    }

    /*
    * @desc Immediately executes an instance of this batch for the object mentioned
    */
    public static void runNowOrders(){
        Database.executeBatch(new NMD_ObjectSharingBatch(Order.getSObjectType()));
    }

    /*
    * @desc Immediately executes an instance of this batch for the object mentioned
    */
    public static void runNowOpportunities(){
        Database.executeBatch(new NMD_ObjectSharingBatch(Opportunity.getSObjectType()));
    }
    
    //Patient Record Types
    static final Set<Id> RECTYPES_NMD_PATIENTS = new Set<Id> {
        PatientManager.RECTYPE_CUSTOMER, PatientManager.RECTYPE_PROSPECT
    };
    
    //Contact Record Types
    static final Set<Id> RECTYPES_NMD_CONTACTS = new Set<Id> {
        ContactManager.RECTYPE_CUSTOMER, ContactManager.RECTYPE_PROSPECT
    };
    
    //Lead Record Types
    static final Set<Id> RECTYPES_NMD_LEAD = new Set<Id> {
        LeadManager.RECTYPE_NMD_PATIENTS
    };

    static final Set<Id> RECTYPES_NMD_ORDERS = new Set<Id> {
        OrderManager.RECTYPE_PATIENT, 
        OrderManager.RECTYPE_PRODUCT, 
        OrderManager.RECTYPE_TRANSFER, 
        OrderManager.RECTYPE_RETURN
    };
    
    //opportunity Record Types
    static final Set<Id> RECTYPES_NMD_OPPORTUNITY = new Set<Id> {
       // OpportunityManager.RECTYPE_TRIAL, 
        OpportunityManager.RECTYPE_TRIAL_NEW, 
        OpportunityManager.RECTYPE_IMPLANT,
        OpportunityManager.RECTYPE_EXPLANT, 
        OpportunityManager.RECTYPE_REVISION,
        OpportunityManager.RECTYPE_REPROGRAMMING
    };
    
    //init record type map
    private static Map<SObjectType,Set<Id>> recTypeMap = new Map<SObjectType,Set<Id>>{
        (Patient__c.getSObjectType()) => RECTYPES_NMD_PATIENTS,
        (Contact.getSObjectType()) => RECTYPES_NMD_CONTACTS,
        (Lead.getSObjectType()) => RECTYPES_NMD_LEAD,
        (Order.getSObjectType()) => RECTYPES_NMD_ORDERS,
        (Opportunity.getSObjectType()) => RECTYPES_NMD_OPPORTUNITY
    };

    private SObjectType objType;
    private Set<Id> objectRecordTypeSet = new Set<Id>();

    global NMD_ObjectSharingBatch(SObjectType objType) {
        this.objType = objType;
        this.objectRecordTypeSet.addAll(recTypeMap.get(objType));
        query = 
            'select ' +
            (objType == Order.getSObjectType() ? 
                'Commissionable_Rep__c, ' +
                'Receiving_Location__c, ' +
                'Shipping_Location__c, '
            : '') +
                'Territory_ID__c, ' +
                'RecordTypeId ' +
            'from ' + objType.getDescribe().getName() + ' ' + 
            'where RecordTypeId in :objectRecordTypeSet';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
            
        return Database.getQueryLocator(this.query);
    }
        /***  @desc Database.Bachable method*/
    global void execute(Database.BatchableContext bc, List<SObject> scope) {

        if (objType == Opportunity.getSObjectType()) {
            new OpportunityManager().opportunitySharing(scope);
        }
        else if (objType == Contact.getSObjectType()) {
            new ContactManager().contactsharing(scope);
        } 
        else if (objType == Patient__c.getSObjectType()) {
            new PatientManager().patientSharing(scope);
        } 
        else if (objType == Lead.getSObjectType()) {
            new LeadManager().leadSharing(scope);
        }
        else if (objType == Order.getSObjectType()) {
            new OrderManager().orderSharing(scope);
        }

    }

    /***  @desc Schedulable method, executes the class instance*/
    global void execute(SchedulableContext context) {
        Database.executeBatch(this);
    }
    global void finish(Database.BatchableContext bc) {
        //if (!logs.isEmpty()) {
        //    Logger.logMessage(logs);
        //}
    }
}