/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ScheduleMirfBatch2 extends openq.ScheduleObjectsScopeBatch {
    global ScheduleMirfBatch2() {

    }
    global ScheduleMirfBatch2(List<Id> userId) {

    }
    global ScheduleMirfBatch2(Boolean doNotUseVisibilityScope) {

    }
    global override String getObjectName() {
        return null;
    }
}
