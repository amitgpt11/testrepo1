/*
 @CreatedDate     08APR2016                                  
 @author          Mayuri-Accenture
 @Description     This is Test class for Batch Apex class'BatchUserTerritoryUpdate' to add User to a territory based on Account_Team_Members__c records. The Account_Team_Members__c records are updated to Salesforce
                  from a batch run by SAP. This batch invokes another batch in its finish() method which updates territory details of Opportunity records.
 
 */



@isTest(SeeAllData=false)
@testVisible
    private class BatchUserTerritoryUpdateTest { 
    public static list<User> usrLst = new list<User>();
    public static list<Personnel_ID__c> pesrnIdlst = new list<Personnel_ID__c>();
    public static list<Account> acctlst = new list<Account>();
    public static list<Account_Team_Member__c> ATMList = new list<Account_Team_Member__c>();
    public static list<My_Objectives__c> MyObjList = new list<My_Objectives__c>();
    public static list<Opportunity> opptyLst = new list<Opportunity>(); 
    public static List<Territory2Type> Ttype;  
    public static Id Territory2ModelIDActive;
    public static List<Territory2> trList=new List<Territory2>();    
    
    @testSetup
    static void setupTerritoryMockData(){
        User userC = UtilForUnitTestDataSetup.newUser('Standard User');
        userC.Username = 'UserNameC1@xyz.com';
        usrLst.add(userC);
        User userR = UtilForUnitTestDataSetup.newUser('Standard User');
        userR.Username = 'UserNameR1@abc.com';
        usrLst.add(userR);
        
        //Inserting 2 Test user record to create Personnel_ID__c 
        insert usrLst;
        
        Ttype = UtilForUnitTestDataSetup.newTerritory2Type();        
        Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
        Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('AA1234',Territory2ModelIDActive,Ttype[0].Id);   
        trList.add(t1);              
        Territory2 t2 = UtilForUnitTestDataSetup.newTerritory2('AA1245',Territory2ModelIDActive,Ttype[0].Id); 
        trList.add(t2); 
        insert trList; 
        System.assertEquals(trList[0].Territory2ModelId,Territory2ModelIDActive);        
        
        
        UserTerritory2Association nAssc = new UserTerritory2Association();
        nAssc.RoleInTerritory2 = 'Territory Manager';
        nAssc.Territory2Id = trList[0].Id;
        nAssc.UserId = usrLst[0].Id;
        insert nAssc;
        
    } 
    
    //Create Account and ObjectTerritory2Association records. We have written 2 methods because setup and non-setup objects cannot be inserted together.
    static void setupCommonMockData(){
        
        list<User> urLst = new list<User>([Select name,Id From User where LastName Like : 'LNA%'  limit 1]);
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'AA%'  ];  
        
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        acctlst.add(acct1);
        Account acct2 = new Account(Name = 'ACCT2NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId());
        acctlst.add(acct2);       
        insert acctlst;
        
        ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
        accountAssociation.ObjectId = acctlst[0].Id;
        accountAssociation.Territory2Id = trLst[0].Id;
        accountAssociation.AssociationCause = 'Territory2Manual';
        insert accountAssociation;
        
        Personnel_ID__c p1 =UtilForUnitTestDataSetup.newPersonnelID(urLst[0].Id);
        pesrnIdlst.add(p1);
        
        Insert pesrnIdlst;
        //Personnel_ID__c person1 = [select Id from Personnel_ID__c where User_for_account_team__c = :urLst[0].Id];
        For(Integer i=0; i<2; i++){
            Account_Team_Member__c a = new Account_Team_Member__c(
                Account_Team_Role__c = 'AccountTeamRole' +i,
                Personnel_ID__c= pesrnIdlst[0].Id,
                Account_Team_Share__c = acctlst[0].Id,
                Deleted__c = False,
                Territory_ID__c = 'AA1234',
                LastModifiedDate = system.today(),
                createdDate = system.today()
                
            );             
            ATMList.add(a);
        } 
        insert ATMList; 
        System.assertEquals(ATMList[0].LastModifiedDate,system.today());          
              
        
    }
        
       
         static testMethod  void testbatchUserTerritoryUpdate(){            
             setupCommonMockData(); 
        Account_Team_Member__c a1 = new Account_Team_Member__c(
                Account_Team_Role__c = 'AccountTeamRole1',
                Personnel_ID__c= pesrnIdlst[0].Id,
                Account_Team_Share__c = acctlst[0].Id,
                Deleted__c = True,
                Territory_ID__c = 'AA1234',
                LastModifiedDate = system.today(),
                createdDate = system.today()
            );
        insert a1;
            
             
            Test.startTest();

            

            ID batchprocessid = Database.executeBatch(new batchUserTerritoryUpdate());

            Test.stopTest();

            System.assert(batchprocessid!=null);

            

        }

    }