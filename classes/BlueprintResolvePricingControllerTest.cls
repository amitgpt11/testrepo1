/**
 * Name : BlueprintResolvePricingControllerTest
 * Author : Accenture - Mayuri
 * Description : Test class used for testing the BlueprintResolvePricingController
 * Date : 20June2016
 */
@isTest
private class BlueprintResolvePricingControllerTest{ 

    static testMethod void testMethod1(){      
      
      // Setup test data
      // This code runs as the system user
      Profile p = [SELECT Id FROM Profile WHERE Name='Watchman Field User']; 
      User u = new User(Alias = 'testUser', Email='testUser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='testUser@unitTest.com');
      
      test.startTest();
      System.runAs(u){
          Account acct1 = new Account(Name = 'ACCT1NAME',Account_Number__c='897654',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId());  
          insert acct1; 
          system.assertEquals(acct1.name,'ACCT1NAME');
          BlueprintResolvedPricing__c rp1 = new BlueprintResolvedPricing__c();
          rp1.Name = 'Watchman';
          rp1.Profile_Names__c = 'Watchman Field User';
          rp1.Tablue_Link__c = 'https://tableau.bsci.com/#/site/SalesReporting/views/Rainmaker/Sales';
          insert rp1;
          
          ApexPages.currentPage().getParameters().put('id', acct1.Id);
          ApexPages.StandardController ctr = new ApexPages.StandardController(acct1);
          BlueprintResolvePricingController controller =  new  BlueprintResolvePricingController(ctr);        
          Test.setCurrentPage(Page.BlueprintResolvePricing);        
          controller.getTablueLinkInfo();
      }
      test.stopTest();
    }    

}