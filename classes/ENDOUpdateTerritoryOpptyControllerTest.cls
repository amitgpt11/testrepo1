/**
 * Name : ENDOUpdateTerritoryOpptyControllerTest
 * Author : Accenture - Mayuri
 * Description : Test class used for testing the ENDOUpdateTerritoryOpptyController
 * Date : 04JUL2016
 */
@isTest
private class ENDOUpdateTerritoryOpptyControllerTest{ 
     @testSetup
    static void setupTerritoryMockData(){
        BatchAccountTerritoryUpdateTest.setupTerritoryMockData();
        Territory2 trLst = [Select name From Territory2 where name Like : 'AA%' LIMIT 1 ];  
        
    }
    
    static testMethod void  testSave_Scenario1(){          
        
        Territory2 trLst = [Select name From Territory2 where name Like : 'AA%' LIMIT 1 ];  
        
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        insert acct1;        
        
        ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
        accountAssociation.ObjectId = acct1.Id;
        accountAssociation.Territory2Id = trLst.Id;
        accountAssociation.AssociationCause = 'Territory2Manual';
        insert accountAssociation;            
         TerritoriesForPlum__c plm = new TerritoriesForPlum__c();
         plm.Name = 'PLUM';
         plm.Plum__c = '500';
         insert plm;
         TerritoriesForGI__c gi = new TerritoriesForGI__c();
         gi.Name = 'GI';
         gi.GI__C = '900';
         insert gi;
         Opportunity oppty = new Opportunity();
         oppty.Name = 'test oppty';
         oppty.AccountId = acct1.Id;
         oppty.CloseDate = system.today()+30;
         oppty.StageName = 'Target Prospect';
         insert oppty;
          
        ApexPages.currentPage().getParameters().put('Id', oppty.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(oppty);
        ENDOUpdateTerritoryOpptyController opptyController=  new  ENDOUpdateTerritoryOpptyController(ctr);
        opptyController.terrSelected = trLst.Id;
        opptyController.GIsearchString = 'AA';
        
        Test.startTest();
        Test.setCurrentPage(Page.ENDOUpdateTerritoryOnOppty);       
        
            
            opptyController.GIsearch();           
            opptyController.getTypes();              
            opptyController.dummyAction();
            opptyController.getSelectedType();
            opptyController.setSelectedType('GI Territory');
            opptyController.saveOppty();
            system.assertEquals(oppty.Territory2Id,trLst.Id);
            opptyController.redirectToPage();
            
            Test.stopTest();
           
    }
    static testMethod void  testSave_Scenario2(){ 
        Territory2 trLst = [Select name From Territory2 where name Like : 'AA%' LIMIT 1 ];  
         Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        insert acct1;
        TerritoriesForPlum__c plm = new TerritoriesForPlum__c();
         plm.Name = 'PLUM';
         plm.Plum__c = '500';
         insert plm;
         TerritoriesForGI__c gi = new TerritoriesForGI__c();
         gi.Name = 'GI';
         gi.GI__C = '900';
         insert gi;
        Opportunity oppty = new Opportunity();
         oppty.Name = 'test oppty';
         oppty.AccountId = acct1.Id;
         oppty.CloseDate = system.today()+30;
         oppty.StageName = 'Target Prospect';         
         insert oppty; 
         
        ApexPages.currentPage().getParameters().put('Id', oppty.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(oppty);
        ENDOUpdateTerritoryOpptyController opptyController=  new  ENDOUpdateTerritoryOpptyController(ctr);
        opptyController.terrSelected = trLst.Id;
        opptyController.PULMsearchString = 'AA';
        
        Test.startTest();
        Test.setCurrentPage(Page.ENDOUpdateTerritoryOnOppty);       
        
            
            opptyController.PULMsearch();           
            opptyController.getTypes();              
            opptyController.dummyAction();
            opptyController.getSelectedType();
            opptyController.setSelectedType('PULM Territory');
            opptyController.saveOppty();
            system.assertEquals(oppty.ENDO_Secondary_Territory__c,trLst.name);
            opptyController.redirectToPage();
            
            Test.stopTest();
    }

}