/*
@CreatedDate     29JUNE2016
@author          Mike Lankfer
@Description     Contains test coverage for ImplantTrigger, ImplantTriggerHandler, and ImplantMatching classes/trigger
@Requirement Id  User Story : DRAFT US2484
*/

@isTest
public class ImplantMatchingTest {
    
    private static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Contact.getRecordTypeInfosByName();    
    private static final Id RECTYPE_WATCHMAN_CONTACT = RECTYPES.get('Watchman General').getRecordTypeId();
    
    private static Account primaryAccount = new Account();
    private static Account referralAccount = new Account();
    private static String primaryAccountNumber = '123456';
    private static String refferalAccountName = 'Watchman Referring Physicians';
    private static Contact implantingPhysician = new Contact();
    private static Contact secondaryImplantingPhysician = new Contact();
    private static Contact referringPhysician = new Contact();
    private static String primaryPhysicianWMUId = '123';
    private static String secondaryPhysicianWMUId = '456';
    private static String referringPhysicianFirstName = 'Referring';
    private static String referringPhysicianLastName = 'Physician';    
    private static String referringPhysicianFullName = referringPhysicianFirstName + ' ' +referringPhysicianLastName;
    private static Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name LIKE '%Watchman%' LIMIT 1];
    private static List<User> testUserList = new List<User>();
    private static Shared_Patient_Case__c primaryPatientCase = new Shared_Patient_Case__c();
    private static Shared_Patient_Case__c fosterParentPatientCase = new Shared_Patient_Case__c();
    private static Datetime primaryPatientCaseStartDateTime = Datetime.now();
    private static Datetime fosterParentStartDateTime = Datetime.newInstance(2000, 01, 01, 00, 00, 00);
    private static LAAC_Implant__c primaryImplant = new LAAC_Implant__c();
    private static LAAC_Implant__c fosterChildImplant = new LAAC_Implant__c();
    private static LAAC_Implant__c updatedPrimaryImplant = new LAAC_Implant__c();
    private static LAAC_Implant__c updatedFosterChildImplant = new LAAC_Implant__c();
    
    static testmethod void createAndMatchImplant(){
        Test.startTest();
        
        //create test accounts
        primaryAccount = TestDataFactory.createTestAccount();
        referralAccount = TestDataFactory.createTestAccount();
        
        referralAccount.Name =  refferalAccountName;
        primaryAccount.Account_Number__c = primaryAccountNumber;
        
        insert primaryAccount;
        insert referralAccount;
        
        //create test contacts
        implantingPhysician = TestDataFactory.createTestContact();
        secondaryImplantingPhysician = TestDataFactory.createTestContact();
        referringPhysician = TestDataFactory.createTestContact();
        
        implantingPhysician.AccountId = primaryAccount.Id;
        secondaryImplantingPhysician.AccountId = primaryAccount.Id;
        referringPhysician.AccountId = referralAccount.Id;
        implantingPhysician.LAAC_WMU_User_Id__c = primaryPhysicianWMUId;
        secondaryImplantingPhysician.LAAC_WMU_User_Id__c = secondaryPhysicianWMUId;
        referringPhysician.FirstName = referringPhysicianFirstName;
        referringPhysician.LastName = referringPhysicianLastName;
        implantingPhysician.RecordTypeId = RECTYPE_WATCHMAN_CONTACT;
        secondaryImplantingPhysician.RecordTypeId = RECTYPE_WATCHMAN_CONTACT;
        referringPhysician.RecordTypeId = RECTYPE_WATCHMAN_CONTACT;
        
        insert implantingPhysician;
        insert secondaryImplantingPhysician;
        insert referringPhysician;
        
        //create test user
        testUserList = TestDataFactory.createTestUsers(1, testProfile);
        
        insert testUserList.get(0);
        
        //create test patient cases
        primaryPatientCase = TestDataFactory.createTestPatientCase();
        fosterParentPatientCase = TestDataFactory.createTestPatientCase();
        
        primaryPatientCase.Shared_Start_Date_Time__c = primaryPatientCaseStartDateTime;
        primaryPatientCase.Shared_End_Date_Time__c = primaryPatientCaseStartDateTime.addHours(1);
        primaryPatientCase.Shared_Facility__c = primaryAccount.Id;
        fosterParentPatientCase.Shared_Start_Date_Time__c = fosterParentStartDateTime;
        fosterParentPatientCase.Shared_End_Date_Time__c = fosterParentStartDateTime.addHours(1);
        fosterParentPatientCase.Shared_Facility__c = primaryAccount.Id;
        primaryPatientCase.LAAC_Lead_WCS__c = testUserList.get(0).Id;
        
        insert primaryPatientCase;
        insert fosterParentPatientCase;
        
        //create test implant records
        primaryImplant = TestDataFactory.createTestImplant();
        fosterChildImplant = TestDataFactory.createTestImplant();
        
        primaryImplant.LAAC_Implanting_Physician_User_ID__c = primaryPhysicianWMUId;
        primaryImplant.LAAC_Secondary_Implanting_Physician_Id__c = secondaryPhysicianWMUId;
        primaryImplant.LAAC_Referring_Physician__c = referringPhysicianFullName;
        primaryImplant.LAAC_Account_Number__c = primaryAccountNumber;
        primaryImplant.LAAC_Date_of_Procedure__c = primaryPatientCaseStartDateTime.date();
        primaryImplant.LAAC_WATCHMAN_User__c = testUserList.get(0).FirstName + ' ' + testUserList.get(0).LastName;
        
        insert primaryImplant;
        insert fosterChildImplant;
        
        //query updated implant records
        updatedPrimaryImplant = [SELECT Id, LAAC_Facility__c, LAAC_Implant_Patient_Case__c, LAAC_Implanting_Physician__c, LAAC_Secondary_Implanting_Physician_LU__c, LAAC_Lead_WCS__c, LAAC_Referring_Physician_LU__c FROM LAAC_Implant__c WHERE Id = :PrimaryImplant.Id];
        updatedFosterChildImplant = [SELECT Id, LAAC_Implant_Patient_Case__c FROM LAAC_Implant__c WHERE Id = :fosterChildImplant.Id];
        
        Test.stopTest();
        
        System.assertEquals(primaryAccount.Id, updatedPrimaryImplant.LAAC_Facility__c);
        System.assertEquals(primaryPatientCase.Id, updatedPrimaryImplant.LAAC_Implant_Patient_Case__c);
        System.assertEquals(implantingPhysician.Id, updatedPrimaryImplant.LAAC_Implanting_Physician__c);
        System.assertEquals(secondaryImplantingPhysician.Id, updatedPrimaryImplant.LAAC_Secondary_Implanting_Physician_LU__c);
        System.assertEquals(testUserList.get(0).Id, updatedPrimaryImplant.LAAC_Lead_WCS__c);
        System.assertEquals(referringPhysician.Id, updatedPrimaryImplant.LAAC_Referring_Physician_LU__c); 
        System.assertEquals(fosterParentPatientCase.Id, updatedFosterChildImplant.LAAC_Implant_Patient_Case__c); 
    }
}