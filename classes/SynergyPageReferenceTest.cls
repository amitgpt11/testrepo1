/**
* Test class
*
* @Author salesforce Services
* @Date 2015-07-15
*/
@IsTest(SeeAllData=false)
private class SynergyPageReferenceTest {
	
	final static String BSCISYNERGY = 'bscisynergy';

	@TestSetup
	static void setup() {

		insert new NMD_Inventory_Settings__c(
			SetupOwnerId = UserInfo.getOrganizationId(),
			URI_Scheme__c = BSCISYNERGY
		);

	}

	static testMethod void test_SynergyPageReference() {

		String externalPath = '/external/path';

		SynergyPageReference spr = new SynergyPageReference(externalPath);
		spr.parameters.put('x', 'foo');

		PageReference pr = spr.getPageReference();

		System.assert(pr.getUrl().startsWith(BSCISYNERGY));
		System.assert(pr.getUrl().contains(externalPath));
		System.assert(pr.getParameters().containsKey('x'));

		pr = spr.getRedirectPageReference();	

	}

}