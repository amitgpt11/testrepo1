/*
 @CreatedDate     31MAR2016                                  
 @ModifiedDate    011APR2016                                  
 @author          Narendra-Accenture
 @Description     OrderCreationController class create Loaner kit Order and Order Items (Products). 
 @Methods         save(),getSAPShippingAddress(),getLoanerKitItems() 
 @Requirement     Shared_Patient_Case__c Id  
 */
public with sharing class OrderCreationController{
    public String patientCaseID{get;set;}
    Public String SelectedItem{get;set;}
    Public ID patientCaseFacilityID{get;set;}
    Public List<Shared_Patient_Case__c> patientCaseList = new List<Shared_Patient_Case__c>();
    public List<Uro_PH_Loaner_Kit_Assignment__c> LKList {get;set;}
    public List<Uro_PH_Loaner_Kit_Relationship__c > SKUList{get;set;}
    Public String poNumber{get;set;}
    Public String ShippingAddress{get;set;}
    Public String ShippingAddressID{get;set;}
    Public Id accountSAP{get;set;}
    Public Id Alternate_BSCI_Resouce {get;set;}
    public List<Contact> shippingContactListSAP{get;set;}
    public list<wrapperLoanerKitRel> wrapperLoanerKitRelList{get;set;}
    
    public OrderCreationController(ApexPages.StandardController controller) {
    
        wrapperLoanerKitRelList = new list<wrapperLoanerKitRel>();
        
        //Getting patientCaseID from url. this ID is must.
        patientCaseID =  ApexPages.currentPage().getParameters().get('Id');
        patientCaseList =[Select Id,Shared_Facility__c,Uro_PH_Alternate_BSCI_Resource__c ,Shared_Implanting_Physician__c from Shared_Patient_Case__c Where Id =:patientCaseID];
        
        if(patientCaseList.Size()>0 ){
            patientCaseFacilityID =   patientCaseList.get(0).Shared_Facility__c;//Account for Patient Case object.
            Alternate_BSCI_Resouce = patientCaseList.get(0).Uro_PH_Alternate_BSCI_Resource__c ; 
        }
        
        LKList =  [SELECT Id,Name,Uro_PH_Facility_Name__c,Uro_PH_Loaner_Kit__c,Uro_PH_Loaner_Kit__r.Name FROM Uro_PH_Loaner_Kit_Assignment__c Where Uro_PH_Facility_Name__c =:patientCaseFacilityID ];
        
        //Shipping address will come from SAP. So need update query and Obejct list accordingly.
        shippingContactListSAP = [SELECT Id, MailingAddress,MailingCity,MailingCountry,MailingGeocodeAccuracy,MailingPostalCode,MailingState,MailingStreet FROM Contact WHERE LastName LIKE 'Leaner Kit%'];
    }//End of constructor
    
    //LoanerKitAssignments method will rerender Uro_PH_Loaner_Kit_Relationship__c records on the basis of Selected Loaner kit in UI.
    public PageReference LoanerKitAssignments() {
        
        getShippingAddress(); //Added by Ashish on 11th April, 2016
        SKUList = [SELECT Id,Name,Uro_PH_Loaner_Kit__c,Uro_PH_Product__c,Uro_PH_Quantity__c FROM Uro_PH_Loaner_Kit_Relationship__c WHERE Uro_PH_Loaner_Kit__c=:SelectedItem];
        
        wrapperLoanerKitRelList = new list<wrapperLoanerKitRel>();
        wrapperLoanerKitRel wrapperLoanerKitRelObj;
        for(Uro_PH_Loaner_Kit_Relationship__c lk : SKUList){
            wrapperLoanerKitRelObj = new wrapperLoanerKitRel();  
            wrapperLoanerKitRelObj.LKRobj = lk;
            wrapperLoanerKitRelObj.lkQuantity = lk.Uro_PH_Quantity__c;   
            wrapperLoanerKitRelList.add(wrapperLoanerKitRelObj);   
        }
        
        return null;
    }

    
    //Create's Order and OrderItem record.
    public PageReference save(){
        
        List<Contact> shippingContact = [SELECT Id, MailingAddress,MailingCity,MailingCountry,MailingGeocodeAccuracy,MailingPostalCode,MailingState,MailingStreet FROM Contact WHERE Id =:ShippingAddressID limit 1];
        Map <String,Schema.RecordTypeInfo> recordTypes_Order = Order.sObjectType.getDescribe().getRecordTypeInfosByName();
        Id recordTypeID_Order = recordTypes_Order.get('Uro/PH Loaner Kit').getRecordTypeId();
        List<PriceBook2> priceBookList = [Select Id from PriceBook2 Where Name ='UroPH' limit 1];
        
        Order tempOrder = new Order();
        tempOrder.AccountId = patientCaseFacilityID;
        tempOrder.Uro_PH_Patient_Case__c = patientCaseID;
        tempOrder.EffectiveDate = System.Today();
        tempOrder.Status = 'Draft';
        tempOrder.RecordTypeId = recordTypeID_Order; //'012P00000000ShC'; //UroPh Record Type
        tempOrder.PriceBook2Id = priceBookList[0].Id; //'01sP00000004fJyIAI'; //UroPH Pricebook
        tempOrder.PoNumber = poNumber;
        tempOrder.Surgeon__c = patientCaseList[0].Shared_Implanting_Physician__c;
        tempOrder.ShippingStreet = shippingContact[0].MailingStreet;
        tempOrder.ShippingCity = shippingContact[0].MailingCity;
        tempOrder.ShippingPostalCode = shippingContact[0].MailingPostalCode;
        tempOrder.ShippingState = shippingContact[0].MailingState;
        tempOrder.ShippingCountry = shippingContact[0].MailingCountry;
        tempOrder.Uro_PH_Alternate_BSCI_Resource__C = Alternate_BSCI_Resouce;
        
        try{
            insert tempOrder;
        }catch(DMLException ex){
            System.debug('Exception : '+ex.getMessage());
        }

        List<ID> productIdList = new List<ID>();
        for(Uro_PH_Loaner_Kit_Relationship__c  t : SKUList){
            productIdList.add(t.Uro_PH_Product__c);
        }
        
        List<PriceBookEntry> priceBookEntry2Products = [SELECT Id,Name,UnitPrice,CurrencyIsoCode,Pricebook2Id,Product2Id FROM PricebookEntry WHERE Product2Id IN :productIdList AND PriceBook2Id =: priceBookList[0].Id];
        Map<ID,ID> product2PriceBookEntryMap = new Map<ID,ID>();
        Map<ID,Decimal> product2UnitPriceMap = new Map<ID,Decimal>();
        List<User> currentUser = [Select Id,CurrencyIsoCode from User Where Id=:UserInfo.getUserId() ];
        for(PriceBookEntry pb : priceBookEntry2Products){
            if(pb.CurrencyIsoCode == currentUser[0].currencyIsoCode){
                product2PriceBookEntryMap.put(pb.Product2Id,pb.Id);
                product2UnitPriceMap.put(pb.Product2Id,pb.UnitPrice); 
            }
        }
        
        List<OrderItem> orderItemList = new List<OrderItem>();
        System.debug('SKUList : '+SKUList);
        for(wrapperLoanerKitRel lk : wrapperLoanerKitRelList){ 
            Uro_PH_Loaner_Kit_Relationship__c  t = lk.LKRobj;
            if(lk.lkQuantity >0){
                OrderItem ot = new OrderItem();
                try{        
                    ot.UnitPrice = (product2UnitPriceMap.get(t.Uro_PH_Product__c) == null ? 1 :product2UnitPriceMap.get(t.Uro_PH_Product__c));
                    ot.Quantity = (lk.lkQuantity > 3 ? 3 : lk.lkQuantity);
                    ot.PricebookEntryId = product2PriceBookEntryMap.get(t.Uro_PH_Product__c);
                    ot.OrderId = tempOrder.Id;
                    
                }catch(Exception ex){
                    
                    System.debug('Exception : '+ex.getMessage());
                    return new PageReference('/' + tempOrder.Id);
                }
                System.debug('OT '+ot);
                orderItemList.add(ot); 
                
            } //End of if
        } //End of Loop
        
        try{ 
            
            insert orderItemList;
        }catch(DMLException ex){
            System.debug('Exception :'+ex.getMessage());
            return new PageReference('/' + tempOrder.Id);
        }
        return new PageReference('/' + tempOrder.Id); //Redirecting to created order detail page.
    }//End of Method 
    
    public List<SelectOption> getLoanerKitItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for(Uro_PH_Loaner_Kit_Assignment__c t : LKList){
            options.add(new SelectOption(t.Uro_PH_Loaner_Kit__c , t.Uro_PH_Loaner_Kit__r.Name));
        }
        return options;
    }//End of Method
    
    public List<SelectOption> getSAPShippingAddress(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Select Shipping Address')); 
        for(Contact c : shippingContactListSAP){
            options.add(new SelectOption(c.Id, c.MailingStreet+', '+c.MailingCity+' '+c.MailingPostalCode+', '+c.MailingState+', '+c.MailingCountry));
        }
        return options;
    }//End of Method
    
    public String getShippingAddress(){
        for(Contact c : shippingContactListSAP){
            if(c.Id == ShippingAddressID){
                ShippingAddress = c.MailingStreet+', '+c.MailingCity+' '+c.MailingPostalCode+', '+c.MailingState+', '+c.MailingCountry;
            }
        }
        return ShippingAddress;
    }
    
    public class wrapperLoanerKitRel{
        public Uro_PH_Loaner_Kit_Relationship__c LKRobj{get;set;}
        public decimal lkQuantity{get;set;}
    }
    
}//End of Class