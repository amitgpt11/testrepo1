/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class ContactListControllerTest {
	
	static testMethod void test_ContactListController() {
		NMD_TestDataManager td = new NMD_TestDataManager();
	
		Account acct = td.createConsignmentAccount();
		insert acct;

		PageReference pr = Page.ContactList;
		pr.getParameters().put('id', acct.Id);
		Test.setCurrentPage(pr);

		ContactListController con = new ContactListController(
			new ApexPages.StandardController(acct)
		);

		List<Contact> contacts = con.contacts;

	}

}