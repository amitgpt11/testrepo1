/**
* Batch class, Change Object Owner Based on Territory Id 
*
* @author   Salesforce services
* @date     2014-04-03
*/
global without sharing class NMD_ObjectOwnerByTerritoryBatch implements Database.Batchable<sObject> {

    //init record type map
    final static Map<SObjectType,Set<Id>> RECTYPES = new Map<SObjectType,Set<Id>> {
        (Patient__c.getSObjectType()) => PatientManager.RECTYPES_NMD,
        (Contact.getSObjectType()) => ContactManager.RECTYPES_NMD,
        (Lead.getSObjectType()) => LeadManager.RECTYPES_NMD,
        (Opportunity.getSObjectType()) => OpportunityManager.RECTYPES_NMD_ALL,
        (Order.getSObjectType()) => new Set<Id> { OrderManager.RECTYPE_PATIENT } // ONLY patient order types
    };

    /*
    * @desc Immediately executes an instance of this batch for the object mentioned
    */
    public static Id runNowPatients() {
        return runNow(Patient__c.getSObjectType());
    }

    /*
    * @desc Immediately executes an instance of this batch for the object mentioned
    */
    public static Id runNowContacts() {
        return runNow(Contact.getSObjectType());
    }

    /*
    * @desc Immediately executes an instance of this batch for the object mentioned
    */
    public static Id runNowLeads() {
        return runNow(Lead.getSObjectType());
    }

    /*
    * @desc Immediately executes an instance of this batch for the object mentioned
    */
    public static Id runNowOpportunities() {
        return runNow(Opportunity.getSObjectType());
    }

    /*
    * @desc Immediately executes an instance of this batch for the object mentioned
    */
    public static Id runNowOrders() {
        return runNow(Order.getSObjectType());
    }

    public static Id runNow(SObjectType objType) {
        return Database.executeBatch(new NMD_ObjectOwnerByTerritoryBatch(objType));
    }
    
    public String query;

    private Set<Id> objRecTypeIds;
    private SObjectType objType;
    
    global NMD_ObjectOwnerByTerritoryBatch(SObjectType objType) {

        String objName = objType.getDescribe().getName();

        this.objType = objType;
        this.objRecTypeIds = RECTYPES.get(objType);
        //dynamic query
        this.query = 
            'select ' +
                (this.objType == Order.getSObjectType() 
                    ? 'Opportunity.Territory_ID__c, ' + 
                      'Opportunity.OwnerId, ' +
                      'Shipping_Location__c, ' +
                      'Receiving_Location__c, '
                    : ''
                ) +
                'OwnerId, ' +
                'RecordTypeId, ' +
                'Territory_ID__c, ' +
                'Territory_ID__r.Current_TM1_Assignee__c ' + 
            'from '+ objName + ' ' +
            'where Mismatched_Owner__c = true ' +
            'and RecordTypeId in :objRecTypeIds '; 

        if (Test.isRunningTest()) {
            this.query += 'order by CreatedDate desc limit 10';
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(this.query);
    }

    /**
    *  @desc    Database.Bachable method
    */
    global void execute(Database.BatchableContext bc, List<SObject> scope) {

        Boolean isOrderType = this.objType == Order.getSObjectType();

        //update Object         
        for (SObject obj : scope) {  
            if (isOrderType) {
                // orders are updated to match their parent opportunity
                Order ord = (Order)obj;
                ord.Territory_ID__c = ord.Opportunity.Territory_ID__c;
                ord.OwnerId = ord.Opportunity.OwnerId;
            }
            else {
                // update the record owner
                obj.put('OwnerId', SObjectManager.getField(obj, 'Territory_ID__r.Current_TM1_Assignee__c'));
            }
        }

        //update Object Persist
        //List<ApplicationLogWrapper> logs = new List<ApplicationLogWrapper>();
        //List<Database.SaveResult> objectSaveResults = Database.update(scope, false);
        List<SObject> updated = new List<SObject>();
        Map<Id,SObject> objMap = new Map<Id,SObject>(scope);
        for (Database.SaveResult result : DML.save(this, scope, false)) {
            //if (!result.isSuccess()) {
            //    for (Database.Error err : result.getErrors()) {
            //        logs.add(new ApplicationLogWrapper(err, result.getId(), err.getMessage()));
            //    }
            //}
            //else {
            if (result.isSuccess()) {
                updated.add(objMap.get(result.getId()));
            }
        } 
        //if (!logs.isEmpty()) {
        //    Logger.logMessage(logs);
        //}
        
        //recalculate object Sharing
        if (this.objType == Contact.getSObjectType()) {
            new ContactManager().contactSharing(updated);
        }
        else if (this.objType == Lead.getSObjectType()) {
            new LeadManager().leadSharing(updated);
        }
        else if (this.objType == Patient__c.getSObjectType()) {
            new PatientManager().patientSharing(updated);
        }
        else if (this.objType == Opportunity.getSObjectType()) {
            new OpportunityManager().opportunitySharing(updated);
        }
        else if (this.objType == Order.getSObjectType()) {
            new OrderManager().orderSharing(updated);
        }

    }

    /**
    *  @desc    Schedulable method, executes the class instance
    */
    /*global void execute(SchedulableContext context) {
        Database.executeBatch(this);
    }*/
    
    global void finish(Database.BatchableContext bc) {
        NMD_SObjectSharingByTerritoryBatch.runNow(this.objType);

    }

}