/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class NMD_PatientSharingExtensionTest {

	static final String PATIENTNAME = 'PatientName';
	
	@testSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		Account acct = td.createConsignmentAccount();
		insert acct;

		Patient__c patient = td.newPatient(PATIENTNAME, PATIENTNAME);
		insert patient;

		Opportunity oppty = td.newOpportunity(acct.Id);
		oppty.Patient__c = patient.Id;
		insert oppty;

		Lead lead = td.newLead();
		lead.Patient__c = patient.Id;
		insert lead;

		User user = td.newUser();
		insert user;

	}

	static testMethod void test_PatientSharing() {
		
		Patient__c patient = [select Id from Patient__c where Patient_Last_Name__c = :PATIENTNAME];
		User otherUser = [select Id from User where Id != :UserInfo.getUserId() and IsActive = true limit 1];

		PageReference pr = Page.PatientSharing;
		pr.getParameters().put('id', patient.Id);
		Test.setCurrentPage(pr);

		NMD_PatientSharingExtension ext = new NMD_PatientSharingExtension(new ApexPages.StandardController(patient));

		patient.User__c = otherUser.Id;

		ext.submit();

	}

	static testMethod void test_PatientSharingSF1() {
		
		Patient__c patient = [select Id from Patient__c where Patient_Last_Name__c = :PATIENTNAME];
		User otherUser = [select Id from User where Id != :UserInfo.getUserId() and IsActive = true limit 1];

		List<Map<String,String>> matches = NMD_PatientSharingExtension.matchUsers(patient.Id, UserInfo.getFirstName());
		NMD_PatientSharingExtension.submitSF1(patient.Id, otherUser.Id);

	}

}