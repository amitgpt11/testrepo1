/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DocuSignRecepientTest {

    @testSetup
    static void setup() {
    
	    Id ProfileId = [Select Id 
	        From Profile 
	        Where Name = 'EU Sales Rep' 
	        Limit 1].Id;
        
	    User euUser = new User(Alias = 'docuser', Email='docueuser@testorg.com', 
	        EmailEncodingKey='UTF-8', LastName='TestDocuUser', LanguageLocaleKey='en_US', 
	        LocaleSidKey='en_US', ProfileId = ProfileId, 
	        TimeZoneSidKey='America/Los_Angeles', UserName='docueuser@testorg.com');
	    
    	User sysAdmin= UtilForUnitTestDataSetup.getUser();
        
        System.runAs(sysAdmin){
        	insert euUser;
        }
        
        System.runAs(euUser){
        	NMD_TestDataManager td = new NMD_TestDataManager();
        
        	Account acctMaster = td.createConsignmentAccount();
        	acctMaster.Name = 'Test Acc';
        

        	insert new List<Account> {
            	acctMaster
        	};
        	Contact con = td.newContact(acctMaster.Id);
            insert con;
        
            dsfs__DocuSign_Status__c dStatus = new dsfs__DocuSign_Status__c();
            dStatus.dsfs__Company__c = acctMaster.Id;
            insert dStatus;
        
            dsfs__DocuSign_Recipient_Status__c recStatus = new dsfs__DocuSign_Recipient_Status__c(dsfs__Parent_Status_Record__c = dStatus.Id,dsfs__Contact__c = con.Id, dsfs__DocuSign_Recipient_Id__c = 'DF351F8A-EB1F-4004-89AB-BC5E18D1BB5E');
            insert recStatus;
        }
    }
    
    
    static testMethod void testStatusCompleted() {
        dsfs__DocuSign_Recipient_Status__c recStatus = [SELECT Id,dsfs__Contact__c FROM dsfs__DocuSign_Recipient_Status__c LIMIT 1];
        recStatus.dsfs__Recipient_Status__c = 'Completed';
        
        User euUser = [SELECT Id FROM User WHERE UserName='docueuser@testorg.com' LIMIT 1];
        
        Test.startTest();
            System.runAs(euUser){
                update recStatus;
            }
        Test.stopTest();
        
        Contact con = [SELECT Id,Remote_Consent__c FROM Contact WHERE Id = :recStatus.dsfs__Contact__c LIMIT 1];
        System.assertEquals('Opt in',con.Remote_Consent__c);
    }
    
    
    static testMethod void testStatusDeclined() {
        dsfs__DocuSign_Recipient_Status__c recStatus = [SELECT Id,dsfs__Contact__c FROM dsfs__DocuSign_Recipient_Status__c LIMIT 1];
        recStatus.dsfs__Recipient_Status__c = 'Declined';
        User euUser = [SELECT Id FROM User WHERE UserName='docueuser@testorg.com' LIMIT 1];
        
         Test.startTest();
            System.runAs(euUser){
                update recStatus;
            }
        Test.stopTest();
        
        Contact con = [SELECT Id,Remote_Consent__c FROM Contact WHERE Id = :recStatus.dsfs__Contact__c LIMIT 1];
        System.assertEquals('Opt out',con.Remote_Consent__c);
    }
}