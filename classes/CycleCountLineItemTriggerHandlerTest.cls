/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@IsTest(SeeAllData=false)
private class CycleCountLineItemTriggerHandlerTest {

	final static String LOT1 = 'LOT1';
	final static String MODEL123 = 'MODEL123';
	final static String SERIAL1 = 'SERIAL1';
	final static String SERIAL2 = 'SERIAL2';
	final static String CCRANAME = 'CCRANAME';
	final static String SURGERYNAME = 'SURGERYNAME';
	final static Date EXPIRATION = System.today().addDays(10);

	@TestSetup
	static void setup() {

		NMD_TestDataManager td = new NMD_TestDataManager();

		// product
		Product2 prod = td.newProduct('Trial');
		prod.Model_Number__c = MODEL123;
		prod.EAN_UPN__c = MODEL123;
		insert prod;

		// account
		Account acct = td.createConsignmentAccount();
		insert acct;

		// inventory tree
		Inventory_Data__c invData = new Inventory_Data__c(
			Account__c = acct.Id,
			Customer_Class__c = 'YY'
		);
		insert invData;

		Inventory_Location__c invLoc = new Inventory_Location__c(
			Inventory_Data_ID__c = invData.Id
		);
		insert invLoc;

		Inventory_Item__c invItem = new Inventory_Item__c(
			Inventory_Location__c = invLoc.Id,
			Model_Number__c = prod.Id,
			Serial_Number__c = SERIAL1,
			Lot_Number__c = LOT1,
			Expiration_Date__c = EXPIRATION,
			SAP_Quantity__c = 1
		);
		insert invItem;

		Inventory_Transaction__c invTrans = new Inventory_Transaction__c(
			Inventory_Item__c = invItem.Id,
			Disposition__c = 'In Transit',
			Quantity__c = 1
		);
		insert invTrans;

		invItem.SAP_Calculated_Quantity__c = 1;
		update invItem;

		// patient
		Patient__c patient = td.newPatient();
		insert patient;

		// surgery
		Surgery__c surgery = new Surgery__c(
			Name = SURGERYNAME,
			Patient__c = patient.Id,
			Model_Number__c = prod.Id,
			Serial_Number__c = SERIAL2,
			Lot_Number__c = LOT1,
			Status__c = 'I'
		);
		insert surgery;

		// cycle count tree
		Cycle_Count_Response_Analysis__c ccra = new Cycle_Count_Response_Analysis__c(
			Name = CCRANAME,
			Status__c = 'Scanning'
		);
		insert ccra;

	}

	static testMethod void test_CycleCountLineItem_NoMatching() {

		Cycle_Count_Response_Analysis__c ccra = [select Id from Cycle_Count_Response_Analysis__c where Name = :CCRANAME];
		Inventory_Item__c invItem = [select Id from Inventory_Item__c where Serial_Number__c = :SERIAL1];

		Cycle_Count_Line_Item__c item = new Cycle_Count_Line_Item__c(
			Cycle_Count_Response_Analysis__c = ccra.Id,
			Inventory_Item__c = invItem.Id,
			Scanned_Lot_Number__c = LOT1,
			Scanned_Model_Number__c = MODEL123
		);
		insert item;

		item = [select Other_Inventory_Item__c, Surgery_Item__c from Cycle_Count_Line_Item__c where Id = :item.Id];
		System.assertEquals(null, item.Other_Inventory_Item__c);
		System.assertEquals(null, item.Surgery_Item__c);

	}

	static testMethod void test_CycleCountLineItem_MatchingInvItem() {

		Cycle_Count_Response_Analysis__c ccra = [select Id from Cycle_Count_Response_Analysis__c where Name = :CCRANAME];
		Inventory_Item__c invItem = [select Id from Inventory_Item__c where Inventory_Location__r.Inventory_Data_ID__r.OwnerId = :UserInfo.getUserId()];

		Cycle_Count_Line_Item__c item0 = new Cycle_Count_Line_Item__c(
			Cycle_Count_Response_Analysis__c = ccra.Id,
			Scanned_Lot_Number__c = LOT1,
			Scanned_Model_Number__c = MODEL123,
			Scanned_Serial_Number__c = SERIAL1,
			Exception_Reason__c = 'Surplus',
			Scanned_Quantity__c = 1
		);
		Cycle_Count_Line_Item__c item1 = new Cycle_Count_Line_Item__c(
			Cycle_Count_Response_Analysis__c = ccra.Id,
			Scanned_Lot_Number__c = LOT1,
			Scanned_Model_Number__c = MODEL123,
			Exception_Reason__c = 'Surplus',
			Scanned_Quantity__c = 2
		);
		Cycle_Count_Line_Item__c item2 = new Cycle_Count_Line_Item__c(
			Cycle_Count_Response_Analysis__c = ccra.Id,
			Scanned_Model_Number__c = MODEL123,
			Scanned_Serial_Number__c = SERIAL1,
			Exception_Reason__c = 'Surplus',
			Scanned_Quantity__c = 1
		);
		insert new List<Cycle_Count_Line_Item__c> { item0, item1, item2 };

		CycleCountLineItemManager.createVerifications(ccra.Id);

		Cycle_Count_Line_Item__c item = [select Other_Inventory_Item__c, Surgery_Item__c from Cycle_Count_Line_Item__c where Id = :item0.Id];
		System.assertEquals(invItem.Id, item.Other_Inventory_Item__c);
		System.assertEquals(null, item.Surgery_Item__c);

	}

	static testMethod void test_CycleCountLineItem_MatchingSurgery() {

		Cycle_Count_Response_Analysis__c ccra = [select Id from Cycle_Count_Response_Analysis__c where Name = :CCRANAME];

		Cycle_Count_Line_Item__c item = new Cycle_Count_Line_Item__c(
			Cycle_Count_Response_Analysis__c = ccra.Id,
			Scanned_Expiration_Date__c = EXPIRATION,
			Scanned_Lot_Number__c = LOT1,
			Scanned_Model_Number__c = MODEL123,
			Scanned_Serial_Number__c = SERIAL2,
			Exception_Reason__c = 'Surplus'
		);
		insert item;

		Surgery__c surgery = [select Id from Surgery__c where Name = :SURGERYNAME];

		item = [select Other_Inventory_Item__c, Surgery_Item__c from Cycle_Count_Line_Item__c where Id = :item.Id];
		System.assertEquals(null, item.Other_Inventory_Item__c);
		System.assertEquals(surgery.Id, item.Surgery_Item__c);

	}
	
}