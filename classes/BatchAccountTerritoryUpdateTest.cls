/*
 @CreatedDate     8 APR 2016                                  
 @author          Mayuri-Accenture
 @Description     Test class for Batch Apex class 'BatchAccountTerritoryUpdateTest' which adds an Account to its territory based on Account_Team_Members__c records. The Account_Team_Members__c records are updated to Salesforce
                  from a batch run by SAP. The batch class invokes another batch to update Territory details of Users in its finish() method.
                  This Test class also covers Batch Apex class ;batchMyObjTerritoryUpdate' and 'batchOpptyTerritoryUpdate'.
 
 */



@isTest(SeeAllData=false)
@testVisible
private class BatchAccountTerritoryUpdateTest{
    public static list<User> usrLst = new list<User>();
    public static list<Personnel_ID__c> pesrnIdlst = new list<Personnel_ID__c>();
    public static list<Account> acctlst = new list<Account>();
    public static list<Account_Team_Member__c> ATMList = new list<Account_Team_Member__c>();
    public static list<My_Objectives__c> MyObjList = new list<My_Objectives__c>();
    public static list<Opportunity> opptyLst = new list<Opportunity>(); 
    public static List<Territory2Type> Ttype;  
    public static Id Territory2ModelIDActive;
    public static List<Territory2> trList=new List<Territory2>();    
    
    @testSetup
    static void setupTerritoryMockData(){
        User userC = UtilForUnitTestDataSetup.newUser('Standard User');
        userC.Username = 'UserNameC1@xyz.com';
        usrLst.add(userC);
        User userR = UtilForUnitTestDataSetup.newUser('Standard User');
        userR.Username = 'UserNameR1@abc.com';
        usrLst.add(userR);
        
        //Inserting 2 Test user record to create Personnel_ID__c 
        insert usrLst;
        
        Ttype = UtilForUnitTestDataSetup.newTerritory2Type();        
        Territory2ModelIDActive= UtilForUnitTestDataSetup.newTerritory2Model();
        Territory2 t1 = UtilForUnitTestDataSetup.newTerritory2('AA1234',Territory2ModelIDActive,Ttype[0].Id);   
        trList.add(t1);              
        Territory2 t2 = UtilForUnitTestDataSetup.newTerritory2('AA1245',Territory2ModelIDActive,Ttype[0].Id); 
        trList.add(t2); 
        insert trList; 
        System.assertEquals(trList[0].Territory2ModelId,Territory2ModelIDActive);        
        
        
        UserTerritory2Association nAssc = new UserTerritory2Association();
        nAssc.RoleInTerritory2 = 'Territory Manager';
        nAssc.Territory2Id = trList[0].Id;
        nAssc.UserId = usrLst[0].Id;
        insert nAssc;
        /*
        UserTerritory2Association nAssc1 = new UserTerritory2Association();
        nAssc1.RoleInTerritory2 = 'Territory Manager';
        nAssc1.Territory2Id = trList[1].Id;
        nAssc1.UserId = usrLst[0].Id;
        insert nAssc1;*/
        
    } 
    
    //Create Account and ObjectTerritory2Association records. We have written 2 methods because setup and non-setup objects cannot be inserted together.
    static void setupCommonMockData(){
        
        list<User> urLst = new list<User>([Select name,Id From User where LastName Like : 'LNA%'  limit 1]);
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'AA%'  ];  
        
        Account acct1 = new Account(Name = 'ACCT1NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId()); 
        acctlst.add(acct1);
        Account acct2 = new Account(Name = 'ACCT2NAME',RecordTypeId = UtilForUnitTestDataSetup.getRECTypeId());
        acctlst.add(acct2);       
        insert acctlst;
        
        ObjectTerritory2Association accountAssociation = new ObjectTerritory2Association();
        accountAssociation.ObjectId = acctlst[0].Id;
        accountAssociation.Territory2Id = trLst[0].Id;
        accountAssociation.AssociationCause = 'Territory2Manual';
        insert accountAssociation;
        
        Personnel_ID__c p1 =UtilForUnitTestDataSetup.newPersonnelID(urLst[0].Id);
        pesrnIdlst.add(p1);
        
        Insert pesrnIdlst;
        //Personnel_ID__c person1 = [select Id from Personnel_ID__c where User_for_account_team__c = :urLst[0].Id];
        For(Integer i=0; i<2; i++){
            Account_Team_Member__c a = new Account_Team_Member__c(
                Account_Team_Role__c = 'AccountTeamRole' +i,
                Personnel_ID__c= pesrnIdlst[0].Id,
                Account_Team_Share__c = acctlst[0].Id,
                Deleted__c = False,
                Territory_ID__c = 'AA1234',
                LastModifiedDate = system.today(),
                createdDate = system.today()
                
            );             
            ATMList.add(a);
        } 
        insert ATMList; 
        System.assertEquals(ATMList[0].LastModifiedDate,system.today());          
              
        
    }
   
    
    static TestMethod void TestbatchAccountTerritoryUpdate(){ 
        setupCommonMockData(); 
        Account_Team_Member__c a1 = new Account_Team_Member__c(
                Account_Team_Role__c = 'AccountTeamRole1',
                Personnel_ID__c= pesrnIdlst[0].Id,
                Account_Team_Share__c = acctlst[0].Id,
                Deleted__c = True,
                Territory_ID__c = 'AA1234',
                LastModifiedDate = system.today(),
                createdDate = system.today()
            );
        insert a1;
        Test.startTest();
            ID batchprocessid = Database.executeBatch(new batchAccountTerritoryUpdate());
        Test.stopTest();
    }
    
    static TestMethod void TestbatchMyObjTerritoryUpdate(){ 
    
        setupCommonMockData();
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'AA%'  ]; 
        list<User> urLst = new list<User>([Select name,Id From User where LastName Like : 'LNA%' limit 1 ]);
        
        for(Integer i=0; i<acctlst.size(); i++){
            My_Objectives__c a = new My_Objectives__c(
                OwnerId = urLst[0].Id, 
                Division__c = 'EP',
                Related_to_Account__c=acctlst[i].Id,
                Status__c = 'New',
                Territory__c = trLst[0].name                 
            );
            MyObjList.add(a);
         }
         insert  MyObjList;  
        Test.startTest();
            ID batchprocessid = Database.executeBatch(new batchMyObjTerritoryUpdate());
        Test.stopTest();
    }
    
    static TestMethod void TestbatchOpptyTerritoryUpdate(){
        setupCommonMockData();        
        List<Territory2> trLst = [Select name From Territory2 where name Like : 'AA%'  ];
        list<User> urLst = new list<User>([Select name,Id From User where LastName Like : 'LNA%' ]); 
        list<Account> accLst = [Select Id,name From Account where name Like : 'ACCT%'  limit 1];
        Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();
        final Id RECTYPE_EU = RECTYPES.get('Europe EP Capital Equipment').getRecordTypeId();
        ETM_Opportunity_Record_Types__c ETM = new ETM_Opportunity_Record_Types__c();
        ETM.Name = RECTYPE_EU;
        insert ETM;
        
        
       
            Opportunity oppty = new Opportunity(
                Name = 'Test Oppty'+'1',
                AccountId = accLst[0].Id,
                Territory2Id =trLst[0].Id,
                OwnerId = urLst[0].Id,
                CloseDate = system.today()+30,
                recordtypeId = RECTYPE_EU,
                StageName = 'Target Prospect'
            );
            opptyLst.add(oppty); 
            Opportunity oppty1 = new Opportunity(
                Name = 'Test Oppty'+'1',
                AccountId = accLst[0].Id,
                Territory2Id =trLst[0].Id,
                OwnerId = urLst[1].Id,
                CloseDate = system.today()+30,
                recordtypeId = RECTYPE_EU,
                StageName = 'Target Prospect'
            );       
           opptyLst.add(oppty1); 
        insert opptyLst ;
        system.assertEquals(opptyLst[0].AccountId,accLst[0].Id);
        system.assertEquals(opptyLst[0].OwnerId,urLst[0].Id);
        system.assertEquals(opptyLst[0].recordtypeId,RECTYPE_EU);
        Test.startTest();
            ID batchprocessid = Database.executeBatch(new batchOpptyTerritoryUpdate());
        Test.stopTest();
    }

}