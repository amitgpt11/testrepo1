/*** Apex version of the default logic.
* If opportunity's assigned account is assigned to
*  Case 1: 0 territories in active model
*            then set territory2Id =   null
*  Case 2: 1 territory in active model
*            then set territory2Id =   account's territory2Id
*  Case 3: 2 or more territories in active model 
*            then set territory2Id = then set territory2Id = null 
*/
global class OppTerrAssignDefaultLogicFilter implements
TerritoryMgmt.OpportunityTerritory2AssignmentFilter {
    /**
* No-arg constructor.
*/ 
    global   OppTerrAssignDefaultLogicFilter() {}
    
    /**
* Get mapping of   opportunity to territory2Id. The incoming list of opportunityIds contains only those with IsExcludedFromTerritory2Filter=false.
* If territory2Id =   null in result map, clear the opportunity.territory2Id if set.
* If opportunity is not present in result map, its territory2Id remains intact.
*/
    global Map<Id,Id> getOpportunityTerritory2Assignments(List<Id> opportunityIds) {
        Map<Id,Id> OppIdTerritoryIdResult = new Map<Id,Id>();
        
        //Get the active territory model Id
        Id activeModelId = getActiveModelId();
          
        if(activeModelId  != null){
            List<Opportunity> opportunities =[Select Id, AccountId, Territory2Id ,OwnerId from Opportunity where Id  IN :opportunityIds];
                
            Set<Id> accountIds = new Set<Id>();
            List<Id> ownerOnOpportunities = new List<Id>();
            system.debug('***Opp Id***'+opportunities.size());
            for(Opportunity opp:opportunities){
                if(opp.AccountId   != null){
                      
                    accountIds.add(opp.AccountId);
                   
                }
                if(opp.OwnerId!=null){
                ownerOnOpportunities.add(opp.OwnerId);
               
                }             
                 system.debug('++++'+opp.OwnerId);
            } 
            system.debug('***accountIds***'+accountIds.size());
            //Queries for and retains the Ids of Account that have related Territories
            List<ObjectTerritory2Association> ObjT2A = [SELECT AssociationCause,Id,ObjectId,SobjectType,Territory2Id FROM ObjectTerritory2Association Where ObjectId IN :AccountIDs AND  Territory2.Territory2ModelId = :activeModelId];
            List<UserTerritory2Association> UserT2A = [SELECT Id,RoleInTerritory2,Territory2Id,UserId FROM UserTerritory2Association Where UserId =:ownerOnOpportunities AND  Territory2.Territory2ModelId = :activeModelId];                        
            List<ID> tempIDList = new List<ID>();
            Map<Id,List<ID>> Account2TerritoryMap = new Map<Id,List<ID>>(); //Map contains Account Id and its related  Territories
            for(ObjectTerritory2Association objterr : ObjT2A){
                for(UserTerritory2Association userterr : UserT2A){
                    if(objterr.Territory2Id == userterr.Territory2Id ){
                        tempIDList.add(objterr.Territory2Id);
                        
                    }//End of If
                }//End of Loop
            }//End of Loop
            System.debug('#####'+ tempIDList);
            if(tempIDList.size()==0){
                System.debug('No Matches Found and  No territory is assigned. '+tempIDList);
            }
            else  if(tempIDList.size()==1){
                System.debug('Single Matches Found and territory is assigned . '+tempIDList);
            }else
                if(tempIDList.size()>1){
                    System.debug('Multiple Matches Found and No territory is assigned. '+tempIDList);
                }
            else{
                for(Opportunity opp: opportunities){
                    System.debug('Exact Matche Found. assigning  territory. '+tempIDList[0]);
                    OppIdTerritoryIdResult.put(opp.Id, tempIDList[0]);
                }
            }
            
        }
        system.debug('***OppIdTerritoryIdResult***'+OppIdTerritoryIdResult);
        return OppIdTerritoryIdResult;
    }
    /**
* Get the Id of the Active Territory Model. 
* If none exists, return   null;
*/
 @testVisible  private Id getActiveModelId() {
        List<Territory2Model> models = [Select Id from Territory2Model where State = 'Active'];
        Id activeModelId = null;
        if(models.size() == 1){
            activeModelId = models.get(0).Id;
        }
        
        return activeModelId;
    }
}