/**
* Trigger Handler class for the custom Inventory_Item__c object
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public with sharing class InventoryItemTriggerHandler extends TriggerHandler {
	
	final InventoryItemManager manager = new InventoryItemManager();

	public override void bulkAfter() {
		//manager.fetchRelatedRecords(trigger.new);
	}

	public override void afterUpdate(SObject oldObj, SObject newObj) {
		//manager.updateChildAvailableQuantities((Inventory_Item__c)oldObj, (Inventory_Item__c)newObj);
	}

	public override void andFinally() {
		//manager.commitChildUpdates();
	}

}