@isTest
private class MyContactDetailsController_Test {

  private static testMethod void test() {
      
      Test.StartTest();
      
      system.Test.setCurrentPage(Page.MyContactDetails);
      MyContactDetailsController objMyContactDetailsController = new MyContactDetailsController();
      
      
        User objCommunityUser = Test_DataCreator.createCommunityUser(); 
        
       
        
        system.runAs(objCommunityUser){
            
            Community_Event__c objCommunity_Event = Test_DataCreator.createCommunityEvent('test Display Name');
            Test_DataCreator.cerateBostonCSConfigTestData(objCommunity_Event.Id);
           
            Shared_Community_Order__c objOrder = Test_DataCreator.createOrder();
            
            objMyContactDetailsController = new MyContactDetailsController();
            objMyContactDetailsController.init();
            system.assertNotEquals(objMyContactDetailsController.lstCommunityOrder.size(),1);
            
            //objMyContactDetailsController.objUser.contact.lastname = 'rtest';
            objMyContactDetailsController.save();    
        }
        
         
       //Create Guest User
        User objGuestUser = Test_DataCreator.createGuestUser();
        system.runAs(objGuestUser){
            
            system.assertNotEquals(null, objMyContactDetailsController.init());
        }
        
        Test.StopTest();
  }
}