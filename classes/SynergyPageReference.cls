/**
* Custom PageReference class for external links to the NMD Synergy iOS app
*
* @author   Salesforce services
* @date     2015-07-14
*/
public class SynergyPageReference {

	public final static String PATH_PARAM = 'u';
	final static String DELIMITER = '://';
	
	final String scheme;
	final String path;

	public Map<String,String> parameters { get; private set; }

	public SynergyPageReference(String path) {

		NMD_Inventory_Settings__c settings = NMD_Inventory_Settings__c.getInstance();

		this.scheme = settings != null ? settings.URI_Scheme__c : '';
		this.parameters = new Map<String,String>();
		this.path = String.isNotBlank(path) && path.startsWith('/') ? path.substring(1) : path;

	}

	public PageReference getPageReference() {

		PageReference pr = new PageReference(this.scheme + DELIMITER + this.path);
		pr.getParameters().putAll(this.parameters);

		return pr;

	}

	public PageReference getRedirectPageReference() {

		PageReference pr = Page.BsciSynergyRedirect;
		pr.getParameters().put(PATH_PARAM, getPageReference().getUrl().substring(this.scheme.length() + DELIMITER.length()));
		
		return pr;

	}

}