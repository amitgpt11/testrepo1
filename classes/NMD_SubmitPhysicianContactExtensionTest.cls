/**
* Test class for the page/class SubmitPhysicianContact
*
* @author   Salesforce services
* @date     2015-02-09
*/
@isTest(SeeAllData=false)
private class NMD_SubmitPhysicianContactExtensionTest {

    static final String PROSPECTACCOUNT = 'ProspectAccount';

    static final NMD_TestDataManager testData = new NMD_TestDataManager();

    @testSetup
    static void setup() {

        //setup test data
        Account acct = testData.newAccount(PROSPECTACCOUNT);
        acct.RecordTypeId = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
       // acct.Sent_to_Quincy__c = true;
        insert acct;

        List<Contact> contacts = new List<Contact>{
            testData.newContact(acct.Id)
        };
        insert contacts;

        testData.setupQuincyContact();

    }
   
    static testMethod void test_SubmitPhysicianContact1() {
      
       //grab data
        Contact cont = [
            select 
                AccountId,
                MailingStreet,
                MailingCity,
                MailingState,
                MailingPostalCode,
                MailingCountry,
                Academic_Title__c,
                Specialty__c,
                Phone,
                Fax
            from Contact 
            where Account.Name = :PROSPECTACCOUNT
        ];
        
        //assert the email was submitted successfully
        cont.MailingStreet = '123 fake street';
        cont.MailingCity = 'FakeCity';
        cont.MailingState = 'FakeState';
        cont.MailingPostalCode = '12345';
        cont.MailingCountry = 'USA';
        cont.Phone = '123456789';
        cont.Fax = '123456789';
        cont.Academic_Title__c = 'Bachelor of Arts';
        cont.Specialty__c = 'Allergy';
        update cont;

        ApexPages.StandardController sc2 = new ApexPages.StandardController(cont);
        NMD_SubmitPhysicianContactExtension extension2 = new NMD_SubmitPhysicianContactExtension(sc2);

        extension2.submit();

    
    }
    static testMethod void test_SubmitPhysicianContact() {
 
        //grab data
        Contact cont = [
            select 
                AccountId,
                MailingStreet,
                MailingCity,
                MailingState,
                MailingPostalCode,
                MailingCountry,
                Academic_Title__c,
                Specialty__c,
                Phone,
                Fax
            from Contact 
            where Account.Name = :PROSPECTACCOUNT
        ];

        Account account = [
            select
                Id
            from Account
            where Id = :cont.AccountId
        ];

        Seller_Hierarchy__c territory = testData.newSellerHierarchy();
        insert territory;

        account.BillingStreet = '123 Fake';
        account.BillingCity = 'city';
        account.BillingState = 'IL';
        account.BillingPostalCode = '12345';
        account.BillingCountry = 'US';
        account.Sold_to_Street__c = '123 Fake';
        account.Sold_to_City__c = 'City';
        account.Sold_to_State__c = 'IL';
        account.Sold_to_Zip_Postal_Code__c = '12345';
        account.Sold_to_Country__c = 'US';
        account.Sold_to_Phone__c = '12345';
        account.Sold_to_Fax__c = '12345';
        account.NMD_Territory__c = territory.Id;
        account.Type= 'Sold to';
        account.Classification__c ='GPO';
        update account;
        
        //setup page
        PageReference pr = Page.SubmitPhysicianContact;
        pr.getParameters().put('id', cont.Id);
        Test.setCurrentPage(pr);
        ApexPages.StandardController sc = new ApexPages.StandardController(cont);
        NMD_SubmitPhysicianContactExtension extension = new NMD_SubmitPhysicianContactExtension(sc);

        //send and verify error message received
        extension.submit();
        //System.assertequals(Label.NMD_Quincy_Prospect_Contact_Required_Fields,ApexPages.GetMessages().get(1).getSummary());

        //verify Sent_to_Quincy
        Account acct = [select Sent_to_Quincy__c from Account where Id = :cont.AccountId];
        //System.assert(acct.Sent_to_Quincy__c);

        Contact contact = [select Sent_to_Quincy__c from Contact where Id = :cont.Id];
        //System.assert(contact.Sent_to_Quincy__c);

    }
    
}