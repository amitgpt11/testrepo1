/*
 @CreatedDate     21 June 2016                                  
 @author          Ashish-Accenture
 @Description     Batch class is used to create Lutonix SRAI records on every start of the month.
       
*/

public class batchCreateLutonixSRAIRecords implements Database.Batchable<sObject>, Schedulable{
    
    // Schedulable method, executes the class instance
    
    Public void execute(SchedulableContext context) {
        Database.executeBatch(new batchCreateLutonixSRAIRecords(), 200);
    }
    
     Set<Id> UserIdset;
     Map<integer,String> MapMonthNumber;
    public batchCreateLutonixSRAIRecords(){
        UserIdset = new Set<Id>();
        UserIdset = UtilityController.GetUserIdsFromGroup(label.Public_Group_For_Daily_Reminder);
        MapMonthNumber = UtilityController.getMapMonthNumber();
    }
    
    //Start method, fetch user records from public group 
    public Database.QueryLocator start(Database.BatchableContext BC) {
       
       String query = 'Select id,name,email from User where id in : UserIdSet';
        return database.getquerylocator(query);
    }
    
    //Lutonix SRAI Records will be created for each user in Public Group
    public void execute(Database.BatchableContext BC, List<User> scope) {
        
        list<PI_Lutonix_SRAI__c> lstInsertLutonixSRAI = new list<PI_Lutonix_SRAI__c>();
        PI_Lutonix_SRAI__c objInsertLutonixSRAI;
        date todaydate = date.Today();
        for(User u : scope){
            objInsertLutonixSRAI = new PI_Lutonix_SRAI__c();
            objInsertLutonixSRAI.PI_Month__c = MapMonthNumber.get(todaydate.Month());
            objInsertLutonixSRAI.PI_Year__c = String.ValueOf(todaydate.Year());
            objInsertLutonixSRAI.Name = MapMonthNumber.get(todaydate.Month()) + ' '+String.ValueOf(todaydate.Year());
            objInsertLutonixSRAI.PI_Partial_Month__c = false;
            objInsertLutonixSRAI.ownerId = u.id;
            lstInsertLutonixSRAI.add(objInsertLutonixSRAI);
        }
        if(lstInsertLutonixSRAI.size() > 0){
            try{
                insert lstInsertLutonixSRAI;
           
            }catch(exception e){
                System.debug('###====e==='+e);
            }
         }
    }
    
    public void finish(Database.BatchableContext BC){
    
     }
    
    

}