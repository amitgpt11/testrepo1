/**
* Test class for NMD_AutoSubmitManager
*
* @Author Salesforce Services
* @Date 2016/03/18
*/ 
@isTest
private class NMD_AutoSubmitManagerTest {
    
    static final String ACCT_MASTER = 'MasterAccount';
    static final String ACCT_TRIALING = 'TrialingAccount';
    static final String ACCT_TRIALING1 = 'TrialingAccount1';
    static final String ACCT_PROCEDURE = 'ProcedureAccount';
    static final String OPPTY_NAME = '/-.-NMD SCS Trial-Implant';
    static final String LNAME = 'LName';
    static final String LEAD_SOURCE = 'Care Card';
    static final String OPPORTUNITY_TYPE = 'STANDARD';
    static final String OPPORTUNITY_TYPE1 = 'Implant Only';


    @testSetup
    static void setup() {

            
        
        NMD_TestDataManager td = new NMD_TestDataManager();
        
        td.setupQuincyContact();
       
        Profile p = [SELECT Id FROM Profile WHERE Name = :'NMD Field Rep']; 
        User user = new User(
          Email = 'suser@boston.com', 
          LastName = 'LNAMETEST',  
          ProfileId = p.Id, 
          UserName = 'suser' + '@boston.com',
          Alias = 'standt', 
          EmailEncodingKey = 'UTF-8',  
          LanguageLocaleKey = 'en_US', 
          LocaleSidKey = 'en_US',  
          TimeZoneSidKey = 'America/Los_Angeles',
          Cost_Center_Code__c = 'string'); 
        insert user;
        
        Seller_Hierarchy__c territory = td.newSellerHierarchy();
        insert territory;

        Account acctMaster = td.createConsignmentAccount();
        acctMaster.Name = ACCT_MASTER;

        Account acctTrialing = td.newAccount(ACCT_TRIALING);
        acctTrialing.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acctTrialing.Type = 'Bill to';
        acctTrialing.Sent_to_Quincy__c = false;
        acctTrialing.NMD_Territory__c = territory.Id;
        
        Account acctTrialing1 = td.newAccount(ACCT_TRIALING1);
        acctTrialing1.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acctTrialing1.Type = 'Bill to';
        acctTrialing1.Sent_to_Quincy__c = false;
        acctTrialing1.NMD_Territory__c = territory.Id;
        
        Account acctProcedure = td.newAccount(ACCT_PROCEDURE);
        acctProcedure.RecordTypeId = AccountManager.RECTYPE_PROSPECT;
        acctProcedure.Type = 'Bill to';
        acctProcedure.Sent_to_Quincy__c = false;
        acctProcedure.NMD_Territory__c = territory.Id;
        
        //Account acctInsurance = td.newAccount('AccountInsurance');
        //acctInsurance.RecordTypeId = AccountManager.RECTYPE_INSPROVIDER;
        //insert acctInsurance;
        
        insert new List<Account> {
            acctMaster, acctTrialing, acctTrialing1, acctProcedure
        };

        Contact trialing = td.newContact(acctTrialing.Id, LNAME);
        trialing.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        trialing.AccountId = acctTrialing.Id;
        trialing.Territory_Id__c = territory.Id;
        trialing.Sent_to_Quincy__c = false;

        Contact procedure = td.newContact(acctProcedure.Id, LNAME);
        procedure.RecordTypeId = ContactManager.RECTYPE_PROSPECT;
        procedure.AccountId = acctProcedure.Id;
        procedure.Territory_Id__c = territory.Id;
        procedure.Sent_to_Quincy__c = false;

        insert new List<Contact> { 
            trialing, procedure 
        };

        Patient__c patient = new Patient__c(
            RecordTypeId = PatientManager.RECTYPE_Prospect,
            Physician_of_Record__c = trialing.Id
            //Primary_Insurance__c = acctInsurance.id
        );
        insert patient;

        Opportunity oppty = td.newOpportunity(acctTrialing1.Id, OPPTY_NAME);
        oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        oppty.Trialing_Account__c = acctTrialing.Id;
        oppty.Trialing_Physician__c = trialing.Id;
        oppty.Procedure_Account__c = acctProcedure.Id;
        oppty.Procedure_Physician__c = procedure.Id;
        oppty.Scheduled_Trial_Date_Time__c = system.today();
        oppty.Territory_ID__c = territory.Id;
        oppty.closeDate = System.today()+30;
        oppty.LeadSource = LEAD_SOURCE;
        oppty.Opportunity_Type__c = OPPORTUNITY_TYPE;
        oppty.CloseDate = system.today();
        oppty.Patient__c = patient.Id;
        //insert oppty;
        
        Opportunity oppty1 = td.newOpportunity(acctTrialing1.Id, OPPTY_NAME);
        oppty1.Territory_ID__c = territory.Id;
        oppty1.Opportunity_Type__c = OPPORTUNITY_TYPE1;
        oppty1.Scheduled_Trial_Date_Time__c = system.today();
        oppty1.closeDate = System.today()+30;
        oppty1.LeadSource = LEAD_SOURCE;
        oppty1.RecordTypeId = OpportunityManager.RECTYPE_IMPLANT;
        oppty1.Procedure_Account__c = acctProcedure.Id;
        oppty1.Scheduled_Procedure_Date_Time__c =system.today();
        oppty1.Procedure_Physician__c = procedure.Id;
        oppty1.CloseDate = system.today();
        oppty1.Patient__c = patient.Id;
        //insert oppty1;
        insert new List<Opportunity> { oppty, oppty1};
        
        /*Opportunity oppty = td.newOpportunity(acctMaster.Id, OPPTY_NAME);
        oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        oppty.AccountId = acctTrialing1.Id;
        oppty.Trialing_Account__c = acctTrialing.Id;
        oppty.Trialing_Physician__c = trialing.Id;
        oppty.Procedure_Account__c = acctProcedure.Id;
        oppty.Procedure_Physician__c = procedure.Id;
        oppty.Patient__c = patient.Id;
        oppty.Scheduled_Trial_Date_Time__c = System.now().addDays(7);
        oppty.Scheduled_Procedure_Date_Time__c = System.now().addDays(7);
        insert oppty;
        
        Opportunity oppty1 = td.newOpportunity(acctMaster.Id, OPPTY_NAME);
        oppty1.RecordTypeId = OpportunityManager.RECTYPE_TRIAL_NEW;
        oppty1.AccountId = acctTrialing1.Id;
        oppty1.Trialing_Account__c = acctTrialing1.Id;
        oppty1.Trialing_Physician__c = trialing.Id;
        oppty1.Procedure_Account__c = acctProcedure.Id;
        oppty1.Procedure_Physician__c = procedure.Id;
        oppty1.Patient__c = patient.Id;
        oppty1.Scheduled_Trial_Date_Time__c = System.now().addDays(7);
        oppty1.Scheduled_Procedure_Date_Time__c = System.now().addDays(7);
        insert oppty1; */
    }
    static testmethod void testScenario()
    {

            
        
        User u = [Select id,Email From user where Email ='suser@boston.com'];
        
         SObject cont = [select id,Firstname,LastName,MailingStreet,MailingCity,MailingState,
                         MailingPostalCode,MailingCountry,Phone,Fax,
                         RecordTypeId,AccountId,Territory_Id__c,Sent_to_Quincy__c 
                         From Contact where Account.Name='TrialingAccount' Limit 1];
                         
          List<Opportunity> opp = [SELECT Id, AccountId, Contact__c, Patient__c,RecordTypeId,
            Scheduled_Trial_Date_Time__c, Scheduled_Procedure_Date_Time__c,
            Trialing_Account__c, Procedure_Account__c, 
            Trialing_Physician__c, Procedure_Physician__c 
            FROM Opportunity where RecordType.Name = 'NMD SCS Trial' or RecordType.Name = 'NMD SCS Implant' ];
            
            /* Opportunity opp1 = [SELECT Id, AccountId, Contact__c, Patient__c,RecordTypeId,Scheduled_Procedure_Date_Time__c,
            Scheduled_Trial_Date_Time__c, Scheduled_Procedure_Date_Time__c,
            Trialing_Account__c, Procedure_Account__c, 
            Trialing_Physician__c, Procedure_Physician__c 
            FROM Opportunity where RecordType.Name = 'NMD SCS Implant' ];*/
        
        
         Map<Id,Id> mapId = new Map<Id,Id>(); 
         mapId.put(Cont.id,u.id);
         Set<Id> setId = New Set<Id>();
         setId.add(cont.id);
         Schema.sObjectType expected = Schema.Contact.getSObjectType();                 
         NMD_AutoSubmitManager autoSubmit = new  NMD_AutoSubmitManager();               
         Test.startTest();
         autoSubmit.init(expected , new List<SObject>{cont});  
          NMD_AutoSubmitManager.sendQuincyReminder(mapId);       
         autoSubmit.validateRecord(cont);    
         NMD_AutoSubmitManager.sendQuincyEmails(setId);
         Test.stopTest();        

        
    }
    /*static testmethod void testScenario2()
    {
         SObject cont = [select id,Firstname,LastName,MailingStreet,MailingCity,MailingState,
                         MailingPostalCode,MailingCountry,Phone,Fax,
                         RecordTypeId,AccountId,Territory_Id__c,Sent_to_Quincy__c 
                         From Contact where Account.Name='TrialingAccount' Limit 1];
                         
          Opportunity opp = [SELECT Id, AccountId, Contact__c, Patient__c,RecordTypeId,Scheduled_Procedure_Date_Time__c,
            Scheduled_Trial_Date_Time__c, Scheduled_Procedure_Date_Time__c,
            Trialing_Account__c, Procedure_Account__c, 
            Trialing_Physician__c, Procedure_Physician__c 
            FROM Opportunity where RecordType.Name = 'NMD SCS Implant' ];
            
                   
         Schema.sObjectType expected = Schema.Contact.getSObjectType();                 
         NMD_AutoSubmitManager autoSubmit = new  NMD_AutoSubmitManager();               
         autoSubmit.init(expected , new List<SObject>{cont});    
         autoSubmit.validateRecord(cont);           
                         
    }*/
    static testmethod void testScenario3()
    {
            
        
         SObject acc = [select id,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,
                        Sold_to_Attn__c,Sold_to_Street__c,Sold_to_City__c,Sold_to_State__c,Sold_to_Zip_Postal_Code__c,
                        Sold_to_Country__c,Sold_to_Phone__c,NMD_Territory__c,Type,Classification__c,
                         RecordTypeId,Sent_to_Quincy__c

                         From Account where Name='TrialingAccount' Limit 1];
                         
         Opportunity opp = [SELECT Id, AccountId, Contact__c, Patient__c,RecordTypeId,
            Scheduled_Trial_Date_Time__c, Scheduled_Procedure_Date_Time__c,
            Trialing_Account__c, Procedure_Account__c, 
            Trialing_Physician__c, Procedure_Physician__c 
            FROM Opportunity where RecordType.Name = 'NMD SCS Trial'];
                             
         
         Schema.sObjectType expected = Schema.Account.getSObjectType();                 
         NMD_AutoSubmitManager autoSubmit = new  NMD_AutoSubmitManager();               
       Test.startTest();
          autoSubmit.init(expected , new List<SObject>{acc});  
          autoSubmit.validateRecord(acc); 
      
        
        Test.stopTest();       
    }
     static testmethod void test_AutoSubmitPatient() {

         Account acct = [select Id from Account where Name = :ACCT_TRIALING1 limit 1];

        Opportunity oppty = [
            SELECT 
                Id,
                Patient__c
            FROM Opportunity 
            WHERE AccountId = :acct.Id Limit 1
        ];

        Patient__c patient = [
            SELECT
                Id
            FROM Patient__c
            WHERE Id = :oppty.Patient__c
        ];

        Test.startTest();

            patient.Patient_First_Name__c = 'FirstName';
            patient.Patient_Last_Name__c = 'LastName';
            patient.Address_1__c = '123 Fake Street';
            patient.City__c = 'Town';
            patient.State__c = 'IL';
            patient.Country__c = 'US';
            patient.County__c = 'Cook';
            patient.Zip__c = '12345';
            patient.Patient_Phone_Number__c = '123456789';
            patient.Patient_Mobile_Number__c = '1213456789';
            patient.Patient_Date_of_Birth__c = System.today();
            patient.Patient_Gender__c = 'Male';
            patient.SAP_ID__c = '';
            patient.BSN_Update__c = '';
            patient.is_Submitted_to_SAP__c = false;
            update patient;

        Test.stopTest();

        patient = [SELECT Id, BSN_Update__c FROM Patient__c WHERE Id = :patient.Id];
        System.assertEquals('Request SAP ID', patient.BSN_Update__c, 'Patient is not set to Request SAP Id!');


    }
    /*static testmethod void testScenario4()
    {
         SObject acc = [select id,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,
                        Sold_to_Attn__c,Sold_to_Street__c,Sold_to_City__c,Sold_to_State__c,Sold_to_Zip_Postal_Code__c,
                        Sold_to_Country__c,Sold_to_Phone__c,NMD_Territory__c,Type,Classification__c,
                         RecordTypeId,Sent_to_Quincy__c

                         From Account where Name='TrialingAccount' Limit 1];
                         
        Opportunity opp = [SELECT Id, AccountId, Contact__c, Patient__c,RecordTypeId,Scheduled_Procedure_Date_Time__c,
            Scheduled_Trial_Date_Time__c, Scheduled_Procedure_Date_Time__c,
            Trialing_Account__c, Procedure_Account__c, 
            Trialing_Physician__c, Procedure_Physician__c 
            FROM Opportunity where RecordType.Name = 'NMD SCS Implant' ];                
                         
         
         Schema.sObjectType expected = Schema.Account.getSObjectType();                 
         NMD_AutoSubmitManager autoSubmit = new  NMD_AutoSubmitManager();               
         autoSubmit.init(expected , new List<SObject>{acc});  
          autoSubmit.validateRecord(acc);             
                         
    }*/

/**
* 
* @Author Tejas Ghalsasi
* 
*/    
    
    static testmethod void testScenario_1()
    {
        
            
        
         SObject acc = [select id,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,
                        Sold_to_Attn__c,Sold_to_Street__c,Sold_to_City__c,Sold_to_State__c,Sold_to_Zip_Postal_Code__c,
                        Sold_to_Country__c,Sold_to_Phone__c,NMD_Territory__c,Type,Classification__c,
                         RecordTypeId,Sent_to_Quincy__c

                         From Account where Name='ProcedureAccount' Limit 1];
                         
         Opportunity opp = [SELECT Id, AccountId, Contact__c, Patient__c,RecordTypeId,
            Scheduled_Trial_Date_Time__c, Scheduled_Procedure_Date_Time__c,
            Trialing_Account__c, Procedure_Account__c, 
            Trialing_Physician__c, Procedure_Physician__c 
            FROM Opportunity where RecordType.Name = 'NMD SCS Implant'];
               opp.Scheduled_Procedure_Date_Time__c=System.Now()+20 ; 
        update opp;
         
         Schema.sObjectType expected = Schema.Account.getSObjectType();                 
         NMD_AutoSubmitManager autoSubmit = new  NMD_AutoSubmitManager();               
       Test.startTest();
          autoSubmit.init(expected , new List<SObject>{acc});  
          autoSubmit.validateRecord(acc); 
        
        
       Test.stopTest();   
        
    }
    
    static testmethod void testScenario_copy()
    {
        
        User u = [Select id,Email From user where Email ='suser@boston.com'];
        
         SObject cont = [select id,Firstname,LastName,MailingStreet,MailingCity,MailingState,
                         MailingPostalCode,MailingCountry,Phone,Fax,
                         RecordTypeId,AccountId,Territory_Id__c,Sent_to_Quincy__c 
                         From Contact where Account.Name='ProcedureAccount' Limit 1];
                         
        
         
             Opportunity opp1 = [SELECT Id, AccountId, Contact__c, Patient__c,RecordTypeId,Scheduled_Procedure_Date_Time__c,
            Scheduled_Trial_Date_Time__c,
            Trialing_Account__c, Procedure_Account__c, 
            Trialing_Physician__c, Procedure_Physician__c 
            FROM Opportunity where RecordType.Name = 'NMD SCS Implant' ];
        opp1.Scheduled_Procedure_Date_Time__c=System.now()+90;
        update opp1;
        
        
         Map<Id,Id> mapId = new Map<Id,Id>(); 
         mapId.put(Cont.id,u.id);
         Set<Id> setId = New Set<Id>();
         setId.add(cont.id);
         Schema.sObjectType expected = Schema.Contact.getSObjectType();                 
         NMD_AutoSubmitManager autoSubmit = new  NMD_AutoSubmitManager();               
         Test.startTest();
         autoSubmit.init(expected , new List<SObject>{cont});  
          NMD_AutoSubmitManager.sendQuincyReminder(mapId);       
         autoSubmit.validateRecord(cont);    
         NMD_AutoSubmitManager.sendQuincyEmails(setId);
         Test.stopTest();        
 

    }
    
    
    static testmethod void testScenario3_copy()
    {

        NMD_TestDataManager td=new NMD_TestDataManager();
         SObject acc2 = td.newAccount();
             
         Opportunity opp = [SELECT Id,RecordTypeID FROM Opportunity where RecordType.Name = 'NMD SCS Trial'];
        
        Schema.sObjectType expected = Schema.Account.getSObjectType();                 
         NMD_AutoSubmitManager autoSubmit = new  NMD_AutoSubmitManager();               
       Test.startTest();
          autoSubmit.init(expected , new List<SObject>{acc2});  
          autoSubmit.validateRecord(acc2); 
      
        
        Test.stopTest();       

 

    }
    
    
    
    
}