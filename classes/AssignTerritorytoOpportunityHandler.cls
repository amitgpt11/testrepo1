/*
 @CreatedDate     5 MAR 2016                                                                   
 @author         Sipra-Accenture
 @Description    This class checks for the territories on account and users, compares the territories , and assign to the opportunity records while Updation of the record.

*/


public with sharing class AssignTerritorytoOpportunityHandler extends TriggerHandler {
        public List<Territory2Model>   models =null;
        
        public Id activeModelId = null;
        public List<Opportunity> lstoldopptys = (List<Opportunity>)trigger.old;
        
        public List<Opportunity> lstnewopptys = (List<Opportunity>)trigger.new;
        
        public Set<Id> accountOnOpportunity = new Set<Id>();          
        public Set<Id> ownerOnOpportunity = new Set<Id>();    
        public List<Id> territoryOnOpportunity = new List<Id>();
        public List<Id> matchedTerritories = new List<Id>();
        public boolean isAccountChangeonOpty =false;
    public void assignTerritoryForOpportunity(boolean isInsert) {
      //UtilForAssignTerritorytoOpportunity utilAssignTerritory = new UtilForAssignTerritorytoOpportunity();
      //utilAssignTerritory.utilassignTerritoryForOpportunity(isInsert,lstnewopptys);

        models =  [Select Id from Territory2Model where State = 'Active'];
    
           //Checking for activeModelId otherwise skip 
        if(models.size() == 1){
            activeModelId = models.get(0).Id;
        }
        
               
        for(Opportunity o : lstnewopptys){
            
            if(o.AccountId != null){
                system.debug('Account Found '+o.AccountId +', '+o);
                accountOnOpportunity.add(o.AccountId);   
            }//finding account on opty
            //checking if account has been changed on the opportunity
            if(!isInsert)
                if( ((Opportunity)trigger.oldMap.get(o.Id)).AccountId != ((Opportunity)trigger.newMap.get(o.Id)).AccountId)
                     isAccountChangeonOpty= true;
                     system.debug('Entered in update value is==>'+isAccountChangeonOpty);

            if(o.OwnerId!=null){
                system.debug('Owner Found '+o.OwnerId);
                ownerOnOpportunity.add(o.OwnerId);
                system.debug('Opty Owner '+o.OwnerId);
            }//finding user on opty  
            if(o.territory2Id!=null){
                system.debug('Territory Found '+o.territory2Id);    
                territoryOnOpportunity.add(o.territory2Id);   
            }
        }//End of Loop
        
        List<ObjectTerritory2Association> territoriesOnNewAcount = [SELECT AssociationCause,Id,ObjectId,SobjectType,Territory2Id FROM ObjectTerritory2Association Where ObjectId IN :accountOnOpportunity AND  Territory2.Territory2ModelId = :activeModelId];
        List<UserTerritory2Association> territoriesOnNewOwner = [SELECT Id,RoleInTerritory2,Territory2Id,UserId FROM UserTerritory2Association Where UserId =:ownerOnOpportunity AND  Territory2.Territory2ModelId = :activeModelId];
        system.debug('territoriesOnNewOwner ==> '+territoriesOnNewOwner +'AccountTerritory==>'+territoriesOnNewAcount.size() );
        //for the ease of calculation, this logic will always consider territories on the 
        //new account and new owner on the opportunity, in below cases
        //1. account change
        //2. owner change
        //3. account and owner change together
        
        for(ObjectTerritory2Association o : territoriesOnNewAcount){
            for(UserTerritory2Association u : territoriesOnNewOwner){
                if(o.Territory2Id == u.Territory2Id ){
                    system.debug('Territory Match Found '+u.Territory2Id);
                    matchedTerritories.add(o.Territory2Id);    
                }//End of IF
            }//End of INNER FOR Loop
        }//End of OUTER FOR Loop 
        
        system.debug('Matched Territories = '+matchedTerritories+ '   - terOptySize'+territoryOnOpportunity.size());                                     
        
        if(matchedTerritories.size()==1){
            //Found exact match - check if this does not matches the existing territory on opportunity
            for(Opportunity o1 : lstnewopptys){
                
                o1.territory2Id = matchedTerritories.get(0);
                o1.territory_assigned__C = true;
                
            }
            
        }else{
            if(matchedTerritories.isEmpty() ||matchedTerritories.size()>1)
            {
             for(Opportunity o1 : lstnewopptys){
                 if(isAccountChangeonOpty){
                     o1.Territory2Id=null;//clearing the existing territory since new account does not have any matching territory.
                 }               
                o1.territory_assigned__C = false;
                
            }
           // system.debug('No Match Found.');
            system.debug('Match(es) Found - '+matchedTerritories);    
        
            
        }
       
        }
        
        
    }//end if trigger if
    
}