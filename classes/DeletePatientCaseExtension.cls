public without sharing class DeletePatientCaseExtension {

public Shared_Patient_Case__c patientCase;
    
    public DeletePatientCaseExtension(ApexPages.StandardController controller) {
        patientCase = (Shared_Patient_Case__c) controller.getRecord();
    }

    
    public PageReference removePatientForWatchMan() {
       try{
       delete patientCase;
       }catch(DMLException ex){
       System.debug('Exception : '+ex.getMessage());
       }
       return new PageReference('/' + patientCase.Shared_Facility__c);
    }
}