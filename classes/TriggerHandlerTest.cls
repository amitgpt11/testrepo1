/**
* Test class
*
* @Author salesforce Services
* @Date 2015/04/22
*/
@isTest(SeeAllData=false)
private class TriggerHandlerTest {
	
	static testMethod void test_TriggerHandler() {
		
		TriggerHandler handler = new TriggerHandler();

		handler.bulkBefore();
		handler.bulkAfter();
		handler.beforeInsert(null);
		handler.beforeUpdate(null, null);
		handler.beforeDelete(null);
		handler.afterInsert(null);
		handler.afterUpdate(null, null);
		handler.afterDelete(null);
		handler.afterUndelete(null);
		handler.andFinally();

	}
}