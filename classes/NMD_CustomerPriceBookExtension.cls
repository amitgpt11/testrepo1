/**
* Extension class for the CustomerPricebook vf page
*
* @Author salesforce Services
* @Date 2015/04/22
*/
public without sharing class NMD_CustomerPriceBookExtension extends NMD_ExtensionBase {

    final static Integer PAGESIZE_DESKTOP = 50;
    final static Integer PAGESIZE_MOBILE = 30;
    final static String MAIN_PRICEBOOK_NAME = 'NMD';

    final static Set<String> whitelistedProfiles = new Set<String> {
        'NMD Field Support',
        'NMD IBU',
        'NMD IBU-PC',
        'NMD RMM',
        'NMD Super User',
        'System Administrator'
    };

    final ApexPages.StandardController sc;
    final Boolean isValidUser;
    final Integer pageSize;

    public String productFamilyFilter {get; set;}
    public String searchString {get; set;}
    public Boolean isSalesforce1 { get; private set; }
    public Integer page { get; private set; }
    public Integer maxPage { 
        get { return Math.ceil((this.total - 1) / this.pageSize).intValue(); } 
    }

    private Integer total;
    private List<NMD_PricebookPair> pbePairs;

    public List<NMD_PricebookPair> products {
        get {

            List<NMD_PricebookPair> entries = new List<NMD_PricebookPair>{};
            
            if (this.isValidUser && this.pbePairs != null && !this.pbePairs.isEmpty()) {

                Integer minoffset = this.page * this.pageSize;
                Integer maxoffset = minoffset + this.pageSize + 1;

                for (Integer i = minoffset; i < Math.min(this.pbePairs.size(), maxoffset); i++) {
                    entries.add(this.pbePairs[i]);
                }

            }

            return entries;

        }
    }

    /**
    * Setup values for the VF page, throw error if the user is not valid
    *
    * @param    ApexPages.StandardController sc
    */
    public NMD_CustomerPriceBookExtension(ApexPages.StandardController sc) {
        //clear filters
        this.productFamilyFilter = '';
        this.searchString = '';

        //setup
        this.sc = sc;
        this.isSalesforce1 = ApexPages.currentPage().getParameters().get('isdtp') == 'p1';
        this.isValidUser = validateUser();

        this.pageSize = this.isSalesforce1 ? PAGESIZE_MOBILE : PAGESIZE_DESKTOP;
        this.page = 0;

        calcTotal();

        if (!this.isValidUser) {
            newErrorMessage('Insufficient Privileges.');
        }

    }

    /**
    * Determine if the usr is valid and able to access the VF page
    *
    * @param    void
    */
    private Boolean validateUser() {

        Boolean isValid = false;
           
        Account acct = (Account)this.sc.getRecord();
        String profileName = [select Name from Profile where Id = :UserInfo.getProfileId()].Name;

        if (acct.OwnerId == UserInfo.getUserId() || whitelistedProfiles.contains(profileName)) {
            isValid = true;
        }
        else {

            // is the user an account team member?
            List<AccountTeamMember> atm = new List<AccountTeamMember>([
                select Id
                from AccountTeamMember
                where AccountId = :acct.Id
                and UserId = :UserInfo.getUserId()
                and User.Division in :NMD_PricebookManager.NEUROMOD
            ]);

            isValid = !atm.isEmpty();

        }

        return isValid;

    }

    /**
    * Resets the page to 0 and recalculates the number of records based on a change in the Family filter or a user search
    *
    * @param    Void
    */
    public void updateProductFilter() {
        this.page = 0;
        calcTotal();
    }

    /**
    * Uses the search/family filter criteria to count the number of records found for the query and stores the value in total
    *   If the user is not valid.
    *
    * @param    void
    */
    public void calcTotal() {

        this.total = 0;

        if(this.isValidUser) {

            Set<Id> acctIds = new Set<Id> { this.sc.getId() };
            String fuzzySearch = '%' + this.searchString + '%';
            NMD_PricebookManager pbman = new NMD_PricebookManager(acctIds, fuzzySearch, this.productFamilyFilter);
            this.pbePairs = pbman.getAccountPricebookEntries(this.sc.getId());
            this.total = this.pbePairs.size();

        }

    }

    /**
    * Populates the select list with Product2.Family values which can be used to filter the Products
    *
    * @param    void
    */
    public List<SelectOption> getProductFamilies() {

        List<SelectOption> options = new List<SelectOption> {
            new SelectOption('', '-- No Filter --')
        };

        for (AggregateResult agg : [
            select 
                Product2.Family 
            from PricebookEntry 
            where Pricebook2.Name = :MAIN_PRICEBOOK_NAME
            and Product2.Family != null
            and IsActive = true
            group by Product2.Family
            order by Product2.Family
        ]) {
            String value = String.valueOf(agg.get('Family'));
            options.add(new SelectOption(value, value));
        }

        return options;

    }

    /**
    * Sets the VF page table page number to 0 moving the table to the very first page of records
    *
    * @param    void
    */
    public void firstPage() {
        this.page = 0;
    }

    /**
    * Moves the VF page table back one page of records
    *
    * @param    void
    */
    public void prevPage() {
        this.page = this.page == 0 ? 0 : this.page - 1;
    }

    /**
    * Moves the VF Page table forward one page of records
    *
    * @param    void
    */
    public void nextPage() {
        this.page = this.page == this.maxPage ? this.page : this.page + 1;
    }

    /**
    * Sets the VF page table page number to the maximum value moving the table to the very last page of records
    *
    * @param    void
    */
    public void lastPage() {
        this.page = this.maxPage;
    }

}