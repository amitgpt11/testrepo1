/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/getprofiledata/*')
global class RestGetProfileData {
    global RestGetProfileData() {

    }
    @HttpPost
    global static openq.OpenMSL.PROFILE_DATA_RETRIEVE_RESPONSE_element getProfileData(openq.OpenMSL.PROFILE_DATA_RETRIEVE_REQUEST_element request_x) {
        return null;
    }
}
