/**
* Extension class for the vf page ConvertLead
*
* @Author salesforce Services
* @Date 02/10/2015
*/
public with sharing class NMD_ConvertLeadExtension extends NMD_ExtensionBase {
	
	final ApexPages.StandardController sc;
	final Boolean isSalesforce1;

	public NMD_ConvertLeadExtension(ApexPages.StandardController sc) {

		this.sc = sc;
		this.isSalesforce1 = ApexPages.currentPage().getParameters().get('isdtp') == 'p1';

	}

	public void convert() {

		if (leadIsValid()) {

			try {
				
				NMD_LeadConversionManager lcm = new NMD_LeadConversionManager(this.sc.getId());
				Boolean isCreated = lcm.convertLead();

				if (isCreated) {
					newInfoMessage(String.format('{0} <a id="continue_url" href="{1}">{2}</a>{3}.', new List<String> { 
						Label.NMD_Lead_Converted,
						buildViewUrl(lcm.newOpportunityId),
						Label.NMD_Click_Here,
						Label.NMD_To_Continue
					}));
				}
				else {
					newErrorMessage(lcm.lastErrorMessage);
					newInfoMessage(String.format('<a id="continue_url" href="{0}">{1}</a> {2}.', new List<String> { 
						buildViewUrl(this.sc.getId()),
						Label.NMD_Click_Here,
						Label.NMD_To_Continue
					}));
				}

			}
			catch (NMD_LeadConversionManager.ConvertLeadException cle) {

				Lead lead = (Lead)this.sc.getRecord();

				newInfoMessage(String.format('{0} <a id="continue_url" href="{1}">{2}</a>{3}.', new List<String> { 
					cle.getMessage(),
					buildViewUrl(lead.ConvertedOpportunityId),
					Label.NMD_Click_Here,
					Label.NMD_To_Continue
				}));

			}

		}

	}

	private String buildViewUrl(Id recordId) {
		String url = '/';
		if (recordId != null) {
			url = this.isSalesforce1
				? 'javascript:sforce.one.navigateToURL(\'/' + recordId + '\');'
				: new ApexPages.StandardController(recordId.getSObjectType().newSObject(recordId)).view().getUrl();
		}
		return url;
	}

	private Boolean leadIsValid() {

		Lead lead = (Lead)this.sc.getRecord();
		Boolean pass = true;

		if (lead.Patient__c == null) {
			pass = false;
			newErrorMessage(Label.NMD_Patient_Required);
		}
	

		if (lead.Trialing_Physician__c == null && lead.Account_Facility__c == null) {
			pass = false;
			newErrorMessage(Label.NMD_Physician_Required);
		}

		//confirmation
		if (!pass) {
			newInfoMessage(String.format(
				'<a id="continue_url" href="{0}">{1}</a> {2}.',
				new List<String> {
					buildViewUrl(this.sc.getId()),
					Label.NMD_Click_Here,
					Label.NMD_To_Go_Back
				}
			));
		}

		return pass;

	}

}