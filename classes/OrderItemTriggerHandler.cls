/**
* Trigger Handler class for the Order Item Sobject
*
* @Author salesforce Services
* @Date 2015-06-24
*/
public with sharing class OrderItemTriggerHandler extends TriggerHandler {
    
    //final OrderItemManager manager = new OrderItemManager();
    final OrderItemManager manager = OrderItemManager.getInstance();

    /**
    *  @desc    Bulk before method
    */
    public override void bulkBefore() {     
        manager.fetchRelatedRecords((List<OrderItem>)(Trigger.isDelete ? trigger.old : trigger.new));
    }

    public override void bulkAfter() {
        if(trigger.isDelete){
            manager.fetchItemsAfterDelete(trigger.old);
        }
    }

    public override void beforeInsert(SObject obj) {

        OrderItem item = (OrderItem)obj;

        manager.validateExplant(item);
        manager.updateItemPricing(item);
        manager.updateItemNumber(item);
        manager.SetChargeTypeNoChargeReason(item);
        //manager.cloneImplantExplantItem(item);

    }

    public override void beforeUpdate(SObject oldObj, SObject newObj) {

        OrderItem oldItem = (OrderItem)oldObj;
        OrderItem newItem = (OrderItem)newObj;
        manager.validateExplant(oldItem, newItem);
        manager.SetChargeTypeNoChargeReason(newItem);
    }

    public override void afterInsert(SObject obj) {

        OrderItem item = (OrderItem)obj;

        manager.checkSerializedValidity(item);
        manager.updateOrderStageToConfirmed(item);
        manager.checkForUnparentedVirtualKitChildren(item);

    }

    public override void afterUpdate(SObject oldObj, SObject newObj) {

        OrderItem oldItem = (OrderItem)oldObj;
        OrderItem newItem = (OrderItem)newObj;

        manager.checkZ1RejectionReason(oldItem, newItem);
        manager.checkDeliveryBlock(oldItem, newItem);
        manager.checkQuantityConfirmations(oldItem, newItem);
        manager.checkSerializedValidity(oldItem, newItem);
        manager.updateParentBillingBlock(oldItem, newItem);
        manager.checkIfConfirmed(oldItem, newItem);
        manager.updateOrderStageToConfirmed(newItem);

    }
    
    /**
    *  @desc    before Delete method
    */
    public override void beforeDelete(SObject obj) {

        manager.checkDeleteValidity( (OrderItem)obj );

    }

    public override void afterDelete(SObject obj) {
        OrderItem item = (OrderItem)obj;

        manager.checkForVirtualKitChildrenToDelete(item);
        manager.checkAndReorderItemsAfterDelete(item);
    }

    public override void andFinally() {

        manager.createInventoryTransactions();
        manager.createChildTransferOrders();
        //manager.commitClonedItems();
        manager.commitUpdatedParents();
        
OpportunityManager OrderOppymanager = new OpportunityManager();
OrderOppymanager.commitOpportunityProducts();
        
        
        if(trigger.isAfter){
            if(trigger.isInsert){
                manager.relateProductsToVirtualKit();
            }

            if(trigger.isDelete){
                manager.deleteVirtualKitChildren();
                manager.updateReorderedOrderItems();
            }
        }

    }

}