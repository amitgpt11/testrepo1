/*
@CreatedDate     15JUNE2016
@author          Mike Lankfer
@Description     Contains all the required methods to populate the Product.Product_Category value on both the Opty Line Item and Opty Header
@Requirement Id  User Story : DRAFT US2511
*/
public class OpportunityProductCategoryUpdate {
    
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Opportunity.getRecordTypeInfosByName();    
    public final Id RECTYPE_ENDO_CAPITAL_OPTY = RECTYPES.get('Endo Capital Business').getRecordTypeId();
    public final Id RECTYPE_ENDO_DISPOSABLE_OPTY = RECTYPES.get('Endo New Disposable Business').getRecordTypeId();
    
    private Set<Id> affectedProductSet = new Set<Id>();
    private List<Product2> affectedProductList = new List<Product2>();
    private Set<Id> parentOptySet = new Set<Id>();
    private Map<Id, String> checkProductCategory = new Map<Id, String>();
    private List<Opportunity> updateOptyList = new List<Opportunity>();
    private List<OpportunityLineItem> updatedOptyLineItems = new List<OpportunityLineItem>();
    private List<OpportunityLineItem> lineItemList = new List<OpportunityLineItem>();
    
    
    
/*
@CreatedDate     15JUNE2016
@author          Mike Lankfer
@Description     Updates Product Category on the Opportunity Header based on the mix of Product Categories at the Opty Line Item level
@Requirement Id  User Story : DRAFT US2511
*/    
    public void updateOptyHeaderProductCategory(List<OpportunityLineItem> optyLineItems){
        
        //build set of all affected optys
        for (OpportunityLineItem oli : optyLineItems){
            if((oli.Opportunity.RecordTypeId == RECTYPE_ENDO_CAPITAL_OPTY) || (oli.Opportunity.RecordTypeId==RECTYPE_ENDO_DISPOSABLE_OPTY))
            {
                parentOptySet.add(oli.OpportunityId);            
            }
        }
        
        
        //loop through line items and update Map with new header value
        for(OpportunityLineItem oli: [SELECT Id, OpportunityId, Opportunity.Endo_Product_Category__c, Endo_Product_Category__c FROM OpportunityLineItem WHERE OpportunityId IN :parentOptySet])
        {
            if(checkProductCategory.get(oli.OpportunityId)==null)
            {
                checkProductCategory.put(oli.OpportunityId,oli.Endo_Product_Category__c);
            } 
            else
            {
                if(checkProductCategory.get(oli.OpportunityId)!=oli.Endo_Product_Category__c)
                {
                    checkProductCategory.remove(oli.OpportunityId);
                    checkProductCategory.put(oli.OpportunityId,'Multiple');
                }
                
            }
        } 
        
        //build list of Optys to be updated with new Product Category values
        for(Opportunity o1: [Select Id, Endo_Product_Category__c from Opportunity where id in :parentOptySet])
        {
            o1.Endo_Product_Category__c = checkProductCategory.get(o1.Id);
            updateOptyList.add(o1);
        }
        
        
        
        //DML update on opty list
        try{            
            update updateOptyList;        
        }
        
        catch (Exception e){
            System.debug('The following exception has occured: ' + e);
        }
    }
    
    
    
/*
@CreatedDate     15JUNE2016
@author          Mike Lankfer
@Description     Updates Opportunity Line Items with new Product Category
@Requirement Id  User Story : DRAFT US2511
*/
    public void updateOptyLineItemProductCategory(List<Product2> products){
        
        //build list of affected line items
        updatedOptyLineItems = [SELECT Id, Endo_Product_Category__c, Product2Id FROM OpportunityLineItem WHERE (Opportunity.RecordTypeId = :RECTYPE_ENDO_CAPITAL_OPTY OR Opportunity.RecordTypeId = :RECTYPE_ENDO_DISPOSABLE_OPTY) AND Product2Id IN :products];
        
        //loop through line item list, updating with new Product Category value from the affected Product list
        for (OpportunityLineItem oli: updatedOptyLineItems){
            for (Product2 product: products){
                if (oli.Product2Id == product.Id){
                    oli.Endo_Product_Category__c = product.Endo_Product_Category__c;
                    lineItemLIst.add(oli);
                    break;
                }
            }
        }
        
        //DML update on opty line item list
        try{            
            update lineItemLIst;        
        }
        
        catch (Exception e){
            System.debug('The following exception has occured: ' + e);
        }       
    }
    
   
    
    
/*
@CreatedDate     15JUNE2016
@author          Mike Lankfer
@Description     Updates Opportunity Line Items with product category before Insert
@Requirement Id  User Story : DRAFT US2511
*/    
    public void updateOptyLineItemProductCategory(List<OpportunityLineItem> optyLineItems){
        
        //build Set of Products that intersects with OptyLineItem Products
        for (OpportunityLineItem oli:optyLineItems){
            if((oli.Opportunity.RecordTypeId == RECTYPE_ENDO_CAPITAL_OPTY) || (oli.Opportunity.RecordTypeId==RECTYPE_ENDO_DISPOSABLE_OPTY))
            {
                affectedProductSet.add(oli.Product2Id); 
            }           
        }
        
        //build List of Products that intersects with OptyLineItem Products
        if(affectedProductSet!=null)
        {
            affectedProductList = [SELECT Id, Endo_Product_Category__c FROM Product2 WHERE Id IN :affectedProductSet];
        }
        
        ////loop through line item list, updating with new Product Category value from the affected Product list 
        for (OpportunityLineItem oli:optyLineItems){
            for (Product2 product:affectedProductList){
                if (oli.Product2Id == product.Id){
                    oli.Endo_Product_Category__c = product.Endo_Product_Category__c;
                }
            }
        }        
    }
}