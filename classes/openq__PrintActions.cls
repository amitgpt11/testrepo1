/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PrintActions {
    global PrintActions(Object sc) {

    }
    @RemoteAction
    global static String directInsertAttachmentImg(List<String> base64ChunkArrayTmp) {
        return null;
    }
    @RemoteAction
    global static String insertAttachmentImg() {
        return null;
    }
    @RemoteAction
    global static void loadBase64BeforePrint(String chunk) {

    }
}
