/**
* Wrapper object for application logs.  This class is used in conjunction with the Logger framework.
*
* @Author Peeyush Awadhiya
* @Created 02/06/2014
*/
public class ApplicationLogWrapper {
    
    /**
	* Default Constructor.
	*
	* 
	*/
    
    public ApplicationLogWrapper() {
        this.timestamp = Datetime.now();
    }
    
    /**
    * Parameterized Constructor.
    * 
    * @param severity - String
    * @param sourceClass - String
    * @param sourceFunction - String
    */
    
    public ApplicationLogWrapper(String severity, String sourceClass, String sourceFunction) {
        this.timestamp = Datetime.now();
        this.debugLevel = severity;
        this.source = sourceClass;
        this.sourceFunction = sourceFunction;
    }

    /**
    * Parameterized Constructor.
    * 
    * @param severity - String
    * @param sourceClass - String
    * @param sourceFunction - String
    */
    
    public ApplicationLogWrapper(String severity, String sourceClass, String sourceFunction, String logMessage) {
        this.timestamp = Datetime.now();
        this.debugLevel = severity;
        this.source = sourceClass;
        this.sourceFunction = sourceFunction;
        this.logMessage = logMessage;
    }

    /**
    * Parameterized Constructor.
    * 
    * @param error - Database.Error
    * @param recordId - Id
    * @param source - String
    */
    
    public ApplicationLogWrapper(Database.Error err, Id recordId, String source) {

        List<String> sources = source.split('.');
        while (sources.size() < 2) {
            sources.add('');
        }

        this.timestamp = Datetime.now();
        this.debugLevel = 'Error';
        this.source = sources[0];
        this.sourceFunction = sources[1];
        this.recordId = recordId;
        this.objectType = recordId != null ? recordId.getSobjectType().getDescribe().getName() : '';
        this.logCode = String.valueOf(err.getStatusCode());
        this.logMessage = err.getMessage();
    }
    
    // Name of the artifact (class, trigger) being logged
    public String source {get;set;}
    
    // Name of the method/function being logged 
    public String sourceFunction {get;set;}
    
    // Salesforce ID of the record of interest
    public String recordId {get;set;}
    
    // SObject type
    public String objectType{get;set;}
    
    // Log message
    public String logMessage {get;set;}
    
    // XML payload for integration logs
    public String payload {get;set;}
    
    // Severity of the message being logged (see Logger for constants)
    public String debugLevel {get;set;}
    
    // error code
    public String logCode {get;set;}
    
    // Exception being logged
    public Exception ex {get;set;}
    
    // Duration of the operation being logged, used in performance logging
    public Long timer {get;set;}
    
    // Timestamp of the logged message, used in asynchronous logging
    public DateTime timestamp {get; set;}
}