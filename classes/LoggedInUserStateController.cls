public class LoggedInUserStateController {
    
    public String strAddress {get;set;}
    public String strState   {get;set;}
        
    public String strHiddenVar{
        get{
            fetchStateDetails();
            return  null;
        }
        set;
    }
    
    @testVisible
    private void fetchStateDetails(){
        
       strState = '';
       
       List<User> loggedInUser = new List<User>([SELECT TimeZoneSidKey, contact.Account.BillingState, contact.Account.ShippingState
                                               FROM User 
                                               WHERE (NOT TimeZoneSidKey like '%Europe%') 
                                               AND Id =: UserInfo.getUserId() ]);
       
       
       if(!loggedInUser.isEmpty()){
           
            
            if(strAddress=='billing'){
                
                strState = loggedInUser[0].contact.Account.BillingState;
            }else{
                
                strState = loggedInUser[0].contact.Account.ShippingState;
            }
       }
    }
}