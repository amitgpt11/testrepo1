@isTest
private class HomePageAuthenticatedController_Test{

	private static testMethod void test() {
	    
	    //Create Account
	    Account objAccount = Test_DataCreator.createAccount('testAccount');
	    
	    //Create Contact with parent Account as testAccount
	    Contact objContact = Test_DataCreator.createContact('testLastName', objAccount.Id);
	    
	    //Create Guest User
        User objUser = Test_DataCreator.createGuestUser();
        
        HomePageAuthenticatedController objHomePageAuthenticatedController = new HomePageAuthenticatedController(); 
        system.Test.setCurrentPage(Page.UnAuthenticatedHomePage);
        
        system.runAs(objUser){
            
            // Guest user will be redirected to UnAuthenticatedHome Page.
            system.assertNotEquals(null, objHomePageAuthenticatedController.init());
        }
	    
	    //Create the Product
	    Product2 objProd = Test_DataCreator.createProduct('productname');
	    
	    objHomePageAuthenticatedController = new HomePageAuthenticatedController(); 
	    Boston_Scientific_Config__c objBostonConfig = Test_DataCreator.cerateBostonCSConfigTestData(null);
	    objBostonConfig.Boston_AuthHome_Endo_FeaturedPrduct_Id__c = objProd.Id;
	    objBostonConfig.Boston_CardioFeaturedEvent_logoId__c='test';
	    objBostonConfig.Boston_CardioFeaturedProduct_logoId__c = 'test';
	    update objBostonConfig;
	    objHomePageAuthenticatedController.init();
	    
	    
	    
	    //Create Community_Event__c with display name as 'test Community Event'
	    Community_Event__c objCommunity_Event = Test_DataCreator.createCommunityEvent('test Display Name');
	    
	    objBostonConfig.Boston_Document_Content_Base_Url__c = '';
	    update objBostonConfig;
	    objHomePageAuthenticatedController.init();
	    
	    //create Boston scientific cofig test data using Test_DataCreator class
        Test_DataCreator.cerateBostonCSConfigTestData(objCommunity_Event.Id); 
        objHomePageAuthenticatedController.init();
        
        system.assertNotEquals(objHomePageAuthenticatedController.objFeaturedProduct,null);
        system.assertNotEquals(objHomePageAuthenticatedController.objFeaturedEvent,null);
    }
}