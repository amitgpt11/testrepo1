/*
 @CreatedDate     10 June 2016                                  
 @author          Ashish-Accenture
 @Description     Batch class is used to link Product record with its parent Product record by populating self lookup field on Product.
       
*/

public class batchProductHierarchy implements Database.Batchable<sObject>, Schedulable{
    
     // Schedulable method, executes the class instance
    
    Public void execute(SchedulableContext context) {
        Database.executeBatch(new batchProductHierarchy(), 200);
    }
    
    Set<Id> rtIdSet;   // set is used to store product record type Ids
    
    //fetch All the Product record type which are having name started with 'Standard Product Hierarchy - Level'
    public batchProductHierarchy(){
        Map<id,RecordType> prRTMap = new Map<id, RecordType>([Select Id,name From RecordType where sobjecttype = 'Product2' and name like 'Standard Product Hierarchy - Level%' ]);
        rtIdSet = prRTMap.keyset();
    }
    
    //Start method, fetch product records which do not have Parent product assigned/identified
    public Database.QueryLocator start(Database.BatchableContext BC) {
       
        String query = 'Select id, name,Level_1__c, Level_2__c, Level_3__c, Level_4__c, Level_5__c, Product_Hierarchy__c, Shared_Parent_Product__c from Product2 where Product_Hierarchy__c != null AND Shared_Parent_Product__c = null AND recordTypeId in : rtIdSet';
        return database.getquerylocator(query);
    }
    
    // Execute method, Identifies the Parent product for product record and populate Parent Product field. 
    // Hence, Parent record assigned for Product record. 
    public void execute(Database.BatchableContext BC, List<Product2> scope) {
         
         try{
             Map<Id,String> mapIdParentProductHierarchy = new Map<Id,String>();
             Set<String> setParentProductHierarchy = new Set<String>();
           
             for(Product2 s : scope){
                 integer i = 5;
                 while(s.get('Level_'+i+'__c') == null || s.get('Level_'+i+'__c') == ''){
                     i--;
                 }
                 String pHierarchy = '';
                 for(integer j=1;j<i;j++ ){
                     if(s.get('Level_'+j+'__c') != null){
                         pHierarchy += s.get('Level_'+j+'__c');
                     }
                 }
                 if(String.IsNotEmpty(pHierarchy)){
                     mapIdParentProductHierarchy.put(s.id,pHierarchy);
                     setParentProductHierarchy.add(pHierarchy);
                    
                 }   
             }
            
             list<Product2> lstProduct = [Select id, name,Product_Hierarchy__c from Product2 where Product_Hierarchy__c In : setParentProductHierarchy AND recordTypeId in : rtIdSet limit 9999 ];
             
             Map<String,Id> mapProductHierarchyId = new Map<String,Id>();
             for(Product2 pr: lstProduct){
                 mapProductHierarchyId.put(pr.Product_Hierarchy__c,pr.Id);
             }
                          
             if(mapProductHierarchyId != null && mapProductHierarchyId.size() > 0){
                 list<Product2> lstProductToUpdate = new list<Product2>();
                 Product2 objProductToUpdate;
                 for(Id prId : mapIdParentProductHierarchy.keyset()){
                     objProductToUpdate = new Product2();
                     objProductToUpdate.Id = prId;
                     objProductToUpdate.Shared_Parent_Product__c = mapProductHierarchyId.get(mapIdParentProductHierarchy.get(prId));
                     lstProductToUpdate.add(objProductToUpdate);
                 }
                 
                 if(lstProductToUpdate.size() > 0){
                     
                     update lstProductToUpdate;
                 }
             }
         }Catch(exception e){
             System.debug('####====e====='+e);
         }
    }
    
    public void finish(Database.BatchableContext BC){
    
    }

    
}