/*
 @CreatedDate     10 June 2016                                  
 @author          Ashish-Accenture
 @Description     Test class for Batch Apex class 'batchProductHierarchy'
 
 */

@isTest
public class batchProductHierarchyTest{

    static TestMethod void TestbatchProductHierarchy(){ 
        
        // Create Product record
        TestDataFactory.createTestProduct('213456789012345678','Standard Product Hierarchy - Level 5');
        TestDataFactory.createTestProduct('2134567890123','Standard Product Hierarchy - Level 4');
        TestDataFactory.createTestProduct('213456789','Standard Product Hierarchy - Level 3');
        TestDataFactory.createTestProduct('21345','Standard Product Hierarchy - Level 2');
        TestDataFactory.createTestProduct('21','Standard Product Hierarchy - Level 1');
        
        Test.startTest();
            //Execute Batch Class
            ID batchprocessid = Database.executeBatch(new batchProductHierarchy());
            System.assertNotEquals(batchprocessid,null);
            
            //Execute Schedulable Method
            batchProductHierarchy sh1 = new batchProductHierarchy();
            String sch = '0 0 23 * * ?'; 
            String jobId = system.schedule('Product Hierarchy Batch', sch, sh1); 
            System.assertNotEquals(jobId,null); 
            
        Test.stopTest();
        
        // Verification code of the batch class output
        list<Product2> lstPrd = [Select id, name,Level_1__c, Level_2__c, Level_3__c, Level_4__c, Level_5__c, Product_Hierarchy__c, Shared_Parent_Product__c from Product2];
        Map<String,Product2> mapHierarchyProduct = new map<String,Product2>();
        for(Product2 prd : lstPrd){
            mapHierarchyProduct.put(prd.Product_Hierarchy__c,prd);
        }
        for(Product2 prd : lstPrd){
             integer i = 5;
             while(prd.get('Level_'+i+'__c') == null || prd.get('Level_'+i+'__c') == ''){
                 i--;
             }
             String pHierarchy = '';
             for(integer j=1;j<i;j++ ){
                 if(prd.get('Level_'+j+'__c') != null){
                     pHierarchy += prd.get('Level_'+j+'__c');
                 }
             }
             
             if(mapHierarchyProduct != null && pHierarchy != null && pHierarchy != ''){
                 System.assertequals(prd.Shared_Parent_Product__c,mapHierarchyProduct.get(pHierarchy).Id);
             }
                 
        }
        
    }
}