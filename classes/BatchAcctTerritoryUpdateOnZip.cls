/*
 @CreatedDate     30JUN2016                                  
 @ModifiedDate    30JUN2016                                  
 @author          Mayuri-Accenture
 @Description     Batch Apex class to re-assign Account to its territory based on Zip codes in Zip_To_Territory records. 
 @Requirement Id  Q3 2016 - US2701 
 */
 
 global class BatchAcctTerritoryUpdateOnZip implements Database.Batchable<sObject> { 
    
      
    static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Zip_To_Territory__c.getRecordTypeInfosByName();
    public static final Id RECTYPE_URO = RECTYPES.get('UroPH').getRecordTypeId();
    global Database.QueryLocator start(Database.BatchableContext BC) {
    Date dt;
    if(Test.isRunningTest()){  
       dt = system.today();
    }
    else{
      dt = system.today()-Integer.valueOf(system.Label.ETM_Batch_Integer_Subtractor_For_Date);
    }
    
    String query =  'select ' +
                    'UroPH_Delete__c, ' +
                    'UroPH_Territory_Name__c, ' +
                    'UroPH_Zip_Codes__c ' +
                    'from Zip_To_Territory__c ' +
                    'where UroPH_Territory_Name__c != null ' +
                    'and UroPH_Zip_Codes__c != null ' +
                    'and RecordTypeId =: RECTYPE_URO ';
        if(Test.isRunningTest()){            
               query += 'and LastModifiedDate =: dt';
       }
       else{
            query += 'and LastModifiedDate >: dt';
       }            
                   
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Zip_To_Territory__c> scope) {
     
        set<Zip_To_Territory__c> remAcctTerritoryLst = new set<Zip_To_Territory__c>();
        set<Zip_To_Territory__c> nonRemAcctTerritoryLst = new set<Zip_To_Territory__c>();
        UtilUroAccountZipTerritoryUpdate util = new UtilUroAccountZipTerritoryUpdate();
        boolean isSuccess;
        
         system.debug(scope+'-1-2-3');
         for(Zip_To_Territory__c a : scope)
         {               
            if(a.UroPH_Delete__c == true){                
                remAcctTerritoryLst.add(a);
            }
            if(a.UroPH_Delete__c == false){                
                nonRemAcctTerritoryLst.add(a);
            }
                      
         } 
         
         if(remAcctTerritoryLst.size() > 0){
             isSuccess = util.UroAccountTerritoryUpdate(remAcctTerritoryLst,null,true);
         }
         system.debug(isSuccess+'@123');
         if(isSuccess = true && nonRemAcctTerritoryLst.size() > 0){
             isSuccess = util.UroAccountTerritoryUpdate(null,nonRemAcctTerritoryLst,false);
         }
         
     
    }   
    
    global void finish(Database.BatchableContext BC) {
            
    }
}