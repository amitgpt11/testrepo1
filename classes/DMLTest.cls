@IsTest(SeeAllData=false)
private class DMLTest {

	final static NMD_TestDataManager td = new NMD_TestDataManager();
	
	static testMethod void test_SaveUpdateDeleteRestore1() {

		Account acct = td.newAccount();
		DML.save(null, acct);

		acct.Name = 'foo';
		DML.save(null, acct);

		DML.remove(null, acct);

		DML.restore(null, acct);

	}

	static testMethod void test_SaveUpdateDeleteRetore2() {

		List<Account> accts = new List<Account> {
			(td.newAccount()),
			(td.newAccount())
		};

		DML.save(null, accts);

		accts[0].Name = 'foo';
		accts[1].Name = 'bar';
		DML.save(null, accts);

		DML.remove(null, accts);

		DML.restore(null, accts);

	}

	static testMethod void test_Exception1() {

		Account acct = td.newAccount();
		acct.RecordTypeId = AccountManager.RECTYPE_CONSIGNMENT;
		acct.Name = '';

		try {
			DML.save(null, acct, true);
			System.assert(false);
		}
		catch(System.DmlException de) {
			System.assert(true);
		}

		DML.save(null, acct, false);

	}

}