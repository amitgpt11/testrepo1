/*
-------10/29/2015 - Mike Lankfer - Created AccountPerformanceTriggerMethods class to hold methods that will be called from the AccountPerformance trigger.  Added updateParentAccount method.
*/

public class AccountPerformanceTriggerMethods {
    
    //Updates the divisonal QBR, RR Revenue Amount, Revenue Potential fields as well as the National Tiering on the Account Performance Record's Parent Account.  Called after insert, after update, after delete, and after undelete.
    public static void updateParentAccount(List<Account_Performance__c> aps){
        
        Set<String> divisions = new Set<String>{'CRM', 'EP', 'IC', 'PI', 'Uro/WH'};  
        Set<Id> parentAccountIdSet = new Set<Id>();      
        
                
        //Build Set of Parent Account Ids from list of Account Performance records that match the division in the "divisions" Set
        for(Account_Performance__c ap1 : aps){
            if(divisions.contains(ap1.Division__c)){
                parentAccountIdSet.add(ap1.Facility_Name__c);
            }
        }
        
        
        //Create new Map<Id, Account> populated with parent accounts of AP records in the trigger context
        Map<Id, Account> parentAccountMap = new Map<Id, Account>([SELECT Id,National_Tiering_CRM__c,National_Tiering_EP__c,National_Tiering_IC__c,National_Tiering_PI__c,National_Tiering_U_WH__c FROM Account WHERE ID IN :parentAccountIdSet]);
        
        
        //loop through AP records in trigger context and set the parent account values
        for(Account_Performance__c ap2 : aps){
            //CRM
            if (ap2.Division__c == 'CRM'){
                //if the trigger is fired on insert, update, or undelete action the parent account divisional values are updated from the corresponding fields on the AP record
                if(Trigger.isDelete == False){
                    parentAccountMap.get(ap2.Facility_Name__c).National_Tiering_CRM__c = ap2.National_Tiering__c;                    
                    
                }
                //if the trigger is fired on a delete action, the parent account divisional values for Tiering, Rev. Potential, and RR Rev are set to NULL, with QBR set to N
                else {
                    parentAccountMap.get(ap2.Facility_Name__c).National_Tiering_CRM__c = NULL;                    
                }
            }
            
            //EP
            if (ap2.Division__c == 'EP'){
                //if the trigger is fired on insert, update, or undelete action the parent account divisional values are updated from the corresponding fields on the AP record
                if(Trigger.isDelete == False){
                    parentAccountMap.get(ap2.Facility_Name__c).National_Tiering_EP__c = ap2.National_Tiering__c;                    
                }
                //if the trigger is fired on a delete action, the parent account divisional values for Tiering, Rev. Potential, and RR Rev are set to NULL, with QBR set to N
                else {
                    parentAccountMap.get(ap2.Facility_Name__c).National_Tiering_EP__c = NULL;                    
                }
            }
            
            //IC
            if (ap2.Division__c == 'IC'){
                //if the trigger is fired on insert, update, or undelete action the parent account divisional values are updated from the corresponding fields on the AP record
                if(Trigger.isDelete == False){
                    parentAccountMap.get(ap2.Facility_Name__c).National_Tiering_IC__c = ap2.National_Tiering__c;     

                }
                //if the trigger is fired on a delete action, the parent account divisional values for Tiering, Rev. Potential, and RR Rev are set to NULL, with QBR set to N
                else {
                    parentAccountMap.get(ap2.Facility_Name__c).National_Tiering_IC__c = NULL;                    
                }
            }
            
            //PI
            if (ap2.Division__c == 'PI'){
                //if the trigger is fired on insert, update, or undelete action the parent account divisional values are updated from the corresponding fields on the AP record
                if(Trigger.isDelete == False){
                    parentAccountMap.get(ap2.Facility_Name__c).National_Tiering_PI__c = ap2.National_Tiering__c;
                }
                //if the trigger is fired on a delete action, the parent account divisional values for Tiering, Rev. Potential, and RR Rev are set to NULL, with QBR set to N
                else {
                    parentAccountMap.get(ap2.Facility_Name__c).National_Tiering_PI__c = NULL;
                }
            }            
            
            //Uro/WH
            if (ap2.Division__c == 'Uro/WH'){
                //if the trigger is fired on insert, update, or undelete action the parent account divisional values are updated from the corresponding fields on the AP record
                if(Trigger.isDelete == False){
                    parentAccountMap.get(ap2.Facility_Name__c).National_Tiering_U_WH__c = ap2.National_Tiering__c;

                }
                //if the trigger is fired on a delete action, the parent account divisional values for Tiering, Rev. Potential, and RR Rev are set to NULL, with QBR set to N
                else {
                    parentAccountMap.get(ap2.Facility_Name__c).National_Tiering_U_WH__c = NULL;
                }
            }            
        }
        
        update parentAccountMap.values();        
        
    }
    
}