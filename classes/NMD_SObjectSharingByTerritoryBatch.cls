/**
* Shares objects by the related Territory and its Assignees
*
* @Author salesforce Services
* @Date 06/12/2015
*/
global without sharing class NMD_SObjectSharingByTerritoryBatch implements Database.Batchable<SObject> {
	
	/***  @desc	Immediately executes an instance of this batch*/
	public static Id runNowContacts() {
		return runNow(Contact.getSObjectType());
	}

	/***  @desc	Immediately executes an instance of this batch*/
	public static Id runNowLeads() {
		return runNow(Lead.getSObjectType());
	}

	/***  @desc	Immediately executes an instance of this batch*/
	public static Id runNowOpportunities() {
		return runNow(Opportunity.getSObjectType());
	}

	/***  @desc	Immediately executes an instance of this batch*/
	public static Id runNowOrders() {
		return runNow(Order.getSObjectType());
	}

	/***  @desc	Immediately executes an instance of this batch*/
	public static Id runNowPatients() {
		return runNow(Patient__c.getSObjectType());
	}

	public static Id runNow(SObjectType sobjType) {
		//If running test create ContactHistory records to loop over in execute method
		//	since they will not be created within the test
		if(Test.isRunningTest()){
			NMD_SObjectSharingByTerritoryBatch sbatch = new NMD_SObjectSharingByTerritoryBatch(sobjType);

			if(Contact.getSObjectType() == sbatch.objType){
				List<ContactHistory> conHistory = new List<ContactHistory>();
				for(Contact con : [SELECT Id FROM Contact LIMIT 100]){
					ContactHistory ch = new ContactHistory(ContactId = con.Id, Field = 'Territory_ID__c');
					conHistory.add(ch);
				}
				sbatch.execute(null, conHistory);
			}

			if(Patient__c.getSObjectType() == sbatch.objType){
				List<Patient__History> history = new List<Patient__History>();
				for(Patient__c pat : [SELECT Id FROM Patient__c LIMIT 100]){
					Patient__History ph = new Patient__History(ParentId = pat.Id, Field = 'Territory_ID__c');
					history.add(ph);
				}
				sbatch.execute(null, history);
			}
		}

		return Database.executeBatch(new NMD_SObjectSharingByTerritoryBatch(sobjType));	
	}

	//init record type map
    private static Map<SObjectType,Set<Id>> recTypeMap = new Map<SObjectType,Set<Id>> {
        (Patient__c.getSObjectType()) => PatientManager.RECTYPES_NMD,
        (Contact.getSObjectType()) => ContactManager.RECTYPES_NMD,
        (Lead.getSObjectType()) => LeadManager.RECTYPES_NMD,
        (Opportunity.getSObjectType()) => OpportunityManager.RECTYPES_NMD_ALL,
        (Order.getSObjectType()) => new Set<Id> { OrderManager.RECTYPE_PATIENT } // ONLY patient order types
    };
    
    
    public String query;

    private String parentId = 'ParentId';
    private Set<Id> objectRecordTypeSet = new Set<Id>();
    private SObjectType objType;
    private String objName;
	
	global NMD_SObjectSharingByTerritoryBatch(SObjectType objType) {

		this.objType = objType;
		this.objName = objType.getDescribe().getName();
		this.objectRecordTypeSet.addAll(recTypeMap.get(objType));


		if (this.objName.endsWith('__c')) {
			this.objName = this.objName.replace('__c', '') + '__History';
		}
		else {
			this.parentId = this.objName + 'Id';
			// special case, standard object histories are [Object]History, except Opportunities, which
			// has the name [Object]FieldHistory
			this.objName += (objType == Opportunity.getSObjectType() ? 'Field' : '') + 'History';
		}

		System.debug('***Type: ' + this.objType);
		System.debug('***Name: ' + this.objName);
		System.debug('***RecordTypes: ' + this.objectRecordTypeSet);
		System.debug('***ParentId field: ' + this.parentId);

		this.query = 
			'select ' +
				parentId + ' ' + 
			'from ' + this.objName + ' ' +
			'where Field = \'Territory_ID__c\' ' +
			'and CreatedDate = LAST_N_DAYS:1 ';

		if (Test.isRunningTest()) {
			this.query += 'order by CreatedDate desc limit 100';
		}
	}

	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(this.query);
	}

	/***  @desc	Database.Bachable method*/
   	global void execute(Database.BatchableContext bc, List<sObject> scope) {
		
		Set<Id> parentIds = new Set<Id>();			
		//populate data
		for(SObject hist : scope) {
			parentIds.add((Id)String.valueOf(hist.get(parentId)));
		}

		List<SObject> parents = new List<SObject>(Database.query(
			'select ' +
				(this.objType == Order.getSObjectType() 
                    ? 'Opportunity.Territory_ID__c, ' + 
                      'Opportunity.Territory_ID__r.Current_TM1_Assignee__c, ' +
                      'Shipping_Location__c, ' +
                      'Receiving_Location__c, '
                    : ''
                ) +
				'RecordTypeId, ' +
				'Territory_ID__c ' +
			'from ' + this.objType + ' ' +		//I think this should be this.objType for Contact instead of ContactHistory, which has no RT
			'where Id in :parentIds ' +
			'and RecordTypeId in :objectRecordTypeSet '
		));

		if (this.objType == Contact.getSObjectType()) {
            new ContactManager().contactSharing(parents);
        }
        else if (this.objType == Lead.getSObjectType()) {
            new LeadManager().leadSharing(parents);
        }
        else if (this.objType == Patient__c.getSObjectType()) {
            new PatientManager().patientSharing(parents);
        }
        else if (this.objType == Opportunity.getSObjectType()) {
            new OpportunityManager().opportunitySharing(parents);
        }
        else if (this.objType == Order.getSObjectType()) {
            new OrderManager().orderSharing(parents);
        }

	}
	
	/***  @desc	Schedulable method, executes the class instance*/
	global void execute(SchedulableContext context) {

		Database.executeBatch(this);
	}

	global void finish(Database.BatchableContext bc) {

	}

}