/*
 @CreatedDate     15 June 2016                                  
 @author          Ashish-Accenture
 @Description     This class is used to sort the  Lutonix SRAI Detail records. It is a component Controller. It is used in Visualforce email templare.
       
*/
public with sharing class sortSRAIDetailComponentController{
    public list<PI_Lutonix_SRAI_Detail__c> lstSRAIDetail;
    public string SRAIHeaderId{get;set;}
    public string SRAIHeaderMonth{get;set;}
    public string SRAIHeaderYear{get;set;}
    public list<PI_Lutonix_SRAI_Detail__c> getlstSRAIDetail(){
        
        lstSRAIDetail = [Select id, PI_Date__c, PI_Lutonix_SRAI__c,PI_Recorded_Temperature__c,PI_Temperature_Scale__c,PI_Units_of_Trunk_Stock__c from PI_Lutonix_SRAI_Detail__c where PI_Lutonix_SRAI__c=: SRAIHeaderId order by PI_Date__c ASC];
        list<PI_Lutonix_SRAI_Detail__c> lstDisplayLutonixSRAIDetail = new list<PI_Lutonix_SRAI_Detail__c>();
        if(lstSRAIDetail.size() > 0){
            Map<date,PI_Lutonix_SRAI_Detail__c> mapDateDetailRec= new Map<date,PI_Lutonix_SRAI_Detail__c>();
            for(PI_Lutonix_SRAI_Detail__c det : lstSRAIDetail){
                mapDateDetailRec.put(det.PI_Date__c,det);        
            }  
            map<String,Integer> MapNoOfMonth = UtilityController.getMapNoOfMonth();
            date firstDateOfmonth = Date.newInstance(integer.ValueOf(SRAIHeaderYear), MapNoOfMonth.get(SRAIHeaderMonth), 1);
         //   integer daysInMonth = date.daysInMonth(integer.ValueOf(SRAIHeaderYear), MapNoOfMonth.get(SRAIHeaderMonth) );
            
           
            PI_Lutonix_SRAI_Detail__c objDisplayLutonixSRAIDetail; 
            
            for(integer i =0;i<31;i++){
               
                if(mapDateDetailRec.get(firstDateOfmonth + i) != null){
                    objDisplayLutonixSRAIDetail = mapDateDetailRec.get(firstDateOfmonth + i);
                    
                }else{
                    objDisplayLutonixSRAIDetail = new PI_Lutonix_SRAI_Detail__c();
                    objDisplayLutonixSRAIDetail.PI_Date__c = firstDateOfmonth + i;
                    //objDisplayLutonixSRAIDetail.PI_Temperature_Scale__c = 'F';
                }
                lstDisplayLutonixSRAIDetail.add(objDisplayLutonixSRAIDetail);
            }
            
        }
        
        return lstDisplayLutonixSRAIDetail;
    }
    public sortSRAIDetailComponentController(){
        
    }
    
}