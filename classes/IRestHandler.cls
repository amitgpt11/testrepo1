/**
* Interface for the REST Handlers
*
* @Author salesforce Services
* @Date 06/12/2015
*/
global interface IRestHandler {

	String getURIMapping();
	void execute(IRestDispatcher caller, Map<String,String> parameters, Blob requestBody);

}