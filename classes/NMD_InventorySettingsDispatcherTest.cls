@IsTest(SeeAllData=false)
private class NMD_InventorySettingsDispatcherTest {

	static testMethod void test_InventorySettingsDispatcher() {

		NMD_TestDataManager td = new NMD_TestDataManager();
		td.createInventorySettings();

		RestContext.response = new RestResponse();
		RestDispatcherV1 dispatcher = new RestDispatcherV1(null);

		NMD_InventorySettingsDispatcher agent = new NMD_InventorySettingsDispatcher();
		String uri = agent.getURIMapping();
		agent.execute(dispatcher, null, null);

	}

}