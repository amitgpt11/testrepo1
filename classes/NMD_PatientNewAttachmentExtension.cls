/**
* Extension class for the VF page PatientNewAttachment
*
* @Author salesforce Services
* @Date 03/04/2015
*/
public with sharing class NMD_PatientNewAttachmentExtension extends NMD_ExtensionBase {
	
	final ApexPages.StandardController sc;

	public final List<SelectOption> classifications { get; private set; }
	public String classification { get; set; }

	public NMD_PatientNewAttachmentExtension(ApexPages.StandardController sc) {
		this.sc = sc;

		this.classification = '';
		this.classifications = new List<SelectOption> { new SelectOption(this.classification, Label.NMD_Select_One) };

		for (PatientImageClassifications__c imgClass : [
			select Name
			from PatientImageClassifications__c
			where Is_Active__c = true
			order by Order__c
		]) {

			this.classifications.add(new SelectOption(imgClass.Name, imgClass.Name));

		}

		if (sc.getId() == null) {
			newErrorMessage('Invalid/Missing Patient record.');
		}

	}

	public PageReference doRedirect() {

		Opportunity oppty = (Opportunity)this.sc.getRecord();

		PageReference pr = ApexPages.currentPage().getParameters().get('isdtp') == 'p1'
			? Page.PatientNewAttachmentSF1
			: Page.PatientNewAttachment;

		pr.getParameters().putAll(new Map<String,String> {
			'id' => oppty.Patient__c,
			'retURL' => new ApexPages.StandardController(oppty).view().getUrl()
		});

		return pr;

	}

	public PageReference checkRedirect() {

		PageReference pr;
		if (ApexPages.currentPage().getParameters().get('isdtp') != 'p1') {
			pr = Page.PatientNewAttachment;
			pr.getParameters().put('id', this.sc.getId());
		}

		return pr;
	}

}