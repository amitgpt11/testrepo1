/**
* Manager class for the Revenue Schedule Sobject
*
* @Author Vikas Malik
* @Date 2016-07-01 
*/
public with sharing class RevenueScheduleManager {
	
	// Singleton pattern
    private static RevenueScheduleManager instance;
    
    public static RevenueScheduleManager getInstance() {
        if (instance == null) {
            instance = new RevenueScheduleManager();
        }
        return instance;
    }
    
    public static Boolean isScheduleUpdate = false;
    public static Boolean isScheduleDelete = false;
    private List<Opportunity_Revenue_Schedule__c> schRecords = new List<Opportunity_Revenue_Schedule__c>();
    
    
    /**
	* This method fetches the Revenue Details for the Schedule object
	*
	*/
    public void updateRevenueAmount(Map<Id,SObject> newrevSchedulesMap, Map<Id,SObject> oldrevScheduleMap){
    	Set<Id> revDetailsId = new Set<Id>();
    	
    	for (SObject sch : newrevSchedulesMap.values()){
    		if (oldrevScheduleMap == null || ((Schedule__c)sch).Revenue__c != ((Schedule__c)oldrevScheduleMap.get(sch.Id)).Revenue__c){
    			isScheduleUpdate = true;
    			revDetailsId.add(((Schedule__c)sch).Revenue_Schedule__c);
    		}
   	 	}
   	 	if (!revDetailsId.isEmpty()){
   	 		updateRevDetailsRecords(revDetailsId);
   	 	}
    }
    
    
    public void processSchDelete(List<Schedule__c> delSchedules){
    	Set<Id> revDetailsId = new Set<Id>();
    	for (Schedule__c sch : delSchedules){
    		isScheduleDelete = true;
    		revDetailsId.add(sch.Revenue_Schedule__c);
   	 	}
    	if (!revDetailsId.isEmpty()){
   	 		updateRevDetailsRecords(revDetailsId);
   	 	}
    }
    
    
    private void updateRevDetailsRecords(Set<Id> revDetailsId){
    	Map<String,Double> recDetailAmtMap = new Map<String,Double>();
   	 	AggregateResult[] revAmtTotals = [SELECT Revenue_Schedule__c,SUM(Revenue__c) revAmt FROM Schedule__c WHERE Revenue_Schedule__c IN :revDetailsId GROUP BY Revenue_Schedule__c];
        for (AggregateResult ar : revAmtTotals){
        	Opportunity_Revenue_Schedule__c schd = new Opportunity_Revenue_Schedule__c(Id = (String)ar.get('Revenue_Schedule__c'),Shared_Opportunity_Revenue__c = (Double)(ar.get('revAmt')));
            this.schRecords.add(schd);
        }
    }
    
    
    /**
	* This method updates the new Revenue Detail records
	*
	*/
    public void commitRevenueDetails(){
        if (this.schRecords.isEmpty()){
            return;
        }
        
        try {
            DML.save(this, this.schRecords);
        }
        catch (System.DmlException de) {
            
        }
    }
    
}