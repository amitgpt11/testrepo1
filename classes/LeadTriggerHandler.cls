/**
* TriggerHandler class for the Lead object
*
* @Author salesforce Services
* @Date 03/23/2015
*/
public with sharing class LeadTriggerHandler extends TriggerHandler{

    final LeadManager manager = new LeadManager();
    
    /**
    * Called in bulk for before triggers
    * 
    * @param void
    */
    public override void bulkBefore() {

        List<Lead> leads = (List<Lead>)trigger.new;
        manager.fetchLeadTerritories(leads);
        manager.fetchLeadPatients(leads);       

    }

    /**
    * Called in bulk for after triggers
    * 
    * @param void
    */
    public override void bulkAfter() {
        //Set<Id> leadIds = trigger.newMap.keySet();
        if (trigger.isInsert) {
            manager.leadSharing(trigger.new);
        }
    }
    

    /**
    * Called once per record
    * 
    * @param void
    */
    public override void beforeInsert(SObject obj) {

        Lead lead = (Lead)obj;
        manager.setLeadSource(lead);
        manager.insertLeadOwner(lead);

    }       
        
    /**
    * Called once per record
    * 
    * @param void
    */
    public override void beforeUpdate(SObject oldObj, SObject obj) {

        Lead oldLead = (Lead)oldObj;
        Lead lead = (Lead)obj;
        manager.updateLeadOwner(oldLead, lead);
        manager.updatePatientPhysician(oldLead, lead);
    }
    
    /**
    * Called in bulk after each trigger
    * 
    * @param void
    */
    public override void andFinally() {

        manager.commitPatientUpdateList();
        manager.createFeedForLead();

    }   

}