public class EducationController extends AuthorizationUtil {

    public List<Shared_Community_Education__c> lstBostonScientificDownloadMaterial {get;set;}
    
   public override void fetchRequestedData() {
       
        //get custom setting values
        User objLoggedInUser = [ SELECT Shared_Community_Division__c, LanguageLocaleKey
                                        FROM User
                                        WHERE Id =: UserInfo.getUserId()];
        
        String strCommunityDivision = String.isBlank(objLoggedInUser.Shared_Community_Division__c) ? 'Endo' : objLoggedInUser.Shared_Community_Division__c;
       
        lstBostonScientificDownloadMaterial = new List<Shared_Community_Education__c>([SELECT Name, Shared_Document_URL__c, Shared_Document_Name__c 
                                                                                        FROM Shared_Community_Education__c
                                                                                        WHERE Shared_Division__c =: strCommunityDivision
                                                                                        AND Shared_Language_Locale_Key__c =: objLoggedInUser.LanguageLocaleKey]);
    }
}