/**
* HotFix for the Patient/Annotated Images, to move existing Attachments to the 
* Chatter API as FeedItems
*
* @author   Salesforce services
* @date     2015-06-25
*/
global class NMD_Attachment2FeedItemBatch implements Database.Batchable<sObject>, System.Schedulable {

	// static entry point
	public static Id runNow() {
		
    	// keep the scope size low in order to not exceed heap size limits
        return Database.executeBatch(new NMD_Attachment2FeedItemBatch(), 2);

	}
	
	String query = 
		'select ' +
			'Name, ' +
			'Body, ' +
			'ParentId, ' +
			'Description ' +
		'from Attachment ' +
		'where Description in :classifications ' +
	    'and Parent.RecordType.SObjectType in (\'Clinical_Data_Summary__c\', \'Patient__c\') ';

	final Set<String> classifications;
	
	global NMD_Attachment2FeedItemBatch() {
		
		// get valid classifications
		this.classifications = PatientImageClassifications__c.getAll().keySet().clone();
		this.query += Test.isRunningTest() ? ' order by CreatedDate desc limit 2' : 'limit 2000';

	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(this.query);
	}

   	global void execute(Database.BatchableContext BC, List<Attachment> attachments) {

   		// map the opportunity to it's patient
		Map<Id,Id> patientIdsByCDSId = new Map<Id,Id>{};

   		for (Attachment attach : attachments) {
   			if (attach.ParentId.getSObjectType() == Clinical_Data_Summary__c.SObjectType) {
				patientIdsByCDSId.put(attach.ParentId, null);
			}
   		}
	
		// find the parent Opportunity's Patient, as the attachment will posted there
   		if (!patientIdsByCDSId.isEmpty()) {

			for (Clinical_Data_Summary__c cds : [
				select Opportunity__r.Patient__c
				from Clinical_Data_Summary__c
				where Id in :patientIdsByCDSId.keySet()
			]) {
				patientIdsByCDSId.put(cds.Id, cds.Opportunity__r.Patient__c);
			}

		}

		for (Attachment attach : attachments) {

			// if the parent id is an Clinical_Data_Summary__c, then the file was an annotation
			Boolean isAnnotation = attach.ParentId.getSObjectType() == Clinical_Data_Summary__c.SObjectType;

			String contentType = attach.Name.toLowercase().endsWith('png')
				? 'image/png'
				: 'image/jpeg';

			Id objectId = isAnnotation
				? patientIdsByCDSId.get(attach.ParentId)
				: attach.ParentId;

			// CREATE A FEED_ITEM_TEXT
			ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
			textSegment.text = isAnnotation
				? 'New Patient Annotation'
				: 'New Patient Image';

			ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
			messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
			messageInput.messageSegments.add(textSegment);

			// POST FILE_FEED_ITEM
			ConnectApi.NewFileAttachmentInput fileIn = new ConnectApi.NewFileAttachmentInput();
			fileIn.title = attach.Name;
			fileIn.description = attach.Description;

			ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
			input.body = messageInput;
			input.attachment = fileIn;

			ConnectApi.BinaryInput feedBinary = new ConnectApi.BinaryInput(attach.Body, contentType, attach.Name);
			ConnectApi.FeedItem feedItem = ConnectApi.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, objectId, input, feedBinary);

		}

		// delete the attachments
		delete attachments;

	}

	global void execute(System.SchedulableContext sc) {

		NMD_Attachment2FeedItemBatch.runNow();

	}
	
	global void finish(Database.BatchableContext bc) {
		
	}
	
}