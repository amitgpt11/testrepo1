/**
 * Name : MyObjectivesTriggerHandler
 * Description : Handler for MyObjectives Trigger
 * Date : 4/2/16
 */
public class MyObjectivesTriggerHandler extends TriggerHandler 
{
   //Get the active territory model Id
   public Id activeModelId = getActiveModelId();
   //Get the parent accounts for MyObjectives
   public Set<Id> accountOnMyObjectives = new Set<Id>();
   //Get matched territory ids
   public List<Id> matchedTerritories = new List<Id>(); 
   map<id,set<Id>> acctTerritorymap = new map<id,set<Id>>();
   map<id,set<Id>> userTerritorymap = new map<id,set<Id>>();
  
   public void onRecordInsert(List<My_Objectives__c> newMyObjList)
   {
       if(Trigger.isBefore)
       {
           getMyObjectiveTerritory2AssignmentsInsert(newMyObjList);  
       }   
       if(Trigger.isAfter)
       {
           onRecordInsert_After(newMyObjList);
       }
   } 
   public void onRecordUpdate(List<My_Objectives__c> oldMyObjList,Map<Id,My_Objectives__c> oldMyObjMap, List<My_Objectives__c> newMyObjList,Map<Id,My_Objectives__c> newMyObjMap)
   {  
       if(Trigger.isBefore)
       {
           List<My_Objectives__c> oldValuesList = new List<My_Objectives__c>();
           List<My_Objectives__c> newValuesList = new List<My_Objectives__c>();
           Map<Id,My_Objectives__c> oldValuesMap = new Map<Id,My_Objectives__c>(); 
           Map<Id,My_Objectives__c> newValuesMap = new Map<Id,My_Objectives__c>(); 
           for(My_Objectives__c myObj :newMyObjList)
           {
               My_Objectives__c myObjOld = oldMyObjMap.get(myObj.id);
               if((myObj.Related_to_Account__c != myObjOld.Related_to_Account__c)  || (myObj.ownerId != myObjOld.ownerId)  ){
                    oldValuesList.add(myObjOld );
                    newValuesList.add(myObj ); 
                    oldValuesMap.put(myObjOld.id,myObjOld );
                    newValuesMap.put(myObj.id,myObj );            
               }
           }
           if(!oldValuesList.isEmpty() && !newValuesList.isEmpty())
           {
               getMyObjectiveTerritory2AssignmentsUpdate(oldValuesList ,oldValuesMap ,newValuesList ,newValuesMap );
           }
       }
       if(Trigger.isAfter)
       {
            getMyObjectiveTerritory2AssignmentsUpdate_After(oldMyObjMap,newMyObjMap,newMyObjList);
       }
     }    
   
   /**
    * Territory assignment on MyObjecitves object befor insert 
   */
   public void getMyObjectiveTerritory2AssignmentsInsert(List<My_Objectives__c> newList)
    {
		List<Id> ownerOnMyObjectives = new List<Id>();
        List<My_Objectives__c> newMyObjLst = new List<My_Objectives__c>();
        for(My_Objectives__c myObj : newList)
        {
            if(myObj.Related_to_Account__c!=null){
                accountOnMyObjectives.add(myObj.Related_to_Account__c);
            }//END of If
            if(myObj.Territory__c!=null){
                newMyObjLst.add(myObj);
            }//END of If
			if(myObj.OwnerId!=null)
            {
                ownerOnMyObjectives.add(myObj.OwnerId);
            }//END of If
        }//END of Loop  
           
        //Queries for and retains the Ids of Account that have related Territories
        List<ObjectTerritory2Association> territoriesOnNewAcount = [SELECT AssociationCause,Id,ObjectId,SobjectType,Territory2Id FROM ObjectTerritory2Association Where ObjectId IN : accountOnMyObjectives  AND  Territory2.Territory2ModelId = :activeModelId]; 
        List<UserTerritory2Association> territoriesOnNewOwner = [SELECT Id,RoleInTerritory2,Territory2Id,UserId FROM UserTerritory2Association Where UserId =:ownerOnMyObjectives AND  Territory2.Territory2ModelId = :activeModelId];
        
		//Below code will create For each User/Account list of Territories
            if(territoriesOnNewAcount!=null && territoriesOnNewAcount.size()>0){

                //UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
                //territoriesOnNewAcount = util.getAcctnTeritryAssctnList(accountIdLst);                                 
                if(territoriesOnNewAcount != null && territoriesOnNewAcount.size() > 0){
                    for(ObjectTerritory2Association at1 : territoriesOnNewAcount){
                        if(acctTerritorymap.containsKey(at1.ObjectId)){
                            acctTerritorymap.get(at1.ObjectId).add(at1.Territory2Id);
                        }
                        else{
                            acctTerritorymap.put(at1.ObjectId,new set<Id>{at1.Territory2Id});
                        }
                    }
                    
                }                     
            }
            if(territoriesOnNewOwner!=null && territoriesOnNewOwner.size()>0){

                //UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
                //territoriesOnNewOwner = util.getUserTeritryAssctnList(userIdLst,true);                                 
                if(territoriesOnNewOwner != null && territoriesOnNewOwner.size() > 0){
                    for(UserTerritory2Association ut : territoriesOnNewOwner){
                        if(userTerritorymap.containsKey(ut.UserId)){
                            userTerritorymap.get(ut.UserId).add(ut.Territory2Id);
                        }
                        else{
                            userTerritorymap.put(ut.UserId,new set<Id>{ut.Territory2Id});
                        }
                    }
                    
                }       
            }
            
            //Preapared maps
            
            system.debug('--userTerritorymap--'+userTerritorymap);
            system.debug('--acctTerritorymap--'+acctTerritorymap);
            map<Id,Id> opptyTerAssignmap = new map<Id,Id>();
			
		
            map<Id,Id>  tempOpttyLst;     
				
            list<Id> opptyIdLstWthFalseCheckbx = new list<Id>();
            for(My_Objectives__c opt : newList){        
                if(userTerritorymap.keyset().Contains(opt.OwnerId) && acctTerritorymap.keyset().Contains(opt.Related_to_Account__c)){       //If User to Territories map doesn't contain Oppty's owner and Account to Territories map doesn't contain Oppty's Account
                    if(userTerritorymap.get(opt.OwnerId).size() > 0 && acctTerritorymap.get(opt.Related_to_Account__c).size() > 0){         //If User to Territories map doesn't have territories for Oppty's owner and Account to Territories map doesn't have territories for Oppty's Account

                        //Find a match for Territory                
                        integer cnt = 0;
                        for(Id tId : userTerritorymap.get(opt.OwnerId)){
                            if(acctTerritorymap.get(opt.Related_to_Account__c).contains(tId)){
                               // Integer matchcount =acctTerritorymap.get(opt.AccountId);
                                
                                cnt++;                                                                              //Increment the count when multiple matches are found
                                if(cnt == 1){
                                    tempOpttyLst = new map<Id,Id>();
                                    tempOpttyLst.put(opt.Id,tId);
                                }
                            }                           
                        }
                        if(cnt == 1){                                                                           //If only one match is found put it in a map of <Oppty Id-->Territory id>
                            opptyTerAssignmap.putAll(tempOpttyLst);
                            /*map<Id,string> terrIdNameMap = new map<Id,string>();
                            if(opptyTerAssignmap.values() != null && opptyTerAssignmap.values().size()>0){
                            	terrIdNameMap = getTerritoryName(opptyTerAssignmap.values());
                        	}                            
                            if(Trigger.isExecuting){
                                Id trrId=opptyTerAssignmap.get(opt.Id);
								List<Id> TerrList = new List<Id>();
								TerrList.add(trrId);
								//String territoryName = getTerritoryName(TerrList);
                                //opt.territory2Id = trrId;
                                if(terrIdNameMap.keyset().size() > 0 && terrIdNameMap.keyset().contains(trrId) && terrIdNameMap.get(trrId) != null){
									opt.Territory__c = terrIdNameMap.get(trrId);
                            	}
                                opt.territory_assigned__C = true;
                            }  */ 
                        }
                        if(cnt>1 || cnt ==0){
                            //OpptyIdLstWthFalseCheckbx.add(opt.id);                        //If multiple matches are found put Oppty id in another map
                            if(Trigger.isExecuting){
                                //if(isAccountChangeonOpty){
                                    opt.Territory__c=null;//clearing the existing territory since new account does not have any matching territory.
                                //}              
                                opt.territory_assigned__C = false;
                            }  
                        }                       
                    }
                    
                }
                else{
                        opt.territory_assigned__C = false;                           //If both UserTerritoryMap and AccountTerritoryMap have no Oppty owner and no Oppty Account in it, mark the checkbox in Oppty to false
                }           
            }
			map<Id,string> terrIdNameMap = new map<Id,string>();
			if(opptyTerAssignmap.values() != null && opptyTerAssignmap.values().size()>0){
                    terrIdNameMap = getTerritoryName(opptyTerAssignmap.values());
            }
			
			for(My_Objectives__c obj : newList){
				String trrId = opptyTerAssignmap.get(obj.Id);
				if(terrIdNameMap.keyset().size() > 0 && terrIdNameMap.keyset().contains(trrId) && terrIdNameMap.get(trrId) != null){
						obj.Territory__c = terrIdNameMap.get(trrId);
						obj.territory_assigned__C = true;
				}
			}

   }
   /**
    *
    */
    public void onRecordInsert_After(List<My_Objectives__c> newMyObjList){
        list<My_Objectives__c> myObjLst = new list<My_Objectives__c>();
        if(newMyObjList!= null && newMyObjList.size() > 0)
        {
            for(My_Objectives__c obj :newMyObjList){
                if(obj.Territory__c != null){
                    myObjLst.add(obj);
                }
                
            }
         }//END of if
         if(myObjLst!= null && myObjLst.size() > 0){
                UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
                util.assignMyObjectvesToAllUsers(myObjLst); 
         }//END of if
     }   
    /**
     * Territory assignment on MyObjecitves object before update 
     */
    public void getMyObjectiveTerritory2AssignmentsUpdate(List<My_Objectives__c> oldList,Map<Id,My_Objectives__c> oldMap, List<My_Objectives__c> newList,Map<Id,My_Objectives__c> newMap)
    {
        System.debug('Size of old and new map==>' + newMap + oldMap);
        List<Id> ownerOnMyObjectives = new List<Id>();    
        boolean isAccountChangeonOpty =false;
        for(  My_Objectives__c myObj : newList)
        {
            if(myObj.Related_to_Account__c != null)
            {
                accountOnMyObjectives.add(myObj.Related_to_Account__c);   
            }//END of If
            if( (oldMap.get(myObj.id)).Related_to_Account__c != newMap.get(myObj.id).Related_to_Account__c)
            {
                isAccountChangeonOpty= true;
            }//END of If
            
            if(myObj.OwnerId!=null)
            {
                ownerOnMyObjectives.add(myObj.OwnerId);
            }//END of If
        
        }//End of Loop 
        List<ObjectTerritory2Association> territoriesOnNewAcount = [SELECT AssociationCause,Id,ObjectId,SobjectType,Territory2Id FROM ObjectTerritory2Association Where ObjectId IN :accountOnMyObjectives AND  Territory2.Territory2ModelId = :activeModelId];
        List<UserTerritory2Association> territoriesOnNewOwner = [SELECT Id,RoleInTerritory2,Territory2Id,UserId FROM UserTerritory2Association Where UserId =:ownerOnMyObjectives AND  Territory2.Territory2ModelId = :activeModelId];
        
		//Below code will create For each User/Account list of Territories
            if(territoriesOnNewAcount!=null && territoriesOnNewAcount.size()>0){

                //UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
                //territoriesOnNewAcount = util.getAcctnTeritryAssctnList(accountIdLst);                                 
                if(territoriesOnNewAcount != null && territoriesOnNewAcount.size() > 0){
                    for(ObjectTerritory2Association at1 : territoriesOnNewAcount){
                        if(acctTerritorymap.containsKey(at1.ObjectId)){
                            acctTerritorymap.get(at1.ObjectId).add(at1.Territory2Id);
                        }
                        else{
                            acctTerritorymap.put(at1.ObjectId,new set<Id>{at1.Territory2Id});
                        }
                    }
                    
                }                     
            }
            if(territoriesOnNewOwner!=null && territoriesOnNewOwner.size()>0){

                //UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
                //territoriesOnNewOwner = util.getUserTeritryAssctnList(userIdLst,true);                                 
                if(territoriesOnNewOwner != null && territoriesOnNewOwner.size() > 0){
                    for(UserTerritory2Association ut : territoriesOnNewOwner){
                        if(userTerritorymap.containsKey(ut.UserId)){
                            userTerritorymap.get(ut.UserId).add(ut.Territory2Id);
                        }
                        else{
                            userTerritorymap.put(ut.UserId,new set<Id>{ut.Territory2Id});
                        }
                    }
                    
                }       
            }
            
            //Preapared maps
            
            system.debug('--userTerritorymap--'+userTerritorymap);
            system.debug('--acctTerritorymap--'+acctTerritorymap);
            map<Id,Id> opptyTerAssignmap = new map<Id,Id>();
			
		
            map<Id,Id>  tempOpttyLst;     
				
            list<Id> opptyIdLstWthFalseCheckbx = new list<Id>();
            for(My_Objectives__c opt : newList){        
                if(userTerritorymap.keyset().Contains(opt.OwnerId) && acctTerritorymap.keyset().Contains(opt.Related_to_Account__c)){       //If User to Territories map doesn't contain Oppty's owner and Account to Territories map doesn't contain Oppty's Account
                    if(userTerritorymap.get(opt.OwnerId).size() > 0 && acctTerritorymap.get(opt.Related_to_Account__c).size() > 0){         //If User to Territories map doesn't have territories for Oppty's owner and Account to Territories map doesn't have territories for Oppty's Account

                        //Find a match for Territory                
                        integer cnt = 0;
                        for(Id tId : userTerritorymap.get(opt.OwnerId)){
                            if(acctTerritorymap.get(opt.Related_to_Account__c).contains(tId)){
                               // Integer matchcount =acctTerritorymap.get(opt.AccountId);
                                
                                cnt++;                                                                              //Increment the count when multiple matches are found
                                if(cnt == 1){
                                    tempOpttyLst = new map<Id,Id>();
                                    tempOpttyLst.put(opt.Id,tId);
                                }
                            }                           
                        }
                        if(cnt == 1){                                                                           //If only one match is found put it in a map of <Oppty Id-->Territory id>
                            opptyTerAssignmap.putAll(tempOpttyLst);
                            /*map<Id,string> terrIdNameMap = new map<Id,string>();
                            if(opptyTerAssignmap.values() != null && opptyTerAssignmap.values().size()>0){
                            	terrIdNameMap = getTerritoryName(opptyTerAssignmap.values());
                        	} 
                            if(Trigger.isExecuting){
                                Id trrId=opptyTerAssignmap.get(opt.Id);
								List<Id> TerrList = new List<Id>();
								TerrList.add(trrId);
								//String territoryName = getTerritoryName(TerrList);
                                //opt.territory2Id = trrId;
                                if(terrIdNameMap.keyset().size() > 0 && terrIdNameMap.keyset().contains(trrId) && terrIdNameMap.get(trrId) != null){
									opt.Territory__c = terrIdNameMap.get(trrId);
                            	}								
                                opt.territory_assigned__C = true;
                            }  */ 
                        }
                        if(cnt>1 || cnt ==0){
                            //OpptyIdLstWthFalseCheckbx.add(opt.id);                        //If multiple matches are found put Oppty id in another map
                            if(Trigger.isExecuting){
                                if(isAccountChangeonOpty){
                                    opt.Territory__c=null;//clearing the existing territory since new account does not have any matching territory.
                                }               
                                opt.territory_assigned__C = false;
                            }  
                        }                       
                    }
                    
                }
                else{
                        opt.territory_assigned__C = false;                           //If both UserTerritoryMap and AccountTerritoryMap have no Oppty owner and no Oppty Account in it, mark the checkbox in Oppty to false
                }           
            }
			map<Id,string> terrIdNameMap = new map<Id,string>();
			if(opptyTerAssignmap.values() != null && opptyTerAssignmap.values().size()>0){
                    terrIdNameMap = getTerritoryName(opptyTerAssignmap.values());
            }
			
			for(My_Objectives__c obj : newList){
				String trrId = opptyTerAssignmap.get(obj.Id);
				if(terrIdNameMap.keyset().size() > 0 && terrIdNameMap.keyset().contains(trrId) && terrIdNameMap.get(trrId) != null){
						obj.Territory__c = terrIdNameMap.get(trrId);
						obj.territory_assigned__C = true;
				}
			}
				
    }  
    /**
     * Territory assignment on MyObjecitves object after update 
     */
    public void getMyObjectiveTerritory2AssignmentsUpdate_After(Map<Id,My_Objectives__c> oldMap, Map<Id,My_Objectives__c> newMap,List<My_Objectives__c> newList)
    {
        list<Id> myObjRemoveLst = new list<Id>();
        list<My_Objectives__c> myObjInsertLst = new list<My_Objectives__c>();
        boolean isSuccess = false;
        UtilityForUserAccountAssociation util = new UtilityForUserAccountAssociation();
        for(  My_Objectives__c myObj : newList)
        {
                   
            if( oldMap.get(myObj.id).Territory__c != newMap.get(myObj.id).Territory__c)
            {
                if(oldMap.get(myObj.id).Territory__c != null)
                {
                    myObjRemoveLst.add(oldMap.get(myObj.id).Id); 
                }//END of If
                if(newMap.get(myObj.id).Territory__c != null)
                {
                    myObjInsertLst.add(newMap.get(myObj.id));                    
                }//END of If 
            }//END of If     
   
        }
        if(myObjRemoveLst.size() > 0)
        {   
            isSuccess = util.removeMyObjectvesShares(myObjRemoveLst);         
        }//END of If            
        if(myObjInsertLst.size() > 0)
        {   
            util.assignMyObjectvesToAllUsers(myObjInsertLst);         
        }//END of If
    }   
    /**
     * Get the Id of the Active Territory Model. 
     * If none exists, return   null
     */
    private Id getActiveModelId()
    {
        //Get the list of active Territories
        List<Territory2Model> models = [Select Id from Territory2Model where State = 'Active'];
        Id activeModel_Id = null;
        if(models.size() == 1)
        {
            activeModel_Id = models.get(0).Id;
        }//END of if
        return activeModel_Id;
    }
    /**
     * Get the Name of the Territory to be assigned . 
     */  
    private map<Id,string> getTerritoryName(List<Id> territoryIdLst)
    {
          map<Id,string> territoryIdNameMap = new map<Id,string>();
          list<territory2> territoryNameLst = new list<territory2>([select Id,Name from territory2 where id IN : territoryIdLst]); 
        if(territoryNameLst != null && territoryNameLst.size() > 0){
            for(territory2 td : territoryNameLst){
                if(td.Name != null){
                    territoryIdNameMap.put(td.Id,td.Name);
                }
            }
        }
          return territoryIdNameMap;     
    }
}