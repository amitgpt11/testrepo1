/**
* Used to help test the virtual NMD_ExtensionBase class
*
* @Author salesforce Services
* @Date 2015/12/16
*/
public with sharing class NMD_ExtensionBaseTestHelper extends NMD_ExtensionBase {
	
	public NMD_ExtensionBaseTestHelper() {
		
		newErrorMessage('foo');
		newInfoMessage('bar');
		newWarningMessage('baz');
		logError('class.method', 'error', 'id', 'obj');

	}

}