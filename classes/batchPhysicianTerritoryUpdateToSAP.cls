global class batchPhysicianTerritoryUpdateToSAP implements Database.Batchable<sObject>,Database.Stateful,Database.AllowsCallouts {


        
        List<Physician_Territory_Realignment__c> Allscope = new List<Physician_Territory_Realignment__c>();
        
        
    global Database.QueryLocator start(Database.BatchableContext BC) {
        DateTime dT = System.now();
        Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
        String query = 'SELECT Id,Name,Contact__c,Contact__r.id,New_Account_Id__c,Old_Account_Id__c,Current_Territory__c,Effective_Realignment_Date__c,New_Territory__c,Realignment_Status__c  FROM Physician_Territory_Realignment__c where Realignment_Status__c = \'Moving To SAP\' and Effective_Realignment_Date__c =: myDate';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Physician_Territory_Realignment__c> scope) {
    
        if(!scope.isEmpty()){
        Allscope.addAll(scope);
        MovePatientToPage3Con InsObj1 = new MovePatientToPage3Con();
        InsObj1.sendToSAPForMultiple(scope);
        }
    }   
    
    global void finish(Database.BatchableContext BC) {
        //write code to make ptr status to Processed
        if(!Allscope.isEmpty()){
            List<Physician_Territory_Realignment__c> processedPtrList = new List<Physician_Territory_Realignment__c>();
            for(Physician_Territory_Realignment__c ptr:Allscope){
                ptr.Realignment_Status__c = 'processed';
                processedPtrList.add(ptr);
            }
            update processedPtrList;
            
            
        }
        
    }
}