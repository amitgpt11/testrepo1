public class SharedFreezeUserController {

    public User u { get; set; }
    public List<User> uList { get; set; }
    

    public SharedFreezeUserController(){
        
        Set<Id> existingRole = new Set<Id>();
        User currentuser=[Select Id,Highest_Level_Role__c, Name,Email from User where Id=:userinfo.getuserId()];
        if(currentuser.Highest_Level_Role__c==null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.Shared_Freeze_User_Error_Message));        
        }
        else
        {
            List<UserRole> uRole = [Select Id from UserRole where DeveloperName=:currentuser.Highest_Level_Role__c];
            if(uRole!=null && uRole.size()>0)
            {
                uList = new List<User>();
                existingRole.add(uRole[0].Id);
                Set<Id> allSubRole = getAllSubRoleIds(existingRole);
                u = [SELECT Id,FirstName,Lastname,UserRoleId,Email FROM User WHERE ID = :ApexPages.currentPage().getParameters().get('id')];
                if(allSubRole.contains(u.UserRoleId))
                {
                    uList.add(u);
                }
                else
                {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.Share_Freeze_User_Role_Permission));
                }
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.Shared_Freeze_User_Correct_Role));
            }
        }
    }

    public PageReference freezeUserButton(){
        UserLogin uLogin = [SELECT Id,IsFrozen  FROM UserLogin WHERE Userid =:u.Id];
        uLogin.IsFrozen  = true;        
        upsert uLogin;
        PageReference redirectUser = new PageReference('/' + u.id);
        redirectUser.setRedirect(true);
        return redirectUser;
    }
    
    public PageReference redirect(){
        PageReference redirectUser = new PageReference('/' + u.id);
        redirectUser.setRedirect(true);
        return redirectUser;
    }
    

  
  public static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {

    Set<ID> currentRoleIds = new Set<ID>();

    // get all of the roles underneath the passed roles
    for(UserRole userRole :[select Id from UserRole where ParentRoleId 
      IN :roleIds AND ParentRoleID != null])
    currentRoleIds.add(userRole.Id);

    // go fetch some more rolls!
    if(currentRoleIds.size() > 0)
      currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));

    return currentRoleIds;

}

}