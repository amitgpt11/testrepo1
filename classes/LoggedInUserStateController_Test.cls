@isTest
private class LoggedInUserStateController_Test {

    private static testMethod void test() {
        
        Test.StartTest();
        
        User objUser = Test_DataCreator.createCommunityUser();   
        LoggedInUserStateController objStateController = new LoggedInUserStateController();
        
        system.runAs(objUser){
            
            objStateController.fetchStateDetails();
            
            objStateController.strAddress = 'billing';
            system.assertEquals(null,objStateController.strHiddenVar);
            //system.assertNotEquals(null,objStateController.strState);
        }
        
        Test.stopTest();

    }

}