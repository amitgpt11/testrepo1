/*
@CreatedDate     3JUNE2016
@author          Julianne Diamond
@Description     Tests Controller for AddContactToFavInline VF Page
@Requirement Id  User Story : US2674
*/

@isTest

public class AddContactToFavInlineControllerTest {
    
    static testmethod void addContactToFavorites(){
        
        Contact con = new Contact(LastName = 'Test1');
        insert con;
        
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        
        AddContactToFavInlineController controller = new AddContactToFavInlineController(sc);
        
        controller.showContactFavorites();
        
        controller.messageText = 'txt';
        System.assertEquals('txt', controller.messageText);
        
        Test.stopTest();
    }
    
    static testmethod void addContactToFavorites2(){
        
        Contact con = new Contact(LastName = 'Test1');
        insert con;
        
        Contact_Favorite__c contactFavorite = new Contact_Favorite__c(Contact__c = con.Id, User__c = UserInfo.getUserId());
        insert contactFavorite;
        
        Test.StartTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        
        AddContactToFavInlineController controller = new AddContactToFavInlineController(sc);
        
        controller.showContactFavorites();
        
        controller.messageText = 'txt';
        System.assertEquals('txt', controller.messageText);
        
        Test.stopTest();
    }
}