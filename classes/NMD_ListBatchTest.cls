/**
* Test class for NMD_ListBatch
*
* @author   Salesforce services
* @date     2014-07-10
*/
// need to be true as the test calls the ConnectApi
@isTest(SeeAllData=true)
private class NMD_ListBatchTest {

    static testMethod void NMD_ListBatchTest() {

        final Integer LENGTH = 1;

        NMD_TestDataManager testData = new NMD_TestDataManager();
        
        //create account
        Account acc = testData.createConsignmentAccount();
        insert acc;     
                
        //create Cycle Count
        Cycle_Count__c cc = new Cycle_Count__c(
            Start_Date__c = System.today().addDays(-7),
            End_Date__c = System.today().addDays(7),
            Status__c = 'Published',
            Description__c = 'TESTSOMETHING',
            Product_Scope__c = 'TEST_PS',
            Name = 'TESTCC'
        );
        insert cc;
        
        //create list obj
        List__c listObj = new List__c(
            RecordTypeId = Schema.SObjectType.List__c.getRecordTypeInfosByName().get('Cycle Count Users').getRecordTypeId(),
            Batch_Ran__c = false,
            Inventory_Account__c = acc.Id,
            Cycle_Count__c = cc.Id
        );
        insert listObj;                       
        
        //run the batch
        System.Test.startTest();
        {
            NMD_ListBatch.runNow();
        }
        System.Test.stopTest();
    }
    
}