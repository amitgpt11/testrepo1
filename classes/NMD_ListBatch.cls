/**
 * Batch class to create cycle count analysis records based on start date on cycle count
 *
 * @author   Salesforce services
 * @date     2015-07-6
 */
global without sharing class NMD_ListBatch implements Database.Batchable < sObject > , Schedulable {
	
    //static final List<ApplicationLogWrapper> logs = new List <ApplicationLogWrapper>{};
    static final Id TYPE_USERS = Schema.SObjectType.List__c.getRecordTypeInfosByName().get('Cycle Count Users').getRecordTypeId();

    /***  @desc Immediately executes an instance of this batch*/
    public static Id runNow() {
        return Database.executeBatch(new NMD_ListBatch());
    }

    public String query = 
		'select '+
			'Inventory_Account__c, ' +
            'Inventory_Account__r.OwnerId, ' +
			'Cycle_Count__r.Start_Date__c, ' +
			'Cycle_Count__r.End_Date__c, ' +
			'Batch_Ran__c ' +
		'from List__c ' + 
        'where RecordTypeId = :TYPE_USERS ' +
		'and Batch_Ran__c = false ' +
        'and Cycle_Count__r.Start_Date__c = LAST_N_DAYS:7 ' + 
		'and Cycle_Count__r.Status__c = \'Published\' ';
    	 				    
    global NMD_ListBatch() {
        if (Test.isRunningTest()) {
            this.query += ' order by CreatedDate desc limit 100';
        }
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(this.query);
    }

    /**
     *  @desc    Database.Bachable Executable method
     *  @param   LIST Cycle_Count__c scope
     */
    global void execute(Database.BatchableContext bc, List<List__c> scope) {
    	
        List<Cycle_Count_Response_Analysis__c> ccraList = new List <Cycle_Count_Response_Analysis__c>{};
        Set<Id> ccrsIds = new Set<Id>{};
		Map<Id,Id> ccrsToAccOwner = new Map<Id,Id>{};

        //iterate on list objects
        for (List__c l: scope) {
            ccraList.add(new Cycle_Count_Response_Analysis__c(
                OwnerId = l.Inventory_Account__r.OwnerId,
                Start_Date__c = l.Cycle_Count__r.Start_Date__c,
                End_Date__c = l.Cycle_Count__r.End_Date__c,
                Status__c = 'Published',
                Inventory_Account__c = l.Inventory_Account__c,
                Cycle_Count__c = l.Cycle_Count__c
            ));

            l.Batch_Ran__c = true;
        }

        DML.deferLogs();
        
        //Insert new CycleCount Analysis Records
        //List<Database.SaveResult> ccraSaveResults = Database.insert(ccraList, false);
        for (Database.SaveResult result : DML.save(this, ccraList, false)) {
            //if (!result.isSuccess()) {
            //    for (Database.Error err: result.getErrors()) {
            //        logs.add(new ApplicationLogWrapper(err, result.getId(), 'Error in Insert of Cycle Count Analysis Obj On NMD_CycleCountBatch.execute'));
            //    }
            //}
            if (result.isSuccess()) {
                ccrsIds.add(result.Id);
            }
        }

        //update list 
        //List<Database.SaveResult> listSaveResults = Database.update(scope, false);
        //for (Database.SaveResult result: listSaveResults) {
        //    if (!result.isSuccess()) {
        //        for (Database.Error err: result.getErrors()) {
        //            logs.add(new ApplicationLogWrapper(err, result.getId(), 'Error in Insert of Cycle Count Analysis Obj On NMD_CycleCountBatch.execute'));
        //        }
        //    }
        //}
        DML.save(this, scope, false);
        DML.flushLogs(false);

        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();

        //prepare data
        for (Cycle_Count_Response_Analysis__c c : [
            select 
				Name, 
				Start_Date__c,
				Status__c,
				Inventory_Account__r.OwnerId 
			from Cycle_Count_Response_Analysis__c 
			where Id = :ccrsIds
		]) {
        												
			String messageString = ' \r\n' + 'Start Date: ' + c.Start_Date__c + ' \r\n' + 'Status: ' + c.Status__c;

            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            mentionSegmentInput.id = c.Inventory_Account__r.OwnerId;
            
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            textSegmentInput.text = messageString;
            
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            messageBodyInput.messageSegments.add(mentionSegmentInput);
            messageBodyInput.messageSegments.add(textSegmentInput);

            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = c.Id;
            
            ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
            batchInputs.add(batchInput);

            if (batchInputs.size() == 500) {
                ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
                batchInputs.clear();                
            }

        }

        if (!batchInputs.isEmpty()) {
            ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
        }

    }

    /***  @desc Schedulable method, executes the class instance*/
    global void execute(SchedulableContext context) {
        NMD_ListBatch.runNow();
    }

    global void finish(Database.BatchableContext bc) {
        //if (!logs.isEmpty()) {
        //    Logger.logMessage(logs);
        //}
    }
}