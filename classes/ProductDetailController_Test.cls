@isTest
private class ProductDetailController_Test {

    private static testMethod void test() {
    
        //Create Guest User
        User objUser = Test_DataCreator.createCommunityUser();
        
        Product2 objProduct = Test_DataCreator.createProduct('Test');
        objProduct.ProductCode = '';
        objProduct.Shared_Includes_Handle_Break__c = '';
        update objProduct;
        
        Product2 objProduct2 = Test_DataCreator.createProduct('Shared Test');
        objProduct2.Shared_Parent_Product__c = objProduct.Id;
        objProduct2.Shared_Includes_Handle_Break__c = 'Yes';
        update objProduct2;
        
        Product2 objProduct3 = Test_DataCreator.createProduct('Shared Test 3');
        objProduct3.Shared_Parent_Product__c = objProduct.Id;
        objProduct3.Shared_Includes_Handle_Break__c = 'Yes';
        objProduct3.Shared_Working_Length_cm__c = 135;
        update objProduct3;
        
        Test.StartTest();
        
        system.runAs(objUser){
            
            PageReference myVfPage = Page.ProductDetail;
            Test.setCurrentPageReference(myVfPage); 
            ProductDetailController objProductDetailController = new ProductDetailController();
            objProductDetailController.init();
            
            objProductDetailController = new ProductDetailController();
            ApexPages.currentPage().getParameters().put('Id','');
            objProductDetailController.fetchRequestedData();
            system.assertNotEquals(objProductDetailController.errorMessage,null);
            
            objProductDetailController = new ProductDetailController();
            ApexPages.currentPage().getParameters().put('Id','testabc');
            objProductDetailController.fetchRequestedData();
            system.assertNotEquals(objProductDetailController.errorMessage,null);
            
            objProductDetailController = new ProductDetailController();
            ApexPages.currentPage().getParameters().put('Id',objProduct.Id);
            objProductDetailController.fetchRequestedData();
            system.assertNotEquals(objProductDetailController.errorMessage,null);
            
            ApexPages.currentPage().getParameters().put('attributeName','Shared_Includes_Handle_Break__c');
            objProductDetailController.mapAttributeToValues.get('Shared_Includes_Handle_Break__c').attributeSelectedOption = 'yes';
	        objProductDetailController.filterProductAttribute(); 
	        
	        ApexPages.currentPage().getParameters().put('attributeName','Shared_Working_Length_cm__c');
            objProductDetailController.mapAttributeToValues.get('Shared_Includes_Handle_Break__c').attributeSelectedOption = '135.0';
	        objProductDetailController.filterProductAttribute(); 
	       
	        objProductDetailController.AddItems();
	        
	        Shared_Community_Order__c objOrder = Test_DataCreator.createOrder();
            objOrder.Status__c = 'In Progress';
            update objOrder;
            objProductDetailController.AddItems();
            
            
            ApexPages.currentPage().getParameters().put('attributeName','Shared_Includes_Handle_Break__c');
	        objProductDetailController.filterProductAttribute(); 
	        objProductDetailController.AddItems();
	       
        }
        
        User objLocaleUser = Test_DataCreator.createCommunityUserWithLocale();
        
        system.runAs(objLocaleUser){
            
            ProductDetailController objProductDetailController = new ProductDetailController();
            
            Shared_Community_Product_Translation__c objSharedTranslation = Test_DataCreator.createProductTranslationRecord(objProduct.Id,objLocaleUser.Shared_Community_Division__c);
            objSharedTranslation.Shared_Language__c = objProductDetailController.mapLocaleToLanguage.get(objLocaleUser.LanguageLocaleKey);
            update objSharedTranslation;
            
            ApexPages.currentPage().getParameters().put('Id',objProduct.Id);
            objProductDetailController.fetchRequestedData();
            
        }
        
        Test.StopTest();
    }
}