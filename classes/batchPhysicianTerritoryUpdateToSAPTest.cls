@isTest(SeeAllData=false)
@testVisible
private class batchPhysicianTerritoryUpdateToSAPTest {
    
    public static   User NMDRBMUser   = new User();
    public static List<Contact> cList = new List<Contact> ();
    public static Date myDate ;
    public static Seller_Hierarchy__c territory1= new Seller_Hierarchy__c();
    public static Seller_Hierarchy__c territory2= new Seller_Hierarchy__c();
    public static Account NewAccount= new Account();
    public static Account OldAccount= new Account();
    public static List<Physician_Territory_Realignment__c> PTRList = new List<Physician_Territory_Realignment__c>();
    
    static void setupCommonMockData(){
        DateTime dT = System.now();
        myDate = date.newinstance(dT.year(), dT.month(), dT.day());
        NMD_TestDataManager td = new NMD_TestDataManager();
        territory1 = td.newSellerHierarchy('T1');
        territory1.Current_TM1_Assignee__c = NMDRBMUser.id;
        insert territory1;
        
        territory2 = td.newSellerHierarchy('T2');
        territory2.Current_TM1_Assignee__c = NMDRBMUser.id;
        insert territory2;
        
        
        NewAccount =td.newAccount('New Account');
        insert NewAccount;
        OldAccount = td.newAccount('Old Account');
        insert OldAccount;
        //String oldAccId=OldAccount.Id;
        for(Integer i=0;i<4;i++){
            Contact temp= td.newContact(OldAccount.Id);
            temp.Territory_ID__c =territory1.Id;
            cList.add(temp);
        }
        if(cList.size()>0){
            insert cList;
            
        }
        
        For(Integer i=0;i<4;i++){
            Physician_Territory_Realignment__c ptrTemp = new Physician_Territory_Realignment__c();
            ptrTemp.Contact__c=cList[1].Id;
            ptrTemp.New_Account_Id__c= NewAccount.Id;
            ptrTemp.Current_Territory__c=territory1.Id;
            ptrTemp.New_Territory__c =territory2.Id;
            ptrTemp.Realignment_Status__c = 'Moving To SAP';
            ptrTemp.Effective_Realignment_Date__c=myDate; 
            PTRList.add(ptrTemp);
            
        }
        insert PTRList;
        
        
        
    }
    
    static TestMethod void TestbatchPhysicianTerritoryUpdateToSAP(){ 
        setupCommonMockData();
        
        /*For(Integer i=0;i<4;i++){
            Physician_Territory_Realignment__c ptrTemp = new Physician_Territory_Realignment__c();
            ptrTemp.Contact__c=cList[1].Id;
            ptrTemp.New_Account_Id__c= NewAccount.Id;
            ptrTemp.Current_Territory__c=territory1.Id;
            ptrTemp.New_Territory__c =territory2.Id;
            ptrTemp.Realignment_Status__c = 'Moving To SAP';
            ptrTemp.Effective_Realignment_Date__c=myDate; 
            PTRList.add(ptrTemp);
            
        }
        insert PTRList;*/
         List<Physician_Territory_Realignment__c> phys=[SELECT Id,Name,Contact__c,New_Account_Id__c,Current_Territory__c,Effective_Realignment_Date__c,New_Territory__c,Realignment_Status__c  FROM Physician_Territory_Realignment__c where Realignment_Status__c ='Moving To SAP' and Effective_Realignment_Date__c =: myDate];
        Test.startTest();
         ID batchprocessid = Database.executeBatch(new batchPhysicianTerritoryUpdateToSAP());
        	
        Test.stopTest();
    
    }
    
    
}