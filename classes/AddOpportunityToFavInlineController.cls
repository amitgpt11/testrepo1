/*
@CreatedDate     3JUNE2016
@author          Julianne Diamond
@Description     Controller for AddOpportunityToFavInline VF Page: Determines whether a user had added the selected record as a favorite, for inline display of the favorites notification
@Requirement Id  User Story : US2674
*/

public class AddOpportunityToFavInlineController {
    List<Opportunity_Favorite__c> lstFavOpportunity;
    public boolean showAddButton{get;set;}
    public String messageText{get;set;}
    Id optId;
    Id opportunityFavId;
    public AddOpportunityToFavInlineController (ApexPages.StandardController controller){
        optId = controller.getId();
        showOpportunityFavorites();
        
    }
    
    public void showOpportunityFavorites(){
        lstFavOpportunity = [Select id, Name, Opportunity__c, User__c from Opportunity_Favorite__c where Opportunity__c = :optId and User__c = :UserInfo.getUserId() ];
        if(lstFavOpportunity.size() > 0){
            opportunityFavId = lstFavOpportunity.get(0).id;
            showAddButton = false;
          //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'This record is added to your favorites.'));
            messageText = 'This record is added to your favorites.';
        }else{
            showAddButton = true;
        }
    }
    
}