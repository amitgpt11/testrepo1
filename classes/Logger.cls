/**
* Logger.class
*
* @author 	Salesforce Services
* @date 	2014-02-03
* @desc 	Logging utility to write logs to Application_Log__c object.
*/
public without sharing class Logger  {
    
    private static final Log_Settings__c LogSettings = Log_Settings__c.getInstance();

    public static String DEBUG = 'Debug';
    public static String WARN = 'Warn';
    public static String INFO = 'Info';
    public static String ERROR = 'Error';
    
    /**
	* Method to invoke logger for debug.
	* 
	* 
	* @param sourceClass - String
	* @param sourceFunction - String
	* @param recordId - String
	* @param sobjectName - String
	* @param logMessage - String
	*/
    
    public static void debug(String sourceClass, String sourceFunction, String recordId, String sobjectName, String logMessage) {
        logMessage(DEBUG, sourceClass, sourceFunction, recordId, sobjectName, logMessage, null, null, 0);
    }
    
    /**
	* Method to invoke logger for debug.
	* 
	* 
	* @param sourceClass - String
	* @param sourceFunction - String
	* @param recordId - String
	* @param sobjectName - String
	* @param logMessage - String
	* @param payLoad - String , 
	* @param ex - Exception
	* @param timeTaken - long 
	*/
    
    public static void debug(String sourceClass, String sourceFunction, String recordId, String sobjectName, String logMessage, String payLoad, Exception ex, long timeTaken) {
        logMessage(DEBUG, sourceClass, sourceFunction, recordId, sobjectName, logMessage, payLoad, ex, timetaken);
    }
    
    /**
	* Method to log warning.
	* 
	* 
	* @param sourceClass - String
	* @param sourceFunction - String
	* @param recordId - String
	* @param sobjectName - String
	* @param logMessage - String
	* @param payLoad - String , 
	* @param ex - Exception
	* @param timeTaken - long 
	*/
    
    public static void warn(String sourceClass, String sourceFunction, String recordId, String sobjectName, String logMessage, String payLoad, Exception ex, long timeTaken) {
        logMessage(WARN, sourceClass, sourceFunction, recordId, sobjectName, logMessage, payLoad, ex, timetaken);
    }
    
	/**
	* Method to log information with severity - info.
	* 
	* 
	* @param sourceClass - String
	* @param sourceFunction - String
	* @param recordId - String
	* @param sobjectName - String
	* @param logMessage - String
	* @param payLoad - String , 
	* @param ex - Exception
	* @param timeTaken - long 
	*/
    
    public static void info(String sourceClass, String sourceFunction, String recordId, String sobjectName, String logMessage, String payLoad, Exception ex, long timeTaken) {
        logMessage(INFO, sourceClass, sourceFunction, recordId, sobjectName, logMessage, payLoad, ex, timetaken);
    }
    
    /**
	* Method to log error.
	* 
	* 
	* @param sourceClass - String
	* @param sourceFunction - String
	* @param recordId - String
	* @param sobjectName - String
	* @param logMessage - String
	* @param payLoad - String , 
	* @param ex - Exception
	* @param timeTaken - long 
	*/
    
    public static void error(String sourceClass, String sourceFunction, String recordId, String sobjectName, String logMessage, String payLoad, Exception ex, long timeTaken) {
        logMessage(ERROR, sourceClass, sourceFunction, recordId, sobjectName, logMessage, payLoad, ex, timetaken);
    }
    
    /**
	* Method to log exception.
	* 
	* 
	* @param sourceClass - String
	* @param sourceFunction - String
	* @param recordId - String
	* @param sobjectName - String
	* @param payLoad - String , 
	* @param ex - Exception
	* 
	*/
    
    public static void exception(String sourceClass, String sourceFunction, String recordId, String sobjectName, String payLoad, Exception ex) {
        String stackTrace = (ex != null) ? ex.getStackTraceString() : null;
        logMessage(ERROR, sourceClass, sourceFunction, recordId, sobjectName, stackTrace, payLoad, ex, 0);
    }
    
    /**
    * Core logging method.  All convenience logging methods should delegate to this.
    * 
    * @param logLevel - DEBUG, INFO, WARN, ERROR
    * @param sourceClass - name of the class being logged
    * @param sourceFunction - name of the class/method being logged
    * @param recordId - Salesforce ID for the record of interest
    * @param sobjectName - SObject type for the record of interest
    * @param logMessage - the message to be logged
    * @param payload - XML payload for integration logging
    * @param ex - Exception to be logged
    * @param timeTaken - milliseconds taken, used for performance logging
    */
    public static void logMessage(String logLevel, String sourceClass, String sourceFunction, String recordId, String sobjectName, String logMessage, String payLoad, Exception ex, long timeTaken) {
        ApplicationLogWrapper msg = new ApplicationLogWrapper();
        
        msg.source = sourceClass;
        msg.logMessage = logMessage;
        msg.sourceFunction = sourceFunction;
        msg.recordId = recordId;
        msg.objectType = sobjectName;
        msg.payload = payLoad;
        msg.debugLevel = logLevel;
        msg.ex = ex;
        msg.Timer = timeTaken;
        
        logMessage( msg );
    }    
    
    public static void logMessage(ApplicationLogWrapper appLog)
    {
        List<ApplicationLogWrapper> appLogs = new List<ApplicationLogWrapper>();
        appLogs.add ( appLog );
        logMessage ( appLogs );
    }
    
    /**
    * Asynchronous logging method for use when in-context logging is not permitted 
    * (e.g., mixed DML operations on restricted SObjects).  
    * The developer should construct and aggregate ApplicationLogWrapper objects, calling this
    * method only at the termination of the method (e.g., finally block).
    * 
    * @param jsonLogs - collection of JSON serialized ApplicationLogWrappers
    */
    @future
    public static void logFutureMessages(String[] jsonLogs) {
        List<ApplicationLogWrapper> appLogs = new List<ApplicationLogWrapper>();
        for(String jsonLog : jsonLogs) {
            try {
                ApplicationLogWrapper log = (ApplicationLogWrapper) JSON.deserialize(jsonLog, ApplicationLogWrapper.class);
                appLogs.add(log);
            } catch(Exception e) {
                exception('Logger', 'logFutureMessages', null, 'Application_Log__c', jsonLog, e);
            }
        }
        
        logMessage(appLogs);
    }
    
    /**
    * Utility method to support the logFutureMessages() method.
    *
    * @param appLogs - collection of ApplicationLogWrappers
    * @return collection of JSON serialized ApplicationLogWrappers
    */
    public static List<String> logWrappersToJSON(List<ApplicationLogWrapper> appLogs)
    {
        List<String> jsonWrappers = new List<String>();
        
        for(ApplicationLogWrapper log : appLogs) {
            jsonWrappers.add(JSON.serialize(log));
        }
        
        return jsonWrappers;
    }
    
    /**
    * Utility method to support the logMessages() method over collection of ApplicationLogWrapper.
    *
    * @param appLogs - collection of ApplicationLogWrappers
    * 
    */
    
    public static void logMessage(List<ApplicationLogWrapper> appLogs)
    {
        List<Application_Log__c> insertAppLogs = new List<Application_Log__c>();
        for(ApplicationLogWrapper appLog : appLogs){
            boolean validInsert = false;
            
            if(appLog.debugLevel == DEBUG && LogSettings.Debug__c){
                validInsert = true;
            } else if(appLog.debugLevel == ERROR && LogSettings.Error__c){
                validInsert = true;
            } else if(appLog.debugLevel == INFO && LogSettings.Info__c){
                validInsert = true;
            } else if(appLog.debugLevel == WARN && LogSettings.Warning__c){
                validInsert = true;
            }
            
            if(validInsert){
                Application_Log__c log = new Application_Log__c(
	                Source_Class__c = left(appLog.source, 255),
	                Source_Function__c = left(appLog.sourceFunction, 200),
	                Record_Id__c = left(appLog.recordId, 18),
	                Record_Object_Type__c = left(appLog.objectType, 255),
	                Message__c = left(appLog.logMessage, 255),
	                Integration_Payload__c = left(appLog.payload, 32768),
	                Severity__c = appLog.debugLevel,
	                Log_Code__c = left(appLog.logCode, 50),
	                Timer__c = appLog.timer,
	                Timestamp__c = appLog.timestamp
	            );
                // TODO - create a trigger that sets the record's timestamp equal to the creation datetime if null
                
                if(appLog.ex != null){
                    log.Stack_Trace__c = appLog.ex.getStackTraceString();
                    log.Message__c = applog.ex.getMessage();
                }
            
                insertAppLogs.add(log);
                System.Debug('~~~ Queued log message from ' + log.source_class__c + ' debug level: ' + log.Severity__c);
            }
        }
        
        if ( insertAppLogs.size() > 0 ) {
            //Savepoint sp = Database.setSavepoint();
            try {
                insert insertAppLogs;
            } catch(Exception e) {
                System.debug('~~~ ERROR: Unable to insert Application logs: ' + e);
                for (Application_Log__c log : insertAppLogs) {
                    System.debug('LOG:' + log);
                }
            }
        }


    }
    
    private static String left(String src, Integer maxLength) {
        String left = src;
        if(src != null && src.length() > maxLength) {
            left = src.substring(0, maxLength);
        }
        return left;
    }
}