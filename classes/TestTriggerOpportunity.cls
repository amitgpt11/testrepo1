@isTest
private class TestTriggerOpportunity {
    static testMethod void Test_Insert() {

        Opportunity prd = new Opportunity(Name ='Test', CloseDate = Date.today(), StageName='Prospecting');
        

        try {
            insert prd;
        } catch(DMLException e) {
            system.debug(LoggingLevel.ERROR, e.getMessage());
            system.assert(false);
        }
        system.assert(prd.Id != null);
    }
}