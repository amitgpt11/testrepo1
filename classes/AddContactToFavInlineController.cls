/*
@CreatedDate     3JUNE2016
@author          Julianne Diamond
@Description     Controller for AddContactToFavInline VF Page: Determines whether a user had added the selected record as a favorite, for inline display of the favorites notification
@Requirement Id  User Story : US2674
*/


public class AddContactToFavInlineController {
    List<Contact_Favorite__c> lstFavContact;
    public boolean showAddButton{get;set;}
    public String messageText{get;set;}
    Id conId;
    Id contactFavId;
    public AddContactToFavInlineController (ApexPages.StandardController controller){
        conId = controller.getId();
        showContactFavorites();
        
    }
    
    public void showContactFavorites(){
        lstFavContact = [Select id, Name, Contact__c, User__c from Contact_Favorite__c where Contact__c = :conId and User__c = :UserInfo.getUserId() ];
        if(lstFavContact.size() > 0){
            contactFavId = lstFavContact.get(0).id;
            showAddButton = false;
          //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'This record is added to your favorites.'));
            messageText = 'This record is added to your favorites.';
        }else{
            showAddButton = true;
        }
    }  
}