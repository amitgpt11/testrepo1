/**
* Test class for NMD_AnnotateImageExtension
*
* @Author salesforce Services
* @Date 03/24/2015
*/
@isTest(SeeAllData=false)
private class NMD_AnnotateImageExtensionTest {

	static final String PATIENTNAME = 'PATIENTNAME';
	
	@testSetup
	static void setup() {

		String classification = 'classification';
		
		NMD_TestDataManager td = new NMD_TestDataManager();

		PatientImageClassifications__c pic = new PatientImageClassifications__c(
			Name = classification,
			Is_Active__c = true,
			Order__c = 0
		);
		insert pic;

		Patient__c patient = td.newPatient(PATIENTNAME, PATIENTNAME);
		insert patient;

		Account acct = td.createConsignmentAccount();
		insert acct;

		Opportunity oppty = td.newOpportunity(acct.Id);
		oppty.RecordTypeId = OpportunityManager.RECTYPE_TRIAL;
		oppty.Opportunity_Type__c = 'Standard';
		oppty.Patient__c = patient.Id;
		insert oppty;

		//Attachment attach = td.newAttachment(patient.Id, 'name', classification, Blob.valueOf('blob'));
		//insert attach;

	}

	static testMethod void test_AnnotateImage() {

		Patient__c patient = [select Id from Patient__c where Patient_First_Name__c = :PATIENTNAME];
		Opportunity oppty = [select Patient__c from Opportunity where Patient__c = :patient.Id];

		PageReference pr = Page.AnnotateImage;
		pr.getParameters().put('id', oppty.Id);
		Test.setCurrentPage(pr);

		NMD_AnnotateImageExtension ext = new NMD_AnnotateImageExtension(new ApexPages.StandardController(oppty));
		ext.checkRedirect();

	}

}