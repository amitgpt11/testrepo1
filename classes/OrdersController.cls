/*
* Description: This controller initilizes the List of Shared Order Item to display on Orders Page.
*/
public class OrdersController extends AuthorizationUtil{
    
    public Integer index {get;set;}
    public User objUser {get;set;}
    public list<Shared_Community_Order_Item__c> lstOrderItem {get;set;}
    public List<Shared_Community_Order_Item__c> lstOrderItemToDelete{get;set;}
    public Boolean hasPurchasingPermission  {get; set;}
    
    /*
        * Description: This method initilizes the list to be displayed and called after initial page load.
    */
    public override void fetchRequestedData() {
        
        hasPurchasingPermission = false;
        lstOrderItem = new List<Shared_Community_Order_Item__c>();
        lstOrderItemToDelete = new List<Shared_Community_Order_Item__c>();
        
        objUser = [Select contactId, Shared_Community_Division__c
                   From User
                   Where Id =: Userinfo.getUserId()];
        
        List<Contact> lstContacts = new List<Contact>([SELECT Purchasing_Permission__c FROM Contact WHERE Id =: objUser.contactId]);
        
        if(!lstContacts.isEmpty()) {
            
            hasPurchasingPermission = lstContacts[0].Purchasing_Permission__c;
        }
        
        String queryOrderItems = 'Select Id, Shared_Order__c, Shared_Quantity__c, Shared_Order__r.Shared_Date_Submitted__c, ' +
            					'Product__r.Shared_Product_Image_URL__c, Product__r.Product_Label__c, Product__r.Id, ' +
            					'Product__r.Shared_Community_Description__c, Product__r.ProductCode, ' +
                        		'Product__r.Shared_Parent_Product__r.Shared_Product_Image_URL__c ' +
                        		'From Shared_Community_Order_Item__c ' + 
                        		'Where Shared_Order__r.Status__c = \'In Progress\' ';
        
        if(!Test.isRunningTest()) {
            
            queryOrderItems += 'AND Shared_Order__r.Contact__r.Id = \'' + objUser.contactId + '\'';
        }
        
        lstOrderItem = Database.query(queryOrderItems);
        
    }
    
    public void updateOrderItem(){
        
        Update lstOrderItem;
        Delete lstOrderItemToDelete;
        fetchRequestedData();
    }
    
    public void deleteOrderItem() {
        
        lstOrderItemToDelete.add(lstOrderItem[index]);
        lstOrderItem.remove(index);
    }
    
    public PageReference goToCheckout() {
        
        String strCheckoutURL = ((String.isBlank(Site.getName())) ? Site.getPathPrefix() : '/apex') + '/Checkout?orderId=' + lstOrderItem[0].Shared_Order__c;
        return new PageReference(strCheckoutURL);
    }
    
}