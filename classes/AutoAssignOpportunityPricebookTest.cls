/*
@CreatedDate     20JUNE2016
@author          Mike Lankfer
@Description     Contains test coverage for OpportunityTrigger, OpportunityTriggerHandler, and AutoAssignOpportunityPricebook
@Requirement Id  User Story : DRAFT US2511
*/

@isTest
public class AutoAssignOpportunityPricebookTest {
    
    private static Profile testProfile = [SELECT Id, Name FROM Profile WHERE Name = 'US ENDO User' LIMIT 1];
    private static Pricebook2 testPricebook = new Pricebook2();
    private static Shared_Pricebook_Profile_Mapping__c endoProfilePricebookMap = new Shared_Pricebook_Profile_Mapping__c();
    private static List<User> testUsers = new List<User>();
    private static Account testAccount = new Account();
    private static Opportunity testOpportunity = new Opportunity();
    private static Opportunity newOpportunity = new Opportunity();  
    
    
    //create an opportunity and verify the correct pricebook is assigned automatically based on the mapping table
    static testmethod void createOpportunity(){
       ApexTrigger apx = [Select id,Status,Name From ApexTrigger where Name = 'OpportunityTrigger'];
        
        Test.StartTest();
        
        //create test pricebook
        testPricebook = TestDataFactory.createTestPricebook();
        insert testPricebook;
        
        //create endo profile to pricebook map
        endoProfilePricebookMap.Name = testProfile.Name;
        endoProfilePricebookMap.Shared_Pricebook__c = testPricebook.Name;
        insert endoProfilePricebookMap;
        
        //create test user
        testUsers = TestDataFactory.createTestUsers(1, testProfile);
        insert testUsers;
        
        //create test account
        testAccount = TestDataFactory.createTestAccount();
        insert testAccount;
        
        //create test opportunity
        testOpportunity = TestDataFactory.createTestOpportunity(testAccount);
        testOpportunity.OwnerId = testUsers.get(0).Id;
        insert testOpportunity;
        
        newOpportunity = [SELECT Id, Pricebook2Id FROM Opportunity WHERE Id = :testOpportunity.Id];
        
        Test.stopTest();
        if(apx.status == 'Active'){
        System.assertEquals(testPricebook.Id, newOpportunity.Pricebook2Id);  }     
    }    
}