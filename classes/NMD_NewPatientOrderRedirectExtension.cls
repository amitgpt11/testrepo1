/**
* Extension class for the vf page NewPatientOrderRedirect
*
* @Author salesforce Services
* @Date 2015-07-13
*/
public with sharing class NMD_NewPatientOrderRedirectExtension {

    public string mySynergyURL {get;set;}

    final Id opptyId;
    public final String PATIENT_LABEL = 'Patient: ';
    public final String CONTACT_LABEL = 'Physician';
    public final String ACCOUNT_LABEL = 'Account';
    public final String TRIALING_LABEL = 'Trialing';
    public final String PROCEDURE_LABEL = 'Procedure';
    final String scheme;

    // custom presentation, not using Apex.PageMessages
    public List<String> requiredFields { get; private set; }

    public NMD_NewPatientOrderRedirectExtension(ApexPages.StandardController sc) {
    
        NMD_Inventory_Settings__c settings = NMD_Inventory_Settings__c.getInstance();

        this.scheme = settings != null ? settings.URI_Scheme__c : '';
        this.opptyId = sc.getId();
        this.mySynergyURL = '<script>  sforce.one.navigateToURL(\''+scheme+'://orders/create?oid='+this.opptyId+'\');</script>';
        //<script>  sforce.one.navigateToURL('synergytest://orders/create?oid=006o000000J9G1h');</script>
        
        this.requiredFields = new List<String>{};

        Opportunity oppty = (Opportunity)sc.getRecord();
        
        if(oppty.RecordTypeId == OpportunityManager.RECTYPE_TRIAL_NEW)
        {
            if (oppty.Primary_Insurance__c == null) {
            this.requiredFields.add(SObjectType.Opportunity.fields.Primary_Insurance__c.getLabel());
            }

          if (String.isBlank(oppty.Pain_Area__c)) {
            this.requiredFields.add(SObjectType.Opportunity.fields.Pain_Area__c.getLabel());
          }
          if (String.isBlank(oppty.Trial_Lead_Placement_Distal__c)) {
            this.requiredFields.add(SObjectType.Opportunity.fields.Trial_Lead_Placement_Distal__c.getLabel());
          }
          if (String.isBlank(oppty.Product_System__c)) {
            this.requiredFields.add(SObjectType.Opportunity.fields.Product_System__c.getLabel());
          }
            
            if (oppty.Scheduled_Trial_Date_Time__c == null){
                    this.requiredFields.add(SObjectType.Opportunity.fields.Scheduled_Trial_Date_Time__c.getLabel());
            }
            
            if (oppty.Trialing_Account__c == null) {
                this.requiredFields.add(SObjectType.Opportunity.fields.Trialing_Account__c.getLabel());
            } else {
                validateAccountFields(oppty.Trialing_Account__r, TRIALING_LABEL);
            }
            
            if (oppty.Trialing_Physician__c == null) {
                this.requiredFields.add(SObjectType.Opportunity.fields.Trialing_Physician__c.getLabel());
            } else {
                validateContactFields(oppty.Trialing_Physician__r, TRIALING_LABEL);
            }
            
    } else if(oppty.RecordTypeId == OpportunityManager.RECTYPE_IMPLANT)               
                {
                   if (oppty.Primary_Insurance__c == null) {
                    this.requiredFields.add(SObjectType.Opportunity.fields.Primary_Insurance__c.getLabel());
                     }
        
                    if (String.isBlank(oppty.Pain_Area__c)) {
                    this.requiredFields.add(SObjectType.Opportunity.fields.Pain_Area__c.getLabel());
                    }   
                    if (String.isBlank(oppty.Procedure_Distal_Lead_Placement__c)) {
                        this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Distal_Lead_Placement__c.getLabel());
                    }
                    if (String.isBlank(oppty.Product_System__c)) {
                        this.requiredFields.add(SObjectType.Opportunity.fields.Product_System__c.getLabel());
                    }
                   if (oppty.Scheduled_Procedure_Date_Time__c == null){
                    this.requiredFields.add(SObjectType.Opportunity.fields.Scheduled_Procedure_Date_Time__c.getLabel());
                   }
                    
                    if (oppty.Procedure_Physician__c == null) {
                    this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Physician__c.getLabel());
                    } else {
                        validateContactFields(oppty.Procedure_Physician__r, PROCEDURE_LABEL);
                    }
    
                    if (oppty.Procedure_Account__c == null) {
                        this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Account__c.getLabel());
                    } else {
                        validateAccountFields(oppty.Procedure_Account__r, PROCEDURE_LABEL);
                    }
                    
         }else if(OpportunityManager.RECTYPES_EXPLANT_REVISION.contains(oppty.RecordTypeId))               
                {
                   if (oppty.Primary_Insurance__c == null) {
                    	this.requiredFields.add(SObjectType.Opportunity.fields.Primary_Insurance__c.getLabel());
                     }
        
                    if (String.isBlank(oppty.Pain_Area__c)) {
                    	this.requiredFields.add(SObjectType.Opportunity.fields.Pain_Area__c.getLabel());
                    }   
                    /*if (String.isBlank(oppty.Procedure_Distal_Lead_Placement__c)) {
                        this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Distal_Lead_Placement__c.getLabel());
                    }
                    
                   if (oppty.Scheduled_Procedure_Date_Time__c == null){
                    this.requiredFields.add(SObjectType.Opportunity.fields.Scheduled_Procedure_Date_Time__c.getLabel());
                   }*/
                    
                    if (oppty.Procedure_Physician__c == null) {
                    	this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Physician__c.getLabel());
                    } else {
                        validateContactFields(oppty.Procedure_Physician__r, PROCEDURE_LABEL);
                    }
    
                    if (oppty.Procedure_Account__c == null) {
                        this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Account__c.getLabel());
                    } else {
                        validateAccountFields(oppty.Procedure_Account__r, PROCEDURE_LABEL);
                    }
                    
    	}
        

       
/**
        //Ignore errors for Trial Record Type and Implant Only
        if((oppty.RecordTypeId == OpportunityManager.RECTYPE_TRIAL || oppty.RecordTypeId == OpportunityManager.RECTYPE_TRIAL_NEW || oppty.RecordTypeId == OpportunityManager.RECTYPE_IMPLANT)
            && (oppty.Opportunity_Type__c == 'Implant Only'
                || oppty.Opportunity_Type__c == 'Competitive Swap'
            )
        ){
            if (oppty.Scheduled_Procedure_Date_Time__c == null){
                this.requiredFields.add(SObjectType.Opportunity.fields.Scheduled_Procedure_Date_Time__c.getLabel());
            }

            if (oppty.Procedure_Physician__c == null) {
                this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Physician__c.getLabel());
            } else {
                validateContactFields(oppty.Procedure_Physician__r, PROCEDURE_LABEL);
            }

            if (oppty.Procedure_Account__c == null) {
                this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Account__c.getLabel());
            } else {
                validateAccountFields(oppty.Procedure_Account__r, PROCEDURE_LABEL);
            }

        }

        if ((new Set<String> {
            'Candidate (Trial implant)',
            'Scheduled Trial',
            'Current Trial'
            }).contains(oppty.StageName)
            && !OpportunityManager.RECTYPES_EXPLANT_REVISION.contains(oppty.RecordTypeId)
        ){

            //Ignore errors for Trial Record Type and Implant Only
            if(!((oppty.RecordTypeId == OpportunityManager.RECTYPE_TRIAL || oppty.RecordTypeId == OpportunityManager.RECTYPE_TRIAL_NEW || oppty.RecordTypeId == OpportunityManager.RECTYPE_IMPLANT)
                && (oppty.Opportunity_Type__c == 'Implant Only'
                    || oppty.Opportunity_Type__c == 'Competitive Swap'
                )
            )){
           
                

            } 

        }
        else if ((new Set<String> {
            'Procedure Candidate (Revision/Explant)',
            'Explant',
            'Revision',
            'Scheduled Revision',
            'Scheduled Explant',
            'Complete (Closed Won)'
        }).contains(oppty.StageName)) {

            if (oppty.Procedure_Physician__c == null) {
                this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Physician__c.getLabel());
            } else {
                validateContactFields(oppty.Procedure_Physician__r, PROCEDURE_LABEL);
            }

            if (oppty.Procedure_Account__c == null) {
                this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Account__c.getLabel());
            } else {
                validateAccountFields(oppty.Procedure_Account__r, PROCEDURE_LABEL);
            }

        }
        else if((new Set<String>{
            'Implant',
            'Scheduled Implant'
        }).contains(oppty.StageName)){


            //Ignore errors for Trial Record Type and (Implant Only or Competitive Swap)
            if(!((oppty.RecordTypeId == OpportunityManager.RECTYPE_TRIAL || oppty.RecordTypeId == OpportunityManager.RECTYPE_TRIAL_NEW || oppty.RecordTypeId == OpportunityManager.RECTYPE_IMPLANT)
                && (oppty.Opportunity_Type__c == 'Implant Only'
                    || oppty.Opportunity_Type__c == 'Competitive Swap'
                )
            )){

                if (oppty.Scheduled_Procedure_Date_Time__c == null){
                    this.requiredFields.add(SObjectType.Opportunity.fields.Scheduled_Procedure_Date_Time__c.getLabel());
                }

                if(String.isBlank(oppty.Product_System__c)){
                    this.requiredFields.add(SObjectType.Opportunity.fields.Product_System__c.getLabel());
                } 

                /*if(String.isBlank(oppty.Trial_Lead_Placement_Distal__c)){
                    this.requiredFields.add(SObjectType.Opportunity.fields.Trial_Lead_Placement_Distal__c.getLabel());
                }

                if(String.isBlank(oppty.Trial_Status__c)){
                    this.requiredFields.add(SObjectType.Opportunity.fields.Trial_Status__c.getLabel());
                }

                if (oppty.Trialing_Physician__c == null) {
                    this.requiredFields.add(SObjectType.Opportunity.fields.Trialing_Physician__c.getLabel());
                } else {
                    validateContactFields(oppty.Trialing_Physician__r, TRIALING_LABEL);
                } */
/*
                if (oppty.Procedure_Physician__c == null) {
                    this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Physician__c.getLabel());
                } else {
                    validateContactFields(oppty.Procedure_Physician__r, PROCEDURE_LABEL);
                }

                if (oppty.Procedure_Account__c == null) {
                    this.requiredFields.add(SObjectType.Opportunity.fields.Procedure_Account__c.getLabel());
                } else {
                    validateAccountFields(oppty.Procedure_Account__r, PROCEDURE_LABEL);
                }

            }

        } */

        //Check Patient field and Patient Field values for Prospect Patients
        if (oppty.Patient__c == null) {
            this.requiredFields.add(SObjectType.Opportunity.fields.Patient__c.getLabel());  
        } else if(oppty.Patient__r.RecordTypeId == PatientManager.RECTYPE_PROSPECT){
            if(String.isBlank(oppty.Patient__r.Patient_First_Name__c)){
                this.requiredFields.add(SObjectType.Patient__c.fields.Patient_First_Name__c.getLabel());
            }

            if(String.isBlank(oppty.Patient__r.Patient_Last_Name__c)){
                this.requiredFields.add(SObjectType.Patient__c.fields.Patient_Last_Name__c.getLabel());
            }

            if(String.isBlank(oppty.Patient__r.Patient_Phone_Number__c)
                && String.isBlank(oppty.Patient__r.Patient_Mobile_Number__c)
            ){
                this.requiredFields.add(
                    SObjectType.Patient__c.fields.Patient_Phone_Number__c.getLabel()
                    + ' or ' +
                    SObjectType.Patient__c.fields.Patient_Mobile_Number__c.getLabel()
                );
            }

            if(oppty.Patient__r.Patient_Date_of_Birth__c == null){
                this.requiredFields.add(SObjectType.Patient__c.fields.Patient_Date_of_Birth__c.getLabel());
            }

            if(String.isBlank(oppty.Patient__r.Patient_Gender__c)){
                this.requiredFields.add(SObjectType.Patient__c.fields.Patient_Gender__c.getLabel());
            }

            if(String.isBlank(oppty.Patient__r.Address_1__c)){
                this.requiredFields.add(PATIENT_LABEL + SObjectType.Patient__c.fields.Address_1__c.getLabel());
            }

            if(String.isBlank(oppty.Patient__r.City__c)){
                this.requiredFields.add(PATIENT_LABEL + SObjectType.Patient__c.fields.City__c.getLabel());
            }

            if(String.isBlank(oppty.Patient__r.State__c)){
                this.requiredFields.add(PATIENT_LABEL + SObjectType.Patient__c.fields.State__c.getLabel());
            }

            if(String.isBlank(oppty.Patient__r.Zip__c)){
                this.requiredFields.add(PATIENT_LABEL + SObjectType.Patient__c.fields.Zip__c.getLabel());
            }

            if(String.isBlank(oppty.Patient__r.Country__c)){
                this.requiredFields.add(PATIENT_LABEL + SObjectType.Patient__c.fields.Country__c.getLabel());
            }

        }

    }

    public void validateContactFields(Contact con, String conType){
        if(con != null && con.RecordTypeId == ContactManager.RECTYPE_PROSPECT){

            String conLabel = conType + ' ' + CONTACT_LABEL + ': ';

            if(String.isBlank(con.Name)){
                this.requiredFields.add(conLabel + SObjectType.Contact.fields.Name.getLabel());
            }

            if(String.isBlank(con.MailingStreet)){
                this.requiredFields.add(conLabel + SObjectType.Contact.fields.MailingStreet.getLabel());
            }

            if(String.isBlank(con.MailingCity)){
                this.requiredFields.add(conLabel + SObjectType.Contact.fields.MailingCity.getLabel());
            }

            if(String.isBlank(con.MailingState)){
                this.requiredFields.add(conLabel + SObjectType.Contact.fields.MailingState.getLabel());
            }

            if(String.isBlank(con.MailingPostalCode)){
                this.requiredFields.add(conLabel + SObjectType.Contact.fields.MailingPostalCode.getLabel());
            }

            if(String.isBlank(con.MailingCountry)){
                this.requiredFields.add(conLabel + SObjectType.Contact.fields.MailingCountry.getLabel());
            }

            if(con.Territory_Id__c == null){
                this.requiredFields.add(conLabel + SObjectType.Contact.fields.Territory_Id__c.getLabel());
            }

            if(String.isBlank(con.Phone)){
                this.requiredFields.add(conLabel + SObjectType.Contact.fields.Phone.getLabel());
            }

            if(String.isBlank(con.Fax)){
                this.requiredFields.add(conLabel + SObjectType.Contact.fields.Fax.getLabel());
            }

        }
    }

    public void validateAccountFields(Account acct, String acctType){
        if(acct != null && acct.RecordTypeId == AccountManager.RECTYPE_PROSPECT){

            String acctLabel = acctType + ' ' + ACCOUNT_LABEL + ': ';

            if(String.isBlank(acct.Name)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.Name.getLabel());
            }
            if(String.isBlank(acct.BillingStreet)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.BillingStreet.getLabel());
            }
            if(String.isBlank(acct.BillingCity)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.BillingCity.getLabel());
            }
            if(String.isBlank(acct.BillingState)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.BillingState.getLabel());
            }
            if(String.isBlank(acct.BillingPostalCode)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.BillingPostalCode.getLabel());
            }
            if(String.isBlank(acct.BillingCountry)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.BillingCountry.getLabel());
            }

            /*if(String.isBlank(acct.Sold_to_Attn__c)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.Sold_to_Attn__c.getLabel());
            }*/
            if(String.isBlank(acct.Sold_to_Street__c)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.Sold_to_Street__c.getLabel());
            }
            if(String.isBlank(acct.Sold_to_City__c)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.Sold_to_City__c.getLabel());
            }
            if(String.isBlank(acct.Sold_to_State__c)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.Sold_to_State__c.getLabel());
            }
            if(String.isBlank(acct.Sold_to_Zip_Postal_Code__c)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.Sold_to_Zip_Postal_Code__c.getLabel());
            }
            if(String.isBlank(acct.Sold_to_Country__c)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.Sold_to_Country__c.getLabel());
            }

            if(String.isBlank(acct.Sold_to_Phone__c)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.Sold_to_Phone__c.getLabel());
            }
            if(String.isBlank(acct.Sold_to_Fax__c)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.Sold_to_Fax__c.getLabel());
            }
            if(String.isBlank(acct.NMD_Territory__c)){
                this.requiredFields.add(acctLabel + SObjectType.Account.fields.NMD_Territory__c.getLabel());
            }

        }

    }

    /**
    *  @desc    build a PageReference to redirect the user to the appropriate bscisynergy resource
    */
    public PageReference redirect() {

        PageReference pr;

        // only create the reference if there are no error messages
        //if (this.requiredFields.isEmpty()) {
            //String str = 'javascript:sforce.one.navigateToURL(\'' + synergytest:'/''/''+orders/create?oid=006o000000J9G1h + '\')';
            SynergyPageReference spr = new SynergyPageReference('/orders/create');
            spr.parameters.put('oid', this.opptyId);

            pr = spr.getPageReference();
        //}

        return pr;

    }

}