@isTest 
private class OrderCreationControllerTest {
 
@testSetup 
static void setupData() {

Map <String,Schema.RecordTypeInfo> recordTypes_Account = Account.sObjectType.getDescribe().getRecordTypeInfosByName();
    Id recordTypeID_Account = recordTypes_Account.get('Customer').getRecordTypeId();

Account lk_Facility = new Account(
                                Name ='Test Account',
                                RecordTypeId = recordTypeID_Account
                                );
insert lk_Facility;

List<Contact> uroPh_LK_Contact_List = new List<Contact>(); 

Map <String,Schema.RecordTypeInfo> recordTypes_Contact = Contact.sObjectType.getDescribe().getRecordTypeInfosByName();
Id recordTypeID_Contact = recordTypes_Contact.get('UroPH General').getRecordTypeId();

for(Integer i=1; i<4;i++){

Contact uroPh_LK_Contact = new Contact(
                                FirstName = 'Test',
                                LastName = 'Leaner Kit'+i,
                                MailingStreet = '100 Boston Scientific Way',
                                MailingCity = 'Marlborough', 
                                MailingPostalCode = '01752',
                                MailingState = 'MA',
                                MailingCountry = 'United States',
                                AccountId = lk_Facility.Id,
                                RecordTypeId = recordTypeID_Contact
                                );  
uroPh_LK_Contact_List.add(uroPh_LK_Contact);                                
}//End of Loop

insert uroPh_LK_Contact_List;

Map <String,Schema.RecordTypeInfo> recordTypes_Patient_Case = Shared_Patient_Case__c.sObjectType.getDescribe().getRecordTypeInfosByName();
Id recordTypeID_Patient_Case = recordTypes_Patient_Case.get('Urology Men\'s Health').getRecordTypeId();



Shared_Patient_Case__c patientCase = new Shared_Patient_Case__c(
                                        Shared_Implanting_Physician__c = uroPh_LK_Contact_List[0].Id,
                                        Shared_Primary_Case_Contact__c = uroPh_LK_Contact_List[1].Id,
                                        Shared_Facility__c = lk_Facility.Id,
                                        Shared_Start_Date_Time__c = System.Now(),
                                        Shared_End_Date_Time__c = System.Now() + 3,
                                        RecordTypeId = recordTypeID_Patient_Case
                                     );
insert patientCase;


    
Map <String,Schema.RecordTypeInfo> recordTypes_Product2_UroPH = Product2.sObjectType.getDescribe().getRecordTypeInfosByName();
Id recordTypeID_Product2_UroPH = recordTypes_Product2_UroPH.get('UroPH').getRecordTypeId(); 

Map <String,Schema.RecordTypeInfo> recordTypes_Product2_LK_Kit = Product2.sObjectType.getDescribe().getRecordTypeInfosByName();
Id recordTypeID_Product2_LK_Kit = recordTypes_Product2_LK_Kit.get('Uro/PH Loaner Kit').getRecordTypeId();   
 
List<Product2> product_List = new List<Product2>();

for(Integer i=1;i<=20;i++){
 
Product2 prod = new Product2(
                    Name = 'Loanker Kit Product '+i, 
                    Family = 'Medical',
                    currencyIsoCode = 'USD',
                    RecordTypeId = (i < 11 ? recordTypeID_Product2_UroPH : recordTypeID_Product2_LK_Kit)
                    );
product_List.add(prod);
} 
      
insert product_List;

Pricebook2 priceBook_UroPH = new Pricebook2(
                                Name = 'UroPH', 
                                isActive = true,
                                CurrencyIsoCode = 'USD'
                                );
insert priceBook_UroPH;

List<PricebookEntry> prod_Entry_List1 = new List<PricebookEntry>();
List<Uro_PH_Loaner_Kit_Assignment__c> lk_Assignment_List = new List<Uro_PH_Loaner_Kit_Assignment__c>(); 

for(Product2 prod : product_List){

PricebookEntry prod_Entry = new PricebookEntry(
                                Pricebook2Id = Test.getStandardPricebookId(), 
                                Product2Id = prod.Id,
                                UnitPrice = 12000,
                                UseStandardPrice=false,
                                IsActive = true
                                );
prod_Entry_List1.add(prod_Entry);
}

    insert prod_Entry_List1;
    
List<PricebookEntry> prod_Entry_List = new List<PricebookEntry>();
   //prod_Entry_List.clear();
    
for(Product2 prod : product_List){

PricebookEntry prod_Entry = new PricebookEntry(
                                Pricebook2Id = priceBook_UroPH.Id, 
                                Product2Id = prod.Id,
                                UnitPrice = 12000,
                                UseStandardPrice=false,
                                IsActive = true,
                                CurrencyIsoCode ='USD'
                                );
prod_Entry_List.add(prod_Entry);    

if(prod.RecordTypeId == recordTypeID_Product2_LK_Kit){
Uro_PH_Loaner_Kit_Assignment__c lk_Assignment = new Uro_PH_Loaner_Kit_Assignment__c(
                                                    Uro_PH_Facility_Name__c = lk_Facility.Id,
                                                    Uro_PH_Loaner_Kit__c =  prod.Id
                                                    );                          
lk_Assignment_List.add(lk_Assignment);
}
}   

insert prod_Entry_List;     
insert lk_Assignment_List;


List<Uro_PH_Loaner_Kit_Relationship__c> lk_Relationship_List = new List<Uro_PH_Loaner_Kit_Relationship__c>();
for(Product2 prod : product_List){
if(prod.RecordTypeId == recordTypeID_Product2_UroPH){
Uro_PH_Loaner_Kit_Relationship__c lk_Relationship = new Uro_PH_Loaner_Kit_Relationship__c(
                                                        Uro_PH_Product__c = prod.Id,
                                                        Uro_PH_Quantity__c = 2.0
                                                        );

    lk_Relationship_List.add(lk_Relationship);
    }
}


Integer count = 0;
for(Uro_PH_Loaner_Kit_Assignment__c lk_Assignmt : lk_Assignment_List){
    lk_Relationship_List[count].Uro_PH_Loaner_Kit__c = lk_Assignmt.Uro_PH_Loaner_Kit__c;
    count++;
}

insert lk_Relationship_List;
 

}//End of SetupData 


 
static testMethod void TestSave() {
    Test.startTest();   
    
   
    List<Shared_Patient_Case__c> patientCase = [Select id from Shared_Patient_Case__c limit 1];
    List<Contact> uroPh_LK_Contact_List = [SELECT Id, MailingAddress,MailingCity,MailingCountry,MailingGeocodeAccuracy,MailingPostalCode,MailingState,MailingStreet FROM Contact];
    List<Uro_PH_Loaner_Kit_Assignment__c > lk_Assignment_List =  [SELECT Id,Name,Uro_PH_Facility_Name__c,Uro_PH_Loaner_Kit__c,Uro_PH_Loaner_Kit__r.Name FROM Uro_PH_Loaner_Kit_Assignment__c ];
     Test.setCurrentPageReference(new PageReference('Page.OrderCreationForm'));
    System.currentPageReference().getParameters().put('id', patientCase[0].Id);

   
    ApexPages.StandardController stc = new ApexPages.StandardController(patientCase[0]);
    OrderCreationController orderCreationRd = new OrderCreationController(stc);
    String shippingAddrsId = uroPh_LK_Contact_List[0].Id;
    orderCreationRd.ShippingAddressID = shippingAddrsId ;   
    orderCreationRd.SelectedItem = lk_Assignment_List[0].Uro_PH_Loaner_Kit__c; 
    orderCreationRd.poNumber = 'PO12354';
    orderCreationRd.Alternate_BSCI_Resouce = Userinfo.getUserId();
   
    orderCreationRd.LoanerKitAssignments();
    System.assert(orderCreationRd.SKUList.Size() > 0);
    
    
    orderCreationRd.save();
    Test.stopTest();
    }    
     
 
static testMethod void TestGetLoanerKitItems() {
    Test.startTest();   
    
    List<Shared_Patient_Case__c> patientCase = [Select id from Shared_Patient_Case__c limit 1];
    Test.setCurrentPageReference(new PageReference('Page.OrderCreationForm'));
    System.currentPageReference().getParameters().put('id', patientCase[0].Id);

    ApexPages.StandardController stc = new ApexPages.StandardController(patientCase[0]);
    OrderCreationController orderCreationRd = new OrderCreationController(stc);
    
    List<SelectOption> returnLoanerKitProducts = orderCreationRd.getLoanerKitItems();
    System.assert(returnLoanerKitProducts.Size() > 0);
    
    Test.stopTest();
    }//End of getItems method    

 
static testMethod void TestGetSAPShippingAddress() {
    Test.startTest();   
    
    List<Shared_Patient_Case__c> patientCase = [Select id from Shared_Patient_Case__c limit 1];
    Test.setCurrentPageReference(new PageReference('Page.OrderCreationForm'));
    System.currentPageReference().getParameters().put('id', patientCase[0].Id);

    ApexPages.StandardController stc = new ApexPages.StandardController(patientCase[0]);
    OrderCreationController orderCreationRd = new OrderCreationController(stc);
    
    List<SelectOption> returnShippingAddress = orderCreationRd.getSAPShippingAddress();
    System.assert(returnShippingAddress .Size() > 0);
        
    Test.stopTest();
    }//End of GetSAPShippingAddress method  
 
 
static testMethod void TestGetShippingAddress() {
    Test.startTest();   
    
    List<Shared_Patient_Case__c> patientCase = [Select id from Shared_Patient_Case__c limit 1];
    Test.setCurrentPageReference(new PageReference('Page.OrderCreationForm'));
    System.currentPageReference().getParameters().put('id', patientCase[0].Id);

    ApexPages.StandardController stc = new ApexPages.StandardController(patientCase[0]);
    OrderCreationController orderCreationRd = new OrderCreationController(stc);
    
    List<Contact> uroPh_LK_Contact_List = [SELECT Id, MailingAddress,MailingCity,MailingCountry,MailingGeocodeAccuracy,MailingPostalCode,MailingState,MailingStreet FROM Contact WHERE LastName LIKE 'Leaner Kit%'];
    
    orderCreationRd.ShippingAddressID = uroPh_LK_Contact_List[0].Id;
    
    String selectedAddress = orderCreationRd.getShippingAddress();
    System.assert(selectedAddress != null);
        
    Test.stopTest();
    }//End of GetSAPShippingAddress method  
    
    
    
}