public class PhysicianTerritoryUtility {
    
    public String createAndSendPhysicianRequest(List<Physician_Territory_Realignment__c> physicianRealignmentRecords){
    
        System.debug('********Received Physician Records: '+physicianRealignmentRecords);
        String status = 'Fail';
        List<NMD_PhysicianTerritoryDataForSAP> Seller_Hierarchy = prepareData(physicianRealignmentRecords);
        //instantiate the generator
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('Seller_Hierarchy');
        //gen.writeStartArray();
        gen.writeObject(Seller_Hierarchy);
        //gen.writeEndArray();
        gen.writeEndObject();
        System.debug('REST REQUEST:'+gen.getAsString());
        
        if(gen.getAsString()!=null && gen.getAsString()!=''){
            if(!System.isBatch()){
            PhysicianRequestHandler.SendHttpRequest(gen.getAsString()); 
            }else{
            System.debug('***************Batch Context Found**************');
                PhysicianRequestHandler.SendHttpRequestFromBatch(gen.getAsString());
            }
            status = 'Success';
        }
        return status;
         
    } 
    
    public List<NMD_PhysicianTerritoryDataForSAP> prepareData(List<Physician_Territory_Realignment__c> physicianRealignmentRecords){
        
        System.debug('********Received Physician Records to Prepare Data: '+physicianRealignmentRecords);
        Set<Id> physicianIds = new set<Id>();
        Set<Id> territoryIds = new set<Id>();
        set<Id> currentTM1s = new set<Id>();
        Set<String> oldAccountIds = new set<String>();
        List<NMD_PhysicianTerritoryDataForSAP> phyTerRecordList= new List<NMD_PhysicianTerritoryDataForSAP>();
        List<Account> ciAccounts = new List<Account>();
        List<Account> oldAccounts = new List<Account>();
        
        
        Map<Id,Seller_Hierarchy__c> mapTerritorydata = new Map<id,Seller_Hierarchy__c>();
        Map<Id,Contact> mapContactData = new Map<id,Contact>();
        Map<Id,TerritoryData> mapAssigneeWithParentTerritory = new Map<id,TerritoryData>();
        Map<Id,String> mapOldAccountWithCustomerNumber = new Map<id,String>();
        Map<Id,Account> mapCurrentTM1WithCIAccount = new Map<id,Account>();
        
        
        if(physicianRealignmentRecords.size()!=0){
            for(Physician_Territory_Realignment__c ptr: physicianRealignmentRecords){
                physicianIds.add(ptr.Contact__c);
              //  mapPhysiciansWithOldTerritory.put(ptr.Contact__r.Id, ptr.Current_Territory__c);
            //    mapPhysiciansWithNewTerritory.put(ptr.Contact__r.Id, ptr.New_Territory__c);
                territoryIds.add(ptr.Current_Territory__c);
                territoryIds.add(ptr.New_Territory__c);
                oldAccountIds.add(ptr.Old_Account_Id__c);
            }
        
        //fetching all relevant territory Details for physicians
        List<Seller_Hierarchy__c> allTerritories = [SELECT 
                Id, 
                Name,
                Current_TM1_Assignee__c, 
                Field_Support_Rep__c, 
                Territory_Name__c,
                Reports_to__r.Id,
                Reports_to__r.Reports_to__r.id
            FROM Seller_Hierarchy__c
            where 
                id in :territoryIds];
                
        Set<Id> parentTerritories = new set<id>();
         
          system.debug('____allTerritories___'+allTerritories);      
        //fetching all CI Accounts for Current_TM1_Assignee__c
        if(allTerritories.size()>0){
            for(Seller_Hierarchy__c sh: allTerritories){
                if(sh.Current_TM1_Assignee__c!=null)
                    currentTM1s.add(sh.Current_TM1_Assignee__c);
                mapTerritorydata.put(sh.id, sh);  
                parentTerritories.add(sh.id);
                parentTerritories.add(sh.Reports_to__r.Id);
                parentTerritories.add(sh.Reports_to__r.Reports_to__r.id);
            }
        } 
        
        //Pulling all assignee Records and assigning them to the Assignee map. 
        List<Assignee__c> assigns = [select id, Assignee__c, Territory__c, Rep_Designation__c, Assignee__r.name from Assignee__c where Territory__c in :parentTerritories ];
        system.debug('____assigns___----'+assigns);  


        
        for(Seller_Hierarchy__c sh1: allTerritories){
            TerritoryData td = new TerritoryData();
            for(Assignee__c an: assigns){
                system.debug('Assignee :'+an);
                if((an.Territory__c == sh1.Reports_to__c) && an.Rep_Designation__c=='RBM'){
                    //RBM user match 
                    td.Field_Support_Region = an.Assignee__r.Name;
                }
                if((an.Territory__c == sh1.Reports_to__r.Reports_to__c) && an.Rep_Designation__c=='AVP'){
                     //AVP user match 
                    td.Field_Support_Area = an.Assignee__r.Name;
                }
            }
            system.debug (''+sh1.name+' >>>>>>> :'+td);
            
            mapAssigneeWithParentTerritory.put(sh1.id, td);
        }
         system.debug('____currentTM1s`````` '+currentTM1s);  
        //pull CI Accounts for Territory TM1s
        if(currentTM1s.size()>0)
         ciAccounts= [select id, Name, NMD_Territory__c,Account_Number__c, Personnel_ID__r.name, Owner.Id from Account where OwnerId in :currentTM1s and RecordTypeId = :AccountManager.RECTYPE_CONSIGNMENT];
        
        for(Account a1: ciAccounts){
            mapCurrentTM1WithCIAccount.put(a1.Owner.id, a1);
        }
         System.debug('*******Physician IDs : '+physicianIds);
         System.debug('*******Customer REcord TypeID : '+ContactManager.RECTYPE_CUSTOMER);
         //Pulling customer Numbers of Old Account on Physician if they exists. 
         if(oldAccountIds.size()>0){
             oldAccounts = [Select id, Account_Number__c from Account where id in :oldAccountIds];
         }
         
         if(oldAccounts.size()>0){
             for(Account a1 : oldAccounts){
                  for(Physician_Territory_Realignment__c ptr: physicianRealignmentRecords){
                      if(a1.id==ptr.Old_Account_Id__c){
                          mapOldAccountWithCustomerNumber.put(a1.id, a1.Account_Number__c);
                      }
                  }
             }
         }
         
        //pulling only Customer physicians as prospect physicians are not created in SAP yet.     
        List<Contact> physiciansData = [Select id,Territory_ID__c, Account.Account_Number__c, SAP_ID__c from contact where id in :physicianIds]; 
        
        System.debug('************ Physicians Selected : '+physiciansData);
        
        //adding them to map now... 
        for(Contact c1 : physiciansData){
            mapContactData.put(c1.id, c1);
        }         
         System.debug('*****************mapContactData :'+mapContactData);
        //preparing data now 
        for(Physician_Territory_Realignment__c ptr1 : physicianRealignmentRecords){
            System.debug('*********Preparing Data for :'+ptr1 );
            //Change Indicator = 'D'
            if(ptr1.Current_Territory__c!=null){
             NMD_PhysicianTerritoryDataForSAP newRecord = new NMD_PhysicianTerritoryDataForSAP();
             
             if(mapContactData.get(ptr1.Contact__c)!=null){
                 //check if the account is changed on the physician, if so, use that accounts customer number. 
                 If(ptr1.Old_Account_Id__c!=null && ptr1.Old_Account_Id__c!='' && mapOldAccountWithCustomerNumber.get(ptr1.Old_Account_Id__c)!=null){
                     newRecord.customer_number =  mapOldAccountWithCustomerNumber.get(ptr1.Old_Account_Id__c);
                 }else{
                     //account is not changed on physician so pull the current account number 
                newRecord.customer_number =  mapContactData.get(ptr1.Contact__c).Account.Account_Number__c;
                 }
             }
             System.debug('CUSTOMER NUMBER for Delete :'+newRecord.customer_number );
            //considering sales_Rep_ID is TM1
            if((mapTerritorydata.get(ptr1.Current_Territory__c)).Current_TM1_Assignee__c!=null && (mapCurrentTM1WithCIAccount.get((mapTerritorydata.get(ptr1.Current_Territory__c)).Current_TM1_Assignee__c)!=null)){
                newRecord.sales_Rep_ID = (mapCurrentTM1WithCIAccount.get((mapTerritorydata.get(ptr1.Current_Territory__c)).Current_TM1_Assignee__c)).Personnel_ID__r.name;
                newRecord.sales_Rep_Sold_To = (mapCurrentTM1WithCIAccount.get((mapTerritorydata.get(ptr1.Current_Territory__c)).Current_TM1_Assignee__c)).Account_Number__c;
            }
            
            if(mapContactData.get(ptr1.Contact__c)!=null)
                newRecord.physician_SAP_ID = mapContactData.get(ptr1.Contact__c).SAP_ID__c;
            //hierarchy : Territory -> Region -> Area
            if(mapTerritorydata.get(ptr1.Current_Territory__c)!=null){
            newRecord.Sales_Territory_ID = (mapTerritorydata.get(ptr1.Current_Territory__c)).Territory_Name__c;
            newRecord.field_Support_Rep = (mapTerritorydata.get(ptr1.Current_Territory__c)).Field_Support_Rep__c;
            newRecord.Field_Support_Region= (mapAssigneeWithParentTerritory.get(ptr1.Current_Territory__c)).Field_Support_Region;
            newRecord.Field_Support_Area = (mapAssigneeWithParentTerritory.get(ptr1.Current_Territory__c)).Field_Support_Area;
            newRecord.change_Indicator='D';
            }
            phyTerRecordList.add(newRecord);
                
            }
            
            //Change Indicator = 'I'
            if(ptr1.New_Territory__c!=null){
            NMD_PhysicianTerritoryDataForSAP newRecord = new NMD_PhysicianTerritoryDataForSAP();
            
             if(mapContactData.get(ptr1.Contact__c)!=null){
                newRecord.customer_number =  mapContactData.get(ptr1.Contact__c).Account.Account_Number__c;
                newRecord.physician_SAP_ID = mapContactData.get(ptr1.Contact__c).SAP_ID__c;
            }
            //considering sales_Rep_ID is TM1
            if((mapTerritorydata.get(ptr1.New_Territory__c)).Current_TM1_Assignee__c!=null && (mapCurrentTM1WithCIAccount.get((mapTerritorydata.get(ptr1.New_Territory__c)).Current_TM1_Assignee__c)!=null)){
                newRecord.sales_Rep_ID = (mapCurrentTM1WithCIAccount.get((mapTerritorydata.get(ptr1.New_Territory__c)).Current_TM1_Assignee__c)).Personnel_ID__r.name;
                newRecord.sales_Rep_Sold_To = (mapCurrentTM1WithCIAccount.get((mapTerritorydata.get(ptr1.New_Territory__c)).Current_TM1_Assignee__c)).Account_Number__c;
            }
            
            //hierarchy : Territory -> Region -> Area
            if(mapTerritorydata.get(ptr1.New_Territory__c)!=null && mapAssigneeWithParentTerritory.get(ptr1.New_Territory__c)!= null){
            newRecord.Sales_Territory_ID = (mapTerritorydata.get(ptr1.New_Territory__c)).Territory_Name__c;
            newRecord.field_Support_Rep = (mapTerritorydata.get(ptr1.New_Territory__c)).Field_Support_Rep__c;
            newRecord.Field_Support_Region= (mapAssigneeWithParentTerritory.get(ptr1.New_Territory__c)).Field_Support_Region;
            newRecord.Field_Support_Area = (mapAssigneeWithParentTerritory.get(ptr1.New_Territory__c)).Field_Support_Area;
            newRecord.change_Indicator='I';
            }
            phyTerRecordList.add(newRecord);
            }
            
        }
        System.debug('PHYSICIANS RECORD LIST size:'+phyTerRecordList.size());
        return phyTerRecordList;
        }
        
        return null;
    }
    
    public class TerritoryData{
        public String Field_Support_Area {get;set;}
        public String Field_Support_Region {get;set;}

    }
}