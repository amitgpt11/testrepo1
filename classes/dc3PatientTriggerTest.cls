@isTest
private class dc3PatientTriggerTest {
    static testMethod void Test_Insert() {

        Patient__c prd = new Patient__c(Patient_First_Name__c ='Test', Patient_Last_Name__c='PatientTrigger');


        try {
            insert prd;
        } catch(DMLException e) {
            system.debug(LoggingLevel.ERROR, e.getMessage());
            system.assert(false);
        }
        system.assert(prd.Id != null);
    }
}