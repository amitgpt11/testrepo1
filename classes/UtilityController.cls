/*
 @CreatedDate     21June2016                                  
 @author          Ashish-Accenture
 @Description     Utility class for reusable methods
 */
public without sharing class UtilityController{

    //Get all users of Public group
    public Static Set<id> GetUserIdsFromGroup(String grpName)
    {
        // store the results in a set so we don't get duplicates
        Set<Id> result=new Set<Id>();
        String userType = Schema.SObjectType.User.getKeyPrefix();
        String groupType = Schema.SObjectType.Group.getKeyPrefix();
        
        // Loop through all group members in a group
        for (GroupMember m : [Select Id, UserOrGroupId From GroupMember Where group.name = :grpName])
        {
            // If the user or group id is a user
            if (((String)m.UserOrGroupId).startsWith(userType))
            {
                result.add(m.UserOrGroupId);
            }
            // If the user or group id is a group
            // Note: there may be a problem with governor limits if this is called too many times
            else if (((String)m.UserOrGroupId).startsWith(groupType))
            {
                // Call this function again but pass in the group found within this group
                result.addAll(GetUSerIdsFromGroup(m.UserOrGroupId));
            }
        }
        
        return result;  
    }
    
    public static Map<integer,String> getMapMonthNumber(){
        Map<integer, string> mapMonthNumber = new Map<Integer,string>{
            1 =>'January',
                2 => 'February',
                3 => 'March',
                4 => 'April',
                5 => 'May',
                6 => 'June',
                7 => 'July',
                8 => 'August',
                9 => 'September',
                10 => 'October',
                11 => 'November',
                12 => 'December'
                };
                    return mapMonthNumber;
    }
    
    public static Map<String,Integer> getMapNoOfMonth(){
       map<String,Integer> MapNoOfMonth = new map<String,Integer>{
            'January' => 1,
            'February' => 2,
            'March' => 3,
            'April' => 4,
            'May' => 5,
            'June' => 6,
            'July' => 7,
            'August' => 8,
            'September' => 9,
            'October' => 10,
            'November' => 11,
            'December' => 12
            };
           return MapNoOfMonth;
    }
}