/**
* Manager class for custom Inventory_Item__c object
*
* @Author salesforce Services
* @Date 06/12/2015
*/
public with sharing class InventoryItemManager {
	
	static final Map<String,Schema.RecordTypeInfo> RECTYPES = Schema.SObjectType.Inventory_Item__c.getRecordTypeInfosByName();
	public static final Id RECTYPE_NMD = (RECTYPES.get('NMD Inventory Item').getRecordTypeId());

	private Map<Id,List<Cycle_Count_Line_Item__c>> childCycleCountItems = new Map<Id,List<Cycle_Count_Line_Item__c>>();
	private Map<Id,Cycle_Count_Line_Item__c> updatedCycleCountItems = new Map<Id,Cycle_Count_Line_Item__c>();

	/*public void fetchRelatedRecords(List<Inventory_Item__c> items) {

		Set<Id> itemIds = new Set<Id>{};
		for (Inventory_Item__c item : items) {
			if (item.Id != null) {
				itemIds.add(item.Id);
			}
		}

		if (!itemIds.isEmpty()) {
			
			for (Cycle_Count_Line_Item__c ccItem : [
				select 
					Actual_Quantity__c,
					Inventory_Item__c
				from Cycle_Count_Line_Item__c
				where Inventory_Item__c in :itemIds
			]) {

				if (this.childCycleCountItems.containsKey(ccItem.Inventory_Item__c)) {
					this.childCycleCountItems.get(ccItem.Inventory_Item__c).add(ccItem);
				}
				else {
					this.childCycleCountItems.put(ccItem.Inventory_Item__c, new List<Cycle_Count_Line_Item__c> { ccItem });
				}

			}

		}

	}

	public void updateChildAvailableQuantities(Inventory_Item__c oldItem, Inventory_Item__c newItem) {

		if (oldItem.SAP_Quantity__c != newItem.SAP_Quantity__c
		 && this.childCycleCountItems.containsKey(newItem.Id)
		) {

			for (Cycle_Count_Line_Item__c ccItem : this.childCycleCountItems.get(newItem.Id)) {

				if (ccItem.Actual_Quantity__c != newItem.SAP_Quantity__c) {
					ccItem.Actual_Quantity__c = newItem.SAP_Quantity__c;
					this.updatedCycleCountItems.put(ccItem.Id, ccItem);
				}

			}

		}

	}

	public void commitChildUpdates() {

		if (!this.updatedCycleCountItems.isEmpty()) {
			//update this.updatedCycleCountItems.values();
			DML.save(this, this.updatedCycleCountItems.values(), false);
		}

	}*/

}