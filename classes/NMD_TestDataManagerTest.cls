/**
* Test class for the TestData manager.
*
* @author   Salesforce services
* @date     2015-02-09
*/
@isTest(SeeAllData=false)
private class NMD_TestDataManagerTest {
	
	static testMethod void test_TestDataManager() {

		NMD_TestDataManager testData = new NMD_TestDataManager();
		Account acct = testData.newAccount();
		Account acctName = testData.newAccount('account');
		Contact cont = testData.newContact(acct.Id);
		AccContactRelationship__c arc = testData.newAccContactRelationship(acct.Id, cont.Id);
		Lead lead = testData.newLead();
		Task task = testData.newTask(UserInfo.getUserId(), 'subject', 'status', System.today());
		String randomStr5 = testData.randomString5();
		String randomStr = testData.randomString();

	}
	
}