/**
* Trigger for the Task object
*
* @Author salesforce Services
* @Date 03/01/2015
*/
trigger TaskTrigger on Task (before insert, before update, before delete,after insert) {

    TriggerHandlerManager.createAndExecuteHandler(TaskTriggerHandler.class);
    if(Trigger.isAfter){
        BSMITaskHandler.emailAlert_afterInsert(Trigger.new);
    }
}