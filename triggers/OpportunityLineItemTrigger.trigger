/*
@CreatedDate     15JUNE2016
@author          Mike Lankfer
@Description     The trigger for the OpportunityLineItem standard object.  This class will call all the methods that should fire before and/or after create/update/delete/undelete DML operations.
@Requirement Id  User Story : DRAFT US2511
*/

trigger OpportunityLineItemTrigger on OpportunityLineItem (before insert, before update, after insert, after update, before delete, after delete) {
    TriggerHandlerManager.createAndExecuteHandler(OpportunityLineItemTriggerHandler.class);
}