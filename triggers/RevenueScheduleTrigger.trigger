/**
* RevenueScheduleTrigger trigger
*
* @Author Vikas Malik
* @Date 2016-07-01
*/
trigger RevenueScheduleTrigger on Schedule__c (after insert, after update, after delete, before insert, before update, before delete) {
    
    TriggerHandlerManager.createAndExecuteHandler(RevenueScheduleTriggerHandler.class);
}