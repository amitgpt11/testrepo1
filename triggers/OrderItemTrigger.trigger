/**
* Order Item trigger
*
* @Author salesforce Services
* @Date 2015-06-24
*/
trigger OrderItemTrigger on OrderItem (before insert, before update, before delete, after insert, after update, after delete) {

	TriggerHandlerManager.createAndExecuteHandler(OrderItemTriggerHandler.class);

}