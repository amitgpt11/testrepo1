/*
* Trigger for the Account object
*
* @Author salesforce Services
* @Date 04/12/2016
*/
trigger CampaignTrigger on Campaign (after insert) {
    TriggerHandlerManager.createAndExecuteHandler(CampaignTriggerHandler.class);
}