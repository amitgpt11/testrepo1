/*
@CreatedDate     16JUNE2016
@author          Mike Lankfer
@Description     The trigger for the Product2 standard object.  This class will call all the methods that should fire before and/or after create/update/delete/undelete DML operations.
@Requirement Id  User Story : DRAFT US2511
*/

trigger ProductTrigger on Product2 (before insert, before update, after insert, after update) {
    TriggerHandlerManager.createAndExecuteHandler(ProductTriggerHandler.class);
}