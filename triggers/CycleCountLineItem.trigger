trigger CycleCountLineItem on Cycle_Count_Line_Item__c (
	after insert, 
	after update, 
	before insert, 
	before update
) {

	TriggerHandlerManager.createAndExecuteHandler(CycleCountLineItemTriggerHandler.class);

}