/**
* Trigger for the Patient__c object
*
* @Author salesforce Services
* @Date 2015-04-16
*/
trigger PatientTrigger on Patient__c (before insert, before update, after insert, after update, after delete, after undelete){

/*Added for duplicate check as duplicate check already wrapped code in managed package handler */
  dupcheck.dc3Trigger triggerTool = new dupcheck.dc3Trigger(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete, 'dc3Disable_Duplicate_Check__c');
    String errorString = triggerTool.processTrigger(trigger.oldMap, trigger.new); 

    if (String.isNotEmpty(errorString)) { trigger.new[0].addError(errorString,false);  }
    system.debug('errorString ---'+errorString ); 
/*Trigger handler manager used for all custom code*/
    TriggerHandlerManager.createAndExecuteHandler(PatientTriggerHandler.class);

}