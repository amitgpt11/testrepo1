/*
* Trigger for the Account object
*
* @Author salesforce Services
* @Date 06/11/2015
*/
trigger AccountTrigger on Account (before insert, before update, after insert, after update) {
    TriggerHandlerManager.createAndExecuteHandler(AccountTriggerHandler.class);
}