/*
* Trigger for the Contact object
*
* @Author salesforce Services
* @Date 04/16/2015
*/
trigger ContactTrigger on Contact (after update, after insert, before update) {
    TriggerHandlerManager.createAndExecuteHandler(ContactTriggerHandler.class);
}