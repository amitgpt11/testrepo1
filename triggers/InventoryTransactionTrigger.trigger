trigger InventoryTransactionTrigger on Inventory_Transaction__c (
	before insert,
	after insert, 
	after update, 
	after delete, 
	after undelete
) {	
	// execute handler
	TriggerHandlerManager.createAndExecuteHandler(InventoryTransactionTriggerHandler.class);
}